/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Batch class which deletes all expired Coverages (MemberPlans)
*   where the EffectiveTo < TODAY
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Shelby Dremely
* @modifiedBy     Shelby Dremely
* @version        1.0
* @created        2021-08-04
* @modified       2021-08-04
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.0            Shelby Dremely
* 2021-08-04      Created batchable apex
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class BatchDeleteExpiredCoverages implements Database.Batchable<sObject>{
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT id, EffectiveTo FROM MemberPlan WHERE EffectiveTo < TODAY';
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext bc, List<MemberPlan> mpList){
        try {
            // Delete the MemberPlans
            delete mpList;
        } catch(Exception e) {
            System.debug(e);
        }
    }    
    public void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }  
}