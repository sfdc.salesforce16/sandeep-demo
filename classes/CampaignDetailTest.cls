/**
 * Created by dcano on 5/26/21.
 */

@IsTest
private class CampaignDetailTest
{
    @IsTest
    static void testBehavior()
    {
        Campaign camp = new Campaign();
        camp.Name = 'TEST CAMPAIGN';
        insert camp;

        Test.startTest();

        List<Campaign> camps = CampaignDetail.getCampaign(new List<String>{ camp.Id });

        Test.stopTest();
    }
}