global class LeadCreateFlowController implements Process.Plugin { 

    webservice static Process.PluginResult invoke(Process.PluginRequest request) {
        Map<String, String> result = new Map<String, String>();
        String email = (String) request.inputParameters.get('Email');
        String firstName = (String) request.inputParameters.get('FirstName');
        String lastName = (String) request.inputParameters.get('LastName');
        String phone = (String) request.inputParameters.get('Phone');
        String question = (String) request.inputParameters.get('Question');
        String hearAboutUs = (String) request.inputParameters.get('HowDidYouHearAboutUs');
        
        Lead lead = new Lead();
        
        lead.Email = email;
        lead.FirstName = firstName;
        lead.LastName = lastName;
        lead.Phone = phone;
        lead.Registrant_Questions__c = question;
        lead.How_did_you_hear_about_this_event__c = hearAboutUs;
        
        insert lead;
        
        result.put('leadId', lead.Id);

        return new Process.PluginResult(result); 
    }
    
    global Process.PluginDescribeResult describe() { 
        Process.PluginDescribeResult result = new Process.PluginDescribeResult(); 
        result.Name = 'Create Lead Flow';
        result.Tag = 'createLeadFlow';
        result.inputParameters = new 
            List<Process.PluginDescribeResult.InputParameter>{ 
                new Process.PluginDescribeResult.InputParameter('Email', Process.PluginDescribeResult.ParameterType.STRING, true),
                new Process.PluginDescribeResult.InputParameter('FirstName', Process.PluginDescribeResult.ParameterType.STRING, true),
                new Process.PluginDescribeResult.InputParameter('LastName', Process.PluginDescribeResult.ParameterType.STRING, true),
                new Process.PluginDescribeResult.InputParameter('Phone', Process.PluginDescribeResult.ParameterType.STRING, true),
                new Process.PluginDescribeResult.InputParameter('Question', Process.PluginDescribeResult.ParameterType.STRING, true),
                new Process.PluginDescribeResult.InputParameter('HowDidYouHearAboutUs', Process.PluginDescribeResult.ParameterType.STRING, true)
            }; 
        result.outputParameters = new 
            List<Process.PluginDescribeResult.OutputParameter>{
                new Process.PluginDescribeResult.OutputParameter('leadId', Process.PluginDescribeResult.ParameterType.BOOLEAN)
            };
        return result;
    }
}