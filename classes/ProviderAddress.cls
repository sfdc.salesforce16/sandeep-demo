/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description A wrapper Apex class that contains a few details for a Provider Address record. 
* This class is used to package a few details of a Provider Address record in a serializable format. 
* When returned from a remote Apex controller action, the properties with @AuraEnabled annotation are defined on the client-side. 
*/

public with sharing class ProviderAddress {
    public ProviderAddress() {}

    @AuraEnabled
    public string UniqueID { get; set; }    

    @AuraEnabled
    public string StreetLine1 { get; set; }    

    @AuraEnabled
    public string StreetLine2 { get; set; }    

    @AuraEnabled
    public string StreetLine3 { get; set; }    

    @AuraEnabled
    public string City { get; set; }    

    @AuraEnabled
    public string Zip { get; set; }    

    @AuraEnabled
    public string Phone { get; set; }    

    @AuraEnabled
    public string ExternalPlaceName { get; set; }    

    @AuraEnabled
    public string ExternalAddressID { get; set; }    

    @AuraEnabled
    public Boolean IsActive { get; set; }    

    @AuraEnabled
    public Boolean IsPrimary { get; set; }    

    @AuraEnabled
    public Boolean IsSharedAddress { get; set; }    

    @AuraEnabled
    public Boolean IsInternalAddress { get; set; }    

    @AuraEnabled
    public State State { get; set; }    

    @AuraEnabled
    public Country Country { get; set; }    

    @AuraEnabled
    public String mapMarker { get{ return '[{location: {Street: \'1 Market St\',City: \'San Francisco\',PostalCode:\'78757\',Country: \'USA\' } }]';}}

    @AuraEnabled
    public String StateAbbr { get{return this.State == null ? '' : this.State.Abbr;} }    

    @AuraEnabled
    public String CountryAbbr { get{return this.Country == null ? '' : this.Country.Abbr;} }    

    public class State{

        @AuraEnabled
        public string Abbr { get; set; }    
    }

    public class Country{

        @AuraEnabled
        public string Abbr { get; set; }    
    }
}