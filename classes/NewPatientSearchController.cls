public with sharing class NewPatientSearchController {

    @AuraEnabled
    public static string epicOnlySoCreate(String name, string dob, string mrn){
        if(name == null){ return null;}

        if(mrn.contains('Maestro')){
            mrn = mrn.replace('(Maestro Care only)', '').trim();
        }
        List<Account> validateNoAccount = [SELECT Id FROM Account WHERE HealthCloudGA__MedicalRecordNumber__c = :mrn];
        If(!validateNoAccount.isEmpty()){ 
            System.debug('A patient with this MRN already exists in Salesforce');
            return null;
        }

        String title, first, last;
        if(name.contains(',')){
            first = name.split(',')[0];
            last = name.split(',')[1];
        }else if(name.split(' ').size() == 3){
            title = name.split(' ')[0];
            first = name.split(' ')[1];
            last = name.split(' ')[2];
        }else if(name.contains(' ')){
            first = name.split(' ')[0];
            last = name.split(' ')[1];
        }else{
            last = name;
        }
                
        try {
            Id personRtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();            
            Account a = new Account(
                RecordTypeId = personRtId,
                Salutation = title,
                LastName = last,
                FirstName = first,
                PersonBirthdate = Date.valueOf(dob),
                Date_of_birth__c = Date.valueOf(dob),
                HealthCloudGA__MedicalRecordNumber__c = mrn,
                isLegacyPatient__c = true
            );
            insert a;
            return a.Id;

        } catch (Exception e){
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<PatientSearch> searchEpic(String mrn, String dob, String name, String phone){
        system.debug('Sandy ::' + mrn);
        String dobfmt = '';
        try {
            dobfmt = ((Datetime)Date.parse(dob)).formatGMT('yyyy-MM-dd');
        } catch(Exception e) {
            //ignore date, not correct
        }
        
        HttpRequest req = new HttpRequest();
        EpicWebService ews = new EpicWebService();
        req = ews.setHeaderForMuleAPI(req);
        System.debug('SNde ++0' + req);
        String url = 'callout:epic_mule_app/patient';
        String firstName = '', lastName = '';
        if(name.contains(',')){
            Integer lastLen = name.length() - (name.lastIndexOf(',') + 1);
            firstName = name.right(lastLen).trim();
            lastName = name.left(name.length() - (lastLen + 1)).trim();
        }else if(name.contains(' ')){
            Integer lastLen = name.length() - (name.lastIndexOf(' ') + 1);
            lastName = name.right(lastLen).trim();
            firstName = name.left(name.length() - (lastLen + 1)).trim();
        }else{
            lastName = name;
        }
        String endpoint ;
        String mrnUpperCase = ''; //(String.isNotBlank(mrn) ? mrn.toUpperCase() : '');
        String sParameters = '';
        mrnUpperCase = (String.isNotBlank(mrn) ? mrn.toUpperCase() : '');
       // sParameters = String.format('{0}?dob={1}', new String[] {url, dobfmt}); //Failed
        //sParameters = String.format('{0}?mrn={1}&dob={2}&phone={3}', new String[] {url,mrnUpperCase, dobfmt , phone}); //Passed
        //sParameters = String.format('{0}?mrn={1}&dob={2}', new String[] {url,mrnUpperCase, dobfmt});
        //sParameters = String.format('{0}?dob={1}&firstName={2}&lastName={3}', new String[] {url, dobfmt, firstName, lastName});
        sParameters = String.format('{0}?mrn={1}&dob={2}&lastName={3}&firstName={4}&phone={5}', new String[] {url, mrnUpperCase, dobfmt, lastName, firstName, phone});
        /*if (String.isBlank(mrn)){
            sParameters = String.format('{0}?dob={1}', new String[] {url, dobfmt});
        }
        else{
            mrnUpperCase = (String.isNotBlank(mrn) ? mrn.toUpperCase() : '');
            sParameters = String.format('{0}?mrn={1}&dob={2}&phone={3}', new String[] {url, mrnUpperCase, dobfmt, phone});
        }
        if (String.isBlank(firstName)){
            sParameters = String.format('{0}?dob={1}', new String[] {url, dobfmt});
        }
        else {
            sParameters = String.format('{0}?dob={1}&firstName={2}&phone={3}', new String[] {url, dobfmt, firstName, phone});
            
        }
        if ((String.isBlank(lastName) && !String.isBlank(mrn) && !String.isBlank(phone) && !String.isBlank(firstName)) || (String.isBlank(lastName) && String.isBlank(mrn) && !String.isBlank(phone) && !String.isBlank(firstName))){
            sParameters = String.format('{0}?dob={1}', new String[] {url, dobfmt});
        }
        else{
            mrnUpperCase = (String.isNotBlank(mrn) ? mrn.toUpperCase() : '');
            sParameters = String.format('{0}?mrn={1}&dob={2}&lastName={3}&firstName={4}&phone={5}', new String[] {url, mrnUpperCase, dobfmt, lastName, firstName, phone});
        }

        /*if (String.isBlank(phone)){
            sParameters = String.format('{0}?dob={1}', new String[] {url, dobfmt});
        }
        else{
            sParameters = String.format('{0}?dob={1}&phone={2}', new String[] {url, dobfmt, phone});
        }*/
        
        System.debug('endpoint' + sParameters);
        /*if (!String.isBlank(mrnUpperCase) && !String.isBlank(dobfmt) && !String.isBlank(lastName) && !String.isBlank(firstName) & !String.isBlank(phone) ){
            endpoint = String.format('{0}?mrn={1}&dob={2}&lastName={3}&firstName={4}&phone={5}', new String[] {url, mrnUpperCase, dobfmt, lastName, firstName, phone});
        }
        else{
            endpoint =  sParameters;
        }*/

        req.setEndpoint(sParameters);
        req.setMethod('GET');
        req.setTimeout(20000);
        System.debug('req: ' + req);

        Http http = new Http();
        List<PatientSearch> searchResults = new List<PatientSearch>();
        //if(!Test.isRunningTest()){
            HTTPResponse res = http.send(req);
            String body = res.getBody();
            System.debug('body: ' + body);
            if(body.contains('mrn')){
                searchResults = (List<PatientSearch>) JSON.deserialize(res.getBody(), List<PatientSearch>.class);
                System.debug('searchResults: ' + searchResults);
    
            }
        //}
        return searchResults;
    }


    /*
        We perform 3 searches and combine results
        SOSL is limited to searching strings only, which includes phone number
        We start with a SOQL query on the primary elements not supported by SOSL: Date of Birth
		Secondly we use the inbound phone number (INI) along with the search terms and perform a SOSL search
		We use a Set<Account> so that we do not double report results.
		In addition to Account, we also query Set<Case> records, but only by SOSL
		Thirdly not all patients will be in Salesforce, we perform a call out to Epic for additional patients.
    */
    @AuraEnabled(cacheable=true)
    public static List<List<SObject>> findRecords(String searchText, String searchIni, String searchDate, String mrn) {
        List<PatientSearch> epicResults = new List<PatientSearch>();
        Set<String> foundMrns = new Set<String>();
        String searchLastName = searchText + '%';

        if((!String.isBlank(searchDate) && !String.isBlank(searchText)) || (!String.isBlank(mrn))){
            System.debug('searchEpic: ' + searchIni + ', ' + searchText);
            epicResults = searchEpic(mrn, searchDate, searchText, searchIni);
        }

        if ((searchText.length() < 3) || (searchText==null)) {
            searchText = 'ZZZZZZ';
        }
        String searchString = searchText.trim().replace(' ', ' OR ').replace(',', ' OR ');
        if(searchIni != null && searchIni.length() > 3){
            searchString += ' OR ' + searchIni;
        }
        if(mrn != null && mrn.length() > 3){
            searchString += ' OR ' + mrn;
        }
        System.debug('searchString ' + searchString);
        List<List<SObject>> allResults = [FIND :searchString IN ALL FIELDS RETURNING Account(Name, Phone, PersonBirthdate, Date_of_Birth__c, PersonHomePhone, SSN_Last_4__c, HealthCloudGA__Gender__pc, HealthCloudGA__MedicalRecordNumber__c, PersonMailingStreet, Id), Case(CaseNumber, Account.Name, Priority, Category__c, CreatedDate) LIMIT 3];

        System.debug('allResults.sObject: ' + allResults);
        for (SObject o : allResults[0]) {
            System.debug('o: ' + ((Account)o).HealthCloudGA__MedicalRecordNumber__c);
            foundMrns.add(((Account)o).HealthCloudGA__MedicalRecordNumber__c);
        }
        System.debug('foundMrns: ' + foundMrns);
        System.debug('allResults: ' + allResults);

        Date parsedSearchDate;
        if (searchDate.length() < 3) {
            searchDate = '01/01/1900';
        } else if (searchDate != null) {
            try {
                parsedSearchDate = Date.parse(searchDate);
            } catch(Exception e) {
                parsedSearchDate = Date.NewInstance(Integer.valueOf(searchDate.substring(4,8)), 
                Integer.valueOf(searchDate.substring(0,2)), 
                Integer.valueOf(searchDate.substring(2,4)));
            }
        }
        System.debug('parsedSearchDate ' + parsedSearchDate);  

        Set<Account> accounts;

        if(!String.isBlank(searchDate) && !String.isBlank(searchText)){
            accounts = new Set<Account>([SELECT Id, Name, Phone, 
                        PersonBirthdate, Date_of_Birth__c, 
                        PersonHomePhone, SSN_Last_4__c, 
                        HealthCloudGA__Gender__pc, 
                        HealthCloudGA__MedicalRecordNumber__c,
                        PersonMailingStreet
            FROM Account 
            WHERE PersonBirthdate = :parsedSearchDate
                AND LastName LIKE :searchLastName
                AND PersonBirthdate != null
            LIMIT 3]);
        }
        if(String.isBlank(searchDate) && !String.isBlank(searchText)){
            accounts = new Set<Account>([SELECT Id, Name, Phone, 
                        PersonBirthdate, Date_of_Birth__c, 
                        PersonHomePhone, SSN_Last_4__c, 
                        HealthCloudGA__Gender__pc, 
                        HealthCloudGA__MedicalRecordNumber__c,
                        PersonMailingStreet
            FROM Account 
            WHERE LastName LIKE :searchLastName
                AND PersonBirthdate != null
            LIMIT 3]);
            
        }
        if(!String.isBlank(searchDate) && String.isBlank(searchText)){
            accounts = new Set<Account>([SELECT Id, Name, Phone, 
                        PersonBirthdate, Date_of_Birth__c, 
                        PersonHomePhone, SSN_Last_4__c, 
                        HealthCloudGA__Gender__pc, 
                        HealthCloudGA__MedicalRecordNumber__c,
                        PersonMailingStreet
            FROM Account 
            WHERE PersonBirthdate = :parsedSearchDate
                AND PersonBirthdate != null
            LIMIT 3]);
            
        }

        System.debug('accounts ' + accounts);    
        if (accounts != null) {
            for (Account acct : accounts) {
                if(allResults[0].contains(acct)){
                    System.debug('list of accounts contains record ' + acct);
                } else {
                    allResults[0].add(acct);
                    foundMrns.add(acct.HealthCloudGA__MedicalRecordNumber__c);
                }
            }
        }
    

        List<SObject> ar = allResults[0];
        System.debug('ar: ' + ar);

        if(epicResults != null){
            for (PatientSearch p : epicResults) {
                if(!foundMrns.contains(p.mrn)){
                    Account a = new Account(
                        Name = p.name,
                        PersonHomePhone = p.phone,
                        Date_of_Birth__c = String.isBlank(p.birthDate) ? null : Date.valueOf(p.birthDate),
                        HealthCloudGA__Gender__pc = p.gender,
                        HealthCloudGA__MedicalRecordNumber__c = p.mrn + ' (Maestro Care only)',
                        PersonMailingStreet = p.address
                    );
                    if(allResults[0].isEmpty()){
                        allResults[0].add(a);
                    }else{
                        allResults[0].add(0, a);
                    }
                    foundMrns.add(p.mrn);
                }
            }
        }
        System.debug('allResults: ' + allResults);

        return allResults;  
    }   

    /*
    @AuraEnabled(cacheable=true)
    //public static List<List<SObject>> findRecords(String dob, String searchText) {
    public static List<List<SObject>> findRecords(String searchText) {
            //public static String findRecords(String searchText) {
        searchText = searchText.replaceAll('-', '').replaceAll('/', '');
        System.debug(searchText);
        //List<List<SObject>> results = [FIND :searchText IN ALL FIELDS RETURNING Account(Name, Phone, PersonBirthdate, Date_of_Birth__c, PersonHomePhone, SSN_Last_4__c, HealthCloudGA__Gender__pc, HealthCloudGA__MedicalRecordNumber__c), Case(Subject, ContactPhone) LIMIT 3];
        List<List<SObject>> results = [FIND :searchText IN ALL FIELDS RETURNING Account(Name, Phone, PersonBirthdate, Date_of_Birth__c, PersonHomePhone, SSN_Last_4__c, HealthCloudGA__Gender__pc, HealthCloudGA__MedicalRecordNumber__c) LIMIT 3];
        //List<Account> accounts = [SELECT Id, Name FROM Account WHERE PersonBirthDate = :dob];
        //List<Account> accounts = [SELECT Id, Name FROM Account WHERE Phone = '919-123-4567'];
        //ah I need to pass this in
        //919-123-4567
        //results[0]
        System.debug(results);
        return results;
        // List<sObject> allResults = new List<sObject>();

        // for (List<SObject> sobjs : results) {
        //     for (SObject sobj : sobjs) {
        //         allResults.add(sobj);
        //         System.debug('sobj : ' + sobj);
        //         System.debug('allResults : ' + allResults);
        //     }
        // }
        // return allResults;  
        //return JSON.serialize(allResults);

    }    
    */ 
/* HP 12.8.2020 
    @AuraEnabled(cacheable=true)
    public static List<List<SObject>> findRecords(String searchText) {
        searchText = searchText.replaceAll('-', '').replaceAll('/', '');
        System.debug('searchText ' + searchText);

        List<Account> soqlacctresults = [SELECT Id 
                                        FROM Account 
                                        WHERE TextDob__c LIKE :searchText
                                        LIMIT 3];
        System.debug(soqlacctresults);

        List<Case> soqlcaseresults = [SELECT Id 
                                        FROM Case 
                                        WHERE TextDob__c LIKE :SearchDate
                                        LIMIT 3];
        System.debug(soqlcaseresults);

        List<List<SObject>> soslresults = [FIND :searchText 
                                        IN ALL FIELDS 
                                        RETURNING 
                                        Account(Id, Name, Phone WHERE TextDob__c LIKE :searchText)
        //                                Case(Id WHERE Account.TextDob__c LIKE :searchDate)
                                        LIMIT 3];
        System.debug(soslresults);
        return soslresults;

    }   
    */
   
   
}