@isTest
public class ProgramEnrollment_Test {
    private static Id personAcctRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'PersonAccount'].Id;
    
    @testSetup
    static void setup() {
        
        // Program
        CareProgram p = new CareProgram(
        	Name = 'Residential - New Client Weight Loss',
        	Price__c = 100,
        	Deposit__c = 5,
            Duration__c = 7
        );
        insert new List<CareProgram>{p};
        // insert Form Template for forms
        disco__Form_Template__c t = new disco__Form_Template__c(
            Name = 'DDFC New Residential Patient Medical Intake', 
            disco__Status__c = 'In Use'
        );
        insert t;
            
        // Account 
        Account a = new Account(
            FirstName = 'Test',
            LastName = 'Test1',
            Phone = '(999) 999-9999',
            BillingStreet = '123 Fake St',
            BillingCity = 'Star City',
            BillingPostalCode = '12345',
            BillingState = 'CA',
            RecordTypeId = personAcctRecordTypeId,
            PersonEmail = 'testuser@test.com',
            Email__c = 'testuser@test.com'
        ); insert a;
    }
    
    @isTest
    static void newEnrollment() {
        CareProgram p = [SELECT Id FROM CareProgram];
        Account a = [SELECT Id FROM Account];
        Program_Enrollment__c e = new Program_Enrollment__c(
            Account__c = a.Id,
            Program__c = p.Id,
            Start__c = System.today(),
            Status__c = 'Enrolled'
        );
        Test.startTest();
        insert e;
        Test.stopTest();
    }

}