/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class provides unit tests to cover the HealthNotificationCenterController apex class.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Synaptic
 * @project     DHAS
 * @since       February 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

@isTest
public class HealthNotificationCenterController_Test {
    
    @IsTest
    static void testGetReferralInfos1() {
	 	RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', VeteranStatus__c = 'Y', Has_Project_Access_Referral__c = true, 
                        isLegacyPatient__c = true, Interpreter_Needed__c = 'Y', Has_HiTech_Guarantor__c = true, HealthCloudGA__MedicalRecordNumber__pc = '12234', 
                        Fyi_Flag__c = 'Hitech', HealthCloudGA__MedicalRecordNumber__c = '1234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', 
                        County__c = 'Broward', RecordTypeId = rt.Id, IsMarkedForMerge__c=true, IsUnmerged__c=true);
        Skill_Group__c skg = new Skill_Group__c(Name='123DPC BR CREEK PC APPT SG', Skill_Group__c = '123DPC_BR_CREEK_PC_APPT_SG', Specialty__c = 'Oncology');
        User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',
                        Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',
                        ProfileId = p.Id, LanguageLocaleKey = 'en_US');
        Test.startTest();
        Insert a;
		Insert skg; 
        Case c = new Case(AccountId=a.Id, Status='Open');
        MemberPlan mbp = new MemberPlan(MemberId = a.Id, Name = 'Veteran MBP', Create_new_Subscriber__c = true, New_Subscriber_Name__c = 'Bob Wayne', 
                        New_Subscriber_Birthdate__c = date.ValueOf('2020-12-05'), New_Subscriber_Sex__c = 'Male', New_Subscriber_SSN__c = '123-12-1234',
                        New_Subscriber_ID__c = '1234');
        Health_Maintenance__c hm = new Health_Maintenance__c(Patient__c=a.Id);
        Insert c;
		Insert mbp;
		Insert u;
        Agent_Skill_Group__c askg = new Agent_Skill_Group__c(Agent__c = u.Id, Skill_Group__c= skg.Id);
		Insert askg;
		Referral_Information__c ref = new Referral_Information__c(Patient_Name__c = a.Id, Referral_Status__c = 'Open', Referral_Type__c = 'International', 
                        Skill_group__c = skg.id, Referral_ID__c='2457', Scheduling_Status__c='Call-1');
		Recalls__c rec = new Recalls__c(Patient_Name__c = a.Id, Recall_Expiration_Date__c = Date.Today(), Recall_Status__c = 'Open', Skill_group__c = skg.id);
		Provider_Orders__c pvo = new Provider_Orders__c(Patient_Name__c = a.Id, Skill_group__c = skg.id);
        FYI_Flag__c fyiFlag = new FYI_Flag__c(Patient_Account__c=a.Id, FYI_Flag_Status__c='Active');
        Dismissal__c dismissal = new Dismissal__c(Account__c=a.Id, Dismissal_Effective_Date__c=date.ValueOf('2021-05-18'), Dismissal_End_Date__c=date.ValueOf('2025-06-25'));
        Insert ref;
        Insert rec;
        Insert pvo;
        Insert fyiFlag;
        Insert dismissal;
        String accid = a.id;
        System.runAs(u) {
            HealthNotificationCenterController hncc = new HealthNotificationCenterController();
            HealthNotificationCenterController.Alert hnca = new HealthNotificationCenterController.Alert();
            HealthNotificationCenterController.getAlerts(accid);
        }
        Test.stopTest();
    }
    
        @IsTest
    	static void testGetReferralInfos2() {
	 	RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Account a2 = new Account(FirstName = 'Bruce', LastName = 'Wayne', Patient_Status__c = 'Deceased', Interpreter_Needed2__c = true, PatientType__c = 'PRISONER', HealthCloudGA__MedicalRecordNumber__pc = '12234', HealthCloudGA__MedicalRecordNumber__c = '1234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Skill_Group__c skg = new Skill_Group__c(Name='123DPC BR CREEK PC APPT SG', Skill_Group__c = '123DPC_BR_CREEK_PC_APPT_SG', Specialty__c = 'Oncology');
        User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');
        Test.startTest();
        Insert a2;
		Insert skg; 
        MemberPlan mbp = new MemberPlan(MemberId = a2.Id, Name = 'Veteran MBP', Create_new_Subscriber__c = true, New_Subscriber_Name__c = 'Bob Wayne', New_Subscriber_Birthdate__c = date.ValueOf('2020-12-05'), New_Subscriber_Sex__c = 'Male', New_Subscriber_SSN__c = '123-12-1234', New_Subscriber_ID__c = '1234');
		Insert mbp;
		Insert u;
        Agent_Skill_Group__c askg = new Agent_Skill_Group__c(Agent__c = u.Id, Skill_Group__c= skg.Id);
		Insert askg;
		Referral_Information__c ref2 = new Referral_Information__c(Patient_Name__c = a2.Id, Referral_Status__c = 'Open', Referral_Type__c = 'Consultation', Skill_group__c = skg.id, Referral_ID__c='2457');
		Recalls__c rec = new Recalls__c(Patient_Name__c = a2.Id, Recall_Expiration_Date__c = Date.Today(), Recall_Status__c = 'Open', Skill_group__c = skg.id);
		Provider_Orders__c pvo = new Provider_Orders__c(Patient_Name__c = a2.Id, Skill_group__c = skg.id);
        Insert ref2;
        Insert rec;
        Insert pvo;
        String acc2id = a2.id;
        System.runAs(u) {
            HealthNotificationCenterController hncc = new HealthNotificationCenterController();
            HealthNotificationCenterController.Alert hnca = new HealthNotificationCenterController.Alert();
            HealthNotificationCenterController.getAlerts(acc2id);
        }
        Test.stopTest();
    }
    
    @IsTest
    static void testGetReferralInfos3() {
	 	RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Account a3 = new Account(FirstName = 'Bruce', LastName = 'Wayne', VeteranStatus__c = 'Y', HealthCloudGA__MedicalRecordNumber__pc = '12234', HealthCloudGA__MedicalRecordNumber__c = '1234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Skill_Group__c skg = new Skill_Group__c(Name='123DPC BR CREEK PC APPT SG', Skill_Group__c = '123DPC_BR_CREEK_PC_APPT_SG', Specialty__c = 'Oncology');
        User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');
        Test.startTest();
        Insert a3;
		Insert skg; 
        MemberPlan mbp = new MemberPlan(MemberId = a3.Id, Name = 'Veteran MBP', Create_new_Subscriber__c = true, New_Subscriber_Name__c = 'Bob Wayne', New_Subscriber_Birthdate__c = date.ValueOf('2020-12-05'), New_Subscriber_Sex__c = 'Male', New_Subscriber_SSN__c = '123-12-1234', New_Subscriber_ID__c = '1234');
		Insert mbp;
		Insert u;
        Agent_Skill_Group__c askg = new Agent_Skill_Group__c(Agent__c = u.Id, Skill_Group__c= skg.Id);
		Insert askg;
		Referral_Information__c ref3 = new Referral_Information__c(Patient_Name__c = a3.Id, Referral_Status__c = 'Open', Referral_Type__c = 'Worker\'s Comp', Skill_group__c = skg.id, Referral_ID__c='2457', Scheduling_Status__c='Call-1');
		Recalls__c rec = new Recalls__c(Patient_Name__c = a3.Id, Recall_Expiration_Date__c = Date.Today(), Recall_Status__c = 'Open', Skill_group__c = skg.id);
		Provider_Orders__c pvo = new Provider_Orders__c(Patient_Name__c = a3.Id, Skill_group__c = skg.id);
        Insert ref3;
        Insert rec;
        Insert pvo;
        String acc3id = a3.id;
        System.runAs(u) {
            HealthNotificationCenterController hncc = new HealthNotificationCenterController();
            HealthNotificationCenterController.Alert hnca = new HealthNotificationCenterController.Alert();
            HealthNotificationCenterController.getAlerts(acc3id);
        }
        Test.stopTest();
    }
    
    @IsTest
    static void testGetReferralInfos4() {
	 	RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Account a4 = new Account(FirstName = 'Bruce', LastName = 'Wayne', VeteranStatus__c = 'N', Infrequent_Patient_Status__c = 'Workers Comp', HealthCloudGA__MedicalRecordNumber__pc = '12234', HealthCloudGA__MedicalRecordNumber__c = '1234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Skill_Group__c skg = new Skill_Group__c(Name='123DPC BR CREEK PC APPT SG', Skill_Group__c = '123DPC_BR_CREEK_PC_APPT_SG', Specialty__c = 'Oncology');
        User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');
        Test.startTest();
        Insert a4;
		Insert skg; 
        MemberPlan mbp = new MemberPlan(MemberId = a4.Id, Name = 'Veteran MBP', Create_new_Subscriber__c = true, New_Subscriber_Name__c = 'Bob Wayne', New_Subscriber_Birthdate__c = date.ValueOf('2020-12-05'), New_Subscriber_Sex__c = 'Male', New_Subscriber_SSN__c = '123-12-1234', New_Subscriber_ID__c = '1234');
		Insert mbp;
		Insert u;
        Agent_Skill_Group__c askg = new Agent_Skill_Group__c(Agent__c = u.Id, Skill_Group__c= skg.Id);
		Insert askg;
		Referral_Information__c ref3 = new Referral_Information__c(Patient_Name__c = a4.Id, Referral_Status__c = 'Open', Referral_Type__c = 'Worker\'s Comp', Skill_group__c = skg.id, Referral_ID__c='2457');
		Recalls__c rec = new Recalls__c(Patient_Name__c = a4.Id, Recall_Expiration_Date__c = Date.Today(), Recall_Status__c = 'Open', Skill_group__c = skg.id);
		Provider_Orders__c pvo = new Provider_Orders__c(Patient_Name__c = a4.Id, Skill_group__c = skg.id);
		Wait_List__c wal = new Wait_List__c(Patient_Name__c = a4.Id,Status__c = 'Open', Skill_group__c = skg.id);
        Insert ref3;
        Insert rec;
        Insert pvo;
        Insert wal;
        String acc4id = a4.id;
        System.runAs(u) {
            HealthNotificationCenterController hncc = new HealthNotificationCenterController();
            HealthNotificationCenterController.Alert hnca = new HealthNotificationCenterController.Alert();
            HealthNotificationCenterController.getAlerts(acc4id);
        }
        Test.stopTest();
    }

    @IsTest
    static void testupdateFromEpic() {
	 	RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Account a4 = new Account(FirstName = 'Bruce', LastName = 'Wayne', VeteranStatus__c = 'N', Infrequent_Patient_Status__c = 'Workers Comp', HealthCloudGA__MedicalRecordNumber__pc = '12234', HealthCloudGA__MedicalRecordNumber__c = '1234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Skill_Group__c skg = new Skill_Group__c(Name='123DPC BR CREEK PC APPT SG', Skill_Group__c = '123DPC_BR_CREEK_PC_APPT_SG', Specialty__c = 'Oncology');
        User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');
        Test.startTest();
        Insert a4;
		Insert skg; 
        MemberPlan mbp = new MemberPlan(MemberId = a4.Id, Name = 'Veteran MBP', Create_new_Subscriber__c = true, New_Subscriber_Name__c = 'Bob Wayne', New_Subscriber_Birthdate__c = date.ValueOf('2020-12-05'), New_Subscriber_Sex__c = 'Male', New_Subscriber_SSN__c = '123-12-1234', New_Subscriber_ID__c = '1234');
		Insert mbp;
		Insert u;
        Agent_Skill_Group__c askg = new Agent_Skill_Group__c(Agent__c = u.Id, Skill_Group__c= skg.Id);
		Insert askg;
		Referral_Information__c ref3 = new Referral_Information__c(Patient_Name__c = a4.Id, Referral_Status__c = 'Open', Referral_Type__c = 'Worker\'s Comp', Skill_group__c = skg.id, Referral_ID__c='2457');
		Recalls__c rec = new Recalls__c(Patient_Name__c = a4.Id, Recall_Expiration_Date__c = Date.Today(), Recall_Status__c = 'Open', Skill_group__c = skg.id);
		Provider_Orders__c pvo = new Provider_Orders__c(Patient_Name__c = a4.Id, Skill_group__c = skg.id);
		Wait_List__c wal = new Wait_List__c(Patient_Name__c = a4.Id,Status__c = 'Open', Skill_group__c = skg.id);
        Insert ref3;
        Insert rec;
        Insert pvo;
        Insert wal;
        String acc4id = a4.id;
        System.runAs(u) {
            HealthNotificationCenterController hncc = new HealthNotificationCenterController();
            HealthNotificationCenterController.updateFromEpic(a4.Id);
        }
        Test.stopTest();
    }
    
}