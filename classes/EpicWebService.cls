/******************************************
 * 
 * @author Synaptic AP
 * @since  Feb 2021
 * Project DHAS
 */
public with sharing class EpicWebService {
    /*Below method to set header with Client Id and Client Secret*/
    public HttpRequest setHeaderForMuleAPI(HttpRequest req){
       // Epic_Web_Service__mdt eWebService = Epic_Web_Service__mdt.getInstance('MulesoftTest');
        Epic_Web_Service__mdt eWebService = [SELECT Id,MasterLabel,ClientId__c,ClientSecret__c,IsSandbox__c from Epic_Web_Service__mdt where DeveloperName = 'MulesoftTest' limit 1];
        system.debug('Id ='+ eWebService.ClientId__c);
        system.debug('Secret ='+ eWebService.ClientSecret__c);
        req.setHeader('client_id',eWebService.ClientId__c);
        req.setHeader('client_secret', eWebService.ClientSecret__c);
        return req;
    }

    public HTTPResponse call(String method, String mrn, String verb){
        HttpRequest req = new HttpRequest();
        
		String rootUrl = 'callout:epic_mule_app';
        String endpoint = String.format('{0}/{1}?mrn={2}', new String[] {rootUrl, method, mrn});
        req.setEndpoint(endpoint);
        req.setMethod(verb);
        System.debug('req: ' + req);

        Http http = new Http();
        return http.send(req);
    }

    public HTTPResponse callWithAccount(String method, String mrn, String accountId, String verb){
        HttpRequest req = new HttpRequest();
        String rootUrl = 'callout:epic_mule_app';

        String endpoint = String.format('{0}/{1}?mrn={2}&accountId={3}', new String[] {rootUrl, method, mrn, accountId});
        req.setEndpoint(endpoint);
        req.setMethod(verb);
        System.debug('req: ' + req);

        Http http = new Http();
        req = setHeaderForMuleAPI(req);
        return http.send(req);
    }
}