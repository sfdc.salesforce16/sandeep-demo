@isTest
public class WailtlistController_Test {
    
    @IsTest
    static void testRun() {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', HealthCloudGA__MedicalRecordNumber__c = '1234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Test.startTest();
        try {
            Insert a;
            String accid = a.id;
            MockHttpResponseGenerator fakeResponse = new MockHttpResponseGenerator(200,
                                                     'Complete',
                                                     '[{"Name": "HealthMaintController"}]',
                                                     null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            WaitlistController wlc = new WaitlistController();
            WaitlistController.callEpicWaitlist(accid);
            Test.stopTest();
        } catch(exception e) {
            
        }
    }
}