/**
* @author Synaptic
* @date July 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description This class for mock HTTP response for ProviderAddressLookupController class.
*/
@isTest
global class MockHttpProviderAddress implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('[{"UniqueID":"57645-4","StreetLine1":null,"StreetLine2":null,"StreetLine3":null,"City":null,"Zip":null,"Phone":"919-660-2217","ExternalPlaceName":null,"ExternalAddressID":null,"IsActive":false,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":false,"State":null,"County":null,"Country":null}]');
        res.setStatusCode(200);
        return res;
    }
}