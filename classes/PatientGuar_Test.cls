@isTest
public class PatientGuar_Test {
    
    @IsTest
    static void testRun() {
	 	RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Account a2 = new Account(FirstName = 'Bryan', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Account a3 = new Account(FirstName = 'Bob', LastName = 'Wayne');
        Insert a;
        Insert a2;
        Insert a3;
        System.debug('a.id ' + a.id + 'a2.id ' + a2.id + 'a3.id ' + a3.id);
        HealthCloudGA__EhrRelatedPerson__c guar1 = new HealthCloudGA__EhrRelatedPerson__c(HealthCloudGA__Account__c = a.Id);
        HealthCloudGA__EhrRelatedPerson__c guar2 = new HealthCloudGA__EhrRelatedPerson__c(HealthCloudGA__Relationship__c = 'GUARANTOR', HealthCloudGA__Account__c = a.Id);
        Insert guar1;
        Insert guar2;
        System.debug('guar1.id ' + guar1.id + 'guar2.id ' + guar2.id);
        System.debug('inserted Guars');
        String accid = a.Id;
        String acc2id = a2.Id;
        string guar1id = guar1.id;
        string guar2id = guar2.id;
        string guar1Rel = '123';
        string guar2Rel = 'GUARANTOR';
        Test.startTest();
        PatientGuar pgc = new PatientGuar();
        PatientGuar.Guarantor GuarClass = new PatientGuar.Guarantor();
        GuarClass.id = '123';
        GuarClass.address = '123';
        GuarClass.name = '123';
        GuarClass.phone = '123';
        GuarClass.relationship = '123';
        GuarClass.city = '123';
        GuarClass.state = '123';
        GuarClass.country = '123';
        GuarClass.postalCode = '123';
        GuarClass.ssn = '123';
        GuarClass.employer = '123';
        GuarClass.employmentStatus = '123';     
        MockHttpResponseGenerator fakeResponse = new MockHttpResponseGenerator(200,
                                                 'Complete',
                                                 '[{"Name": "HealthMaintController"}]',
                                                 null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        PatientGuar.getRelated(accid);
//        PatientGuar.getGuar(guar2id, guar2Rel);
        Test.stopTest();
    }
    @IsTest
    static void testRun2() {
        //arrange
	 	RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Account a2 = new Account(FirstName = 'Bryan', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Account a3 = new Account(FirstName = 'Bob', LastName = 'Wayne');
        Insert a;
        Insert a2;
        Insert a3;
        System.debug('a.id ' + a.id + 'a2.id ' + a2.id + 'a3.id ' + a3.id);
        HealthCloudGA__EhrRelatedPerson__c guar1 = new HealthCloudGA__EhrRelatedPerson__c(HealthCloudGA__Account__c = a.Id);
        HealthCloudGA__EhrRelatedPerson__c guar2 = new HealthCloudGA__EhrRelatedPerson__c(HealthCloudGA__Relationship__c = 'GUARANTOR', HealthCloudGA__Account__c = a.Id);
        Insert guar1;
        Insert guar2;
        System.debug('guar1.id ' + guar1.id + 'guar2.id ' + guar2.id);
        System.debug('inserted Guars');
        String accid = a.Id;
        String acc2id = a2.Id;
        string guar1id = guar1.id;
        string guar2id = guar2.id;
        string guar1Rel = 'GUARANTOR';
        string guar2Rel = '123';
        Test.startTest();
        MockHttpResponseGenerator fakeResponse = new MockHttpResponseGenerator(200,
                                                 'Complete',
                                                 '[{"Name": "HealthMaintController"}]',
                                                 null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        PatientGuar pgc = new PatientGuar();

        //act
        PatientGuar.getGuar(acc2id);
        PatientGuar.getGuar(accid);
        Test.stopTest();

        //assert
    }
    @isTest
    public static void testRun3() {
        PatientGuar.Guarantor GuarClass = new PatientGuar.Guarantor();
        GuarClass.id = '123';
        System.debug('GuarClass.id '+ GuarClass.id);
        GuarClass.address = '123';
        GuarClass.name = '123';
        GuarClass.phone = '123';
        GuarClass.relationship = '123';
        GuarClass.city = '123';
        GuarClass.state = '123';
        GuarClass.country = '123';
        GuarClass.postalCode = '123';
        GuarClass.ssn = '123';
        GuarClass.employer = '123';
        GuarClass.employmentStatus = '123';  
    }
}