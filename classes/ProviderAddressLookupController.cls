/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description A controller of Lightning Component. It provides @AuraEnabled methods to Find ans Set Provider Addresses
*/
public without sharing class ProviderAddressLookupController {
    /*******************************************************************************************************
    * @description Constructor 
    */
    public ProviderAddressLookupController() {

    }

    /*******************************************************************************************************
    * @description Find and return the Provider Addresses with respect to a practitionerId  
    * This method calls a RESTFul Api defined in 'callout:epic_mule_app/provider/addresses'
    * @param practitionerId the practitioner Id
    * @return a list of ProviderAddress objects
    */
    @AuraEnabled
    public static List<ProviderAddress> findProviderAddresses(String practitionerId) {
        List<ProviderAddress> addresses = new List<ProviderAddress>();

        HttpRequest req = new HttpRequest();
        String url = 'callout:epic_mule_app/provider/address';
        String endpoint = String.format('{0}?dukePractitionerId={1}', new String[] {url, practitionerId});

       /* String username = '4966163afb2d422783d04c30d2adbf50';
        String password = 'C528DE8E71594E97a9f818a073Bf94Ac';
        req.setHeader('client_id',username);
        req.setHeader('client_secret',password);*/
        EpicWebService ews = new EpicWebService();
        req = ews.setHeaderForMuleAPI(req);
        
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeout(20000);
        System.debug('req: ' + req);

        Http http = new Http();
       // if(!Test.isRunningTest()){ //commented by Karthik
            HTTPResponse res = http.send(req);
            String body = res.getBody();
            //String body = '[{"UniqueID":"57645-7","StreetLine1":"4709 CREEKSTONE DRIVE STE 300","StreetLine2":"DUKE MEDICAL PLAZA PAGE ROAD","StreetLine3":null,"City":"DURHAM","Zip":"27703","Phone":"919-660-5066","ExternalPlaceName":null,"ExternalAddressID":"10236980","IsActive":true,"IsPrimary":true,"IsSharedAddress":true,"IsInternalAddress":true,"State":{"ID":34,"Title":"North Carolina","Abbr":"NC"},"County":{"ID":541,"Title":"DURHAM","Abbr":"DURHAM"},"Country":{"ID":1,"Title":"United States of America","Abbr":"USA"}},{"UniqueID":"57645-6","StreetLine1":"3116 N. Duke Street","StreetLine2":null,"StreetLine3":null,"City":"Durham","Zip":"27704-2102","Phone":"919-660-2200","ExternalPlaceName":null,"ExternalAddressID":"10236978","IsActive":true,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":true,"State":{"ID":34,"Title":"North Carolina","Abbr":"NC"},"County":{"ID":541,"Title":"DURHAM","Abbr":"DURHAM"},"Country":{"ID":1,"Title":"United States of America","Abbr":"USA"}},{"UniqueID":"57645-1","StreetLine1":"DUKE UNIVERSITY MEDICAL CENTER","StreetLine2":null,"StreetLine3":null,"City":"DURHAM","Zip":"27710","Phone":"919-660-2217","ExternalPlaceName":null,"ExternalAddressID":null,"IsActive":false,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":false,"State":{"ID":34,"Title":"North Carolina","Abbr":"NC"},"County":{"ID":541,"Title":"DURHAM","Abbr":"DURHAM"},"Country":{"ID":1,"Title":"United States of America","Abbr":"USA"}},{"UniqueID":"57645-2","StreetLine1":null,"StreetLine2":null,"StreetLine3":null,"City":null,"Zip":null,"Phone":"919-660-2217","ExternalPlaceName":null,"ExternalAddressID":null,"IsActive":false,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":false,"State":null,"County":null,"Country":null},{"UniqueID":"57645-3","StreetLine1":null,"StreetLine2":null,"StreetLine3":null,"City":null,"Zip":null,"Phone":"919-660-2217","ExternalPlaceName":null,"ExternalAddressID":null,"IsActive":false,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":false,"State":null,"County":null,"Country":null},{"UniqueID":"57645-4","StreetLine1":null,"StreetLine2":null,"StreetLine3":null,"City":null,"Zip":null,"Phone":"919-660-2217","ExternalPlaceName":null,"ExternalAddressID":null,"IsActive":false,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":false,"State":null,"County":null,"Country":null},{"UniqueID":"57645-5","StreetLine1":null,"StreetLine2":null,"StreetLine3":null,"City":null,"Zip":null,"Phone":"919-660-2217","ExternalPlaceName":null,"ExternalAddressID":null,"IsActive":false,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":false,"State":null,"County":null,"Country":null},{"UniqueID":"57645-8","StreetLine1":"DUKE HEALTH CTR AT N DUKE ST","StreetLine2":null,"StreetLine3":null,"City":"DURHAM","Zip":"27704","Phone":"919-660-2200","ExternalPlaceName":null,"ExternalAddressID":null,"IsActive":false,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":false,"State":{"ID":34,"Title":"North Carolina","Abbr":"NC"},"County":{"ID":541,"Title":"DURHAM","Abbr":"DURHAM"},"Country":{"ID":1,"Title":"United States of America","Abbr":"USA"}}]';
            System.debug('body: ' + body);
            if(body.contains('UniqueID')){
                addresses = (List<ProviderAddress>) JSON.deserialize(body, List<ProviderAddress>.class);
                System.debug('searchResults: ' + addresses);
    
            }
       // } // commented by Karthik
        return addresses;
    }

    /*******************************************************************************************************
    * @description An @AuraEnabled method responsible for setting Provider Addresses to SFDC with respect to a patient  
    * @param accountId the account Id
    * @param locationId the address Id
    * @param address the address
    * @return a list of FYI_Flag__c objects
    */
    @AuraEnabled
    public static void setProviderAddress(String accountId, String locationId, String address) {
        System.debug('accountId: ' + accountId);
        System.debug('locationId: ' + locationId);
        System.debug('address: ' + address);
        Account a = [SELECT Id, PCP_Address_Id__c, PCP_Address__c FROM Account WHERE Id = :accountId LIMIT 1];
        a.PCP_Address_Id__c = locationId;
        a.PCP_Address__c = address;
        System.debug('a: ' + a);
        update a;
    }
}