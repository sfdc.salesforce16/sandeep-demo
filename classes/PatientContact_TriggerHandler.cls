/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description Implements the TriggerHandler Framework for the Patient_Contact__c object.  
* Syncs Youreka picklist fields for Same_Household and Relationship with the corresponding 
* picklist fields on Patientn Contact that are mapped to the integration.
*/
public with sharing class PatientContact_TriggerHandler extends TriggerHandler {
    public PatientContact_TriggerHandler() {

    }

    public override void beforeInsert(){
        System.debug('beforeInsert');
        insertSyncRelationship();
        insertSyncSame_Household();
    }

    public override void beforeUpdate(){
        System.debug('beforeUpdate');
        updateSyncSameHousehold();
        updateSyncRelationship();
    }

    public override void afterUpdate(){
        isEditForEpic();
    }

    private void isEditForEpic() {
        List<ToEpicNextOfKinEdit__e> epicNextOfKinEditEvents = new List<ToEpicNextOfKinEdit__e>();
        System.debug('isEditForEpic: ' + Trigger.newMap.values());
        Id personRtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        System.debug('personRtId: ' + personRtId);

        List<Id> accountIds = new List<Id>();
        for (Patient_Contact__c c : (List<Patient_Contact__c>)Trigger.newMap.values()) {
            accountIds.add(c.Account__c);
        }
        Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, RecordTypeId, HealthCloudGA__MedicalRecordNumber__c FROM Account WHERE Id IN :accountIds]);

        for(Patient_Contact__c c : (List<Patient_Contact__c>)Trigger.newMap.values()){
            Boolean isUpdate = false;
            Account a = accountMap.get(c.Account__c);
            //check if qualifying fields have been updated    
            if(Trigger.isUpdate){
                Patient_Contact__c oldContact = (Patient_Contact__c)Trigger.oldMap.get(c.ID);
                if(a.RecordTypeId == personRtId){
                     isUpdate =  isSyncFieldChange((Patient_Contact__c)Trigger.oldMap.get(c.ID),c);
                }
            }

            if(isUpdate || Trigger.isInsert){
                ToEpicNextOfKinEdit__e ep = new ToEpicNextOfKinEdit__e();
                    if(Trigger.isUpdate){
                        ep.ChangeType__c = 'UPDATE';
                    }
                    if(Trigger.isInsert){
                        ep.ChangeType__c = 'CREATE';
                    }
                    ep.PatientContactId__c = c.Id;
                    ep.AccountId__c = c.Account__c;
                    ep.MRN__c = a.HealthCloudGA__MedicalRecordNumber__c;

                    epicNextOfKinEditEvents.add(ep);
        
                try {
                    System.debug('publishing: ' + epicNextOfKinEditEvents);
                    if(!epicNextOfKinEditEvents.isEmpty()){
                        List<Database.SaveResult> results = EventBus.publish(epicNextOfKinEditEvents);
                        // Inspect publishing results
                        for (Database.SaveResult result : results) {
                            if (!result.isSuccess()) {
                                for (Database.Error error : result.getErrors()) {
                                    System.debug('Error returned for (epicNextOfKinEditEvents): ' + error.getStatusCode() +' - '+ error.getMessage());
                                }
                            }
                            else{
                                System.debug('published: ' + epicNextOfKinEditEvents);
                            }
                        }            
                    }
            
        
                } catch (Exception ex) {
                    System.debug('PatientContact_TriggerHandler.isEditForEpic.exception: ' + ex);
                }
            }
        }
    }

    /*******************************************************************************************************
    * @description When Patient_Contact__c is created, sync Same_Household__c and Same_Household_Youreka__c
    */
    private static void insertSyncSame_Household(){

        for(Patient_Contact__c con : (List<Patient_Contact__c>)Trigger.new){
            if(con.Same_Household__c != null){
                Schema.DescribeFieldResult fieldResult = Patient_Contact__c.Same_Household__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == con.Same_Household__c){
                        con.Same_Household_Youreka__c = pickListVal.getLabel();
                    }
                }
                System.debug('con.Same_Household_Youreka__c: ' + con.Same_Household_Youreka__c);
            }
            
            if(con.Same_Household_Youreka__c != null){
                Schema.DescribeFieldResult fieldResult = Patient_Contact__c.Same_Household__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == con.Same_Household_Youreka__c){
                    	con.Same_Household__c = pickListVal.getValue();
                    }
                }
                System.debug('con.Same_Household__c: ' + con.Same_Household__c);
            }
        }
    }    
    
    /*******************************************************************************************************
    * @description When Patient_Contact__c is created, sync Relationship__c and Relationship_Youreka__c
    */
    private static void insertSyncRelationship(){

        for(Patient_Contact__c con : (List<Patient_Contact__c>)Trigger.new){
            if(con.Relationship__c != null){
                Schema.DescribeFieldResult fieldResult = Patient_Contact__c.Relationship__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == con.Relationship__c){
                        con.Relationship_Youreka__c = pickListVal.getLabel();
                    }
                }
                System.debug('con.Relationship_Youreka__c: ' + con.Relationship_Youreka__c);
            }
            
            if(con.Relationship_Youreka__c != null){
                Schema.DescribeFieldResult fieldResult = Patient_Contact__c.Relationship__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == con.Relationship_Youreka__c){
                    	con.Relationship__c = pickListVal.getValue();
                    }
                }
                System.debug('con.Relationship__c: ' + con.Relationship__c);
            }
        }
    }

    /*******************************************************************************************************
    * @description When Patient_Contact__c is udated, sync Relationship__c and Relationship_Youreka__c
    */
    private static void updateSyncRelationship(){
        for(Patient_Contact__c con : (List<Patient_Contact__c>)Trigger.newMap.values()){
            Patient_Contact__c oldCon = (Patient_Contact__c)Trigger.oldMap.get(con.ID);
            Patient_Contact__c newCon = (Patient_Contact__c)Trigger.newMap.get(con.ID);
            
            if(newCon.Relationship__c != oldCon.Relationship__c){
                Schema.DescribeFieldResult fieldResult = Patient_Contact__c.Relationship__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == newCon.Relationship__c){
                        newCon.Relationship_Youreka__c = pickListVal.getLabel();
                    }
                }             
            }

            if(newCon.Relationship_Youreka__c != oldCon.Relationship_Youreka__c){
                Schema.DescribeFieldResult fieldResult = Patient_Contact__c.Relationship__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == newCon.Relationship_Youreka__c){
                        string pickVa = pickListVal.getValue();
                        newCon.Relationship__c = pickVa;
                    }
                }
            }
        }
    }

    /*******************************************************************************************************
    * @description When Patient_Contact__c is udated, sync Same_Household__c and Same_Household_Youreka__c
    */
    private static void updateSyncSameHousehold(){
        for(Patient_Contact__c con : (List<Patient_Contact__c>)Trigger.newMap.values()){
            Patient_Contact__c oldCon = (Patient_Contact__c)Trigger.oldMap.get(con.ID);
            Patient_Contact__c newCon = (Patient_Contact__c)Trigger.newMap.get(con.ID);
            
            if(newCon.Same_Household__c != oldCon.Same_Household__c){
                Schema.DescribeFieldResult fieldResult = Patient_Contact__c.Same_Household__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == newCon.Same_Household__c){
                        newCon.Same_Household_Youreka__c = pickListVal.getLabel();
                    }
                }             
            }

            if(newCon.Same_Household_Youreka__c != oldCon.Same_Household_Youreka__c){
                Schema.DescribeFieldResult fieldResult = Patient_Contact__c.Same_Household__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == newCon.Same_Household_Youreka__c){
                        string pickVa = pickListVal.getValue();
                        newCon.Same_Household__c = pickVa;
                    }
                }
            }
        }
    }
    
    /*******************************************************************************************************
    * @description Check if qualifying fields have been changed for sync
    */
    private static boolean isSyncFieldChange(Patient_Contact__c oldContact, Patient_Contact__c updatedContact){
        List<Schema.FieldSetMember> contactSyncCandidates = SObjectType.Patient_Contact__c.FieldSets.ContactSyncCandidates.getFields();
        for(Schema.FieldSetMember fsm: contactSyncCandidates){
            if(oldContact.get(fsm.getFieldPath()) != updatedContact.get(fsm.fieldpath)){
                return true;
            }
		} 
        return false;
    }
}