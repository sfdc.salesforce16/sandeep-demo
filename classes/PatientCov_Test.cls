@isTest
public class PatientCov_Test {
    
    @IsTest
    static void testRun() {
	 	RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward');
		Insert a;
        string accid = a.id;
        Account a2 = new Account(FirstName = 'Bob', LastName = 'Wayne');
        System.debug('a.id ' + a.id);
        Contact c = new Contact(FirstName = 'Bruce', LastName = 'Wayne', AccountId = a2.Id);
        Insert c;
        System.debug('c.id ' + c.id);
        MemberPlan mbp = new MemberPlan(MemberId = a.Id, Name = 'Test MBP', RelationshipToSubscriber = 'Self');
		Insert mbp;
        MemberPlan mbp2 = new MemberPlan(MemberId = a.Id, Name = 'Test MBP', Create_new_Subscriber__c = true, New_Subscriber_Name__c = 'Bob Wayne', New_Subscriber_Birthdate__c = date.ValueOf('2020-12-05'), New_Subscriber_Sex__c = 'Male', New_Subscriber_SSN__c = '123-12-1234', New_Subscriber_ID__c = '1234');
		Insert mbp2;
        System.debug('mbp.id ' + mbp.id);
        string mbpid = mbp.id;
        Test.startTest();
        PatientCov pcc = new PatientCov();
		PatientCov.getCoverages(mbpid);
        String covName = 'Test Plan'; 
        String groupNum = '12345';
        date effFrom = date.valueOf('2020-12-12');
        date effTo = date.valueOf('2020-12-12');
        string memberId = accid; 
        string payerName = a.FirstName; 
        String subscriberName = a.FirstName;
        date subscriberDob = effFrom;
        string subscriberAdd1 = '123 Main St.';
        string subscriberAdd2 = '#222';
        string subscriberCity = 'Portland';
        string subscriberState = 'Oregon';
        string subscriberZip = '97000';
        string subscriberCountry = 'United States';
        PatientCov.createPlan(covName, groupNum, effFrom, effTo, memberId, payerName, subscriberName, subscriberDob, subscriberAdd1, subscriberAdd2, subscriberCity, subscriberState, subscriberZip, subscriberCountry);
        String subscriberId = accid;
        String currentRecordId = mbpid;
        PatientCov.updatePlan(covName, groupNum, effFrom, effTo, subscriberId, currentRecordId); 
		PatientCov.detachPlan(currentRecordId);
        Test.stopTest();     
    }
}