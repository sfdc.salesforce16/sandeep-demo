/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class provides unit tests to cover the ProviderAddressLookupController Class.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Karthik
 * @project     DHAS
 * @since       July 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */
@isTest
public class ProviderAddressLookupController_Test {
    
    @testSetup static void methodName() {
 		RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        List<Account> listOfAccounts = new List<Account>();
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        listOfAccounts.add(a);
        insert listOfAccounts;
	}
    
	@isTest static void testAddress() {
        ProviderAddressLookupController objcls = new ProviderAddressLookupController();
        Account acc = [SELECT Id FROM Account LIMIT 1];
    	ProviderAddressLookupController.setProviderAddress(acc.Id,'testId' ,'TEST');
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpProviderAddress());
        ProviderAddressLookupController.findProviderAddresses('TEST');
        Test.stopTest();
    }

    @isTest static void testProviderAddress() {
        ProviderAddress pa = new ProviderAddress();
        pa.UniqueID = '1234';
        pa.StreetLine1 = '1234';
        pa.StreetLine2 = '1234';
        pa.StreetLine3 = '1234';
        pa.City = 'test';
        pa.Zip ='test';
        pa.Phone='test';
        pa.ExternalPlaceName='test'; 
        pa.ExternalAddressID = 'test';    
        pa.IsActive = true;    
        pa.IsPrimary = true;   
        pa.IsSharedAddress = true; 
        pa.IsInternalAddress = true;    
        ProviderAddress.State s = new ProviderAddress.State();
        s.Abbr = 'CA';
        ProviderAddress.Country c = new ProviderAddress.Country();
        c.Abbr = 'IL';
        pa.State = s;
        pa.Country = c;
    }
}