/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description Provides @AuraEnabled methods that can be called from LWC to create Coverage, update Coverage and detach Coverage.
*/
public with sharing class PatientCov {
    /*******************************************************************************************************
    * @description Retrieve MemberPlan(Coverage) with record Id
    * @param recordId the record Id
    * @return the list of MemberPlan records with matched recordId
    */
    @AuraEnabled(Cacheable=true)
    public static List <MemberPlan> getCoverages(String recordId) {
        return [SELECT Id,Name,GroupNumber,EffectiveFrom,EffectiveTo,Subscriber.Name,SubscriberId,
                        Payer.Name,MemberId,epicId__c,PlanId,PayerId,New_Subscriber_ID__c, MemberPlan.Duke_Plan_Name__c, Duke_Payer_Name__c, MemberPlan.Subscriber_ID__c
                        FROM MemberPlan
                        WHERE  MemberId = :recordId
                        LIMIT 10];

                        //SubscriberId != null
                        //AND PayerId != null
    }   

    /*******************************************************************************************************
    * @description Create MemberPlan(Coverage) with parameters
    * @param covName the name of coverage
    * @param groupNum the Group Number
    * @param effFrom the Effective From Date 
    * @param effTo the Effective To Date
    * @param memberId the Member Id
    * @param payerName the payer's name
    * @param subscriberName the subscriber's name
    * @param subscriberDob the subscriber's DOB
    * @param subscriberAdd1 the subscriber's address 1
    * @param subscriberAdd2 the subscriber's address 2
    * @param subscriberCity the subscriber's city
    * @param subscriberState the subscriber's state
    * @param subscriberZip the subscriber's zip
    * @param subscriberCountry the subscriber's country
    */
    @AuraEnabled
    public static void createPlan(
        String covName, 
        String groupNum, 
        date effFrom, 
        date effTo, 
        string memberId, 
        string payerName, 
        String subscriberName, 
        date subscriberDob, 
        string subscriberAdd1, 
        string subscriberAdd2, 
        string subscriberCity, 
        string subscriberState, 
        string subscriberZip, 
        string subscriberCountry){ 
        try{  
            System.debug('apex started');   
            Account payerAcc = new Account();
            payerAcc.Name = payerName;
            insert payerAcc;         
            System.debug('payerAcc.Id ' + payerAcc.Id);   
            Account subscAcc = new Account();
            subscAcc.Name = subscriberName;
            //subscAcc.PersonBirthdate = subscriberDob;
            //subscriber.Address1 = subscriberAdd1;
            //subscriber.Address2 = subscriberAdd2;
            //subscriber.City = subscriberCity;
            //subscriber.State = subscriberState;
            //subscriber.Zip = subscriberZip;
            //subscriber.Country = subscriberCountry;
            insert subscAcc;         
            System.debug('subsc.Id ' + subscAcc.Id);            
            MemberPlan membP = new MemberPlan();
            membP.Name = covName;
            membP.GroupNumber = groupNum;
            membP.EffectiveFrom = effFrom;
            membP.EffectiveTo = effTo;
            membP.PayerId = payerAcc.Id;
            membP.MemberId = memberId;
            membP.SubscriberId = subscAcc.Id;
            //membP.IsActive__c = true;
            insert membP;         
            System.debug('MemberPlan.Id ' + membP.Id);          
        }catch(Exception e){
            System.debug('--->'+e);
        }
    }   

    /*******************************************************************************************************
    * @description Update MemberPlan(Coverage) with parameters
    * @param covName the name of coverage
    * @param groupNum the Group Number
    * @param effFrom the Effective From Date 
    * @param effTo the Effective To Date
    * @param subscriberId the subscriber's Id
    * @param currentRecordId the current record Id
    */
    @AuraEnabled
    public static void updatePlan(
        String covName, 
        String groupNum, 
        date effFrom,
        date effTo, 
        string subscriberId, 
        string currentRecordId){ 
        try{  
            MemberPlan membP = [SELECT Id FROM MemberPlan WHERE Id = :currentRecordId];
            membP.Name = covName;
            membP.GroupNumber = groupNum;
            membP.EffectiveFrom = effFrom;
            membP.EffectiveTo = effTo;
            update membP;                   
        }catch(Exception e){
            System.debug('--->'+e);
        }
    }

    /*******************************************************************************************************
    * @description Detach MemberPlan(Coverage) with record Id
    * @param currentRecordId the current record Id
    */
    @AuraEnabled
    public static void detachPlan(string currentRecordId){ 
        try{  
            MemberPlan membP = [SELECT Id FROM MemberPlan WHERE Id = :currentRecordId];
            //membP.IsActive__c = false;
            update membP;                   
        }catch(Exception e){
            System.debug('--->'+e);
        }
    }

}