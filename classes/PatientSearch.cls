/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description This is a wrapper Apex class that contains a few details for a patient record. 
* This class is used to package a few details of a patient record in a serializable format.
* When returned from a remote Apex controller action, the properties with @AuraEnabled annotation are defined on the client-side. 
*/
public with sharing class PatientSearch {
    public PatientSearch() {}

    @AuraEnabled
    public string Id { get; set; }    

    @AuraEnabled
    public string mrn { get; set; }    

    @AuraEnabled
    public string title { get; set; } 

    @AuraEnabled
    public string name { get; set; }    

    @AuraEnabled
    public string suffix { get; set; } 

    @AuraEnabled
    public string birthDate { get; set; }    

    @AuraEnabled
    public string gender { get; set; }    

    @AuraEnabled
    public string address { get; set; }    
    
    @AuraEnabled
    public string city { get; set; }    
    
    @AuraEnabled
    public string district { get; set; }    
    
    @AuraEnabled
    public string state { get; set; }    
    
    @AuraEnabled
    public string postalCode { get; set; }    
    
    @AuraEnabled
    public string country { get; set; }    
    
    @AuraEnabled
    public string phone { get; set; }    

    @AuraEnabled
    public string rank { get; set; }    
}