/*
    Amount (positive is debit, negative is credit)

    Revenue Recognition (same as Revenue Recognition) - per day for remaining balance with discount
        Debit DR
        Debit Sales - Discounts Allowed
        Credit Membership Dues (DR + Discount)

    Payment by Scholarship
        Debit to Financial Aid
        Credit to AR

    Scenario 1: Service/Program with a specific date, or date range
                Notes: No revenue is recognized until the service is recieved. Registration is NOT a deposit, and is refundable because it is not considered revenue until service is rendered.
        Registration
            Debit to AR
            Credit to DR
        Payment
            Debit to Cash
            Credit to AR
        Revenue Recognition - per day of service/program period
            Debit DR
            Credit to Sales

    Scenario 2: Product Purchases
                Notes: Revenue is recognized on the date of purchase. No dates need to be specified for these items. 
        Purchase
            Debit to AR
            Credit to Sales
        Payment
            Debit to Cash
            Credit to AR

    Scenario 3: Program with Deposit
        Payment (Deposit)
            Debit to Cash
            Credit to Membership Dues
        Registration for remaining balance
            Debit AR
            Credit DR
        Payment for remaining balance (same as Payment)
            Debit to Cash
            Credit to AR
        Revenue Recognition (same as Revenue Recognition) - per day for remaining balance
            Debit DR
            Credit Membership Dues

    Scenario 4: Program without Deposit
        Registration
            Debit to AR
            Credit to DR
        Payment for remaining balance (same as Payment)
            Debit to Cash
            Credit to AR
        Revenue Recognition - per day of service/program period
            Debit DR
            Credit to Sales
        Revenue Recognition (same as Revenue Recognition) - per day for remaining balance
            Debit DR
            Credit Membership Dues

    Scenario 5: Program with Deposit with Scholarship
        Payment (Deposit)
            Debit to Cash
            Credit to Membership Dues
        Registration for remaining balance
            Debit AR
            Credit DR
        Payment by Scholarship
            Debit to Financial Aid
            Credit to AR
        Revenue Recognition (same as Revenue Recognition) - per day for remaining balance
            Debit DR
            Credit Membership Dues

    Scenario 6: Program with Deposit
        Payment (Deposit)
            Debit to Cash
            Credit to Membership Dues
        Registration for remaining balance
            Debit AR
            Credit DR
        Payment for remaining balance (same as Payment)
            Debit to Cash
            Credit to AR
        Revenue Recognition with Discount (same as Revenue Recognition?) - per day for remaining balance
            Debit DR
            Debit Discount
            Credit Membership Dues (DR + Discount)

*/
global class InvoiceItem_Batch implements Database.Batchable<sObject>, schedulable {

    public Date batchDate;
    public String queryCriteria;

    public Map<Id, GL_Account__c> GLaccts;
    public Map<Id, CareProgram> programs;
    public Map<Id, Product__c> products;
    public GL_Account__c discountGL;
    //public Map<Id, Cost_Center__c> costCtrs;

    // constructor for scheduler
    global void execute(SchedulableContext sc) {
        // run for yesterday
        batchDate = System.today().addDays(-1);
        // default criteria is Order Items that need to be proccessed
        queryCriteria = 'Order__r.Status__c != \'Draft\' AND '
                      + '(Deposit_Invoice_Item_Complete__c = false OR Registration_Invoice_Item_Complete__c = false OR '
                      + '(Product__c != null AND Invoice_Item_Processing_Complete__c = false) OR '
                      + '(Program__c != null AND Start_Date__c <= :batchDate AND (End_Date__c = null OR End_Date__c >= :batchDate)))'; 
        InvoiceItem_Batch b = new InvoiceItem_Batch(batchDate, queryCriteria);
        Database.executeBatch(b);
    }
    
    // scheduler constructor
    public InvoiceItem_Batch() {}

    // batch run constructor
    // rDate = date we are running the batch for
    // qCriteria = open Orders
    public InvoiceItem_Batch(Date rDate, String qCriteria) {
        batchDate = rDate;
        queryCriteria = qCriteria;
        GLaccts = getGLAccounts();
        for(GL_Account__c gl : GLaccts.values()) {
            if(gl.Name.equalsIgnoreCase('Sales - Discounts Allowed')) { discountGL = gl; break; }
        }
        //costCtrs = getCostCenters();
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id, Type__c, Discount__c, Quantity__c, Program__c, Product__c, Duration__c, Deposit__c, Order__c, Full_Amount__c, '
                     + 'Deposit_Invoice_Item_Complete__c, Registration_Invoice_Item_Complete__c, Invoice_Item_Processing_Complete__c, '
                     + 'Amount__c, Price__c, Start_Date__c, End_Date__c, Discount_Amount__c '
                     + 'FROM Order_Item__c WHERE ' + queryCriteria + ' ORDER BY CreatedDate ASC';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Order_Item__c> scope){

        // get all records in scope
        Set<Id> programIds = new Set<Id>();
        Set<Id> productIds = new Set<Id>();
        Set<Id> orderIds = new Set<Id>();
        for(Order_Item__c i : scope) {
            orderIds.add(i.Order__c);
            programIds.add(i.Program__c);
            productIds.add(i.Product__c);
        }
        programs = getPrograms(programIds);
        products = getProducts(productIds);

        // process Order Items
        List<Invoice_Item__c> newInvoiceItems = new List<Invoice_Item__c>();
        newInvoiceItems.addAll(processOrderItems(scope));

        insert newInvoiceItems;
        update scope;

    }

    global void finish(Database.BatchableContext bc){
        // send error emails?
    }

    private List<Invoice_Item__c> processOrderItems(List<Order_Item__c> oItems) {

        List<Invoice_Item__c> programOrderItems = new List<Invoice_Item__c>();

        for(Order_Item__c i : oItems) {

            // handle ProductOrder Item Scenarios
            if(i.Product__c != null && !i.Invoice_Item_Processing_Complete__c) {
                // handle purchases
                programOrderItems.addAll(handlePurchases(i));
            }

            // handle Program Order Item Scenarios
            else if(i.Program__c != null) {
                CareProgram program = programs.get(i.Program__c);

                // deposits occur on the purchase date when there is a Deposit configured
                if(program.Deposit__c != null && program.Deposit__c > 0 && !i.Deposit_Invoice_Item_Complete__c) {
                    programOrderItems.addAll(handleDeposits(i));
                }

                // registrations occur on the purchase date for all Program types
                if(!i.Registration_Invoice_Item_Complete__c) {
                    programOrderItems.addAll(handleRegistrations(i));
                }

                // revenue recognition during start date range for programs -- start date is the service date for single day services
                System.debug('Starting revenue recognition?' + batchDate + ' ' + i);
                if(batchDate >= i.Start_Date__c && (i.End_Date__c == null || batchDate <= i.End_Date__c)) {
                    System.debug('Starting revenue recognition?');
                    programOrderItems.addAll(handleRevenueRecognition(i));
                }

            }
        }

        return programOrderItems;
    }

    private List<Invoice_Item__c> handleDeposits(Order_Item__c i) {
        CareProgram program = programs.get(i.Program__c);

        List<Invoice_Item__c> newItems = new List<Invoice_Item__c>();
        // Debit to Cash
        Invoice_Item__c debit = new Invoice_Item__c(
            GL_Account__c = program.Payments_GL_Account__c,
            Order__c = i.Order__c,
            Date__c = batchDate, 
            Debit__c = program.Deposit__c,
            Type__c = 'Payment (Deposit)',
            Order_Item__c = i.Id
        ); newItems.add(debit);

        // Credit to Membership Dues
        Invoice_Item__c credit = new Invoice_Item__c(
            GL_Account__c = program.Revenue_GL_Account__c,
            Order__c = i.Order__c,
            Date__c = batchDate, 
            Credit__c = program.Deposit__c,
            Type__c = 'Payment (Deposit)',
            Order_Item__c = i.Id
        ); newItems.add(credit);

        i.Deposit__c = program.Deposit__c;
        i.Deposit_Invoice_Item_Complete__c = true;

        return newItems;
    }

    private List<Invoice_Item__c> handleRegistrations(Order_Item__c i) {
        CareProgram program = programs.get(i.Program__c);
        Double amount = (i.Deposit__c == null) ? i.Price__c : i.Price__c - i.Deposit__c;

        List<Invoice_Item__c> newItems = new List<Invoice_Item__c>();
        // Debit to AR
        Invoice_Item__c debit = new Invoice_Item__c(
            GL_Account__c = program.Accounts_Receivable_GL_Account__c,
            Order__c = i.Order__c,
            Date__c = batchDate, 
            Debit__c = amount,
            Type__c = 'Registration',
            Order_Item__c = i.Id
        ); newItems.add(debit);

        // Credit to DR
        Invoice_Item__c credit = new Invoice_Item__c(
            GL_Account__c = program.Deferred_Revenue_GL_Account__c,
            Order__c = i.Order__c,
            Date__c = batchDate, 
            Credit__c = amount,
            Type__c = 'Registration',
            Order_Item__c = i.Id
        ); newItems.add(credit);

        i.Registration_Invoice_Item_Complete__c = true;

        return newItems;
    }

    // per day of service/program period
    private List<Invoice_Item__c> handleRevenueRecognition(Order_Item__c i) {
        CareProgram program = programs.get(i.Program__c);
        Integer dayX = (i.Start_Date__c != null) ? i.Start_Date__c.daysBetween(batchDate) + 1 : 1;
        Double balanceAmount = (i.Deposit__c == null) ? i.Amount__c : (i.Amount__c - i.Deposit__c); // discounted
        Double balanceFullAmount = (i.Deposit__c == null) ? i.Full_Amount__c : (i.Full_Amount__c - i.Deposit__c); // discounted

        Double fullamount = (i.Deposit__c == null) ? (i.Full_Amount__c / i.Duration__c).setScale(2) : (balanceFullAmount / i.Duration__c).setScale(2); // not discounted
        Double amount = (balanceAmount / i.Duration__c).setScale(2); // discounted
        Double discount = ((Decimal)(fullamount - amount)).setScale(2);

        // adjust amount if this is the last date of the period
        if(i.End_Date__c == batchDate) {
            // set amounts to the difference between amounts and calculated amounts applied for the duration
            amount = balanceAmount - (amount * (i.Duration__c - 1)); 
            fullamount = balanceFullAmount - (fullamount * (i.Duration__c - 1)); 
            discount = (balanceFullAmount - balanceAmount) - (discount * (i.Duration__c - 1)); 
        }

        List<Invoice_Item__c> newItems = new List<Invoice_Item__c>();
        // Debit DR
        Invoice_Item__c debit = new Invoice_Item__c(
            GL_Account__c = program.Deferred_Revenue_GL_Account__c,
            Order__c = i.Order__c,
            Date__c = batchDate, 
            Debit__c = amount,
            Type__c = 'Day ' + dayX,
            Order_Item__c = i.Id
        ); newItems.add(debit);

        // Discount
        if(discount > 0) { newItems.addAll(handleDiscounts(i, discount, dayX)); }

        // Credit to Revenue
        Invoice_Item__c credit = new Invoice_Item__c(
            GL_Account__c = program.Revenue_GL_Account__c,
            Order__c = i.Order__c,
            Date__c = batchDate, 
            Credit__c = fullamount,
            Type__c = 'Day ' + dayX,
            Order_Item__c = i.Id
        ); newItems.add(credit);

        if(i.End_Date__c == null || batchDate == i.End_Date__c) {
            i.Invoice_Item_Processing_Complete__c = true;
        }

        return newItems;
    }

    // discount for Programs or Products
    private List<Invoice_Item__c> handleDiscounts(Order_Item__c i, Double discount, Integer dayX) {

        List<Invoice_Item__c> newItems = new List<Invoice_Item__c>();
        // Debit to Sales - Discounts Allowed
        Invoice_Item__c debit = new Invoice_Item__c(
            GL_Account__c = discountGL.Id,
            Order__c = i.Order__c,
            Date__c = batchDate, 
            Debit__c = discount,
            Type__c = (i.Product__c != null) ? 'Discount' : 'Discount Day ' + dayX,
            Order_Item__c = i.Id
        ); newItems.add(debit);
        return newItems;
    }

    private List<Invoice_Item__c> handlePurchases(Order_Item__c i) {
        Product__c product = products.get(i.Product__c);

        Double amount = i.Amount__c;
        Double fullamount = i.Full_Amount__c;
        Double discount = fullamount - amount;

        List<Invoice_Item__c> newItems = new List<Invoice_Item__c>();
        // Debit to AR
        Invoice_Item__c debit = new Invoice_Item__c(
            GL_Account__c = product.Accounts_Receivable_GL_Account__c,
            Order__c = i.Order__c,
            Date__c = batchDate, 
            Debit__c = amount,
            Type__c = 'Purchase',
            Order_Item__c = i.Id
        ); newItems.add(debit);

        // Discount
        if(discount > 0) { newItems.addAll(handleDiscounts(i, discount, null)); }

        // Credit to Revenue
        Invoice_Item__c credit = new Invoice_Item__c(
            GL_Account__c = product.Revenue_GL_Account__c,
            Order__c = i.Order__c,
            Date__c = batchDate, 
            Credit__c = fullamount,
            Type__c = 'Purchase',
            Order_Item__c = i.Id
        ); newItems.add(credit);

        i.Invoice_Item_Processing_Complete__c = true;

        return newItems;
    }

    private Map<Id, GL_Account__c> getGLAccounts() {
        return new Map<Id, GL_Account__c>([SELECT Id, Name FROM GL_Account__c]);
    }

    /*private Map<Id, Cost_Center__c> getCostCenters() {
        return new Map<Id, Cost_Center__c>([SELECT Id, Name FROM Cost_Center__c]);
    }*/

    private Map<Id, CareProgram> getPrograms(Set<Id> scope) {
        return new Map<Id, CareProgram>([SELECT Id, Price__c, Deposit__c, Duration__c, 
                       Payments_GL_Account__c, Deferred_Revenue_GL_Account__c, Accounts_Receivable_GL_Account__c, Revenue_GL_Account__c
                FROM CareProgram 
                WHERE Id IN :scope]);
    }

    private Map<Id, Product__c> getProducts(Set<Id> scope) {
        return new Map<Id, Product__c>([SELECT Id, Selling__c,
                       Payments_GL_Account__c, Accounts_Receivable_GL_Account__c, Revenue_GL_Account__c
                FROM Product__c 
                WHERE Id IN :scope]);
    }
}