/**
 * @author Ian McNear
 * @date 03.17.2021
 * @group DCRC
 * @testCoverage 80%
 */

@RestResource(urlMapping='/session/*')
global with sharing class ApiSession {
    
    @HttpGet
    global static GetResponseWrapper retrieveSession() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        GetResponseWrapper responseWrapper = new GetResponseWrapper();
        // Move this date to a custom setting/metadata 
        Datetime apiCutoff = Datetime.now().addDays(-31);
        try {
            List<Session__c> sessionReturnList = new List<Session__c>();
            res.StatusCode = 200;

            // Session List to Return
            sessionReturnList = [
                SELECT Id, Name, Program_Name__c, State__c, Zip_Code__c, Address__c, Facility__c, Event_Fee__c, Event_Host__c,
                    HTML_Description__c, Available_Openings__c, Total_Registered__c, Scheduled_Start__c, Scheduled_End__c, Parent_Session__c,
                	Address_2__c, Category__c, Status__c, City__c, Event_Type__c, Speakers__c, Phone_Number__c, Location__c, Virtual__c
                FROM Session__c
                WHERE Status__c = 'Active' AND
					RecordType.Name = 'Event' AND
                	Scheduled_End__c > :apiCutoff
            ];
            
            responseWrapper.sessionList = sessionReturnList;
            responseWrapper.returnSize = sessionReturnList.size();
        } catch (Exception e) {
            res.StatusCode = 500;
            responseWrapper.status = 'Error';
            responseWrapper.message = 'Your request failed with the following error: ' + e.getMessage();
        }
        
        return responseWrapper;
    } 
    
    // Response Wrapper so we can have our own status's and messages
    global with sharing class GetResponseWrapper {
        @TestVisible List<Session__c> sessionList;
        @TestVisible String status;
        @TestVisible String message;
        @TestVisible Integer returnSize;

        public GetResponseWrapper() {
            // Set to success on default. Avoid having to set it later.
            status = 'Success';
            message = 'Sessions returned successfully';
            returnSize = 0;
        }
    }
}