@isTest
public class SessionTriggerTest {
    static testmethod void testunit1(){
        Session__c s = new Session__c();
        s.Event_Fee__c  = '2000';
        s.Address__c  = 'street1';
        s.Address_2__c   ='floor 2';
        s.Facility__c  = 'uke University Hospital';
        s.Available_Openings__c  = 100;
        s.Category__c   = 'Allergies' ;
        s.City__c   = 'city1' ;
        s.Description__c   = 'test desc' ;
        s.Event_Host__c   = 'host';
        s.Event_Type__c    = 'Art Class';
        s.Phone_Number__c    = '1234567890';
        //s.Scheduled_End__c    = system.now();
        //s.Scheduled_Start__c    = system.now();
        s.Session_Details__c    = 'Session';
        s.Session_Link__c    = 'Session 1' ;
        s.Session_Link_Text__c    = 'Session 2';
        s.Speakers__c    = 'JBL';
        s.State__c    = 'State 1';
        s.Status__c    = 'Active' ;
        s.Virtual__c     = false;
        s.Zip_Code__c     = '1234';
        s.HTML_Description__c    = 'test' ; 
        insert s;
        Session__c s1 = new Session__c();
        s1.Event_Fee__c  = '2000';
        s1.Parent_Session__c = s.id;
        insert s1;
        s.Event_Fee__c  = '2000';
        update s;   
    }
}