/******************************************
 * 
 * @author Synaptic AP
 * @since  Feb 2021
 * Project DHAS
 */
public with sharing class DismissalController {
    public DismissalController() {}

    @AuraEnabled
    public static List<Dismissal> callEpicDismissal(String accountId){
        Account patient = [SELECT Id, HealthCloudGA__MedicalRecordNumber__c FROM Account WHERE Id = :accountId LIMIT 1];
        String mrn = patient.HealthCloudGA__MedicalRecordNumber__c;

        //validation
        if(String.isBlank(mrn)){ return null;}

        EpicWebService ews = new EpicWebService();
        HTTPResponse res = ews.call('dismissal', mrn, 'GET');

        List<Dismissal> dismissals = (List<Dismissal>) JSON.deserialize(res.getBody(), List<Dismissal>.class);
        System.debug('dismissals: ' + dismissals);


        return dismissals;
    }
}