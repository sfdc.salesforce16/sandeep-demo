/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class provides unit tests to cover the Referral_Information__c Trigger and 
 * Referral_Information_c_TriggerHandler apex class.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Synaptic
 * @project     DHAS
 * @since       April 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

@isTest
public class ReferralTriggerHandlerTest {
    
    @IsTest
    static void testReferralTrigger() {
		String personRtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
		Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = personRtId);
    	try {
        	insert a;
	    	Referral_Information__c ref1 = new Referral_Information__c(Patient_Name__c = a.Id, Referral_Type__c = 'VA', Referral_Status__c = 'Authorized', Referral_ID__c = '123456');
	    	Referral_Information__c ref2 = new Referral_Information__c(Patient_Name__c = a.Id, Referral_Type__c = 'Tricare Prime', Referral_Status__c = 'Authorized', Referral_ID__c = '123456');
	    	Referral_Information__c ref3 = new Referral_Information__c(Patient_Name__c = a.Id, Referral_Type__c = 'Worker\'s Comp', Referral_Status__c = 'Authorized', Referral_ID__c = '123456');
	    	Referral_Information__c ref4 = new Referral_Information__c(Patient_Name__c = a.Id, Referral_Type__c = 'Project Access', Referral_Status__c = 'Authorized', Referred_to_Provider_Specialty__c = 'Family Medicine', Referral_ID__c = '123456');
	    	Referral_Information__c ref5 = new Referral_Information__c(Patient_Name__c = a.Id, Referral_Type__c = 'International', Referral_Status__c = 'Authorized', Referral_ID__c = '123456');
	    	Referral_Information__c ref6 = new Referral_Information__c(Patient_Name__c = a.Id, Referral_Type__c = 'CFS Non-Contracted Review', Referral_Status__c = 'Authorized', Referral_ID__c = '123456');
			HealthcareProvider hp = new HealthcareProvider (Department_Specialty__c = 'Family Medicine', Name = 'Doctor Fish');
            insert hp;
    		Skill_Group__c skg = new Skill_Group__c(Name='123DPC BR CREEK PC APPT SG', Skill_Group__c = '123DPC_BR_CREEK_PC_APPT_SG', Duke_Department__c = hp.Id, Specialty__c='123');
			insert skg;
            Case c = new Case(AccountId = a.Id, Status = 'New', Skill_Group__c = skg.Id);
			insert c;
            insert ref1;
            insert ref2;
            insert ref3;
            insert ref4;
            insert ref5;
            insert ref6;
            ref1.Referral_Status__c = 'Open';
            ref2.Referral_Status__c = 'Open';
            ref3.Referral_Status__c = 'Open';
            ref4.Referral_Status__c = 'Open';
            ref5.Referral_Status__c = 'Open';
            ref6.Referral_Status__c = 'Open';
            update ref1;
            update ref2;
            update ref3;
            update ref4;
            update ref5;
            update ref6;
	    } catch (exception e){
        	system.debug(e);
    	}
    }
}