public without sharing class CampaignMemberMatching {
    
    @AuraEnabled
    @InvocableVariable(label='Ids')
    public List<String> ids;

    @InvocableMethod(label='Campaign Member Matching' description='Get Campaign Details by Campaign Id.' category='Campaign')
    public static List<CampaignMember> getCampaign(List<Id> ids) {
        System.debug('ids:::' + ids);
        if (ids.isEmpty() == FALSE) {
            List<CampaignMember> CampaignMemberList = [SELECT Id,Name,CompanyOrAccount FROM CampaignMember WHERE CampaignId = :ids.get(0) AND CompanyOrAccount = :ids.get(1)] ;
            return CampaignMemberList;
        }
        return null;
    }
}