/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class provides unit tests to cover the PatientContact Trigger and 
 * PatientContact_TriggerHandler apex class.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Synaptic
 * @project     DHAS
 * @since       February 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

@isTest
public class PatientContact_TriggerHandlerTest {
    
    @IsTest
    static void testPatientContactTrigger() {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        insert a;
        Patient_Contact__c c = new Patient_Contact__c(Contact_Name__c = 'Bruce Wayne', Account__c = a.Id);
        c.Same_Household__c = 'Yes';
        c.Relationship__c = '2';
        insert c;
        try{
            c.Same_Household__c = 'No';
            c.Relationship__c = '1';
            update c;
        }
        catch(DMLException e){
            //System.assert(e);
        }        
    }    
}