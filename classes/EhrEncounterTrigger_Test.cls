/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class provides unit tests to cover the EhrEncounter Trigger and EhrEncounter_TriggerHandler apex class.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Synaptic
 * @project     DHAS
 * @since       February 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

@isTest
public class EhrEncounterTrigger_Test {
    
    @IsTest
    static void testEhrEncounterTrigger() {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account a = new Account(
            FirstName = 'Bob', 
            LastName = 'Wayne', 
            RecordTypeId = rt.Id
         );
        a.Email_Verified__c = false;
        a.Contact_Information_Verified__c = false;
        a.Address_Verified__c = false;
        a.Special_Needs_Verified__c = false;
        a.Interpreter_Needed_Verified__c = false;
        a.Guarantor_Verified__c = false;
        a.PCP_Verified__c = false;
        a.Coverage_Verified__c = false;
        insert a;
        try{
	        HealthCloudGA__EhrEncounter__c eec = new HealthCloudGA__EhrEncounter__c(
    	        Account__c = a.Id,
                HealthCloudGA__Account__c = a.Id,
        	    Creation_Date__c = Date.Today()
        	);
        Insert eec;            
        }catch(exception e){
            system.debug(e);
        }
    }    
    @IsTest
    static void testEhrEncounterTrigger2() {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account a2 = new Account(
            FirstName = 'Bruce', 
            LastName = 'Wayne', 
            RecordTypeId = rt.Id, 
            Address_Verified__c = true,
            Address_Verified_Date__c =  Date.Today() - 5,
            Email_Verified__c = true,
            Email_Verified_Date__c =  Date.Today() - 5,
            Contact_Information_Verified__c = true,
            Contact_Information_Verified_Date__c =  Date.Today() - 5,
            Special_Needs_Verified__c = true,
            Special_Needs_Verified_Date__c =  Date.Today() - 5,
            Interpreter_Needed_Verified__c = true,
            Interpreter_Needed_Verified_Date__c =  Date.Today() - 5,
            PCP_Verified__c = true,
            PCP_Verified_Date__c =  Date.Today() - 5,
            Guarantor_Verified__c = true,
            Guarantor_Verified_Date__c =  Date.Today() - 5
         );
        insert a2;
        try{
	        HealthCloudGA__EhrEncounter__c eec = new HealthCloudGA__EhrEncounter__c(
    	        Account__c = a2.Id,
                HealthCloudGA__Account__c = a2.Id,
        	    Creation_Date__c = Date.Today()
        	);
        Insert eec;     
        
        }catch(exception e){
            system.debug(e);
        }
    }    
}