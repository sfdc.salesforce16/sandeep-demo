public class Form_TriggerHandler {
    
    public static void afterUpdate() {
        List<disco__Form__c> forms = (List<disco__Form__c>)Trigger.new;
        
        Set<Id> newlySubmittedForms = new Set<Id>();
        for(disco__Form__c f : forms) {
            disco__Form__c old = (disco__Form__c)Trigger.oldMap.get(f.Id);
            if(old.disco__Original_Submitted_Date__c == null && f.disco__Original_Submitted_Date__c != null) {
                newlySubmittedForms.add(f.Id);
            }
        }
        if(!newlySubmittedForms.isEmpty()) handleNewlySubmitted(newlySubmittedForms);
    }
    
    // moves Application Status to Pending Approval when no other Forms are found related to the application that are not submitted
    public static void handleNewlySubmitted(Set<Id> scope) {
        
        Map<Id, Boolean> appsMissingForms = new Map<Id, Boolean>();
        // loop through newly submitted forms
        for(Id fId : scope) {
            // the form in trigger context that was just submitted
            disco__Form__c f = (disco__Form__c)Trigger.newMap.get(fId);
            
            // if the form is related to an Application, we need to check the template
			if(f.Program_Application__c != null) 
                appsMissingForms.put(f.Program_Application__c, false);
        }
        
        // get all forms related to the apps from newly submitted forms
        List<disco__Form__c> otherAppForms = [
            SELECT Program_Application__c 
            FROM disco__Form__c
            WHERE Program_Application__c IN : appsMissingForms.keySet() 
              AND disco__Original_Submitted_Date__c = null
        	  AND Id NOT IN :Trigger.newMap.keySet()]; // don't query against Forms in the trigger context
        
        // mark the App as missing a completed form from the query results
        for(disco__Form__c f : otherAppForms) {
            appsMissingForms.put(f.Program_Application__c, true);
        }
        
        // compare against Forms in the trigger context and makr the App as missing a completed form, if needed
        for(disco__Form__c f : (List<disco__Form__c>)Trigger.new) {
            if(f.disco__Original_Submitted_Date__c == null && f.Program_Application__c != null) {
                appsMissingForms.put(f.Program_Application__c, true);
            }
        }
        
        List<Program_Application__c> appsForUpdate = new List<Program_Application__c>();
        for(Id pId : appsMissingForms.keySet()) {
            if(appsMissingForms.get(pId) == false) {
                Program_Application__c a = new Program_Application__c(
                	Id = pId,
                	Status__c = 'Pending Approval',
                    Completed_Date__c = System.today()
                );
                appsForUpdate.add(a);
            }
        }
        update appsForUpdate;

    }

}