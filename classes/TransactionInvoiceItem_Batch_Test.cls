@isTest
public class TransactionInvoiceItem_Batch_Test {
    
    @testSetup
    static void setup() {
        
        // GL Accounts
        GL_Account__c gl1 = new GL_Account__c(Name = 'Financial Aid', GL_Account_Number__c = '1');
        GL_Account__c gl2 = new GL_Account__c(Name = 'Accounts Receivable', GL_Account_Number__c = '1');
        GL_Account__c gl3 = new GL_Account__c(Name = 'Revenue', GL_Account_Number__c = '1');
        GL_Account__c gl4 = new GL_Account__c(Name = 'Payment', GL_Account_Number__c = '1');
        GL_Account__c gl5 = new GL_Account__c(Name = 'Deferred Revenue', GL_Account_Number__c = '1');
        insert new List<GL_Account__c>{gl1, gl2, gl3, gl4, gl5};
            
        // Product
        Product__c prod = new Product__c(
        	Name = 'Shirt',
            Selling__c = 10,
            Accounts_Receivable_GL_Account__c = gl2.Id,
            Revenue_GL_Account__c = gl3.Id,
            Payments_GL_Account__c = gl4.Id
        );
        insert prod;
        
        // Program
        CareProgram p = new CareProgram(
        	Name = 'Residential Weight Loss',
        	Price__c = 100,
        	Deposit__c = 5,
            Duration__c = 7,
            Accounts_Receivable_GL_Account__c = gl2.Id,
            Revenue_GL_Account__c = gl3.Id,
            Payments_GL_Account__c = gl4.Id,
            Deferred_Revenue_GL_Account__c = gl5.Id
        );
        
        insert new List<CareProgram>{p};
            
        // Account 
        Account a = new Account(
        	Name = 'Test Test'
        ); insert a;
        
        // Order 
        Order_Custom__c o = new Order_Custom__c(
        	Member__c = a.Id,
            Status__c = 'Draft'
        ); insert o;
        
        // Product Order Item
        Order_Item__c i1 = new Order_Item__c(
            Type__c = 'Product',
            Order__c = o.Id, 
        	Product__c = prod.Id,
            Quantity__c = 1
        );
        
        // Program Order Item - with deposit
        Order_Item__c i2 = new Order_Item__c(
            Type__c = 'Program',
            Order__c = o.Id, 
        	Program__c = p.Id,
            Start_Date__c = System.today().addDays(30)
        );
        
        insert new List<Order_Item__c>{i1, i2};
            
        Transaction__c trx1 = new Transaction__c(
        	Order__c = o.Id,
            Transaction_Type__c = 'Charge',
            Account__c = a.Id,
            Charge_Type__c = 'Cash',
            Amount__c = 10,
            Date__c = System.today()
        );
            
        Transaction__c trx2 = new Transaction__c(
        	Order__c = o.Id,
            Transaction_Type__c = 'Charge',
            Account__c = a.Id,
            Charge_Type__c = 'Scholarship',
            Amount__c = 5,
            Date__c = System.today()
        );
            
        Transaction__c trx3 = new Transaction__c(
        	Order__c = o.Id,
            Transaction_Type__c = 'Charge',
            Account__c = a.Id,
            Charge_Type__c = 'Cash',
            Amount__c = 95,
            Date__c = System.today()
        );
        
        insert new List<Transaction__c>{trx1, trx2, trx3};
        
        Applied_Fund__c af1 = new Applied_Fund__c(
        	Order__c = o.Id,
            Amount__c = 10,
        	Transaction__c = trx1.Id,
            Order_Item__c = i1.Id
        );
        
        Applied_Fund__c af2 = new Applied_Fund__c(
        	Order__c = o.Id,
            Amount__c = 5,
        	Transaction__c = trx2.Id,
            Order_Item__c = i2.Id
        );
        
        Applied_Fund__c af3 = new Applied_Fund__c(
        	Order__c = o.Id,
            Amount__c = 95,
        	Transaction__c = trx3.Id,
            Order_Item__c = i2.Id
        );
        
        insert new List<Applied_Fund__c>{af1, af2, af3};
        
        
    }
    
    @isTest
    static void runBatch() {
        
        Test.startTest();
        Date batchDate = System.today().addDays(30);
        // removed Draft criteria to run for Order without Transactions
        String queryCriteria = 'Transaction_Type__c = \'Charge\' '
            		  + 'AND Invoice_Item_Processing_Complete__c = false'; 
        TransactionInvoiceItem_Batch b = new TransactionInvoiceItem_Batch(batchDate, queryCriteria);
        Database.executeBatch(b);
        Test.stopTest();
        
    }
    
    @isTest
    static void scheduleBatch() {
        
        Test.startTest();
        // daily at midnight
        String dailyMidnight = '0 0 0 * * ? '; // Seconds Minutes Hours Day_of_month Month Day_of_week Optional_year
        TransactionInvoiceItem_Batch b = new TransactionInvoiceItem_Batch(); 
        System.schedule('Transactions Invoice Item Batch', dailyMidnight, b);
        Test.stopTest();
        
    }

}