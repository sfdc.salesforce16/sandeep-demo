/**
* @author Synaptic
* @date 2014
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description Apex Batch to create Invoice Items and update Order Items based on transaction__c records.
*/
global class TransactionInvoiceItem_Batch implements Database.Batchable<sObject>, schedulable{

    public Date batchDate;
    public String queryCriteria;

    public Map<Id, GL_Account__c> GLaccts;
    public Map<Id, CareProgram> programs;
    public Map<Id, Product__c> products;
    public GL_Account__c financialAidGL;

    /*******************************************************************************************************
    * @description constructor for scheduler
    * @param sc tSchedulableContext
    */
    global void execute(SchedulableContext sc) {
        // run for yesterday
        batchDate = System.today().addDays(-1);
        // default criteria is Order Items that need to be proccessed
        queryCriteria = 'Transaction_Type__c = \'Charge\' '
            		  + 'AND Invoice_Item_Processing_Complete__c = false'; 
        TransactionInvoiceItem_Batch b = new TransactionInvoiceItem_Batch(batchDate, queryCriteria);
        Database.executeBatch(b);
    }
    
    // scheduler constructor
    /*******************************************************************************************************
    * @description scheduler constructor
    */
    public TransactionInvoiceItem_Batch() {}

    // batch run constructor
    // rDate = date we are running the batch for
    // qCriteria = new Transactions
     /*******************************************************************************************************
     * @description batch run constructor
     * @param rDate date we are running the batch for
     * @param qCriteria new Transactions
     */
    public TransactionInvoiceItem_Batch(Date rDate, String qCriteria) {
        batchDate = rDate;
        queryCriteria = qCriteria;
        GLaccts = getGLAccounts();
        for(GL_Account__c gl : GLaccts.values()) {
            if(gl.Name.equalsIgnoreCase('Financial Aid')) { financialAidGL = gl; break; }
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        String query = 'SELECT Id, Amount__c, Charge_Type__c, Date__c, Order__c, '
                     + '(SELECT Id, Amount__c, Order_Item__c, Order_Item__r.Program__c, Order_Item__r.Product__c, '
                     + 'Order_Item__r.Deposit__c, Order_Item__r.Deposit_Amount_Collected__c FROM Applied_Funds__r) '
                     + 'FROM Transaction__c WHERE ' + queryCriteria;
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Transaction__c> scope){

        // get all records in scope
        Set<Id> programIds = new Set<Id>();
        Set<Id> productIds = new Set<Id>();
        for(Transaction__c t : scope) {
            if(t.Applied_Funds__r == null || t.Applied_Funds__r.isEmpty()) continue;
            for(Applied_Fund__c f : t.Applied_Funds__r) {
                programIds.add(f.Order_Item__r.Program__c);
                productIds.add(f.Order_Item__r.Product__c);
            }
        }
        programs = getPrograms(programIds);
        products = getProducts(productIds);

        // process Transactions and Applied Funds
        List<Invoice_Item__c> newInvoiceItems = new List<Invoice_Item__c>();
        Map<Id, Order_Item__c> updatedOrderItems = new Map<Id, Order_Item__c>();
        List<Transaction__c> updatedTransactions = new List<Transaction__c>();
        for(Transaction__c t : scope) {
            // scholarships
            if(t.Charge_Type__c != null && t.Charge_Type__c.equalsIgnoreCase('Scholarship')) {
                newInvoiceItems.addAll(handleScholarships(t));
            }

            // payments
            else {
                newInvoiceItems.addAll(handlePayments(t, updatedOrderItems));
            }

            updatedTransactions.add(t);
        }

        insert newInvoiceItems;
        update updatedOrderItems.values();
        update updatedTransactions;

    }

    global void finish(Database.BatchableContext bc){
        // send error emails?
    }
    
    private List<Invoice_Item__c> handlePayments(Transaction__c trx, Map<Id, Order_Item__c> orderItemsWithDeposits) {
        List<Invoice_Item__c> newItems = new List<Invoice_Item__c>();
        if(trx.Applied_Funds__r == null || trx.Applied_Funds__r.isEmpty()) return newItems;

        for(Applied_Fund__c f : trx.Applied_Funds__r) {
            CareProgram program = programs.get(f.Order_Item__r.Program__c);
            Product__c product = products.get(f.Order_Item__r.Product__c);
            Decimal amount = f.Amount__c;

            // adjust to not double count deposits, which may be in split Transactions
            // deposits only occur for programs
            if(program != null && f.Order_Item__r.Deposit__c != 0) {
                // get the Order Item with the number for it's Deposit Collected
                Order_Item__c i = orderItemsWithDeposits.get(f.Order_Item__c);
                if(i == null) {
                    i = new Order_Item__c(
                        Id = f.Order_Item__c,
                        Deposit__c = f.Order_Item__r.Deposit__c,
                        Deposit_Amount_Collected__c = f.Order_Item__r.Deposit_Amount_Collected__c
                    );
                }

                System.debug(f);
                System.debug(i);
                // check is the full amount has been collected or not -- we will only record new lines for overflow form deposits
                Decimal depoRemaining = (i.Deposit_Amount_Collected__c != null) ? i.Deposit__c - i.Deposit_Amount_Collected__c : i.Deposit__c;
                System.debug(depoRemaining);
                if(depoRemaining > 0) {
                    // determine max amount I can apply to amt collected
                    Decimal depoApply = (depoRemaining > f.Amount__c) ? f.Amount__c : depoRemaining;
                    i.Deposit_Amount_Collected__c += depoApply;
                    amount = amount - depoApply;
                }
                System.debug(amount);

                // put the updated Order Item back in the map for next time!
                orderItemsWithDeposits.put(i.Id, i);
            }

            // if we used all of the applied funds on balancing with the deposit, move on!
            if(amount <= 0) continue;

            // Debit to Payments
            Invoice_Item__c debit = new Invoice_Item__c(
                GL_Account__c = (product != null) ? product.Payments_GL_Account__c : program.Payments_GL_Account__c,
                Order__c = trx.Order__c,
                Date__c = trx.Date__c, 
                Debit__c = amount,
                Type__c = 'Payment',
                Transaction__c = trx.Id,
                Order_Item__c = f.Order_Item__c
            ); newItems.add(debit);

            // Credit to AR
            Invoice_Item__c credit = new Invoice_Item__c(
                GL_Account__c =  (product != null) ? product.Accounts_Receivable_GL_Account__c : program.Accounts_Receivable_GL_Account__c,
                Order__c = trx.Order__c,
                Date__c = trx.Date__c, 
                Credit__c = amount,
                Type__c = 'Payment',
                Transaction__c = trx.Id,
                Order_Item__c = f.Order_Item__c
            ); newItems.add(credit);
        }

        trx.Invoice_Item_Processing_Complete__c = true;

        return newItems;
    }

    private List<Invoice_Item__c> handleScholarships(Transaction__c trx) {
        List<Invoice_Item__c> newItems = new List<Invoice_Item__c>();
        if(trx.Applied_Funds__r == null || trx.Applied_Funds__r.isEmpty()) return newItems;

        for(Applied_Fund__c f : trx.Applied_Funds__r) {
            CareProgram program = programs.get(f.Order_Item__r.Program__c);

            // Debit to Financial Aid
            Invoice_Item__c debit = new Invoice_Item__c(
                GL_Account__c = financialAidGL.Id,
                Order__c = trx.Order__c,
                Date__c = trx.Date__c, 
                Debit__c = f.Amount__c,
                Type__c = 'Scholarship',
                Transaction__c = trx.Id,
                Order_Item__c = f.Order_Item__c
            ); newItems.add(debit);

            // Credit to AR
            Invoice_Item__c credit = new Invoice_Item__c(
                GL_Account__c = program.Accounts_Receivable_GL_Account__c,
                Order__c = trx.Order__c,
                Date__c = trx.Date__c, 
                Credit__c = f.Amount__c,
                Type__c = 'Scholarship',
                Transaction__c = trx.Id,
                Order_Item__c = f.Order_Item__c
            ); newItems.add(credit);
        }

        trx.Invoice_Item_Processing_Complete__c = true;

        return newItems;
    }

    private Map<Id, GL_Account__c> getGLAccounts() {
        return new Map<Id, GL_Account__c>([SELECT Id, Name FROM GL_Account__c]);
    }

    private Map<Id, CareProgram> getPrograms(Set<Id> scope) {
        return new Map<Id, CareProgram>([SELECT Id, Price__c, Deposit__c, Duration__c, 
                       Payments_GL_Account__c, Deferred_Revenue_GL_Account__c, Accounts_Receivable_GL_Account__c, Revenue_GL_Account__c
                FROM CareProgram 
                WHERE Id IN :scope]);
    }

    private Map<Id, Product__c> getProducts(Set<Id> scope) {
        return new Map<Id, Product__c>([SELECT Id, Selling__c,
                       Payments_GL_Account__c, Accounts_Receivable_GL_Account__c, Revenue_GL_Account__c
                FROM Product__c 
                WHERE Id IN :scope]);
    }
}