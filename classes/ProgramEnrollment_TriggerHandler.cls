/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description A Trigger Handler of custom object Program_Application__c.  
* This class is responsible for handling Program Application Youreka form creation automation.
*/
public class ProgramEnrollment_TriggerHandler {
    /*******************************************************************************************************
    * @description Attribute enrFormConfigs, type of List<Form_Creator_Configuration__mdt>,
    * are retrieved from Custom Metadata Type named Form_Creator_Configuration__mdt
    * with search criteria 
    *   Object_Trigger__c = 'Program Enrollment'
    *   Template_Name__c != null
    *   Program_Name_Like__c != null
    */
    public static List<Form_Creator_Configuration__mdt> enrFormConfigs = [
        SELECT Program_Name_Like__c, Object_Trigger__c, Template_Name__c
        FROM Form_Creator_Configuration__mdt
    	WHERE Object_Trigger__c = 'Program Enrollment'
    	  AND Template_Name__c != null AND Program_Name_Like__c != null
    ];
    /*******************************************************************************************************
    * @description Handle the afterInsert trigger event. 
    * Delegate the Program Enrollments configuration to formCreationAutomation method.
    */
    public static void afterInsert() {
        List<Program_Enrollment__c> pEnrs = (List<Program_Enrollment__c>)Trigger.new;
        
        // list all Programs for Enrollments being inserted
        Map<Id, CareProgram> programsMap = new Map<Id, CareProgram>();
        for(Program_Enrollment__c e : pEnrs) {
            programsMap.put(e.Program__c, null);
        }
        programsMap = new  Map<Id, CareProgram>([
            SELECT Id, Name 
            FROM CareProgram
            WHERE Id IN :programsMap.keySet()
        ]);
        
        // if configurations exist for Program Enrollments, handle
        if(!enrFormConfigs.isEmpty()) formCreationAutomation(enrFormConfigs, programsMap);
        
    }
    
    /*******************************************************************************************************
    * @description Create Program Enrollments Youreka form based on configuration  
    * @param formConfigs, type of List<Form_Creator_Configuration__mdt>, contain Youreka form configuration of Program Enrollments
    * @param programsMap, type of Map<Id, CareProgram>, contain Program Enrollments data
    */
    public static void formCreationAutomation(List<Form_Creator_Configuration__mdt> formConfigs, Map<Id, CareProgram> programsMap) {
        List<Program_Enrollment__c> pEnrs = (List<Program_Enrollment__c>)Trigger.new;
        
		Set<String> templateNames = new Set<String>();
        Map<String, List<Program_Enrollment__c>> templatesToEnrollments = new Map<String, List<Program_Enrollment__c>>();
        for(Form_Creator_Configuration__mdt c : formConfigs) { 
            for(Program_Enrollment__c e : pEnrs) {
                System.debug(e);
                // does application match any configured Programs?
                if(programsMap.get(e.Program__c).Name.containsIgnoreCase(c.Program_Name_Like__c)) {
                    templateNames.add('%' + c.Template_Name__c + '%');
                    List<Program_Enrollment__c> Enrollments = templatesToEnrollments.get(c.Program_Name_Like__c);
                    if(Enrollments == null) Enrollments = new List<Program_Enrollment__c>{e};
                    templatesToEnrollments.put(c.Program_Name_Like__c, Enrollments);
                }
            }
        }
        
        // find any Youreka From Templates for configurations at play
        List<disco__Form_Template__c> appTemplates;
        if(!templateNames.isEmpty()) {
            appTemplates = [
                SELECT Id, Name 
                FROM disco__Form_Template__c
                WHERE Name LIKE :templateNames
				  AND disco__Status__c = 'In Use'
                ORDER BY LastModifiedDate];
        }
        
        // run through configurations again to compare to Enrollments, again, and create Forms
        List<disco__Form__c> newForms = new List<disco__Form__c>();
        for(Form_Creator_Configuration__mdt c : formConfigs) { 
            // check each configuration against each app
            for(Program_Enrollment__c e : pEnrs) {
                // does application match the configurations?
                if(programsMap.get(e.Program__c).Name.containsIgnoreCase(c.Program_Name_Like__c)) {
                    // check each configuration against each template
                    for(disco__Form_Template__c t : appTemplates) {
                        // does the template match the configuration?
                        if(t.Name.containsIgnoreCase(c.Template_Name__c)) {
                            // create a new form, automatically linked to Program Application, Account, and Form Template
                            disco__Form__c f = new disco__Form__c(
                            	Program_Enrollment__c = e.Id,
                                disco__Form_Template__c = t.Id,
                                disco__Form_Account__c = e.Account__c
                            );
                            newForms.add(f);
                            System.debug('Form Configuration ' + c);
                            System.debug('Enrollment ' + e);
                            System.debug('Added form for auto Intake Assessment creation! ' + f);
                        }
                    }
                }
            }
        }
        insert newForms;
    }

}