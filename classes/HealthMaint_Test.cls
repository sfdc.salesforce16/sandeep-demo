@isTest 
public class HealthMaint_Test {

    @IsTest
    static void testRun() {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account acc1 = new Account(FirstName = 'Fred', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', RecordTypeId = rt.Id, HealthCloudGA__MedicalRecordNumber__pc = '123');
        insert acc1;

        Test.startTest();
        HealthMaint hm = new HealthMaint();
        hm.ID = acc1.Id;
        hm.Name = 'Test Maint';
        hm.DueDate = '2020-12-05';
        hm.Status = 'New';
        hm.LastSatisfactionDate = '2020-12-05';
		Test.stopTest();
        
    }
}