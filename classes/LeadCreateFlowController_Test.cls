@IsTest
public class LeadCreateFlowController_Test {

    @IsTest
    public static void testCreateLead() {
        Map<String, String> inputParams = new Map<String, String>();
        String email = 'test@test.com';
        LeadCreateFlowController flowController = new LeadCreateFlowController();

        inputParams.put('Email', email);
        inputParams.put('FirstName', 'testFirstName');
        inputParams.put('LastName', 'testLastName');
        inputParams.put('Phone', '1234567890');
        inputParams.put('Question', 'testQuesion');
        inputParams.put('HowDidYouHearAboutUs', 'Other');

        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        flowController.describe();          
        LeadCreateFlowController.invoke(request); // create a lead with above values

        // asserts to check the lead creation
        List<Lead> leadList = [SELECT Id FROM Lead WHERE email =: email];
        System.assertEquals(1, leadList.size());
    }
}