public without sharing class ProviderAddressSearchController {

    @AuraEnabled
    public static List<ProviderAddress> findProviderAddresses(String practitionerId) {
        List<ProviderAddress> addresses = new List<ProviderAddress>();

        HttpRequest req = new HttpRequest();
        EpicWebService ews = new EpicWebService();
        req = ews.setHeaderForMuleAPI(req);

        String url = 'callout:epic_mule_app/provider/address';
        String endpoint = String.format('{0}?dukePractitionerId={1}', new String[] {url, practitionerId});
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeout(20000);
        System.debug('req: ' + req);

        Http http = new Http();
       // if(!Test.isRunningTest()){ //commented by Karthik
            HTTPResponse res = http.send(req);
            String body = res.getBody();
            //String body = '[{"UniqueID":"57645-7","StreetLine1":"4709 CREEKSTONE DRIVE STE 300","StreetLine2":"DUKE MEDICAL PLAZA PAGE ROAD","StreetLine3":null,"City":"DURHAM","Zip":"27703","Phone":"919-660-5066","ExternalPlaceName":null,"ExternalAddressID":"10236980","IsActive":true,"IsPrimary":true,"IsSharedAddress":true,"IsInternalAddress":true,"State":{"ID":34,"Title":"North Carolina","Abbr":"NC"},"County":{"ID":541,"Title":"DURHAM","Abbr":"DURHAM"},"Country":{"ID":1,"Title":"United States of America","Abbr":"USA"}},{"UniqueID":"57645-6","StreetLine1":"3116 N. Duke Street","StreetLine2":null,"StreetLine3":null,"City":"Durham","Zip":"27704-2102","Phone":"919-660-2200","ExternalPlaceName":null,"ExternalAddressID":"10236978","IsActive":true,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":true,"State":{"ID":34,"Title":"North Carolina","Abbr":"NC"},"County":{"ID":541,"Title":"DURHAM","Abbr":"DURHAM"},"Country":{"ID":1,"Title":"United States of America","Abbr":"USA"}},{"UniqueID":"57645-1","StreetLine1":"DUKE UNIVERSITY MEDICAL CENTER","StreetLine2":null,"StreetLine3":null,"City":"DURHAM","Zip":"27710","Phone":"919-660-2217","ExternalPlaceName":null,"ExternalAddressID":null,"IsActive":false,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":false,"State":{"ID":34,"Title":"North Carolina","Abbr":"NC"},"County":{"ID":541,"Title":"DURHAM","Abbr":"DURHAM"},"Country":{"ID":1,"Title":"United States of America","Abbr":"USA"}},{"UniqueID":"57645-2","StreetLine1":null,"StreetLine2":null,"StreetLine3":null,"City":null,"Zip":null,"Phone":"919-660-2217","ExternalPlaceName":null,"ExternalAddressID":null,"IsActive":false,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":false,"State":null,"County":null,"Country":null},{"UniqueID":"57645-3","StreetLine1":null,"StreetLine2":null,"StreetLine3":null,"City":null,"Zip":null,"Phone":"919-660-2217","ExternalPlaceName":null,"ExternalAddressID":null,"IsActive":false,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":false,"State":null,"County":null,"Country":null},{"UniqueID":"57645-4","StreetLine1":null,"StreetLine2":null,"StreetLine3":null,"City":null,"Zip":null,"Phone":"919-660-2217","ExternalPlaceName":null,"ExternalAddressID":null,"IsActive":false,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":false,"State":null,"County":null,"Country":null},{"UniqueID":"57645-5","StreetLine1":null,"StreetLine2":null,"StreetLine3":null,"City":null,"Zip":null,"Phone":"919-660-2217","ExternalPlaceName":null,"ExternalAddressID":null,"IsActive":false,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":false,"State":null,"County":null,"Country":null},{"UniqueID":"57645-8","StreetLine1":"DUKE HEALTH CTR AT N DUKE ST","StreetLine2":null,"StreetLine3":null,"City":"DURHAM","Zip":"27704","Phone":"919-660-2200","ExternalPlaceName":null,"ExternalAddressID":null,"IsActive":false,"IsPrimary":false,"IsSharedAddress":false,"IsInternalAddress":false,"State":{"ID":34,"Title":"North Carolina","Abbr":"NC"},"County":{"ID":541,"Title":"DURHAM","Abbr":"DURHAM"},"Country":{"ID":1,"Title":"United States of America","Abbr":"USA"}}]';
            System.debug('body: ' + body);
            if(body.contains('UniqueID')){
                addresses = (List<ProviderAddress>) JSON.deserialize(body, List<ProviderAddress>.class);
                System.debug('searchResults: ' + addresses);
    
            }
       // } // commented by Karthik
        return addresses;
    }

    @AuraEnabled
    public static void setProviderAddress(String accountId, String locationId, String address, String hpContactId) {
        System.debug('accountId: ' + accountId);
        System.debug('locationId: ' + locationId);
        System.debug('address: ' + address);
        Account a = [SELECT Id, PCP_Address_Id__c, PCP_Address__c FROM Account WHERE Id = :accountId LIMIT 1];
        HealthcareProvider hp = [SELECT Id FROM HealthcareProvider WHERE Practitioner.Id = :hpContactId LIMIT 1];
        a.PCP_Address_Id__c = locationId;
        a.PCP_Address__c = address;
        a.Practitioner_New__c = hp.Id;
        System.debug('a: ' + a);
        update a;
    }
}