/**
 * Created by dcano on 5/24/21.
 */

@IsTest
private class ClearOldExceptionsTest
{
    @IsTest
    static void testBehavior()
    {
        Test.startTest();
        ClearOldExceptions myClass = new ClearOldExceptions ();
        String chron = '0 0 23 * * ?';
        system.schedule('Test Sched', chron, myClass);
        Test.stopTest();
    }
}