/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description This class handles retrieval of FYI Flags, Dismissals, HealthMaintInfos,
*              ReferralInfos, Recalls, Orders, and Waitlists
*/

public inherited sharing class PatientReferrals {

    /*******************************************************************************************************
    * @description An @AuraEnabled method responsible for retrieving FYI Flags from SFDC with respect to a patient  
    * @param recordId the current record Id
    * @return a list of FYI_Flag__c objects
    */
    @AuraEnabled(Cacheable = true)
    public static List<FYI_Flag__c> getFyiFlags( string recordId ){
        System.debug('getFyiFlags.recordId: ' + recordId);
        List<FYI_Flag__c> returnVal = new List<FYI_Flag__c>();

        // Select Active FYI Flags for Account
        returnVal = [
                SELECT 
                    Id,
                    FYI_Flag_Summary__c,
                    FYI_Flag_Text__c,
                    FYI_Flag__c,
                    FYI_Flag_Status__c,
                    FYI_Flag_Date__c,
                    FYI_Flag_Time__c,
                    EHR_Encounter__c,
                    EHR_Encounter__r.Name
                FROM 
                    FYI_Flag__c 
                WHERE 
                    Patient_Account__c = :recordId
                    AND FYI_Flag_Status__c = 'Active'
               ];
            System.debug('fyiFlags ' + returnVal);
        return returnVal;
    }

    /*******************************************************************************************************
    * @description An @AuraEnabled method responsible for retrieving Dismissals from SFDC with respect to a patient  
    * @param recordId the current record Id
    * @return a list of Dismissal__c objects
    */
    @AuraEnabled(Cacheable = true)
    public static List<Dismissal__c> getDismissals( string recordId ){
        System.debug('getDismissals.recordId: ' + recordId);
        List<Dismissal__c> returnVal = new List<Dismissal__c>();

        // Select Dismissals with a date falling between effective and end date
        returnVal = [
                SELECT 
                    Centers_Dismissed__c,
                    Dept_Dismissed_Name__c,
                    Dismissal_Effective_Date__c,
                    Dismissal_End_Date__c,
                    Dismissal_Level__c,
                    Providers_Dismissed__c,
                    Revenue_Locations_Dismissed__c,
                    Service_Areas_Dismissed__c,
                    Specialties_Dismissed__c,
                    Dismissal_Notes__c,
                    Id
                FROM 
                    Dismissal__c 
                WHERE 
                    Account__c = :recordId
                    AND ((Dismissal_Effective_Date__c <= TODAY
                        AND Dismissal_End_Date__c >= TODAY)
                    OR (Dismissal_Effective_Date__c <= TODAY
                        AND Dismissal_End_Date__c = NULL))
               ];
           
            System.debug('dismissals ' + returnVal);
        return returnVal;
    }

    /*******************************************************************************************************
    * @description An @AuraEnabled method responsible for retrieving Health Maintenence Infos from SFDC with respect to a patient  
    * @param recordId the current record Id
    * @return a list of Health_Maintenance__c objects
    */
    @AuraEnabled(Cacheable = true)
    public static List<Health_Maintenance__c> getHealthMaintInfos( string recordId ){
        System.debug('getHealthMaintInfos.recordId: ' + recordId);
        List<Health_Maintenance__c> returnVal = new List<Health_Maintenance__c>();

        returnVal = [
                SELECT 
                    Name,
                    Due_Date__c,
                    Last_Satisfied__c,
                    Status__c,
                    Id
                FROM 
                    Health_Maintenance__c 
                    WHERE 
                    Patient__c = :recordId AND
                    Name IN ('Glaucoma Screening',
                        'Hepatitis B Vaccines',
                        'Mammogram',
                        'Pap Smear',
                        'Eye Exam',
                        'DXA Bone Density Scan',
                        'Colonoscopy',
                        'HPV Vaccines',
                        'Tdap/Td Vaccines',
                        'Adult Tetanus (Td And Tdap)',
                        'Shingrix',
                        'Medicare Initial or AWV',
                        'Annual Visit/Physical/Well Child Check',
                        'Pneumococcal Vaccine: 65+',
                        'COVID-19 Vaccine',
                        'Influenza Vaccine')
                    AND Status__c IN ('Overdue', 'Due', 'Due On')
               ];
            //    AND 
            //    Skill_group__c IN :skgs];
            System.debug('healthMaintInfos ' + returnVal);
            
        return returnVal;
    }

    /*******************************************************************************************************
    * @description An @AuraEnabled method responsible for retrieving ReferralInfos from SFDC with respect to a patient  
    * @param recordId the current record Id
    * @return a list of Referral_Information__c objects
    */
    @AuraEnabled(Cacheable = true)
    public static List<Referral_Information__c> getReferralInfos( string recordId ){
        System.debug('getReferralInfos.recordId: ' + recordId);
        List<Referral_Information__c> returnVal = new List<Referral_Information__c>();

        // List<String> skgs = New List<String>();
        // List<Agent_Skill_Group__c> asgs = [SELECT Skill_Group__r.Id FROM Agent_Skill_Group__c WHERE Agent__c = :UserInfo.getUserId()];
        // for (Agent_Skill_Group__c asg : asgs) {
        //     skgs.add(asg.Skill_Group__r.Id); 
        // }
        // System.debug('skgs: ' + skgs);

        returnVal = [
                SELECT 
                    Referral_ID__c,
                    Referral_Type__c,
                    Name,
                    Referred_to_Provider__c,
                    Referred_to_Department__c,
                    toLabel(Referral_Status__c),
                    Referred_By__c,
                    Referred_To__c,
                    Referred_By_ID__c,
                    Referred_To_ID__c,
                    Start__c,
                    toLabel(Scheduling_Status__c),
                    Effective_Date__c,
                    Id
                FROM 
                    Referral_Information__c 
                WHERE 
                    Patient_Name__c = :recordId
                    AND
                    Referral_Status__c<>'Closed'
               ];
            //    AND 
            //    Skill_group__c IN :skgs];
            System.debug('referralInfos ' + returnVal);
        return returnVal;
    }

    /*******************************************************************************************************
    * @description An @AuraEnabled method responsible for retrieving Recalls from SFDC with respect to a patient  
    * @param recordId the current record Id
    * @return a list of Recalls__c objects
    */
    @AuraEnabled(Cacheable = true)
    public static List<Recalls__c> getRecalls( string recordId ){
        List<Recalls__c> returnVal = new List<Recalls__c>();

        // Commented out by Shelby Dremely from Synaptic Advisors on 7/30
        // Removing DML call since it is not used in class
        // List<String> skgs = New List<String>();
        // List<Agent_Skill_Group__c> asgs = [SELECT Skill_Group__r.Id FROM Agent_Skill_Group__c WHERE Agent__c = :UserInfo.getUserId()];
        // for (Agent_Skill_Group__c asg : asgs) {
        //     skgs.add(asg.Skill_Group__r.Id); 
        // }
        // System.debug('skgs: ' + skgs);

        // Select all recalls for Account that are not Scheduled/Linked
        List<Recalls__c> recallInfos = [
                SELECT 
                    Recall_Record_ID__c,
                    Providers__c,
                    Specialty_or_Department__c,
                    Recall_Status__c,
                    Visit_Type__c,
                    Recall_Date__c,
                    Id
                FROM 
                    Recalls__c 
                WHERE 
                    Patient_Name__c = :recordId
                    AND Recall_Status__c <> 'Scheduled/Linked'
        ];
        System.debug('recallInfos ' + recallInfos);
        return recallInfos;
    }

    /*******************************************************************************************************
    * @description An @AuraEnabled method responsible for retrieving Waitlists from SFDC with respect to a patient  
    * @param recordId the current record Id
    * @return a list of Wait_List__c objects
    */
    @AuraEnabled()//can't mix DML with cacheable
    public static List<Wait_List__c> getWaitlists( string recordId ){
        List<Wait_List__c> returnVal = new List<Wait_List__c>();

        Account patient = [SELECT Id, HealthCloudGA__MedicalRecordNumber__c FROM Account WHERE Id = :recordId LIMIT 1];
        if(patient.HealthCloudGA__MedicalRecordNumber__c == null){ 
            System.debug('no mrn - return empty list.');
            return returnVal;
        }

        // Select all Waitlist records for Account where Status is Pending
        List<Wait_List__c> waitlist = [
            SELECT 
                Until_Date__c,
                Visit_Type__c,
                Status__c,
                Start_Date__c,
                Department__c,
                Provider__c,
                Account__c
            FROM 
                Wait_List__c 
            WHERE 
                Account__c = :recordId
                AND Status__c = 'Pending'
            ];
        return waitlist;
    }

    /*******************************************************************************************************
    * @description An @AuraEnabled method responsible for retrieving Orders from SFDC with respect to a patient  
    * @param recordId the current record Id
    * @return a list of Provider_Orders__c objects
    */
    @AuraEnabled(Cacheable = true)
    public static List<Provider_Orders__c> getOrders( string recordId ){
        List<Provider_Orders__c> returnVal = new List<Provider_Orders__c>();

        List<String> skgs = New List<String>();
        List<Agent_Skill_Group__c> asgs = [SELECT Skill_Group__r.Id FROM Agent_Skill_Group__c WHERE Agent__c = :UserInfo.getUserId()];
        for (Agent_Skill_Group__c asg : asgs) {
            skgs.add(asg.Skill_Group__r.Id); 
        }
        System.debug('skgs: ' + skgs);

        List<Provider_Orders__c> OrderInfos = [
                SELECT 
                    Proc_Visit_Type__c,
                    Requesting_Provider__c,
                    Category__c,
                    Id
                FROM 
                    Provider_Orders__c 
                WHERE 
                    Patient_Name__c = :recordId
            //   ];
            //    AND 
            //    Skill_group__c IN :skgs];
        ];
        System.debug('OrderInfos ' + OrderInfos);
        return OrderInfos;
    }
}