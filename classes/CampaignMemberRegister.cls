global without sharing class CampaignMemberRegister implements Process.Plugin {

    webservice static Process.PluginResult invoke(Process.PluginRequest request) {
        Map<String, String> result = new Map<String, String>();
        String campaignId = (String) request.inputParameters.get('CampaignId');
        String accountId = (String) request.inputParameters.get('AccountId');
        String leadId = (String)request.inputParameters.get('LeadId');
        String contactId = (String)request.inputParameters.get('ContactId');
        
        CampaignMember cm = new CampaignMember();
        cm.Account__c = accountId;
        cm.LeadId = leadId;
        cm.CampaignId = campaignId;
        cm.ContactId = contactId;
		
        insert cm;
        return new Process.PluginResult(result); 
    }
    
    global Process.PluginDescribeResult describe() { 
        Process.PluginDescribeResult result = new Process.PluginDescribeResult(); 
        result.Name = 'Register for Campaign';
        result.Tag = 'registerForCampaign';
        result.inputParameters = new 
            List<Process.PluginDescribeResult.InputParameter>{ 
                new Process.PluginDescribeResult.InputParameter('CampaignId', Process.PluginDescribeResult.ParameterType.STRING, true),
                new Process.PluginDescribeResult.InputParameter('AccountId', Process.PluginDescribeResult.ParameterType.STRING, FALSE),
                new Process.PluginDescribeResult.InputParameter('LeadId', Process.PluginDescribeResult.ParameterType.STRING, FALSE),
                new Process.PluginDescribeResult.InputParameter('ContactId', Process.PluginDescribeResult.ParameterType.STRING, FALSE)
            }; 
        result.outputParameters = new List<Process.PluginDescribeResult.OutputParameter>{};
        return result;
    }

}