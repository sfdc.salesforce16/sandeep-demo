public with sharing class EpicLookupByMrnController {

    //@AuraEnabled
    //public Id ehrPatientId { get; set; }
    public Id accountId { get; set; }

    public EpicLookupByMrnController(ApexPages.StandardController stdController) {
        this.accountId = stdController.getId();
        System.debug('accountId: ' + this.accountId);
	}

    //@AuraEnabled
    public PageReference epicNavigateToMrn(){
        List<EpicLookupByMrn__e> epicLookupEvents = new List<EpicLookupByMrn__e>();

        System.debug('epicNavigateToMrn.accountId: ' + this.accountId);
        //HealthCloudGA__EhrPatient__c patient = [SELECT HealthCloudGA__Account__r.HealthCloudGA__MedicalRecordNumber__c FROM HealthCloudGA__EhrPatient__c WHERE Id = :this.ehrPatientId LIMIT 1];
        //HealthCloudGA__EhrPatient__c patient = [SELECT HealthCloudGA__MedicalRecordNumber__c, HealthCloudGA__BirthDate__c FROM HealthCloudGA__EhrPatient__c WHERE Id = :this.ehrPatientId LIMIT 1];
        Account patient = [SELECT HealthCloudGA__MedicalRecordNumber__c, Date_of_Birth__c FROM Account WHERE Id = :this.accountId LIMIT 1];
        User user = [SELECT FederationIdentifier FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.debug('patient: ' + patient);

        EpicLookupByMrn__e epicLookupEvent = new EpicLookupByMrn__e(
            MedicalRecordNumber__c = patient.HealthCloudGA__MedicalRecordNumber__c,
            Patient_DOB__c = patient.Date_of_Birth__c,
            FederationId__c = user.FederationIdentifier
        );
        epicLookupEvents.add(epicLookupEvent);

        try {
            System.debug('publishing: ' + epicLookupEvents);
            if(!epicLookupEvents.isEmpty()){
                List<Database.SaveResult> results = EventBus.publish(epicLookupEvents);
                // Inspect publishing results
                for (Database.SaveResult result : results) {
                    if (!result.isSuccess()) {
                        for (Database.Error error : result.getErrors()) {
                            System.debug('Error returned: ' + error.getStatusCode() +' - '+ error.getMessage());
                        }
                    }
                    else{
                        System.debug('published: ' + epicLookupEvents);
                    }
                }            
            }
    

        } catch (Exception ex) {
            System.debug('exception: ' + ex);
        }

        PageReference page = new ApexPages.StandardController(patient).view();
        page.setRedirect(false);
        return page;
    }
}