@isTest
public class CommunitiesPrograms_Controller_Test {
    
    private static Id parentProgramId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Parent_Program' AND SObjectType = 'CareProgram'].Id;
    
    @testSetup
    static void setup() {
        // Service Line
        Service_Line__c s = new Service_Line__c(Name = 'Diet & Fitness');
        insert s;
        
        // Parent Program
        CareProgram p = new CareProgram(
        	Name = 'Residential Weight Loss',
            Service_Line__c = s.Id,
            RecordTypeId = parentProgramId,
        	Price__c = 100,
        	Deposit__c = 5,
            Duration__c = 7,
            Is_Community_Visible__c = true,
            Description__c = 'Test'
        ); insert p;
        
        // Program
        CareProgram p2 = new CareProgram(
        	Name = 'Residential Weight Loss - 1 week',
            Service_Line__c = s.Id,
            ParentProgramId = p.Id,
        	Price__c = 100,
        	Deposit__c = 5,
            Duration__c = 7,
            Is_Community_Visible__c = true,
            Description__c = 'Test'
        ); insert p2;
        
    }
    
    @isTest
    static void query_noParent() {
        List<CareProgram> result = CommunitiesPrograms_Controller.getCommunityVisiblePrograms(null);
        System.assert(result.size() == 2); // finds all
    }
    
    @isTest
    static void query_fromParent() {
        CareProgram p = [SELECT Id FROM CareProgram WHERE Name = 'Residential Weight Loss'];
        List<CareProgram> result = CommunitiesPrograms_Controller.getCommunityVisiblePrograms(p.Id);
        System.assert(result.size() == 2); // finds itself and it's child
    }
    
    @isTest
    static void query_fromChild() {
        CareProgram p = [SELECT Id FROM CareProgram WHERE Name = 'Residential Weight Loss - 1 week'];
        List<CareProgram> result = CommunitiesPrograms_Controller.getCommunityVisiblePrograms(p.Id);
        System.assert(result.size() == 1); // only finds itself
    }

}