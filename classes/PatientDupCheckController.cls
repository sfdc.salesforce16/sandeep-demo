/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description A controller for a Lightning component. Performs duplication check by calling Epic RESTFul API dupeCheck
*/
public without sharing class PatientDupCheckController {
    /*******************************************************************************************************
    * @description Check the patient duplication based on DOB, FirstName, LastName, Gender, and SSN
    *   This method calls out to 'callout:epic_mule_app/dupeCheck' to check the duplication
    * @param dob the date of birth
    * @param first the first name
    * @param last the last name 
    * @param gender the gender
    * @param ssn the SSN
    * @return a list of patients
    */
    @AuraEnabled(cacheable=true)
    public static List<Account> dupeCheck(String dob, String first, String last, String gender, String ssn) {
        List<Account> returnVal = new List<Account>();
        Set<String>setMRN = new Set<String>();
        System.debug('dupeCheck: ' + dob);
        String dobfmt;
        if(!String.isBlank(dob)){
            dobfmt = ((Datetime)Date.valueOf(dob)).formatGMT('yyyy-MM-dd');
        }
        
        HttpRequest req = new HttpRequest();
        EpicWebService ews = new EpicWebService();
        req = ews.setHeaderForMuleAPI(req);
        String url = 'callout:epic_mule_app/dupeCheck';

        // dobfmt = '1965-03-21';
        // first = 'Tay';
        // last = 'Munden';
        // gender = 'Male';

        String endpoint = String.format('{0}?dob={1}&lastName={2}&firstName={3}&gender={4}', new String[] {url, dobfmt, last, first, gender});
        System.debug('endpoint: ' + endpoint);
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        System.debug('req: ' + req);

        Http http = new Http();
        List<PatientSearch> searchResults;
        
        HTTPResponse res = http.send(req);
        System.debug('res.getBody(): ' + res.getBody());

        searchResults = (List<PatientSearch>) JSON.deserialize(res.getBody(), List<PatientSearch>.class);
        System.debug('searchResults: ' + searchResults);
        Map <String , String> mapreturnVal = new Map<String , String> ();
        for (PatientSearch p : searchResults) {
            setMRN.add(p.mrn);
        }
        if (!setMRN.isEmpty()){
            List<Account> lstAccount = [Select Id, HealthCloudGA__MedicalRecordNumber__c FROM ACCOUNT WHERE HealthCloudGA__MedicalRecordNumber__c IN : setMRN];

            for(Account oacc : lstAccount){
                if(!mapreturnVal.containsKey(oacc.Id)){
                    mapreturnVal.put(oacc.HealthCloudGA__MedicalRecordNumber__c, oacc.Id);
                }
            }
        }
        
       

        for (PatientSearch p : searchResults) {
            //Date_of_Birth__c = p.b
            System.debug('Sandy ++' + mapreturnVal.get(p.mrn));
            Account a = new Account(
                Id = mapreturnVal.get(p.mrn),
                Salutation = p.title,
                Name = p.name,
                Suffix = p.suffix,
                PersonHomePhone = p.phone,
                PersonBirthdate = Date.valueOf(p.birthDate), 
                Date_of_Birth__c = Date.valueOf(p.birthDate), 
                HealthCloudGA__Gender__pc = p.gender,
                HealthCloudGA__MedicalRecordNumber__c = p.mrn,
                PersonMailingStreet = p.address,
                Rank__c = Integer.valueOf(p.rank)
                
            );
            
            returnVal.add(a);
        }
        
        System.debug('returnVal: ' + returnVal);
        return returnVal;
    }

}