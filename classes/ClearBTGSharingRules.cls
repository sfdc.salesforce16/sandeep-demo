/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class creates a batch job to delete sharing rules for Account records shared through a
 * Break_the_Glass__c request that have expired.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Synaptic
 * @project     DHAS
 * @since       April 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */
    
global class ClearBTGSharingRules implements Schedulable {
    global void execute(SchedulableContext ctx) {
        List<Break_the_Glass_Request__c> btgs = [SELECT Id, Permission_Expiration_Date__c, Account_Share_ID__c
            FROM Break_the_Glass_Request__c
            WHERE Permission_Expiration_Date__c < TODAY];
        for(Break_the_Glass_Request__c btg: btgs){
            string shareId = btg.Account_Share_ID__c;
            if(shareId != null){
                system.debug('shareId ' + shareId);
                AccountShare shareRule = [SELECT Id, IsDeleted FROM AccountShare WHERE Id = :shareId AND RowCause = 'Manual' LIMIT 1];
                system.debug('deleting ' + shareRule.Id);
                delete shareRule;
             }
        }
    }
}