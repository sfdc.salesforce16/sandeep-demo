/******************************************
 * 
 * @author Synaptic AP
 * @since  Feb 2021
 * Project DHAS
 */
public inherited sharing class CoveragesPojo {
    public CoveragesPojo() {}

    @AuraEnabled
    public string epicId {get; set;}    

    @AuraEnabled
    public string GroupNumber {get; set;} 
    
    @AuraEnabled
    public string GroupName {get; set;}    

    @AuraEnabled
    public string MedicareCoverageType {get; set;}    

    @AuraEnabled
    public string EffectiveFromDate {get; set;}    

    @AuraEnabled
    public string EffectiveToDate {get; set;}    

    @AuraEnabled
    public string PayorName {get; set;}    

    @AuraEnabled
    public string PlanName {get; set;}    

    @AuraEnabled
    public string Subscriber {get; set;}    

    @AuraEnabled
    public string Status {get; set;}    

    @AuraEnabled
    public string PayorID {get; set;}    

    @AuraEnabled
    public string PlanID {get; set;}    
}