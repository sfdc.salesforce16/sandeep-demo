/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class implements the TriggerHandler Framework for the Case object.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Synaptic
 * @project     DHAS
 * @since       April 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public without sharing class Case_TriggerHandler extends TriggerHandler {
    public Case_TriggerHandler() {

    }

    public override void beforeInsert(){
        System.debug('beforeInsert');
    }


    public override void afterInsert(){
        System.debug('afterInsert');
        insertCheckForAccountUpdates();
    }


    public override void beforeUpdate(){
        System.debug('beforeUpdate');
        updateCheckForAccountUpdates();
    }

    /*
    * Runs through multiple checks and updates Account appropriately
    */
    private static void insertCheckForAccountUpdates(){
        for(Case c : (List<Case>)Trigger.new){
            if (c.AccountId != null){
                Account a = getAccountByCase(c);
                if(a.Cases.size()>0){
                        if(a.Cases[0].Id == c.Id){
                        a = checkVASpecialty(c, a);
                        a = checkSpecialty(c,a);
                        a = checkInCounty(c,a);
                        try {
                            update a;
                        } catch (Exception ex) {
                            System.debug('insertCheckForAccountUpdates.exception: ' + ex);
                        }
                    }
                }
            }
        }
    }

    /*
    * Runs through multiple checks and updates Account appropriately
    */
    private static void updateCheckForAccountUpdates(){
        for(Case c : (List<Case>)Trigger.newMap.values()){
            Case oldC = (Case)Trigger.oldMap.get(c.ID);
            Case newC = (Case)Trigger.newMap.get(c.ID);
            system.debug('in updateCheckVASpecialty');

            if (c.AccountId != null){
                Account a = getAccountByCase(c);
                if(a.Cases.size()>0){
                    if(a.Cases[0].Id == c.Id){
                        a = checkVASpecialty(newC, a);
                        a = checkSpecialty(c,a);
                        a = checkInCounty(c,a);

                        try {
                            update a;
                        } catch (Exception ex) {
                            System.debug('updateCheckForAccountUpdates.exception: ' + ex);
                        }
                    }
                }
            }
        }
    }

    /*
    * Gets the Account
    */
    private static Account getAccountByCase(Case c){
        Account a = [SELECT Id, 
            Is_In_County__c,
            County__C,
            Is_Specialty_Match__c, 
            Has_Project_Access_Referral__c,
            Is_VA_Specialty_Match__c, 
            (SELECT Id, 
                CreatedDate,
                Status
                FROM Cases 
                WHERE Status IN ('New', 'Open', 'Escalated')
                ORDER BY CreatedDate 
                DESC LIMIT 1)
        FROM Account 
        WHERE Id = :c.AccountId LIMIT 1];
        return a;
    }

    /*
    *   Check if Duke_Department_County__c and Account.County__c !=null and if they match
    *   If so, then Account.Is_In_County__c = true; else Account.Is_In_County__c = false 
    */

    private static Account checkInCounty(Case c, Account a){
        if((c.Duke_Department_County__c != null) && (a.County__c !=null)){
            system.debug('a.County__c ' + a.County__c);
            if(c.Duke_Department_County__c == a.County__c){
                a.Is_In_County__c = true;
            } else {
                a.Is_In_County__c = false;
            }
        } else {
            a.Is_In_County__c = false;
        }
        return a;      
    }  
 
    /*  
    *    When Case is inserted or updated, check to see if this is the most current case
    *    then check to see if there is a referral information with Referral_Type__c == 'Project Access'
    *    if so, Account.Has_Project_Access_Referral__c = true; else Has_Project_Access_Referral__c = false
    *    then check to see if there is a mmatch between Case.Duke_Specialty_Area__c 
    *       and Referral_Information__c.Referred_to_Dept_Spec__c 
    *       on the most recent active case (Case.Status = New, Open or Escalated).
    *    if so, Account.Is_Specialty_Match__c = true; else Is_Specialty_Match__c = false
    *    Then check if Duke_Department_County__c and Account.County__c !=null and if they match
    *    if so, then Account.Is_In_County__c = true; else Account.Is_In_County__c = false 
    */
    private static Account checkSpecialty(Case c, Account a){
        if(c.Duke_Specialty_Area__c !=null){
            // Get all Referrals associated with Account so can loop through and compare
            List<Referral_Information__c> refList = [SELECT Id, 
                                                    Referred_to_Dept_Spec__c, 
                                                    Referral_Type__c,
                                                    Referral_Status__c,
                                                    Scheduling_Status__c
                                                    FROM Referral_Information__c
                                                    WHERE Referral_Type__c = 'Project Access'
                                                    AND Account__c = :a.id];
            if(refList.size() > 0){
                a.Has_Project_Access_Referral__c = true;
                Boolean hasSpecialtyMatch=false;
                for (Referral_Information__c r : refList){
                    if(r.Referred_to_Dept_Spec__c == c.Duke_Specialty_Area__c
                        && (r.Referral_Status__c == 'IN PROCESS' || r.Referral_Status__c == 'NEW' || r.Referral_Status__c == 'OPEN' 
                            || r.Referral_Status__c == 'AUTH' || r.Referral_Status__c == 'PEND' || r.Referral_Status__c == 'INCOMPLETE')
                        && (r.Scheduling_Status__c != 'Duplicate' && r.Scheduling_Status__c != 'Letter' && r.Scheduling_Status__c != 'Appt Scheduled' 
                            && r.Scheduling_Status__c != 'Patient Declined')){
                        hasSpecialtyMatch = true;
                    }
                }
                a.Is_Specialty_Match__c=hasSpecialtyMatch;

            } else {
                a.Is_Specialty_Match__c = false;
                a.Has_Project_Access_Referral__c = false;
            }
            if((c.Duke_Department_County__c != null) && (a.County__c !=null)){
                system.debug('a.County__c ' + a.County__c);
                if(c.Duke_Department_County__c == a.County__c){
                    a.Is_In_County__c = true;
                } else {
                    a.Is_In_County__c = false;
                }
            } else {
                a.Is_In_County__c = false;
            }
        } 
        return a; 
    }

    /*  
    *    When Case is inserted or updated, check to see if this is the most current case
    *    then check to see if there is a referral information with Referral_Status__c ==  ‘VA’ or ‘Tricare Prime’
    *    then check to see if there is a mmatch between Case.Duke_Specialty_Area__c 
    *       and Referral_Information__c.Referred_to_Dept_Spec__c 
    *       on the most recent active case (Case.Status = New, Open or Escalated).
    *    if so, Account.Is_VA_Specialty_Match__c = true; else Is_VA_Specialty_Match__c = false
    */
    private static Account checkVASpecialty(Case c, Account a){
        if(c.Duke_Specialty_Area__c !=null){
            // Get all Referrals associated with Account so can loop through and compare
            List<Referral_Information__c> refList = [SELECT Id
                                                    , Referred_to_Dept_Spec__c
                                                    , Referral_Status__c 
                                                    FROM Referral_Information__c 
                                                    WHERE Account__c = :a.id
                                                    AND (Referral_Status__c='VA'
                                                    OR Referral_Status__c='Tricare Prime')];    
            if(refList.size() > 0){
                Boolean hasVASpecialtyMatch=false;
                for (Referral_Information__c r : refList){
                    if(r.Referred_to_Dept_Spec__c == c.Duke_Specialty_Area__c
                    && (r.Referral_Status__c == 'IN PROCESS' || r.Referral_Status__c == 'NEW' || r.Referral_Status__c == 'OPEN' 
                        || r.Referral_Status__c == 'AUTH' || r.Referral_Status__c == 'PEND' || r.Referral_Status__c == 'INCOMPLETE')
                    && (r.Scheduling_Status__c != 'Duplicate' && r.Scheduling_Status__c != 'Letter' && r.Scheduling_Status__c != 'Appt Scheduled' 
                        && r.Scheduling_Status__c != 'Patient Declined')){
                        hasVASpecialtyMatch = true;
                    }
                }
                a.Is_VA_Specialty_Match__c = hasVASpecialtyMatch;
            } else {
                a.Is_VA_Specialty_Match__c = false;
            }
        }
        return a;
    }
}