/******************************************
 * 
 * @author Synaptic AP
 * @since  Feb 2021
 * Project DHAS
 */
public with sharing class HealthMaintController {
    public HealthMaintController() {}

    @AuraEnabled
    public static List<HealthMaint> callEpicHealthMaint(String accountId){
        Account patient = [SELECT Id, HealthCloudGA__MedicalRecordNumber__c FROM Account WHERE Id = :accountId LIMIT 1];
        String mrn = patient.HealthCloudGA__MedicalRecordNumber__c;

        //validation
        if(String.isBlank(mrn)){ return null;}

        EpicWebService ews = new EpicWebService();
        HTTPResponse res = ews.call('healthMaintenance', mrn, 'GET');

        List<HealthMaint> epicHMTopics = (List<HealthMaint>) JSON.deserialize(res.getBody(), List<HealthMaint>.class);
        System.debug('epicHMTopics: ' + epicHMTopics);

        return epicHMTopics;
    }
}