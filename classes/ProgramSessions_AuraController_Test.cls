@isTest
public class ProgramSessions_AuraController_Test {
    
    private static Id parentProgramId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Parent_Program' AND SObjectType = 'CareProgram'].Id;
    private static Id communityUserProfileId = [SELECT Id FROM Profile WHERE Name = 'Duke Health Customer Community Plus Login User' LIMIT 1].Id;
    private static Id personAcctRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'PersonAccount'].Id;
    
    @testSetup
    static void setup() {
        // Service Line
        Service_Line__c s = new Service_Line__c(Name = 'Diet & Fitness');
        insert s;
        
        // Parent Program
        CareProgram p = new CareProgram(
            Name = 'Residential Weight Loss',
            Service_Line__c = s.Id,
            RecordTypeId = parentProgramId,
            Price__c = 100,
            Deposit__c = 5,
            Duration__c = 7,
            Is_Community_Visible__c = true,
            Description__c = 'Test',
            Featured_Program__c = 1
        ); insert p;
        
        // Program
        CareProgram p2 = new CareProgram(
            Name = 'Residential Weight Loss - 1 week',
            Service_Line__c = s.Id,
            ParentProgramId = p.Id,
            Price__c = 100,
            Deposit__c = 5,
            Duration__c = 7,
            Is_Community_Visible__c = true,
            Description__c = 'Test',
            Featured_Program__c = 2
        ); insert p2;
        
        Lead l = new Lead(
            FirstName = 'Test',
            LastName = 'Test',
            Email = 'testlead@test.com'
        );
        insert l;
        
        List<Account> accounts = new List<Account>();
        List<User> users = new List<User>();
        
        Account account1 = new Account(
            FirstName = 'Test',
            LastName = 'Test1',
            Phone = '(999) 999-9999',
            BillingStreet = '123 Fake St',
            BillingCity = 'Star City',
            BillingPostalCode = '12345',
            BillingState = 'CA',
            RecordTypeId = personAcctRecordTypeId,
            PersonEmail = 'testuser@test.com',
            Email__c = 'testuser@test.com'
        );
        Account account2 = new Account(
            FirstName = 'Test',
            LastName = 'Test2',
            Phone = '(999) 999-9999',
            BillingStreet = '123 Fake St',
            BillingCity = 'Star City',
            BillingPostalCode = '12345',
            BillingState = 'CA',
            RecordTypeId = personAcctRecordTypeId,
            PersonEmail = 'test@test.com',
            Email__c = 'test@test.com'
        );
        accounts.add(account1);
        accounts.add(account2);
        insert accounts;
        
        Contact contact1 = [SELECT Id FROM Contact WHERE Lastname = 'Test1'];
        
        User user1 = new User();
        user1.Email = 'testuser@test.com';
        user1.ContactId = contact1.Id;
        user1.ProfileId = communityUserProfileId;
        user1.Username = 'testuser@testingthings.com';
        user1.LastName = 'Test';
        user1.Alias = 'testuser';
        user1.languagelocalekey = UserInfo.getLocale();
        user1.localesidkey = UserInfo.getLocale();
        user1.emailEncodingKey = 'UTF-8';
        user1.timeZoneSidKey = UserInfo.getTimezone().getID();
        users.add(user1);
        //insert users;
        
    }
    
    /*@isTest
    static void query_AccountNull() {
        String result;
        try { result = ProgramSessions_AuraController.queryAccount(null); }
        catch(Exception e) {}
        System.assert(result == null); // error--no result
        
    }
    
    @isTest
    static void query_AccountSuccess() {
        User u = [SELECT Id, Contact.AccountId FROM User WHERE Username = 'testuser@testingthings.com' LIMIT 1];
        String result;
        try { 
            System.runAs(u) { result = ProgramSessions_AuraController.queryAccount(null); }
        }
        catch(Exception e) {}
        System.assert(result == u.Contact.AccountId); // returns Account Id of User
        
    }
    
    @isTest
    static void query_ProgramSuccess() {
        User u = [SELECT Id, Contact.AccountId FROM User WHERE Username = 'testuser@testingthings.com' LIMIT 1];
        CareProgram result = ProgramSessions_AuraController.queryProgram(u.Id); 
        System.assert(result == null); // returns nothing because we passed in a User Id
        
    }
    
    @isTest
    static void query_ProgramError() {
        CareProgram p = [SELECT Id FROM CareProgram LIMIT 1];
        CareProgram result = ProgramSessions_AuraController.queryProgram(p.Id); 
        System.assert(result.Id == p.Id); // returns same Program
        
    }
    
    @isTest
    static void query_ProgramSessionsEmpty() {
        CareProgram p = [SELECT Id FROM CareProgram LIMIT 1];
        List<Session__c> result = ProgramSessions_AuraController.querySessions(p.Id); 
        System.assert(result.size() == 0); // returns empty list
        
    }
    
    @isTest
    static void query_ProgramSessions() {
        CareProgram p = [SELECT Id FROM CareProgram LIMIT 1];
        Session__c s = new Session__c(
            Program__c = p.Id,
            Scheduled_Start__c = System.now().addDays(100)
        ); insert s;
        Test.startTest();
        List<Session__c> result = ProgramSessions_AuraController.querySessions(p.Id); 
        Test.stopTest();
        System.assert(result.size() == 1); // returns the 1 Session
        
    }
    
    @isTest
    static void query_ProgramSessionsAll() {
        CareProgram p = [SELECT Id FROM CareProgram LIMIT 1];
        Session__c s = new Session__c(
            Program__c = p.Id,
            Scheduled_Start__c = System.now().addDays(100)
        ); insert s;
        Test.startTest();
        List<Session__c> result = ProgramSessions_AuraController.querySessions(s.Id); // pass in Session Id which will instead query for all Sessions
        Test.stopTest();
        System.assert(result.size() == 1); // returns the 1 Session
        
    }
    
    @isTest
    static void query_ProgramSessionsCatch() {
        CareProgram p = [SELECT Id FROM CareProgram LIMIT 1];
        Session__c s = new Session__c(
            Program__c = p.Id,
            Scheduled_Start__c = System.now().addDays(100)
        ); insert s;
        Test.startTest();
        List<Session__c> result = ProgramSessions_AuraController.querySessions('123'); // pass in invalid Id which will query for all Sessions in catch
        Test.stopTest();
        System.assert(result.size() == 1); // returns the 1 Session
        
    }*/
    
    @isTest
    static void findMatch_None() {
        List<String> emails = new List<String>{'123'};
        List<EmailResults> result = ProgramSessions_AuraController.findMatch(emails); 
        System.assert(result[0].leadId == null && result[0].accountId == null); // returns empty result
        
    }
    
    @isTest
    static void findMatch_Lead() {
        List<String> emails = new List<String>{'testlead@test.com'};
        List<EmailResults> result = ProgramSessions_AuraController.findMatch(emails); 
        System.assert(result[0].leadId != null); // returns the 1 Lead
        
    }
    
    @isTest
    static void findMatch_Account() {
        List<String> emails = new List<String>{'testuser@test.com'};
        List<EmailResults> result = ProgramSessions_AuraController.findMatch(emails); 
        System.assert(result[0].accountId != null); // returns the 1 Account
        
    }

}