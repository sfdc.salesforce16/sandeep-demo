/**
* @author Synaptic
* @date 2014
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description This class implements the TriggerHandler Framework for the Referral_Information__c object and checks for the statuses
*   of International Patient, Project Access Patient, VA Referral, Worker Comp Referral, and CFS Referral.
*   
*   
*/
public with sharing class Referral_Information_c_TriggerHandler extends TriggerHandler {
    /*******************************************************************************************************
    * @description Default Constructor
    */
    public Referral_Information_c_TriggerHandler() {

    }
    /*******************************************************************************************************
    * @description Delegates to insertCheckForAccountUpdates
    */
    public override void beforeInsert(){
        System.debug('beforeInsert');
        insertCheckForAccountUpdates();
    }
    /*******************************************************************************************************
    * @description Delegates to updateCheckForAccountUpdates
    */
    public override void beforeUpdate(){
        System.debug('beforeUpdate');
        updateCheckForAccountUpdates();
    }

    /*******************************************************************************************************
    * @description Handler to process all Account updates in one DML call for inserted Referrals
    */
    private static void insertCheckForAccountUpdates(){
        for(Referral_Information__c ref : (List<Referral_Information__c>)Trigger.new){
            Account a = getAccountCase(ref);
            a = insertInternationalPatient(ref, a);
            a = insertProjectAccessPatient(ref, a);
            a = insertVAReferral(ref, a);
            a = insertWorkersCompReferral(ref, a);
            a = insertCFSReferral(ref, a);

            try {
                update a;
            } catch (Exception ex) {
                System.debug('insertCheckForAccountUpdates.exception: ' + ex);
            }
        }
    }

    /*******************************************************************************************************
    * @description Handler to process all Account updates in one DML call for updated Referrals
    */
    private static void updateCheckForAccountUpdates(){
        for(Referral_Information__c ref : (List<Referral_Information__c>)Trigger.newMap.values()){
            Referral_Information__c oldRef = (Referral_Information__c)Trigger.oldMap.get(ref.ID);
            Referral_Information__c newRef = (Referral_Information__c)Trigger.newMap.get(ref.ID);

            if(newRef != oldRef){
                Account a = getAccountCase(newRef);
                a = updateInternationalPatient(newRef, a);
                a = updateProjectAccessPatient(newRef, a);
                a = updateVAReferral(newRef, a);
                a = updateWorkersCompReferral(newRef, a);
                a = updateCFSReferral(newRef, a);

                try {
                    update a;
                } catch (Exception ex) {
                    System.debug('insertCheckForAccountUpdates.exception: ' + ex);
                }
            }
        }
    }

    /*******************************************************************************************************
    * @description Method to get the account in a single DML call
    * @return Account
    */
    private static Account getAccountCase(Referral_Information__c ref){
        string accId = ref.Patient_Name__c;
        Account a = [SELECT Id, 
                            Has_VA_Referral__c,
                            Has_International_Referral__c,
                            International_Referral_Status__c,
                            Has_Workers_Comp_Referral__c,
                            Workers_Comp_Referral_Status__c,
                            Has_Active_CFS_Referrals__c,
                            CFS_Referral_Status__c,
                            (SELECT Id, 
                                    Duke_Specialty_Area__c, 
                                    AccountId, 
                                    Status 
                            FROM Cases 
                            WHERE Status IN ('New','Open','Escalated') 
                            ORDER BY CreatedDate 
                            DESC LIMIT 1)
                    FROM Account 
                    WHERE Id = :accId];
        return a;
    }


    /*******************************************************************************************************
    * @description On insert Referral_Information__c.Referral_Type__c = “International”, set "Has_International_Referral__c" = TRUE on Account (Patient_Name__c) 
    *   record, and copy value of the "Referral Status" from the Patient's related Referral_Information__c Record to Account.International_Referral_Status__c
    * @param ref Referral_Information__c 
    * @param a Account
    * @return Account
    */
    private static Account insertInternationalPatient(Referral_Information__c ref, Account a){
        if(ref.Referral_Type__c == 'International' && ref.Patient_Name__c != null && ref.Referral_Status__c != null){
                a.Has_International_Referral__c = true;
                a.International_Referral_Status__c = ref.Referral_Status__c;
        }  
        return a;         
    }

    /*******************************************************************************************************
    * @description On insert Referral_Information__c.Referral_Type__c = “International”, set "Has_International_Referral__c" = TRUE on Account (Patient_Name__c) 
    *   record, and copy value of the "Referral Status" from the Patient's related Referral_Information__c Record to Account.International_Referral_Status__c
    * @param ref Referral_Information__c 
    * @param a Account
    * @return Account
    */
    private static Account updateInternationalPatient(Referral_Information__c newRef, Account a){
        if(newRef.Referral_Type__c == 'International' && newRef.Patient_Name__c != null && newRef.Referral_Status__c != null){
            a.Has_International_Referral__c = true;
            a.International_Referral_Status__c = newRef.Referral_Status__c;
        }
        return a;
    }

    /*******************************************************************************************************
    * @description On insert, if Referral_Information__c.Referral_Type__c = Project Access, set "Has_Project_Access_Referral__c" = TRUE on Account (Patient_Name__c) 
    *   record.
    *   If Referral_Information__c.Referred_to_Provider_Specialty__c = Case.Duke_Specialty_Area__c from recent related Case record with Status = 'New'
    * @param ref Referral_Information__c 
    * @param a Account
    * @return Account
    */
    private static Account insertProjectAccessPatient(Referral_Information__c ref, Account a){
        if(ref.Referral_Type__c == 'Project Access' && ref.Patient_Name__c != null){
                a.Has_Project_Access_Referral__c = true;
                if(a.Cases.size() > 0){
                    if(ref.Referred_to_Dept_Spec__c != null && a.Cases[0].Duke_Specialty_Area__c != null){
                        if(ref.Referred_to_Dept_Spec__c == a.Cases[0].Duke_Specialty_Area__c
                            && (ref.Referral_Status__c == 'IN PROCESS' || ref.Referral_Status__c == 'NEW' || ref.Referral_Status__c == 'OPEN' 
                                || ref.Referral_Status__c == 'AUTH' || ref.Referral_Status__c == 'PEND' || ref.Referral_Status__c == 'INCOMPLETE')
                            && (ref.Scheduling_Status__c != 'Duplicate' && ref.Scheduling_Status__c != 'Letter' && ref.Scheduling_Status__c != 'Appt Scheduled' 
                                && ref.Scheduling_Status__c != 'Patient Declined')){
                                    a.Is_Specialty_Match__c = true;
                        }
                    }
                }
        }   
        return a;         
    }

    /*******************************************************************************************************
    * @description On update, if Referral_Information__c.Referral_Type__c = Project Access, set "Has_Project_Access_Referral__c" = TRUE on Account (Patient_Name__c) 
    *   record
    *   If Referral_Information__c.Referred_to_Provider_Specialty__c = Case.Duke_Specialty_Area__c from recent related Case record with Status = 'New'
    * @param ref Referral_Information__c 
    * @param a Account
    * @return Account
    */
    private static Account updateProjectAccessPatient(Referral_Information__c newRef, Account a){
        if(newRef.Referral_Type__c == 'Project Access' && newRef.Patient_Name__c != null){
            a.Has_Project_Access_Referral__c = true;
            if(a.Cases.size() > 0){
                if(newRef.Referred_to_Dept_Spec__c != null && a.Cases[0].Duke_Specialty_Area__c != null){
                    if(newRef.Referred_to_Dept_Spec__c == a.Cases[0].Duke_Specialty_Area__c
                        && (newRef.Referral_Status__c == 'IN PROCESS' || newRef.Referral_Status__c == 'NEW' || newRef.Referral_Status__c == 'OPEN' 
                            || newRef.Referral_Status__c == 'AUTH' || newRef.Referral_Status__c == 'PEND' || newRef.Referral_Status__c == 'INCOMPLETE')
                        && (newRef.Scheduling_Status__c != 'Duplicate' && newRef.Scheduling_Status__c != 'Letter' && newRef.Scheduling_Status__c != 'Appt Scheduled' 
                            && newRef.Scheduling_Status__c != 'Patient Declined')){
                        a.Is_Specialty_Match__c = true;
                    }        
                }  
            } 
        }
        return a;
    }

    /*******************************************************************************************************
    * @description On insert Referral_Information__c.Referral_Type__c IN ('VA', 'Tricare Prime'), set "Has_VA_Referral__c" = TRUE on Account (Patient_Name__c) 
    *   record
    * @param ref Referral_Information__c 
    * @param a Account
    * @return Account
    */
    private static Account insertVAReferral(Referral_Information__c ref, Account a){
        if(((ref.Referral_Type__c == 'VA' && ref.Patient_Name__c != null)||
                (ref.Referral_Type__c == 'Tricare Prime' && ref.Patient_Name__c != null))){
                a.Has_VA_Referral__c = true;
                if(a.Cases.size() > 0){
                    if(ref.Referred_to_Dept_Spec__c != null && a.Cases[0].Duke_Specialty_Area__c != null){
                        if(ref.Referred_to_Dept_Spec__c == a.Cases[0].Duke_Specialty_Area__c
                            && (ref.Referral_Status__c == 'IN PROCESS' || ref.Referral_Status__c == 'NEW' || ref.Referral_Status__c == 'OPEN' 
                                || ref.Referral_Status__c == 'AUTH' || ref.Referral_Status__c == 'PEND' || ref.Referral_Status__c == 'INCOMPLETE')
                            && (ref.Scheduling_Status__c != 'Duplicate' && ref.Scheduling_Status__c != 'Letter' && ref.Scheduling_Status__c != 'Appt Scheduled' 
                                && ref.Scheduling_Status__c != 'Patient Declined')){
                                    a.Is_VA_Specialty_Match__c = true;
                        } 
                    }  
                } 
        }   
        return a;         
    }

    /*******************************************************************************************************
    * @description On update Referral_Information__c.Referral_Type__c IN ('VA', 'Tricare Prime'), set "Has_VA_Referral__c" = TRUE on Account (Patient_Name__c) 
    *   record
    * @param ref Referral_Information__c 
    * @param a Account
    * @return Account
    */
    private static Account updateVAReferral(Referral_Information__c newRef, Account a){
        if((newRef.Referral_Type__c == 'VA' && newRef.Patient_Name__c != null)||
        (newRef.Referral_Type__c == 'Tricare Prime' && newRef.Patient_Name__c != null)){
            a.Has_VA_Referral__c = true;
            if(a.Cases.size() > 0){
                if(newRef.Referred_to_Dept_Spec__c != null && a.Cases[0].Duke_Specialty_Area__c != null){
                    if(newRef.Referred_to_Dept_Spec__c == a.Cases[0].Duke_Specialty_Area__c
                        && (newRef.Referral_Status__c == 'IN PROCESS' || newRef.Referral_Status__c == 'NEW' || newRef.Referral_Status__c == 'OPEN' 
                            || newRef.Referral_Status__c == 'AUTH' || newRef.Referral_Status__c == 'PEND' || newRef.Referral_Status__c == 'INCOMPLETE')
                        && (newRef.Scheduling_Status__c != 'Duplicate' && newRef.Scheduling_Status__c != 'Letter' && newRef.Scheduling_Status__c != 'Appt Scheduled' 
                            && newRef.Scheduling_Status__c != 'Patient Declined')){
                                a.Is_VA_Specialty_Match__c = true;
                    } 
                }  
            } 
        }
        return a;
    }

    /*******************************************************************************************************
    * @description On insert Referral_Information__c.Referral_Type__c = 'Worker\'s Comp', set "Has_Workers_Comp_Referral__c" = TRUE on Account (Patient_Name__c) 
    *   record
    * @param ref Referral_Information__c 
    * @param a Account
    * @return Account
    */
    private static Account insertWorkersCompReferral(Referral_Information__c ref, Account a){
        if(ref.Referral_Type__c == 'Worker\'s Comp' && ref.Patient_Name__c != null && ref.Referral_Status__c != null){
            a.Has_Workers_Comp_Referral__c = true;
            a.Workers_Comp_Referral_Status__c = ref.Referral_Status__c;
        }            
       return a;
    }

    /*******************************************************************************************************
    * @description On update Referral_Information__c.Referral_Type__c = 'Worker\'s Comp', set "Has_Workers_Comp_Referral__c" = TRUE on Account (Patient_Name__c) 
    *   record
    * @param ref Referral_Information__c 
    * @param a Account
    * @return Account
    */
    private static Account updateWorkersCompReferral(Referral_Information__c newRef, Account a){
        if(newRef.Referral_Type__c == 'Worker\'s Comp' && newRef.Patient_Name__c != null && newRef.Referral_Status__c != null){
            a.Has_Workers_Comp_Referral__c = true;
            a.Workers_Comp_Referral_Status__c = newRef.Referral_Status__c;
        }   
        return a;
    }

    /*******************************************************************************************************
    * @description On update Referral_Information__c.Referral_Type__c = 'CFS Non-Contracted Review', set "Has_Active_CFS_Referrals__c" = TRUE on Account (Patient_Name__c) 
    *   record and CFS_Referral_Status__c = Referral_Information__c.Referral_Status__c
    * @param ref Referral_Information__c 
    * @param a Account
    * @return Account
    */
    private static Account insertCFSReferral(Referral_Information__c ref, Account a){
        if(ref.Referral_Type__c == 'CFS Non-Contracted Review' && ref.Patient_Name__c != null && ref.Referral_Status__c != null){
                a.Has_Active_CFS_Referrals__c = true;
                a.CFS_Referral_Status__c = ref.Referral_Status__c;
        }    
        return a;       
    }

    /*******************************************************************************************************
    * @description On update Referral_Information__c.Referral_Type__c = 'CFS Non-Contracted Review', set "Has_Active_CFS_Referrals__c" = TRUE on Account (Patient_Name__c) 
    *   record and CFS_Referral_Status__c = Referral_Information__c.Referral_Status__c
    * @param ref Referral_Information__c 
    * @param a Account
    * @return Account
    */
    private static Account updateCFSReferral(Referral_Information__c newRef, Account a){
        if(newRef.Referral_Type__c == 'CFS Non-Contracted Review' && newRef.Patient_Name__c != null && newRef.Referral_Status__c != null){
            a.Has_Active_CFS_Referrals__c = true;
            a.CFS_Referral_Status__c = newRef.Referral_Status__c;
        }   
        return a;
    }

}