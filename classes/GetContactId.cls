global without sharing class GetContactId implements Process.Plugin {

    webservice static Process.PluginResult invoke(Process.PluginRequest request) {
        Map<String, String> result = new Map<String, String>();
        List<CampaignMember> campaignMemeberList = new List<CampaignMember>();
        String accountId = (String) request.inputParameters.get('AccountId');
        
        if (String.isNotBlank(accountId)) {
        	List<Contact> conList = [SELECT Id FROM Contact WHERE accountId =: accountId];
        
        if (conList.isEmpty() == TRUE) {
        	result.put('contactId', null);
        } else {
        	result.put('contactId', conList[0].Id);
        }
        } else {
        	result.put('contactId', null);
        }
        
        return new Process.PluginResult(result);
    }
    
    global Process.PluginDescribeResult describe() {
        Process.PluginDescribeResult result = new Process.PluginDescribeResult();
        result.Name = 'Get Contact By Account Id';
        result.Tag = 'getContactByAccountId';
        result.inputParameters = new
        List<Process.PluginDescribeResult.InputParameter>{
        	new Process.PluginDescribeResult.InputParameter('AccountId', Process.PluginDescribeResult.ParameterType.STRING, FALSE)
        };
        result.outputParameters = new
        List<Process.PluginDescribeResult.OutputParameter>{
            new Process.PluginDescribeResult.OutputParameter('contactId', Process.PluginDescribeResult.ParameterType.STRING)
        };
        return result;
    }
}