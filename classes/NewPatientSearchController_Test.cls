@isTest
public class NewPatientSearchController_Test {
    
    @IsTest
    static void testSearch() {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account acc1 = new Account(FirstName = 'Fred', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', RecordTypeId = rt.Id);
        insert acc1;
        Account acc2 = new Account(FirstName = 'Peter', LastName = 'Parker', Phone = '1234567890', PersonBirthdate=date.ValueOf('2020-12-01'), PersonHomePhone = '333-123-7777', HealthCloudGA__MedicalRecordNumber__c='Maestro3333');
        insert acc2;
        Contact con1 = new Contact(FirstName = 'Eric', LastName = 'Peters', Phone = '333-123-8888');
        insert con1;
        Case cas1 = new Case(Subject = 'TestCase001', AccountId = acc2.Id, ContactId = con1.Id);
        insert cas1;
        
        String testResponse = '[{"mrn":"Maestro3333","title":"test","name":"my test"}]';
        MockHttpResponseGenerator mockResponse = new MockHttpResponseGenerator(200, 'Ok', testResponse, new Map<String, String>{'Content-Type'=>'application/json'});
        Test.setMock(HttpCalloutMock.class, mockResponse);
        Test.startTest();
        List<List<sObject>> allResults = NewPatientSearchController.findRecords(
            'Wayne OR Fred',
            '333-123-8888',
            '12/05/2020',
            null
            );
        System.debug('allResults ' + allResults);    
        NewPatientSearchController.findRecords(
            'Wayne , Fred',
            '333-123-8888',
            '12/05/2020',
            null
            );
        NewPatientSearchController.epicOnlySoCreate('Wayne , Fred', '2020-12-05', 'Maestro3333');
        NewPatientSearchController.epicOnlySoCreate('Wayne OR Fred', '2020-12-05', 'Test');
        NewPatientSearchController.epicOnlySoCreate('Wayne Fred', '2020-12-05', 'Test123');
        NewPatientSearchController.epicOnlySoCreate('Wayne;Fred', '2020-12-05', 'Test456');
        NewPatientSearchController.epicOnlySoCreate('Wayne,Fred', '2020-12-05', 'Test789');
        Test.stopTest();
    }
    /*@IsTest
    static void testSearchNoAcc() {
        Test.startTest();
        List<List<sObject>> allResults2 = NewPatientSearchController.findRecords(
            'Bob OR Steve',
            '111-111-1111',
            '12/11/1999'
            );
        System.debug('allResults2 ' + allResults2);    
        Test.stopTest();
    }*/
    
    @IsTest
    static void testSearchBlank() {
        Test.startTest();
        List<List<sObject>> allResults3 = NewPatientSearchController.findRecords(
            ' ',
            '60',
            '  ',
            null
            );
        System.debug('allResults3 ' + allResults3);    
        Test.stopTest();
    }
}