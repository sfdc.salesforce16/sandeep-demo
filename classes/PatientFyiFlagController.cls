/******************************************
 * 
 * @author Synaptic AP
 * @since  Feb 2021
 * Project DHAS
 */
public inherited sharing class PatientFyiFlagController {
    public PatientFyiFlagController() {}

    @AuraEnabled
    public static List<FYIFlag> callEpicFyiFlags(String accountId){
        Account patient = [SELECT Id, HealthCloudGA__MedicalRecordNumber__c FROM Account WHERE Id = :accountId LIMIT 1];
        String mrn = patient.HealthCloudGA__MedicalRecordNumber__c;

        //validation
        if(String.isBlank(mrn)){ return null;}

        EpicWebService ews = new EpicWebService();
        HTTPResponse res = ews.call('fyiFlags', mrn, 'GET');

        System.debug('mrn: ' + mrn);
        System.debug('fyiFlags.body: ' + res.getBody());
        List<FYIFlag> fyiFlags = (List<FYIFlag>) JSON.deserialize(res.getBody(), List<FYIFlag>.class);
        System.debug('fyiFlags: ' + fyiFlags);


        return fyiFlags;
    }
}