/**
 * Created by dcano on 5/26/21.
 */

@IsTest
private class PatientDupCheckControllerTest {
    @IsTest
    static void testBehavior()
    {
        String testResponse = '[{"mrn":"Maestro3333","title":"test","name":"my test", "birthDate":"1980-8-11","rank":"5"}]';
        MockHttpResponseGenerator mockResponse = new MockHttpResponseGenerator(200, 'Ok', testResponse, new Map<String, String>{'Content-Type'=>'application/json'});
        Test.setMock(HttpCalloutMock.class, mockResponse);
        Test.startTest();

        PatientDupCheckController controller = new PatientDupCheckController();
        PatientDupCheckController.dupeCheck('1965-03-21', 'Tay', 'Munden', 'Male', null);

        Test.stopTest();
    }
}