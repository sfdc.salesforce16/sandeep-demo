/**
* @author Synaptic
* @date 2014
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description A wrapper Apex class that contains a few details for a Session_Registrant__c record. 
* This class is used to package a few details of a Session_Registrant__c record in a serializable format.
*/
public without sharing class SessionRegistration {
    @AuraEnabled
    @InvocableVariable(label='Session Id')
    public String sessionId;
    
    @AuraEnabled
    @InvocableVariable(label='Lead Id')
    public String leadId;
    
    @AuraEnabled
    @InvocableVariable(label='Account Id')
    public String accountId;
    
    @AuraEnabled
    @InvocableVariable(label='Email Address')
    public String email;
    
    @AuraEnabled
    @InvocableVariable(label='Successful Registration')
    public Boolean success;
    /*******************************************************************************************************
    * @description Register for a Session using Session Id and Lead Id/Account Id
    * @param regs List<SessionRegistration> staging registration sessions
    * @return List<SessionRegistration>
    */
    @InvocableMethod(label='Register for Session' description='Register for a Session using Session Id and Lead Id/Account Id')
    public static List<SessionRegistration> findMatch(List<SessionRegistration> regs) {
        System.debug('Create Session Registration for: ' + regs);
        
        // check for value
        if(regs.isEmpty()) return null;
        SessionRegistration r = regs[0];
        
        try {
            Session_Registrant__c sr = new Session_Registrant__c(
                Lead__c = (r.leadId == '') ? null : r.leadId,
                Account__c = (r.accountId == '') ? null : r.accountId,
                Registrant_Email__c = r.email,
                Session__c = r.sessionId
            );
            insert sr;
            r.success = true;
        }
        catch(Exception e) {
            System.debug('Error registering for session! ' + e.getMessage() + '\n' + e.getStackTraceString());
        }
        
        return new List<SessionRegistration>{r};
    }

}