@isTest 
public class HealthMaintController_Test {

    @IsTest
    static void testRun() {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account acc1 = new Account(FirstName = 'Fred', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', RecordTypeId = rt.Id, HealthCloudGA__MedicalRecordNumber__pc = '123');
        insert acc1;
        string accid = acc1.id;

        Test.startTest();
        HealthMaintController hmc = new HealthMaintController();
        MockHttpResponseGenerator fakeResponse = new MockHttpResponseGenerator(200,
                                                 'Complete',
                                                 '[{"Name": "HealthMaintController"}]',
                                                 null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        HealthMaintController.callEpicHealthMaint(accid);
        Test.stopTest();
        
    }

}