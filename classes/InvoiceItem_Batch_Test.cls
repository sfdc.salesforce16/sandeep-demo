@isTest
public class InvoiceItem_Batch_Test {
    
    @testSetup
    static void setup() {
        
        // GL Accounts
        GL_Account__c gl1 = new GL_Account__c(Name = 'Sales - Discounts Allowed', GL_Account_Number__c = '1');
        GL_Account__c gl2 = new GL_Account__c(Name = 'Accounts Receivable', GL_Account_Number__c = '1');
        GL_Account__c gl3 = new GL_Account__c(Name = 'Revenue', GL_Account_Number__c = '1');
        GL_Account__c gl4 = new GL_Account__c(Name = 'Payment', GL_Account_Number__c = '1');
        GL_Account__c gl5 = new GL_Account__c(Name = 'Deferred Revenue', GL_Account_Number__c = '1');
        insert new List<GL_Account__c>{gl1, gl2, gl3, gl4, gl5};
            
        // Product
        Product__c prod = new Product__c(
        	Name = 'Shirt',
            Selling__c = 10,
            Accounts_Receivable_GL_Account__c = gl2.Id,
            Revenue_GL_Account__c = gl3.Id,
            Payments_GL_Account__c = gl4.Id
        );
        insert prod;
        
        // Program
        CareProgram p = new CareProgram(
        	Name = 'Residential Weight Loss',
        	Price__c = 100,
        	Deposit__c = 5,
            Duration__c = 7,
            Accounts_Receivable_GL_Account__c = gl2.Id,
            Revenue_GL_Account__c = gl3.Id,
            Payments_GL_Account__c = gl4.Id,
            Deferred_Revenue_GL_Account__c = gl5.Id
        );
        
        // Service
        CareProgram s = new CareProgram(
        	Name = 'Massage',
        	Price__c = 10,
            Duration__c = 1,
            Accounts_Receivable_GL_Account__c = gl2.Id,
            Revenue_GL_Account__c = gl3.Id,
            Payments_GL_Account__c = gl4.Id,
            Deferred_Revenue_GL_Account__c = gl5.Id
        );
        
        insert new List<CareProgram>{p, s};
            
        // Account 
        Account a = new Account(
        	Name = 'Test Test'
        ); insert a;
        
        // Order 
        Order_Custom__c o = new Order_Custom__c(
        	Member__c = a.Id,
            Status__c = 'Draft'
        ); insert o;
        
        // Product Order Item
        Order_Item__c i1 = new Order_Item__c(
            Type__c = 'Product',
            Order__c = o.Id, 
        	Product__c = prod.Id,
            Quantity__c = 1
        );
        
        // Product Order Item - with discount
        Order_Item__c i2 = new Order_Item__c(
            Type__c = 'Product',
            Order__c = o.Id, 
        	Product__c = prod.Id,
            Quantity__c = 1,
            Discount__c = 0.5
        );
        
        // Service Order Item
        Order_Item__c i3 = new Order_Item__c(
            Type__c = 'Service',
            Order__c = o.Id, 
        	Program__c = s.Id,
            Start_Date__c = System.today().addDays(30)
        );
        
        // Program Order Item - with deposit
        Order_Item__c i4 = new Order_Item__c(
            Type__c = 'Program',
            Order__c = o.Id, 
        	Program__c = p.Id,
            Start_Date__c = System.today().addDays(30)
        );
        
        
        // Program Order Item - with deposit and discount
        Order_Item__c i5 = new Order_Item__c(
            Type__c = 'Program',
            Order__c = o.Id, 
        	Program__c = p.Id,
            Start_Date__c = System.today().addDays(30),
            Discount__c = 0.5
        );
        
        insert new List<Order_Item__c>{i1, i2, i3, i4, i5};
        
        
    }
    
    @isTest
    static void runBatch() {
        
        Test.startTest();
        Date batchDate = System.today().addDays(30);
        // removed Draft criteria to run for Order without Transactions
        String queryCriteria = '(Deposit_Invoice_Item_Complete__c = false OR Registration_Invoice_Item_Complete__c = false OR '
                      + '(Product__c != null AND Invoice_Item_Processing_Complete__c = false) OR '
                      + '(Program__c != null AND Start_Date__c <= :batchDate AND (End_Date__c = null OR End_Date__c >= :batchDate)))'; 
        InvoiceItem_Batch b = new InvoiceItem_Batch(batchDate, queryCriteria);
        Database.executeBatch(b);
        Test.stopTest();
        
    }
    
    @isTest
    static void scheduleBatch() {
        
        Test.startTest();
        // daily at midnight
        String dailyMidnight = '0 0 0 * * ? '; // Seconds Minutes Hours Day_of_month Month Day_of_week Optional_year
        InvoiceItem_Batch b = new InvoiceItem_Batch(); 
        System.schedule('Invoice Items Batch', dailyMidnight, b);
        Test.stopTest();
        
    }

}