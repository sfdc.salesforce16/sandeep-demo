/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description This class controls the display logic for the following Notification 
* Center alerts: Dismissals, FYI Flags, Release of Information, Legacy Epic Pt Record, 
* Referrals, Recalls, Wait List, Provider Orders, Health Maintenance, VA Pt, 
* Medicaid Pt, Self-Pay Pt and Non-Contracted Pt.
*/
 public without sharing class HealthNotificationCenterController {
    public HealthNotificationCenterController() {

    }

    /*******************************************************************************************************
    * @description Call Maestro Care getWsDependentData API with Account Id as parameter
    * @param accountId the Id of the account to look up
    */
    @AuraEnabled
    public static void updateFromEpic(String accountId){
    try {
            Account patient = [SELECT Id, HealthCloudGA__MedicalRecordNumber__c FROM Account WHERE Id = :accountId LIMIT 1];
            String mrn = patient.HealthCloudGA__MedicalRecordNumber__c;
            System.debug('updateFromEpic.mrn: ' + mrn);
    
            //validation
            if(String.isBlank(mrn)){ return;}
    
           
            EpicWebService ews = new EpicWebService();
            HTTPResponse res = ews.callWithAccount('patient/refresh', mrn, accountId, 'GET');
        } catch(Exception e) {
            System.debug(e.getMessage());
        }
    }

    /*******************************************************************************************************
    * @description Returns a list of Alerts for ROI, Referrals, Recalls, Waitlists, Orders, Coverages, Health Maintenances,...
    * @param accountId the Id of the account to look up
    */
    @AuraEnabled
    public static List<Alert> getAlerts(Id accountId){

        Map<Id, Skill_Group__c> mapSkills = new Map<Id, Skill_Group__c>([SELECT Id FROM Skill_Group__c WHERE Id IN (SELECT Skill_Group__c FROM Agent_Skill_Group__c WHERE Agent__c = :UserInfo.getUserId())]);

        List<Alert> returnVal = new List<Alert>();
        Integer key = 0;
        Boolean isInSkillGroup = false;
        Boolean hasVaCoverage = false;
        Boolean hasReferrals = false;

        Boolean isVeteranStatus = false;
        Boolean isWorkersComp = false;

        Boolean isPrisoner = false;
        Boolean isInternationalPatient = false;
        Boolean isHitechPatient = false;
        Boolean isProjectAccess = false;
        Boolean isLegacyPatient = false;
        Boolean isMarkedForMerge = false;
        Boolean isUnmerged = false;
        Boolean hasROIDocuments = false;
        Boolean hasInterpreterNeeded = false;
        Boolean hasDueHealthMaint = false;
        Boolean closeTheCase = false;
        Boolean isDeceasedPatient = false;

        try {
            System.debug('accountId: ' + accountId);

            //Retrieve the Account and related info
            Account a = [SELECT 
                            Id, 
                            //Current_Case__r.Skill_Group__c, 
                            VeteranStatus__c,
                            Infrequent_Patient_Status__c,
                            PatientType__c,
                            Interpreter_Needed__c,
                            Fyi_Flag__c,
                            Number_of_VA_Plans__c,
                            Has_HiTech_Guarantor__c,
                            Has_Project_Access_Referral__c,
                            Has_International_Referral__c,
                            isLegacyPatient__c,
                            IsMarkedForMerge__c,
                            IsUnmerged__c,
                            Patient_Status__c,
                            (SELECT 
                                Id,
                                Document_Type__c,
                                Date_Received__c,
                                Date_Scanned__c
                            FROM Release_of_Informations__r where Date_Expires__c > TODAY),
                            (SELECT 
                                Id, 
                                Referral_Status__c,
                                //Referred_to_Department__c,
                                Expiration_Date__c,
                                Referral_Type__c
                            FROM Referral_Informations__r 
                            WHERE Referral_Status__c IN ('PROCESS','NEW','OPEN','AUTH','PEND') 
                            AND Scheduling_Status__c IN ('Call-1','Call-2','Community-Co','Hub Called 1','Hub Called 2','Hub to Cal','Pending','Int Sch','MDC/WC/SP Ve','Ecomm Pendin','Provider Dec')
                                ),
                            (SELECT 
                                Id
                                FROM Recalls__r 
                                WHERE Recall_Status__c in ('New','Notification Sent')
                                ),
                            (SELECT 
                                Id
                                FROM Wait_List__r 
                                WHERE status__c = 'Pending'
                                ),
                            (SELECT 
                                Id
                            FROM Provider_Orders__r
                            WHERE Status__c NOT IN ('Schedule', 'Cancel Request', 'Schedule Externally')
                                ),
                            (SELECT
                                Name, 
                                IsActive__c
                            FROM MemberPlans),
                            (SELECT 
                                Id,
                                Name
                            FROM Health_Maintenances__r where Name IN ('Glaucoma Screening',
                                                                        'Hepatitis B Vaccines',
                                                                        'Mammogram',
                                                                        'Pap Smear',
                                                                        'Eye Exam',
                                                                        'DXA Bone Density Scan',
                                                                        'Colonoscopy',
                                                                        'HPV Vaccines',
                                                                        'Tdap/Td Vaccines',
                                                                        'Adult Tetanus (Td And Tdap)',
                                                                        'Shingrix',
                                                                        'Medicare Initial or AWV',
                                                                        'Annual Visit/Physical/Well Child Check',
                                                                        'Pneumococcal Vaccine: 65+',
                                                                        'COVID-19 Vaccine',
                                                                        'Influenza Vaccine')
                            AND Status__c IN ('Overdue', 'Due', 'Due On')),
                            (SELECT 
                                Status,
                                Id
                            FROM Cases
                            ORDER BY CreatedDate
                            DESC LIMIT 1)
                            // (SELECT 
                            //     Account_Type__c, 
                            //     Id
                            // FROM HealthCloudGA__EhrRelatedPerson__r
                            // WHERE Account_Type__c = 'HiTech'),
                            //(SELECT 
                            //     FYI_Flag__c, 
                            //     Id
                            // FROM FYI_Flag__c
                            // WHERE FYI_Flag__c = '1225'),                            
                            FROM Account 
                            WHERE Id = :accountId LIMIT 1];


            // List<HealthCloudGA__EhrRelatedPerson__c> Ehrp = [SELECT Account_Type__c, Id FROM HealthCloudGA__EhrRelatedPerson__c WHERE HealthCloudGA__Account__c = :accountId AND Account_Type__c = 'HiTech'];
            
            //Retrieve a list of all associated FYI Flags
            List<FYI_Flag__c> fyiFlags = [SELECT FYI_Flag__c, Id FROM FYI_Flag__c WHERE Patient_Account__c = :accountId AND FYI_Flag_Status__c = 'Active'];
            List<Dismissal__c> dismissals = [SELECT Id 
                                                FROM Dismissal__c 
                                                WHERE Account__c = :accountId 
                                                    AND ((Dismissal_Effective_Date__c <= TODAY AND Dismissal_End_Date__c >= TODAY)
                                                        OR (Dismissal_Effective_Date__c <= TODAY AND Dismissal_End_Date__c = NULL))];
           
            // List<Referral_Information__c> Refi = [SELECT Referral_Type__c FROM Referral_Information__c WHERE Patient_Name__c = :accountId AND Referral_Type__c = 'Project Access'];

            if (a.VeteranStatus__c == 'Y'){
                isVeteranStatus = true;
                System.debug('isVeteranStatus ' + isVeteranStatus);
            } else if (a.Number_of_VA_Plans__c > 0) {
                hasVaCoverage = true;
                isVeteranStatus = true;
                System.debug('isVeteranStatus ' + isVeteranStatus);
            }
        
            if(a.PatientType__c == 'Prisoner'){
                isPrisoner = true;
                System.debug('isPrisoner ' + isPrisoner);
            }

            if(a.Has_Project_Access_Referral__c == true){
                // if(Refi.size() > 0){
                isProjectAccess = true;
                System.debug('isProjectAccess ' + isProjectAccess);
            }

            if(a.isLegacyPatient__c == true){
                isLegacyPatient = true;
                System.debug('isLegacyPatient ' + isLegacyPatient);
            }

            if(a.IsMarkedForMerge__c == true){
                isMarkedForMerge = true;
                System.debug('isMarkedForMerge ' + isMarkedForMerge);
            }

            if(a.IsUnmerged__c == true){
                isUnmerged = true;
                System.debug('isUnmerged ' + isUnmerged);
            }
            
            if(a.Interpreter_Needed__c == 'Y'){
                hasInterpreterNeeded = true;
                System.debug('hasInterpreterNeeded ' + hasInterpreterNeeded);
            }

            
            if(a.Has_HiTech_Guarantor__c == true){
            // if((Ehrp.size() > 0)||
                isHitechPatient = true;
                System.debug('isHitechPatient ' + isHitechPatient);
            }

            if(a.Cases.size() > 0){
                if(a.Cases[0].Status == 'Open'){
                    // if((Ehrp.size() > 0)||
                    closeTheCase = true;
                    System.debug('closeTheCase ' + closeTheCase);
                } else if(a.Cases[0].Status == 'New'){
                    // if((Ehrp.size() > 0)||
                    closeTheCase = true;
                    System.debug('closeTheCase ' + closeTheCase);
                }
            }

            if(a.Patient_Status__c == 'Deceased'){
                isDeceasedPatient = true;
                System.debug('isDeceasedPatient ' + isDeceasedPatient);
            }

            if(a.Health_Maintenances__r.size() > 0){
                hasDueHealthMaint = true;
            }

            if(a.Release_of_Informations__r.size() > 0){
                hasROIDocuments = true;
            }

            
            // (a.FYI_Flag__r.FYI_Flag__c == 'HiTech')
            

            // if(a.ROIDocuments != null && a.cases != null) {
            //     for (Case cas : a.cases){
            //         if(cas.NotPatient == true){
            //             hasROIDocuments = true;
            //             System.debug('hasROIDocuments ' + hasROIDocuments);
            //         }
            //     }
            // }

            if(a.Referral_Informations__r.size() > 0){
                hasReferrals = true;
                System.debug('hasReferrals ' + hasReferrals);
                for (Referral_Information__c refInfo : a.Referral_Informations__r) {
                    if(a.Has_International_Referral__c == true){
                    // if(refInfo.Referral_Type__c == 'International'){
                        // Determine International Patient Status
                        isInternationalPatient = true;
                       System.debug('isInternationalPatient ' + isInternationalPatient);
                //    } else if(refInfo.Referral_Type__c == 'Project Access'){
                        // Determine Project Access status
                //         isProjectAccess = true;
                //         System.debug('isProjectAccess ' + isProjectAccess);
                    } else if(refInfo.Referral_Type__c == 'Worker\'s Comp'){
                        // Determine Workers Comp Status
                        isWorkersComp = true;
                        System.debug('isWorkersComp ' + isWorkersComp);        
                    } else if(refInfo.Referral_Type__c == 'VA' || refInfo.Referral_Type__c == 'Tricare Prime'){
                        // Determine VA Status
                        isVeteranStatus = true;
                        System.debug('isVeteranStatus ' + isVeteranStatus);        
                    }
                }
            }

            // Legacy Patient Notification
            if (isLegacyPatient) {
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Pending Work Queue Request';
                alert.Message = '  Partial Patient Record.';
                alert.Action = 'Check the patient record in Maestro Care for the full appointment history.';
                alert.FontStyle = 'color: white !important;';
                alert.NotificationStyle = 'background-color: #FF6663 !important; color: white !important;';
                alert.IconColor = 'slds-current-color';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);                
            }

            // Marked for Merge Patient Notification
            if (isMarkedForMerge) {
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Marked for Merge';
                alert.Message = '  Patient Record has been marked for merge.';
                alert.Action = 'Check the patient record in Maestro Care for duplicates.';
                alert.FontStyle = 'color: white !important;';
                alert.NotificationStyle = 'background-color: #FF6663 !important; color: white !important;';
                alert.IconColor = 'slds-current-color';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);                
            }

            // Marked for Unmerge Patient Notification
            if (isUnmerged) {
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Has Been Unmerged';
                alert.Message = '  Patient Record has been unmerged.';
                alert.Action = 'Check the patient record in Maestro Care for the latest.';
                alert.FontStyle = 'color: white !important;';
                alert.NotificationStyle = 'background-color: #FF6663 !important; color: white !important;';
                alert.IconColor = 'slds-current-color';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);                
            }
            
            // Deceased Patient Notification
            if (isDeceasedPatient) {
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Pending Work Queue Request';
                alert.Message = '  Deceased Patient.';
                alert.Action = 'No future appointments can be scheduled for this Patient.';
                alert.FontStyle = 'color: white !important;';
                alert.NotificationStyle = 'background-color: #FF6663 !important; color: white !important;';
                alert.IconColor = 'slds-current-color';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);                
            }
            

            // Close Case Notification
            if (closeTheCase) {
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Pending Work Queue Request';
                alert.Message = '  Close the current open case before taking the next call.';
                alert.Action = '';
                alert.FontStyle = 'color: white !important;';
                alert.NotificationStyle = 'background-color: #FF6663 !important; color: white !important;';
                alert.IconColor = 'slds-current-color';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);                
            }
                     

            // VA Patient Notification
            if ((isVeteranStatus)||(hasVaCoverage)) {
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Pending Work Queue Request';
                alert.Message = '  Possible VA Patient.';
                alert.Action = 'Review detailed instructions on the Referral form prior to scheduling.';
                alert.FontStyle = 'color: #222;/*font-style: italic;*/';
                alert.NotificationStyle = 'background-color: rgb(253 253 150) !important;';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);                
            }
            
            // Workers Comp Notifications
            if (isWorkersComp) {
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Pending Work Queue Request';
                alert.Message = '  Possible Workers Comp Patient.';
                alert.Action = 'Follow detailed instructions on the Registration form.';
                alert.FontStyle = 'color: #222;/*font-style: italic;*/';
                alert.NotificationStyle = 'background-color: rgb(253 253 150) !important;';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);  
            }

            // Prisoner Patient Notification
            if (isPrisoner) {
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Pending Work Queue Request';
                alert.Message = '  Prisoner Patient Type.';
                alert.Action = 'Verify that the Patient is in custody. Check Knowledge for details on Prisoner Patient registration process.';
                alert.FontStyle = 'color: #222;/*font-style: italic;*/';
                alert.NotificationStyle = 'background-color: rgb(253 253 150) !important;';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);                
            }

            // HITECH Patient Notification
            if (isHitechPatient) {
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Pending Work Queue Request';
                alert.Message = '  HiTech Patient.';
                alert.Action = 'Search Knowledge for more information about the HiTech Patient scheduling process.';
                alert.FontStyle = 'color: #222;/*font-style: italic;*/';
                alert.NotificationStyle = 'background-color: rgb(253 253 150) !important;';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);                
            }

            // International Patient Notification
            if (isInternationalPatient) {
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Pending Work Queue Request';
                alert.Message = '  International Patient.';
                alert.Action = 'Check Knowledge for details on scheduling International Patients.';
                alert.FontStyle = 'color: #222;/*font-style: italic;*/';
                alert.NotificationStyle = 'background-color: rgb(253 253 150) !important;';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);                
            }

        
            // Project Access Patient Notification
           if (isProjectAccess) {
               Alert alert = new Alert();
               alert.Key = key;
               alert.Title = 'Pending Work Queue Request';
               alert.Message = '  Project Access Patient.';
               alert.Action = 'Check Project Access referral status and specialty area prior to scheduling.';
               alert.FontStyle = 'color: #222;/*font-style: italic;*/';
               alert.NotificationStyle = 'background-color: rgb(253 253 150) !important;';
               returnVal.add(alert);
               key++;
               System.debug('alert: ' + alert);                
           }
        
            // Has Interpreter Needed Patient Notification
            if (hasInterpreterNeeded) {
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Pending Work Queue Request';
                alert.Message = '  Interpreter Needed.';
                alert.Action = 'Please make sure the Patient has access to an onsite Interpreter on the day of the appointment.';
                alert.FontStyle = 'color: #222;/*font-style: italic;*/';
                alert.NotificationStyle = 'background-color: rgb(253 253 150) !important;';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);                
            }   

            if(hasDueHealthMaint){
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Health Maintenance';
                alert.Message = '  Health Maintenance Due or Overdue';
                alert.Action = 'The Patient has due or overdue Health Maintenance items.  Check the Health Maintenance Info tab for details.';
                alert.FontStyle = 'color: #222;/*font-style: italic;*/';
                alert.NotificationStyle = 'background-color: rgb(253 253 150) !important;';
                returnVal.add(alert);
                key++;
                System.debug('alert.hasDueHealthMaint: ' + alert);                
            }

            // Caller Not Patient / ROI Notification
           if (hasROIDocuments) {
               Alert alert = new Alert();
               alert.Key = key;
               alert.Title = 'Release of Information';
               alert.Message = '  Patient has ROI Authorization(s) on file.';
               alert.Action = 'Go to the ROI tab to review details.';
               alert.FontStyle = 'color: #222;/*font-style: italic;*/';
               returnVal.add(alert);
               key++;
               System.debug('alert: ' + alert);                
           }

            system.debug('a.Referral_Informations__r ' + a.Referral_Informations__r);
            if(hasReferrals){
//            if(a.Recalls__r != null && isInSkillGroup){
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Pending Work Queue Request';
                alert.Message = '  The Patient has open Referral(s)';
                alert.Action = 'Please schedule from Maestro Care.';
                alert.FontStyle = 'color: #222;/*font-style: italic;*/';
                alert.NotificationStyle = 'background-color: rgb(253 253 150) !important;';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);
            }
        
            system.debug('a.Recalls__r ' + a.Recalls__r);
            if(a.Recalls__r.size() > 0){
//            if(a.Recalls__r != null && isInSkillGroup){
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Pending Work Queue Request';
                alert.Message = '  The Patient has open Recall request(s)';
                alert.Action = 'Please schedule from Maestro Care.';
                alert.FontStyle = 'color: #222;/*font-style: italic;*/';
                alert.NotificationStyle = 'background-color: rgb(253 253 150) !important;';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);
            }

            system.debug('a.Wait_List__r ' + a.Wait_List__r);
            if(a.Wait_List__r.size() > 0){
//            if(a.Wait_List__r != null && isInSkillGroup){
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Pending Work Queue Request';
                alert.Message = '  There are Wait List openings available for this Patient';
                alert.Action = 'Please schedule from Maestro Care.';
                alert.FontStyle = 'color: #222;/*font-style: italic;*/';
                alert.NotificationStyle = 'background-color: rgb(253 253 150) !important;';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);
            }
             
            system.debug('a.Provider_Orders__r ' + a.Provider_Orders__r);
            if(a.Provider_Orders__r.size() > 0){
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Pending Work Queue Request';
                alert.Message = '  The Patient has Provider Order(s) ready for scheduling';
                alert.Action = 'Please schedule from Maestro Care.';
                alert.FontStyle = 'color: #222;/*font-style: italic;*/';
                alert.NotificationStyle = 'background-color: rgb(253 253 150) !important;';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);
            }


            // Active FYI Flag Notification
            if(fyiFlags.size() > 0){
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Active FYI Flag(s)';
                alert.Message = '  The patient has active FYI Flag(s).';
                alert.Action = 'Check the ‘FYI Flag/Dismissal’ tab for details.';
                alert.NotificationStyle = 'background-color: #9EC1CF !important;';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);       
            }

            // Active Dismissals Notification
            if(dismissals.size() > 0){
                Alert alert = new Alert();
                alert.Key = key;
                alert.Title = 'Active Dismissal(s)';
                alert.Message = '  This patient has active Dismissal(s).';
                alert.Action = 'Check the ‘FYI Flag/Dismissal’ tab for details.';
                alert.NotificationStyle = 'background-color: #9EC1CF !important;';
                returnVal.add(alert);
                key++;
                System.debug('alert: ' + alert);     
            }

            // if(hasVaCoverage){
            //     Alert alert = new Alert();
            //     alert.Key = key;
            //     alert.Title = 'VA Coverage';
            //     alert.Message = '  This patient has VA Coverage';
            //     alert.Action = 'Check for VA Referral.';
            //     alert.NotificationStyle = 'background-color: rgb(253 253 150) !important;';
            //     returnVal.add(alert);
            //     key++;
            //     System.debug('alert: ' + alert);
            // }
        } catch (Exception ex) {
            System.debug('HealthNotificationCenterController.getAlerts.exception: ' + ex);
        }

        return returnVal;
    }

    public class Alert{

        @AuraEnabled
        public Integer Key;

        @AuraEnabled
        public string Title;

        @AuraEnabled
        public string Message;

        @AuraEnabled
        public string Action;

        @AuraEnabled
        public string FontStyle;

        @AuraEnabled
        public string NotificationStyle;

        @AuraEnabled
        public string IconColor;
    }
}