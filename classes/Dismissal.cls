/******************************************
 * 
 * @author Synaptic AP
 * @since  Feb 2021
 * Project DHAS
 */
public with sharing class Dismissal {
    public Dismissal() {}


    @AuraEnabled
    public string EffDateOfDismissal {get; set;} 
    
    @AuraEnabled
    public string DismissalEndDate {get; set;}    

    @AuraEnabled
    public string DismissalLevel {get; set;}    

    @AuraEnabled
    public string DeptDismissed {get; set;}    

    @AuraEnabled
    public string DismissalNotes {get; set;}    

    @AuraEnabled
    public string ReasonForDismissal {get; set;}    
}