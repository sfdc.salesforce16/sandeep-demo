/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class provides unit tests to cover the MemberPlan Trigger and MemberPlan_TriggerHandler apex class.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Synaptic
 * @project     DHAS
 * @since       February 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

 @isTest
public class MemberPlanTrigger_Test {
    
    @IsTest
    static void testTrigger() {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward');
		Insert a;
        Account a2 = new Account(FirstName = 'Bob', LastName = 'Wayne');
        System.debug('a.id ' + a.id);
        Contact c = new Contact(FirstName = 'Bruce', LastName = 'Wayne', AccountId = a2.Id);
        Insert c;
        System.debug('c.id ' + c.id);
        MemberPlan mbp = new MemberPlan(MemberId = a.Id, Name = 'Test MBP', RelationshipToSubscriber = 'Self', Sex__c='M', Sex_Youreka__c='Male',Relationship_to_Subscriber_Youreka__c='Self');
		Insert mbp;
        MemberPlan mbp2 = new MemberPlan(MemberId = a.Id, Name = 'Test MBP', Create_new_Subscriber__c = true, New_Subscriber_Name__c = 'Bob Wayne', New_Subscriber_Birthdate__c = date.ValueOf('2020-12-05'), New_Subscriber_Sex__c = 'Male', New_Subscriber_SSN__c = '123-12-1234', New_Subscriber_ID__c = '1234');
		Insert mbp2;
        System.debug('mbp.id ' + mbp.id);
        try{
            mbp.Name = 'Test MBP 2';
            mbp.RelationshipToSubscriber = 'Empl';
            update mbp;
            mbp.Sex__c='F';
            mbp.Sex_Youreka__c='Female';
            update mbp;
        }
        catch(DMLException e){
            //System.assert(e);
        }        
    }    
}