@isTest
public class WaitlistController_Test {
    //public virtual class errException extends Exception{}
    
    @IsTest
    static void testRun() {
	 	RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', VeteranStatus__c = 'Y', HealthCloudGA__MedicalRecordNumber__pc = '12234', HealthCloudGA__MedicalRecordNumber__c = '1234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        WaitListEntry wle = new WaitListEntry();
        System.debug('wle.Department: ' + wle.Department);
        System.debug('wle.Provider: ' + wle.Provider);
        wle.StartDate = 'a';
        wle.OnUntilDate = 'b';
        wle.Priority = 'c';
        wle.VisitTypeName = 'd';
        wle.AppointmentNotes = new List<String>{'e'};
        WaitListEntry.Provider p = new WaitListEntry.Provider();
        p.DepartmentName = 'h';
        p.ProviderName = 'i';
        wle.Providers.add(p);

        wle.Status = 's';
        wle.Status = wle.Department;
        System.debug('wle.Department: ' + wle.Department);
        System.debug('wle.Provider: ' + wle.Provider);
        wle.Status = wle.Provider;
        wle.compareTo(wle);
        WaitListEntry wle2 = (WaitListEntry)JSON.deserialize(JSON.serialize(wle), WaitListEntry.class);
        wle2.Providers[0].DepartmentName = 'z';
        wle.compareTo(wle2);
        WaitListEntry wle3 = (WaitListEntry)JSON.deserialize(JSON.serialize(wle), WaitListEntry.class);
        wle3.Providers[0].DepartmentName = 'a';
        wle.compareTo(wle3);

        Test.startTest();
        try {
            Insert a;
            String accid = a.id;
            MockHttpResponseGenerator fakeResponse = new MockHttpResponseGenerator(200,
                                                     'Complete',
                                                     '[{"Name": "HealthMaintController"}]',
                                                     null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            WaitlistController wlc = new WaitlistController();
            WaitlistController.callEpicWaitlist(accid);
            //wle.StartDate = '2020-12-05';
            Test.stopTest();
        } catch(exception e) {
            
        }
    }
}