@isTest
private class ProgramApplication_Test {
    
    public static Id profileId = [SELECT Id FROM Profile WHERE Name='Duke Health Customer Community Plus Login User'].Id;
    public static Id accountRoleId = [SELECT Id FROM UserRole WHERE Name = 'Partner' LIMIT 1].Id;
    
     @testSetup static void setup() {
         
        String recordTypeId = [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id;
        List<Account> accList = new List<Account>();
        
        accList = new List<Account>{
            new Account(FirstName = 'Test', LastName = 'Account1', RecordTypeId = recordTypeId),
            new Account(FirstName = 'Test', LastName = 'Account2', RecordTypeId = recordTypeId)
        };
        insert accList;
        
        // insert Form Template for forms
        List<disco__Form_Template__c> templateList = new List<disco__Form_Template__c>{
            new disco__Form_Template__c(Name = 'DHFC Membership Application', disco__Status__c = 'In Use'),
            new disco__Form_Template__c(Name = 'DDFC Graduate Residential Patient Medical Intake', disco__Status__c = 'In Use'),
            new disco__Form_Template__c(Name = 'DDFC Graduate Residential Patient Medical Intake', disco__Status__c = 'In Use')
        };
        insert templateList;
         
        // insert care program for program applicaiton
        CareProgram careprogram = new CareProgram();
        careprogram.Name = 'Fitness Center Membership';
        insert careprogram;
        
     }
    
    @isTest
    public static void testGetApplications() {
    
        List<Account> accList = [SELECT Id, PersonContactId, RecordTypeId FROM Account];
        CareProgram careprogram = [SELECT Id FROM CareProgram];
        List<disco__Form_Template__c> templateList = [SELECT Id FROM disco__Form_Template__c];
        List<Program_Application__c> programAppList = new List<Program_Application__c>();
        
        // get details for user instance
        
                
        // insert Program Application
        programAppList = new List<Program_Application__c>{
            new Program_Application__c(Name = 'test ProgramApplication 1', Member__c = accList[0].Id, Program__c = careprogram.Id),
            new Program_Application__c(Name = 'test ProgramApplication 2', Member__c = accList[0].Id, Program__c = careprogram.Id),
            new Program_Application__c(Name = 'test ProgramApplication 3', Member__c = accList[0].Id, Program__c = careprogram.Id)
        };
        insert programAppList;
		
        // insert forms under the program applicaiton Program_Application__c = programAppList[0].Id
        List<disco__Form__c> formList = new List<disco__Form__c>{
            new disco__Form__c(disco__Form_Template__c = templateList[0].Id, Program_Application__c = programAppList[0].Id),
            new disco__Form__c(disco__Form_Template__c = templateList[1].Id, Program_Application__c = programAppList[0].Id),
            new disco__Form__c(disco__Form_Template__c = templateList[2].Id, disco__Save_Complete__c = true, Program_Application__c = programAppList[2].Id)
        };
        insert formList;
        
        System.debug('User.ContactId'+ user.ContactId);
        
        //Test.startTest();
        
        //List<ProgramApplicationController.ProgramApplication> result = ProgramApplicationController.getApplications();
        //System.debug('result::' + result);
        
        // check asserts
        //System.assertEquals(result[0].formId, formList[1].Id); // 1st programApplication's 2nd form
        //System.assertEquals(result[1].hasForm, FALSE);
        //System.assertEquals(result[2].hasForm, FALSE);
        
        //Test.stopTest();
    }
}