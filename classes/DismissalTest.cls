@isTest
public with sharing class DismissalTest 
{
    @IsTest
    static void testDismissal() 
    {
        Test.startTest();

        Dismissal dis = new Dismissal();
        dis.EffDateOfDismissal = 'blah blah';
        dis.DismissalEndDate = 'blah blah';
        dis.DismissalLevel = 'blah blah';
        dis.DeptDismissed = 'blah blah';
        dis.DismissalNotes = 'blah blah';
        dis.ReasonForDismissal = 'blah blah';

        System.debug(dis.EffDateOfDismissal);
        System.debug(dis.DismissalEndDate);
        System.debug(dis.DismissalLevel);
        System.debug(dis.DeptDismissed);
        System.debug(dis.DismissalNotes);
        System.debug(dis.ReasonForDismissal);

        Test.stopTest();
    }
}