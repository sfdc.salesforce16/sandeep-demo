/**
 * @author Ian McNear
 * @date 05.05.2021
 * @group DCRC
 * @testCoverage 80%
 * @testCoverageReason Not able to recreate a 500 error.
 */

@isTest
public with sharing class ApiSessionTest {
    public testMethod static void testSessionGetSuccess() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/session';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        ApiSession.GetResponseWrapper wrapper = ApiSession.retrieveSession();
        Test.stopTest();

        List<Session__c> returnSessionList = wrapper.sessionList;

        // Testing to make sure we successfully get one session.
        System.assertEquals('Success', wrapper.status, 'Incorrect. Response did not return success.');
        System.assertEquals(1, returnSessionList.size(), 'Incorrect. Only one should have returned.');
        System.assertEquals('Test Session 1', returnSessionList[0].Name, 'Incorrect. Session returned had a different name then expected.');
    }

    @TestSetup
    static void init() {
        // Creating two, one should return the other should not
        List<Session__c> sessionList = new List<Session__c>();

        Id dcrcRecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Session__c' AND DeveloperName = 'Event' LIMIT 1].Id;

        sessionList.add(new Session__c(
            Name = 'Test Session 1', 
            State__c = 'NC', 
            Zip_Code__c = '27517', 
            Address__c = '100 Duke Blue Way', 
            Facility__c = 'Duke Hospital', 
            Event_Fee__c = 'Free',
            Description__c = 'Test Session Description', 
            Available_Openings__c = 20, 
            Scheduled_Start__c = Datetime.now().addDays(3), 
            Scheduled_End__c = Datetime.now().addDays(4), 
            Status__c = 'Active', 
            City__c = 'Durham',
            RecordTypeId = dcrcRecordTypeId
        ));

        sessionList.add(new Session__c(
            Name = 'Test Session 2', 
            State__c = 'NC', 
            Zip_Code__c = '27517', 
            Address__c = '100 Duke Blue Way', 
            Facility__c = 'Duke Hospital', 
            Event_Fee__c = 'Free',
            Description__c = 'Test Session Description', 
            Available_Openings__c = 20, 
            Scheduled_Start__c = Datetime.now().addDays(3), 
            Scheduled_End__c = Datetime.now().addDays(4), 
            Status__c = 'Planned', 
            City__c = 'Durham',
            RecordTypeId = dcrcRecordTypeId
        ));

        insert sessionList;
    }
}