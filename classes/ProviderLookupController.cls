public without sharing class ProviderLookupController {
    @AuraEnabled(cacheable=true)
    public static List<Contact> getProviders() {
        Id providerRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business').getRecordTypeId();
        return [
            SELECT Id, Name, Clinician_Title__c, Provider_SER_Number__c, Duke_Practitioner_ID__c
            FROM Contact
            WHERE RecordTypeId = :providerRecordType
            AND Practitioner_Status__c ='Active'
            WITH SECURITY_ENFORCED
            ORDER BY Name
            LIMIT 20
        ];
    }

    @AuraEnabled(cacheable=true)
    public static List<Contact> findContacts(String searchKey) {
        Id providerRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business').getRecordTypeId();
        String key = '%' + searchKey + '%';
        return [
            SELECT Id, Name, Clinician_Title__c, Provider_SER_Number__c, Duke_Practitioner_ID__c
            FROM Contact
            WHERE Name LIKE :key AND RecordTypeId = :providerRecordType
            WITH SECURITY_ENFORCED
            ORDER BY Name
            LIMIT 20
        ];
    }

    // @AuraEnabled(cacheable=true)
    // public static Contact getSingleContact() {
    //     return [
    //         SELECT Id, Name, Title, Phone, Email, Picture__c
    //         FROM Contact
    //         WITH SECURITY_ENFORCED
    //         LIMIT 1
    //     ];
    // }
}