@isTest
private class BatchDeleteExpiredCoverages_Test {
    @isTest
    static void BatchDeleteExpiredCoverages_Test() {
        // Set up test data with 200+ accounts so it tests the batching
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        List<Account> accts = new List<Account>();
        for(Integer i=0;i<250;i++) {
            Account a = new Account(FirstName='TestAccount' + i, LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
            accts.add(a);
        }
        insert accts;
        Date yesterday = Date.Today().AddDays(-1);
        Date tomorrow = Date.Today().AddDays(1);
        List<MemberPlan> mpList = new List<MemberPlan>();
        // Half of MP records had an Effective To date of yesterday
        for(Integer j=0;j<125;j++) {
            Account acct = accts[j];
            MemberPlan mp = new MemberPlan(Name=acct.Name + ' Member Plan', EffectiveTo=yesterday, MemberId=acct.Id);
            mpList.add(mp);
        }
        // Half of MP records had an Effective To date of tomorrow
        for(Integer j=125;j<250;j++) {
            Account acct = accts[j];
            MemberPlan mp = new MemberPlan(Name=acct.Name + ' Member Plan', EffectiveTo=tomorrow, MemberId=acct.Id);
            mpList.add(mp);
        }
        insert mpList;

        Test.startTest();
        database.executeBatch( new BatchDeleteExpiredCoverages());
        Test.stopTest();

        integer count = [SELECT count() FROM MemberPlan WHERE EffectiveTo < TODAY];
        System.assertEquals(0, count);
        count=[SELECT count() FROM MemberPlan];
        System.assertEquals(125, count);
    }
}