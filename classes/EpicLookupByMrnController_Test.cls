@isTest 
public class EpicLookupByMrnController_Test {

    @IsTest
    static void testRun() {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account acc1 = new Account(FirstName = 'Fred', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', RecordTypeId = rt.Id);
        insert acc1;

		PageReference pageRef = Page.EpicLookupByMrn;
        pageRef.getParameters().put('id',acc1.id);
		Test.setCurrentPage(pageRef);
		Test.startTest();
		ApexPages.StandardController con = new ApexPages.StandardController(acc1);
		EpicLookupByMrnController mycon = new EpicLookupByMrnController(con);
		mycon.epicNavigateToMrn();
		Test.stopTest();
        
    }
}