@isTest
public class PatientReferrals_Test {

    @isTest
    static void testGetFyiFlags() {
        // Create test data
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Account a2 = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');
		
        Test.startTest();

        Insert a;
        Insert a2;
        Insert u;
        FYI_Flag__c fyi = new FYI_Flag__c(Patient_Account__c=a.Id, FYI_Flag_Status__c = 'Active');
        FYI_Flag__c fyi2 = new FYI_Flag__c(Patient_Account__c=a.Id, FYI_Flag_Status__c = 'Inactive');
        Insert fyi;
        Insert fyi2;
        String accid = a.id;

        System.runAs(u) {
            PatientReferrals paref = new PatientReferrals();
            List<FYI_Flag__c> getFyi = PatientReferrals.getFyiFlags(accid);
            // Assert that list size is 1, and only got record associated with account
            System.assert(getFyi.size()==1);
        }

        Test.stopTest();
    }

    @isTest
    static void testGetDismissals() {
        // Create test data
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Account a2 = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');

        // Create date data
        Date dt = System.Today();
        Date twoDaysAgo = dt.addDays(-2);
        Date twoDaysFromNow = dt.addDays(2);
        Date fourDaysFromNow = dt.addDays(4);

        Test.startTest();

        Insert a;
        Insert a2;
        Insert u;
        // Create 2 dismissals - one active and one inactive (based on dates)
        Dismissal__c dis = new Dismissal__c(Account__c=a.Id, Dismissal_Effective_Date__c=twoDaysAgo, Dismissal_End_Date__c=twoDaysFromNow);
        Dismissal__c dis2 = new Dismissal__c(Account__c=a.Id, Dismissal_Effective_Date__c=twoDaysFromNow, Dismissal_End_Date__c=fourDaysFromNow);
        Dismissal__c dis3 = new Dismissal__c(Account__c=a.Id, Dismissal_Effective_Date__c=twoDaysAgo);
        Insert dis;
        Insert dis2;
        Insert dis3;
        String accid = a.id;

        System.runAs(u) {
            PatientReferrals paref = new PatientReferrals();
            List<Dismissal__c> getDis = PatientReferrals.getDismissals(accid);
            // Assert that list size is 1, and only got record associated with account
            System.assert(getDis.size()==2);
        }

        Test.stopTest();
    }

    @isTest
    static void testGetHealthMaintInfos() {
        // Create test data
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Account a2 = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');
		
        Test.startTest();

        Insert a;
        Insert a2;
        Insert u;
        Health_Maintenance__c hm = new Health_Maintenance__c(Patient__c=a.Id, Name='Glaucoma Screening',  Status__c='Overdue');
        Insert hm;
        String accid = a.id;

        System.runAs(u) {
            PatientReferrals paref = new PatientReferrals();
            // Assert that list size is 1, and only got record associated with account
            List<Health_Maintenance__c> getHM = PatientReferrals.getHealthMaintInfos(accid);
            System.assert(getHM.size()==1);
        }

        Test.stopTest();
    }

    @isTest
    static void testGetReferralInfos() {
        // Create test data
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Account a2 = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');
		
        Test.startTest();

        Insert a;
        Insert a2;
        Insert u;
        Referral_Information__c refInfo = new Referral_Information__c(Patient_Name__c=a.Id, Referral_Status__c='Open', Referral_ID__c='1234');
        Referral_Information__c refInfo2 = new Referral_Information__c(Patient_Name__c=a.Id, Referral_Status__c='Closed', Referral_ID__c='2345');
        Insert refInfo;
        Insert refInfo2;
        String accid = a.id;

        System.runAs(u) {
            PatientReferrals paref = new PatientReferrals();
            // Assert that list size is 1, and only got record associated with account
            List<Referral_Information__c> getRefInfo = PatientReferrals.getReferralInfos(accid);
            System.assert(getRefInfo.size()==1);
        }

        Test.stopTest();
    }

    @isTest
    static void testGetRecalls() {
        // Create test data
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Account a2 = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');
		
        Test.startTest();

        Insert a;
        Insert a2;
        Insert u;
        Recalls__c rec = new Recalls__c(Patient_Name__c=a.Id, Recall_Status__c = 'New');
        Recalls__c rec2 = new Recalls__c(Patient_Name__c=a.Id, Recall_Status__c = 'Scheduled/Linked');
        Insert rec;
        Insert rec2;
        String accid = a.id;

        System.runAs(u) {
            PatientReferrals paref = new PatientReferrals();
            // Assert that list size is 1, and only got record associated with account
            List<Recalls__c> getRec = PatientReferrals.getRecalls(accid);
            System.assert(getRec.size()==1);
        }

        Test.stopTest();
    }

    @isTest
    static void testGetWaitlists() {
        // Create test data
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', HealthCloudGA__MedicalRecordNumber__c='12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Account a2 = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', HealthCloudGA__MedicalRecordNumber__c=null, PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');
		
        Test.startTest();

        Insert a;
        Insert a2;
        Insert u;
        Wait_List__c wl = new Wait_List__c(Account__c=a.Id, Status__c = 'Pending');
        Wait_List__c wl2 = new Wait_List__c(Account__c=a.Id, Status__c = 'Scheduled');
        Insert wl;
        Insert wl2;
        String accid = a.id;
        String accid2 = a2.id;

        System.runAs(u) {
            PatientReferrals paref = new PatientReferrals();
            // Assert that list size is 1, and only got record associated with account
            List<Wait_List__c> getWl = PatientReferrals.getWaitlists(accid);
            System.assert(getWl.size()==1);

            // Assert that the list size is 0, since the HealthCloudGA__MedicalRecordNumber__c field is null
            List<Wait_List__c> getWl2 = PatientReferrals.getWaitlists(accid2);
            System.assert(getWl2.size()==0);
        }

        Test.stopTest();
    }

    @isTest
    static void testGetOrders() {
        // Create test data
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Account a2 = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');
		
        Test.startTest();

        Insert a;
        Insert a2;
        Insert u;
        Provider_Orders__c po = new Provider_Orders__c(Patient_Name__c=a.Id);
        Insert po;
        String accid = a.id;

        System.runAs(u) {
            PatientReferrals paref = new PatientReferrals();
            // Assert that list size is 1, and only got record associated with account
            List<Provider_Orders__c> getPO = PatientReferrals.getOrders(accid);
            System.assert(getPO.size()==1);
        }
        
        Test.stopTest();
    }

    

    // Commented out by SD from Synaptic on 7/28/2021. 
    // Reworked test classes above.

    // @IsTest
    // static void testGetReferralInfos() {
	//  	RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
    //     Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    //     Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
    //     Skill_Group__c skg = new Skill_Group__c(Name='123DPC BR CREEK PC APPT SG', Skill_Group__c = '123DPC_BR_CREEK_PC_APPT_SG');
    //     User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');
	// 	Referral_Information__c ref = new Referral_Information__c(Patient_Name__c = a.Id);
	// 	Recalls__c rec = new Recalls__c(Patient_Name__c = a.Id);
	// 	Provider_Orders__c pvo = new Provider_Orders__c(Patient_Name__c = a.Id);
	// 	Wait_List__c wal = new Wait_List__c(Patient_Name__c = a.Id);
    //     try {
    //         Test.startTest();
    //         Insert a;
    //         Insert u;
    //         Insert skg; 
    //         Insert ref;
    //         Insert rec;
    //         Insert pvo;
    //         Insert wal;
    //         Agent_Skill_Group__c askg = new Agent_Skill_Group__c(Agent__c = u.Id, Skill_Group__c= skg.Id);
    //         Insert askg;
    //         String accid = a.id;
    //         System.runAs(u) {
    //             PatientReferrals paref = new PatientReferrals();
    //             List<Referral_Information__c> getRef = PatientReferrals.getReferralInfos(accid);
    //             System.assert(getRef.size()==1);
    //             List<Recalls__c> getRec = PatientReferrals.getRecalls(accid);
    //             List<Provider_Orders__c> getPvo = PatientReferrals.getOrders(accid);
    //             List<Wait_List__c> getWal = PatientReferrals.getWaitlists(accid);
    //         }
    //         Test.stopTest();
    //     } catch(exception e) {
            
    //     }
    // }
    // @IsTest
    // static void testGetWaitlists() {
	//  	RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
	//     Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    //     Account a2 = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__c = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
    //     Skill_Group__c skg = new Skill_Group__c(Name='123DPC BR CREEK PC APPT SG', Skill_Group__c = '123DPC_BR_CREEK_PC_APPT_SG');
    //     User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');
	// 	Wait_List__c wal = new Wait_List__c(Patient_Name__c = a2.Id);
    //     try {
    //         Test.startTest();
    // //        Insert a;
    //         Insert a2;
    //         Insert u;
    //         Insert skg; 
    //         Insert wal;
    //         Agent_Skill_Group__c askg = new Agent_Skill_Group__c(Agent__c = u.Id, Skill_Group__c= skg.Id);
    //         Insert askg;
    // //        String accid = a.id;
    //         String acc2id = a2.id;
    //         MockHttpResponseGenerator fakeResponse = new MockHttpResponseGenerator(200,
    //                                                  'Complete',
    //                                                  '[{"Name": "HealthMaintController"}]',
    //                                                  null);
    //         Test.setMock(HttpCalloutMock.class, fakeResponse);
    //         System.runAs(u) {
    //             PatientReferrals paref = new PatientReferrals();
    // //        	PatientReferrals.getWaitlists(accid);
    //             PatientReferrals.getWaitlists(acc2id);
    //         }
    //         Test.stopTest();
    //     } catch(exception e) {
            
    //     }

    // }
}