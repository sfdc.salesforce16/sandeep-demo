/**
 * @author Ian McNear
 * @date 05.05.2021
 * @group DCRC
 * @testCoverage 93%
 */

@isTest
public with sharing class ApiRegistrantTest {
    // Testing with a successful payload
    public testMethod static void testRegistrantPostSuccess() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/session/registrant';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Session__c session = [SELECT Id FROM Session__c LIMIT 1];

        Test.startTest();
        ApiRegistrant.PostResponseWrapper wrapper = ApiRegistrant.createRegistrant(
            session.Id, 'Mr.', 'Harry', 'James', 'Potter', null, 
            '4 Privet Drive', null, 'Surrey', 'NC', '28000', 
            'harry.potter@duke.edu', '0000000', 2, null, 'Owl', 'No',
            null, null, null, null, null
        );
        Test.stopTest();

        // Testing to make sure we successfully get one session.
        System.assertEquals('Success', wrapper.status, 'Incorrect. Response did not return success.');
        System.assertNotEquals(null, wrapper.registrantId, 'Incorrect. Registrant Id should not be null.');
    }

    // Testing with a bad input
    public testMethod static void testRegistrantPostInvalidInput() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/session/registrant';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Session__c session = [SELECT Id FROM Session__c LIMIT 1];

        Test.startTest();
        ApiRegistrant.PostResponseWrapper wrapper = ApiRegistrant.createRegistrant(
            session.Id, 'Mr.', 'Harry', 'James', null, null, 
            '4 Privet Drive', null, 'Surrey', 'NC', '28000', 
            'harry.potter@duke.edu', '0000000', 2, null, 'Owl', 'Yes',
            null, null, null, null, null
        );
        Test.stopTest();

        // Testing to make sure we successfully get one session.
        System.assertEquals('Error', wrapper.status, 'Incorrect. Response did not return an error.');
        System.assertEquals(null, wrapper.registrantId, 'Incorrect. Registrant Id should be null.');
    }

    // Testing with too many guests
    public testMethod static void testRegistrantPostOverfill() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/session/registrant';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Session__c session = [SELECT Id FROM Session__c LIMIT 1];

        Test.startTest();
        ApiRegistrant.PostResponseWrapper wrapper = ApiRegistrant.createRegistrant(
            session.Id, 'Mr.', 'Harry', 'James', 'Potter', null, 
            '4 Privet Drive', null, 'Surrey', 'NC', '28000', 
            'harry.potter@duke.edu', '0000000', 50, null, 'Owl', 'Yes',
            null, null, null, null, null
        );
        Test.stopTest();

        // Testing to make sure we successfully get one session.
        System.assertEquals('Error', wrapper.status, 'Incorrect. Response did not return an error.');
        System.assertEquals(null, wrapper.registrantId, 'Incorrect. Registrant Id should be null.');
    }

    // Testing with too many guests
    public testMethod static void testRegistrantPostInvalidSessionId() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/session/registrant';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        ApiRegistrant.PostResponseWrapper wrapper = ApiRegistrant.createRegistrant(
            '00012345', 'Mr.', 'Harry', 'James', 'Potter', null, 
            '4 Privet Drive', null, 'Surrey', 'NC', '28000', 
            'harry.potter@duke.edu', '0000000', 2, null, 'Owl', 'No',
            null, null, null, null, null
        );
        Test.stopTest();

        // Testing to make sure we successfully get one session.
        System.assertEquals('Error', wrapper.status, 'Incorrect. Response did not return an error.');
        System.assertEquals(null, wrapper.registrantId, 'Incorrect. Registrant Id should be null.');
    }

    @TestSetup
    static void init() {
        List<Session__c> sessionList = new List<Session__c>();

        Id dcrcRecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Session__c' AND DeveloperName = 'Event' LIMIT 1].Id;

        sessionList.add(new Session__c(
            Name = 'Test Session 1', 
            State__c = 'NC', 
            Zip_Code__c = '27517', 
            Address__c = '100 Duke Blue Way', 
            Facility__c = 'Duke Hospital', 
            Event_Fee__c = 'Free',
            Description__c = 'Test Session Description', 
            Available_Openings__c = 20, 
            Scheduled_Start__c = Datetime.now().addDays(3), 
            Scheduled_End__c = Datetime.now().addDays(4), 
            Status__c = 'Active', 
            City__c = 'Durham',
            RecordTypeId = dcrcRecordTypeId
        ));

        insert sessionList;
    }
}