/**
 * Created by dcano on 5/24/21.
 */

@IsTest
private class CoveragesPojoTest
{
    @IsTest
    static void testBehavior()
    {
        Test.startTest();

        CoveragesPojo pojo = new CoveragesPojo();
        pojo.epicId = 'Something';
        pojo.GroupNumber = 'Something';
        pojo.GroupName = 'Something';
        pojo.MedicareCoverageType = 'Something';
        pojo.EffectiveFromDate = 'Something';
        pojo.EffectiveToDate = 'Something';
        pojo.PayorName = 'Something';
        pojo.PlanName = 'Something';
        pojo.Subscriber = 'Something';
        pojo.Status = 'Something';
        pojo.PayorID = 'Something';
        pojo.PlanID = 'Something';

        System.debug('Field Value: ' + pojo.epicId);
        System.debug('Field Value: ' + pojo.GroupNumber);
        System.debug('Field Value: ' + pojo.GroupName);
        System.debug('Field Value: ' + pojo.MedicareCoverageType);
        System.debug('Field Value: ' + pojo.EffectiveFromDate);
        System.debug('Field Value: ' + pojo.EffectiveToDate);
        System.debug('Field Value: ' + pojo.PayorName);
        System.debug('Field Value: ' + pojo.PlanName);
        System.debug('Field Value: ' + pojo.Subscriber);
        System.debug('Field Value: ' + pojo.Status);
        System.debug('Field Value: ' + pojo.PayorID);
        System.debug('Field Value: ' + pojo.PlanID);

        Test.stopTest();
    }
}