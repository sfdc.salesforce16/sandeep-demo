public with sharing class HealthMaint {
    public HealthMaint() {}

    @AuraEnabled
    public string ID { get; set; }    

    @AuraEnabled
    public string Name { get; set; }    

    @AuraEnabled
    public string DueDate { get; set; }    

    @AuraEnabled
    public string Status { get; set; }    
    
    @AuraEnabled
    public string LastSatisfactionDate { get; set; }    

}