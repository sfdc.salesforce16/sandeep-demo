@isTest
public with sharing class DismissalControllerTest 
{
    static void testDismissalController() 
    {
        Account acct = new Account();
        acct.Name = 'Blah blah';
        insert acct;

        Test.startTest();

        DismissalController.callEpicDismissal(acct.Id);

        Test.stopTest();
    }
}