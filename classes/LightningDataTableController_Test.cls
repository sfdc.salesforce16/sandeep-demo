/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class provides unit tests to cover the HealthNotificationCenterController apex class.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Synaptic
 * @project     DHAS
 * @since       February 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

@isTest
public class LightningDataTableController_Test 
{   
    @IsTest
    static void testLightningDataTableController() 
    {
	 	Test.startTest();

         //LightningDataTableController controller = new LightningDataTableController();
         LightningDataTableController.DataTableResponse response = LightningDataTableController.getSobjectRecords('Fyi_Flag__c', 'Scheduler_App_Related_List', '', 'Fyi_Flag_Status__c = \'Active\'', '', true, 'Name');

        Test.stopTest();
    }

    @IsTest
    static void testLightningDataTableController2() 
    {
	 	Test.startTest();

         //LightningDataTableController controller = new LightningDataTableController();
         LightningDataTableController.DataTableResponse response = LightningDataTableController.getSobjectRecords('MemberPlan', 'Patient_Page_Related_List', '', 'createddate < NOW()', '', true, 'Name');

        Test.stopTest();
    }
    
}