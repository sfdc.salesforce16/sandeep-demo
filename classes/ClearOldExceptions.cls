global class ClearOldExceptions implements Schedulable {
    global void execute(SchedulableContext ctx) {
        List<DHAS_Exception__c> exceptions = [SELECT Id
            FROM DHAS_Exception__c
            WHERE CreatedDate <  LAST_N_DAYS:1];

        delete exceptions;
    }
}