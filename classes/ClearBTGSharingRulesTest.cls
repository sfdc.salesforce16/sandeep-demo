/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class provides unit tests to cover the ClearBTGSharingRules apex class.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Synaptic
 * @project     DHAS
 * @since       April 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

@isTest
public class ClearBTGSharingRulesTest {

    @IsTest
    static void testClearBTG() {
		String personRtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
		Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = personRtId);
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(FirstName='Test', LastName='Last',UserName='standarduser@testorgxyz.edu', Email = 'tester123@test.com',Alias = '12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',EmailEncodingKey = 'ISO-8859-1',ProfileId = p.Id, LanguageLocaleKey = 'en_US');
        try{
            insert a;
            insert u;
			AccountShare accShare = new AccountShare(AccountId = a.Id, 
                                                     UserOrGroupId = u.Id, 
                                                     AccountAccessLevel = 'Edit',
                                                    OpportunityAccessLevel = 'Edit',
                                                    CaseAccessLevel = 'Edit');
            insert accShare;
            Break_the_Glass_Request__c btg = new Break_the_Glass_Request__c(Account_Share_ID__c = accShare.Id, Date_of_Request__c = date.ValueOf('2020-12-05'), Permission_Expiration_Date__c = date.ValueOf('2020-12-05'));
            insert btg;
            system.debug(btg);
            ClearBTGSharingRules cbgs = new ClearBTGSharingRules();
			String Sched = Datetime.now().addSeconds(1).format('s m H d M ? yyyy');  
        	string mySched = System.schedule('My Scheduled Job', Sched, cbgs);
            system.debug(mySched);
        } catch (exception e){
            system.debug(e);
        }
    }
}