/**
 * Created by dcano on 5/26/21.
 */

@IsTest
private class GetContactIdTest
{
    @IsTest
    static void testBehavior()
    {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];

        Account a = new Account(
                FirstName = 'Bob',
                LastName = 'Wayne',
                RecordTypeId = rt.Id
        );

        a.Email_Verified__c = false;
        a.Contact_Information_Verified__c = false;
        a.Address_Verified__c = false;
        a.Special_Needs_Verified__c = false;
        a.Interpreter_Needed_Verified__c = false;
        a.Guarantor_Verified__c = false;
        a.PCP_Verified__c = false;
        a.Coverage_Verified__c = false;

        insert a;

        Test.startTest();

        GetContactId plugin = new GetContactId();

        Map<String, Object> inputParams = new Map<String, Object>();
        inputParams.put('accountId', a.Id);

        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        GetContactId.invoke(request);
        plugin.describe();

        Test.stopTest();
    }
}