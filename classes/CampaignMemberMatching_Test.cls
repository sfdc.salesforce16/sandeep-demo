@isTest
public class CampaignMemberMatching_Test {

    @isTest
    static void nullTest() {
        List<CampaignMember> result = CampaignMemberMatching.getCampaign(new List<Id>());
    }

    @isTest
    static void otherIdTest() {
        Id uId = UserInfo.getUserId();
        List<CampaignMember> result = CampaignMemberMatching.getCampaign(new List<Id>{uId, uId});
    }
}