@isTest
public class Form_TriggerHandler_Test {
    private static Id personAcctRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'PersonAccount'].Id;
    
    @testSetup
    static void setup() {
        
        // Program
        CareProgram p = new CareProgram(
        	Name = 'Fitness Center Membership',
        	Price__c = 100,
        	Deposit__c = 5,
            Duration__c = 7
        );
        insert new List<CareProgram>{p};
            
        // insert Form Template for forms
        disco__Form_Template__c t = new disco__Form_Template__c(
            Name = 'DHFC Membership Application', 
            disco__Status__c = 'In Use'
        );
        insert t;
            
        // Account 
        Account a = new Account(
            FirstName = 'Test',
            LastName = 'Test1',
            Phone = '(999) 999-9999',
            BillingStreet = '123 Fake St',
            BillingCity = 'Star City',
            BillingPostalCode = '12345',
            BillingState = 'CA',
            RecordTypeId = personAcctRecordTypeId,
            PersonEmail = 'testuser@test.com',
            Email__c = 'testuser@test.com'
        ); insert a;
        
        // create Application
        Program_Application__c app = new Program_Application__c(
            Member__c = a.Id,
            Program__c = p.Id
        );
        insert app;
        
        // make extra Forms
        List<disco__Form__c> allForms = new List<disco__Form__c>();
        disco__Form__c f = new disco__Form__c(
            Program_Application__c = app.Id,
            disco__Form_Template__c = t.Id,
            disco__Form_Account__c = a.Id
        );
        allForms.add(f);
        disco__Form__c f2 = new disco__Form__c(
            Program_Application__c = app.Id,
            disco__Form_Template__c = t.Id,
            disco__Form_Account__c = a.Id,
            disco__Original_Submitted_Date__c = System.today()
        );
        allForms.add(f2);
        insert allForms;
    }
    
    @isTest
    static void completeOneForm() {
        disco__Form__c f = [SELECT Id FROM disco__Form__c WHERE disco__Original_Submitted_Date__c = null LIMIT 1];
        f.disco__Original_Submitted_Date__c = System.today();
        Test.startTest();
        update f;
        Test.stopTest();
    }
    
    @isTest
    static void completeAnotherForms() {
        List<disco__Form__c> allForms = new List<disco__Form__c>();
        for(disco__Form__c f : [SELECT Id FROM disco__Form__c WHERE disco__Original_Submitted_Date__c = null]) {
            f.disco__Original_Submitted_Date__c = System.today();
            allForms.add(f);
        }
        
        Test.startTest();
        update allForms;
        Test.stopTest();
    }
    
    @isTest
    static void completeAllForms() {
        List<disco__Form__c> allForms = [SELECT Id FROM disco__Form__c WHERE disco__Original_Submitted_Date__c = null];
        allForms[0].disco__Original_Submitted_Date__c = System.today();
        
        Test.startTest();
        update allForms;
        Test.stopTest();
    }
}