/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class provides unit tests to cover the EhrRelatedPerson Trigger and 
 * Guarantor_TriggerHandler apex class.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Synaptic
 * @project     DHAS
 * @since       February 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

 @isTest
public class EhrRelatedPersonTrigger_Test {
    
    @IsTest
    static void testTrigger() {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        List<Account> listOfAccounts = new List<Account>();
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        listOfAccounts.add(a);
        insert listOfAccounts;
        system.debug(a);
        
        List<HealthCloudGA__EhrRelatedPerson__c> listOfGuars = new List<HealthCloudGA__EhrRelatedPerson__c>();
        HealthCloudGA__EhrRelatedPerson__c ehrP = new HealthCloudGA__EhrRelatedPerson__c(Responsible_Party__c = 'SLF', HealthCloudGA__Account__c = a.Id);
        ehrP.Sex__c = 'M';
        ehrP.Account_Status__c = 'Internationa'; // added by Karthik
        ehrP.Account_Status_Youreka__c = 'International'; // added by Karthik
        ehrP.Selected_For_Registration__c = TRUE; // added by Karthik
        ehrP.Sex_Youreka__c = 'Male'; // added by Karthik
        ehrP.self_pay__c = true; // added by Roy Lou
        ehrP.active__c = true; // added by Roy Lou
    	listOfGuars.add(ehrP);
        //Added by Karthik
        HealthCloudGA__EhrRelatedPerson__c ehrP2 = new HealthCloudGA__EhrRelatedPerson__c(Responsible_Party__c = 'SLF', HealthCloudGA__Account__c = a.Id);
        ehrP2.Sex__c = 'M';        
        ehrP2.Selected_For_Registration__c = TRUE; // added by Karthik
        ehrP2.Account_Type__c = 'HiTech'; // added by Karthik
        ehrP2.Sex_Youreka__c = 'Male'; // added by Karthik
        ehrP2.self_pay__c = false; // added by Roy Lou
        ehrP2.active__c = false; // added by Roy Lou
    	listOfGuars.add(ehrP2);
        // Ended by Karthik
        try{
            insert listOfGuars;
	        system.debug(ehrP);
            
            ehrP = listOfGuars[0];
            ehrP.County__c = 'Clackamas';
            ehrP.Sex__c = 'F';
            ehrP.Account_Status__c = 'Internationa'; // added by Karthik
            ehrP.Sex_Youreka__c = 'Female'; // added by Karthik
            ehrP.Account_Type__c = 'HiTech'; // added by Karthik
            ehrP.Responsible_Party__c = 'SPO';
            ehrP.Account_Status__c = 'RM'; // added by Karthik
        	ehrP.Account_Status_Youreka__c = 'Returned Mail';   // added by Karthik      
            update ehrP;
            
            // Added by Karthik
            ehrP2 = listOfGuars[1];
            ehrP2.Account_Status__c = 'Internationa';     
            ehrP2.self_pay__c = true; // added by Roy Lou
            ehrP2.active__c = true; // added by Roy Lou
            update ehrP2;
            
            // Ended by Karthik
	        system.debug(ehrP2);
        }
        catch(DMLException e){
            System.debug(e);
        }

    }    
}