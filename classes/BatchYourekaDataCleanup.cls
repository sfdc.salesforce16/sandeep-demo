/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Batch class which deletes all Youreka data older than 30 days
* FOR LOWER ENVIRONMENTS ONLY
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Shelby Dremely - Synaptic Advisors
* @modifiedBy     Shelby Dremely - Synaptic Advisros
* @version        1.0
* @created        2021-08-20
* @modified       2021-08-20
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.0            Shelby Dremely
* 2021-08-04      Created schedulable batchable apex
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

global class BatchYourekaDataCleanup implements Database.Batchable<string>, Database.Stateful, Schedulable {
    
    global boolean bReRun = false; //will be used to determine if batch has to re-run in case there are more that 10K of records
    
    global Iterable<String> start(Database.BatchableContext bc) {
        return new list<String> { 'disco__Answer__c', 'disco__Question_Value__c'}; //list of strings with my object names

    }

    global void execute(Database.BatchableContext bc, list<string> lstsObjectName){
        list<sObject> lstDeleteRecords = new list<sObject>();
        for(string strObjectName : lstsObjectName) {
            for(sObject objsObject : database.query('Select Id from ' + strObjectName + ' where CreatedDate < LAST_N_DAYS:30 ORDER BY CreatedDate ASC')) {
                if(lstDeleteRecords.size() < 9998)
                    lstDeleteRecords.add(objsObject);
                else {
                    bReRun = true;
                    break;
                }
            }
        }
        lstDeleteRecords.sort();
        delete lstDeleteRecords;
    }    
    global void finish(Database.BatchableContext bc){
        // Execute any post-processing operations
        if(bReRun) {
            Database.executebatch(new BatchYourekaDataCleanup());
       }
    }  

    global void execute(SchedulableContext sc) {
        BatchYourekaDataCleanup b = new BatchYourekaDataCleanup();
        database.executebatch(b);
    }
}