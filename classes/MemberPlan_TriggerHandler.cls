/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description This class implements the TriggerHandler Framework for the MemberPlan object.  
* Syncs Youreka picklist fields for Sex and Relationship to Subscriber with the corresponding 
* MemberPlan picklist fields mapped to the integration.
*/
 public with sharing class MemberPlan_TriggerHandler extends TriggerHandler {
    /*******************************************************************************************************
    * @description Constructor
    */
    public MemberPlan_TriggerHandler() {

    }

    /*******************************************************************************************************
    * @description Trigger handler for beforeInsert
    */
    public override void beforeInsert(){
        System.debug('beforeInsert');
        insertSyncRelationship();
        insertSyncSex();
        MapContactToSubscriber();
    }

    /*******************************************************************************************************
    * @description Trigger handler for beforeUpdate
    */
    public override void beforeUpdate(){
        System.debug('beforeUpdate');
        MapContactToSubscriber();
        updateSyncRelationshipToSubscriber();
        updateSyncSex();
    }

    public override void afterUpdate(){
        
    }

     
    /*******************************************************************************************************
    * @description When MemberPlan is created, sync Sex__c and Sex_Youreka__c
    */
    private static void insertSyncSex(){

        for(MemberPlan mbp : (List<MemberPlan>)Trigger.new){
            if(mbp.Sex__c != null){
                Schema.DescribeFieldResult fieldResult = MemberPlan.Sex__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == mbp.Sex__c){
                        mbp.Sex_Youreka__c = pickListVal.getLabel();
                    }
                }
                System.debug('mbp.Sex_Youreka__c: ' + mbp.Sex_Youreka__c);
            }
            
            if(mbp.Sex_Youreka__c != null){
                Schema.DescribeFieldResult fieldResult = MemberPlan.Sex__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == mbp.Sex_Youreka__c){
                    	mbp.Sex__c = pickListVal.getValue();
                    }
                }
                System.debug('mbp.Sex__c: ' + mbp.Sex__c);
            }
        }
    }


    /*******************************************************************************************************
    * @description When MemberPlan is created, sync RelationshipToSubscriber and Relationship_to_Subscriber_Youreka__c
    */
    private static void insertSyncRelationship(){

        for(MemberPlan mbp : (List<MemberPlan>)Trigger.new){
            if(mbp.RelationshipToSubscriber != null){
                Schema.DescribeFieldResult fieldResult = MemberPlan.RelationshipToSubscriber.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == mbp.RelationshipToSubscriber){
                        mbp.Relationship_to_Subscriber_Youreka__c = pickListVal.getLabel();
                    }
                }
                System.debug('mbp.Relationship_to_Subscriber_Youreka__c: ' + mbp.Relationship_to_Subscriber_Youreka__c);
            }
            
            if(mbp.Relationship_to_Subscriber_Youreka__c != null){
                Schema.DescribeFieldResult fieldResult = MemberPlan.RelationshipToSubscriber.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == mbp.Relationship_to_Subscriber_Youreka__c){
                    	mbp.RelationshipToSubscriber = pickListVal.getValue();
                    }
                }
                System.debug('mbp.RelationshipToSubscriber: ' + mbp.RelationshipToSubscriber);
            }
        }
    }

    /*******************************************************************************************************
    * @description When MemberPlan is created and Relationship to Subscriber = 'Self', the MemberPlan fields (SSN, Address, City, Country, Birthdate, Sex)
        are populated with field data from the Contact record associated with the Patient Account. 

        If the Relationship to Subscriber != 'Self', then a new contact record is created and its fields are populated with field date from
        the New_ MemberPlan record fields (New_Subscriber_Name__c, New_Subscriber_Birthdate__c, New_Subscriber_RelationshipToSubscriber, New_Subscriber_SSN__c, 
        New_Subscriber_ID__c). Then the Contact record is associated with the MemberPlan record (via mbp.SubscriberId = c.Id;)
    */
    private static void MapContactToSubscriber(){
        System.debug('MapContactToSubscriber');

        for(MemberPlan mbp : (List<MemberPlan>)Trigger.new){
            System.debug('mbp.RelationshipToSubscriber ' + mbp.RelationshipToSubscriber);
            if(mbp.RelationshipToSubscriber == 'Self'){
                try {
                    String MemberId = mbp.MemberId;
                    System.debug('MemberId ' + MemberId);
                    Account a = [SELECT Id, Name, Date_of_Birth__c, Gender__c, SSN__c FROM Account WHERE Id = :MemberId];
                    System.debug('a: ' + a);
                    mbp.Subscriber_Name__c = a.Name;
                    mbp.SSN__c = a.SSN__c;
                    mbp.Birthdate__c = a.Date_of_Birth__c;
                    mbp.Sex__c = a.Gender__c;
                } catch (Exception ex) {
                    System.debug('CopyGuarantorRecord.exception: ' + ex);
                } 
            }
            // } else if((mbp.RelationshipToSubscriber != 'Self') && 
            // (mbp.Create_new_Subscriber__c == true)){
            //     System.debug('not self');
            //     Contact c = new Contact();
            //     c.FirstName = mbp.New_Subscriber_Name__c.split(' ')[0];
            //     c.LastName = mbp.New_Subscriber_Name__c.split(' ')[1];
            //     c.HealthCloudGA__PreferredName__c = mbp.New_Subscriber_Name__c;
            //     c.Birthdate = mbp.New_Subscriber_Birthdate__c;
            //     c.HealthCloudGA__Gender__c = mbp.New_Subscriber_Sex__c;
            //     c.SSN__c = mbp.New_Subscriber_SSN__c;
            //     c.Subscriber_ID__c = mbp.New_Subscriber_ID__c;
            //     insert c;
            //     //mbp.SubscriberId = c.Id;
            //     mbp.Subscriber_Contact__c = c.Id;
            // }
        }
    }

    /*******************************************************************************************************
    * @description When MemberPlan is updated, sync RelationshipToSubscriber and Relationship_to_Subscriber_Youreka__c
    */
    private static void updateSyncRelationshipToSubscriber(){
        for(MemberPlan mbp : (List<MemberPlan>)Trigger.newMap.values()){
            MemberPlan oldMbp = (MemberPlan)Trigger.oldMap.get(mbp.ID);
            MemberPlan newMbp = (MemberPlan)Trigger.newMap.get(mbp.ID);
            
            if(newMbp.RelationshipToSubscriber != oldMbp.RelationshipToSubscriber){
                Schema.DescribeFieldResult fieldResult = MemberPlan.RelationshipToSubscriber.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == newMbp.RelationshipToSubscriber){
                        newMbp.Relationship_to_Subscriber_Youreka__c = pickListVal.getLabel();
                    }
                }             
            }

            if(newMbp.Relationship_to_Subscriber_Youreka__c != oldMbp.Relationship_to_Subscriber_Youreka__c){
                Schema.DescribeFieldResult fieldResult = MemberPlan.RelationshipToSubscriber.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == newMbp.Relationship_to_Subscriber_Youreka__c){
                        string pickVa = pickListVal.getValue();
                        newMbp.RelationshipToSubscriber = pickVa;
                    }
                }
            }
        }
    }

    /*******************************************************************************************************
    * @description When MemberPlan is updated, sync Sex__c and Sex_Youreka__c
    */
    private static void updateSyncSex(){
        for(MemberPlan mbp : (List<MemberPlan>)Trigger.newMap.values()){
            MemberPlan oldMbp = (MemberPlan)Trigger.oldMap.get(mbp.ID);
            MemberPlan newMbp = (MemberPlan)Trigger.newMap.get(mbp.ID);
            
            if(newMbp.Sex__c != oldMbp.Sex__c){
                Schema.DescribeFieldResult fieldResult = MemberPlan.Sex__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == newMbp.Sex__c){
                        newMbp.Sex_Youreka__c = pickListVal.getLabel();
                    }
                }             
            }

            if(newMbp.Sex_Youreka__c != oldMbp.Sex_Youreka__c){
                Schema.DescribeFieldResult fieldResult = MemberPlan.Sex__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == newMbp.Sex_Youreka__c){
                        string pickVa = pickListVal.getValue();
                        newMbp.Sex__c = pickVa;
                    }
                }
            }
        }
    }
}