public class SessionController {
    @AuraEnabled(cacheable=true)
    public static List<Session__c> fetchChildSessions(String parentSessionId) {
        List<Session__c> sessions = [
            SELECT Id, Scheduled_End__c, Scheduled_Start__c, Location__c, Parent_Session__c FROM Session__c
            WHERE Parent_Session__c = :parentSessionId            
        ];
        System.debug(sessions);
        return sessions;
    }

    @AuraEnabled()
    public static void deleteChildSessions(String SessionId) {
        system.debug('SessionId'+SessionId);    
    database.delete(SessionId);
    }


    @AuraEnabled
    public static List<Session__c>  updateSessions(Object data, String parentSessionId) {
        Session__c parentSession = [SELECT Id, Campaign__c, Name, Description__c, 
                                            Program__c, Scheduled_Start__c, Session_Details__c, Session_Link__c, 
                                            Available_Openings__c, Category__c, 
                                            HTML_Description__c, DoNotUse__c, Event_Type__c, Parent_Session__c, Scheduled_End__c, 
                                            Speakers__c, Status__c, Virtual__c, 
                                            Address_2__c, Address__c, City__c, Facility__c, Location__c, 
                                            Phone_Number__c, Session_Link_Text__c, Zip_Code__c, Event_Host__c, Event_Fee__c 
                                            FROM Session__c WHERE Id = :parentSessionId LIMIT 1];

        
        
        List<Session__c> sessionsForUpdate = (List<Session__c>) JSON.deserialize(
            JSON.serialize(data),
            List<Session__c>.class
        );
        for(Session__c sessionRec: sessionsForUpdate){
            sessionRec.Name = parentSession.Name;
            sessionRec.Campaign__c = parentSession.Campaign__c;
            sessionRec.Description__c = parentSession.Description__c;
            //sessionRec.Scheduled_Start__c = parentSession.Scheduled_Start__c;
            sessionRec.Session_Details__c = parentSession.Session_Details__c;
            sessionRec.Session_Link__c = parentSession.Session_Link__c;
            sessionRec.Available_Openings__c = parentSession.Available_Openings__c;
            sessionRec.Category__c = parentSession.Category__c;
            sessionRec.HTML_Description__c = parentSession.HTML_Description__c;
            sessionRec.DoNotUse__c = parentSession.DoNotUse__c;
            sessionRec.Event_Type__c = parentSession.Event_Type__c;
            //sessionRec.Scheduled_End__c = parentSession.Scheduled_End__c;
            sessionRec.Speakers__c = parentSession.Speakers__c;
            sessionRec.Status__c = parentSession.Status__c;
            sessionRec.Virtual__c = parentSession.Virtual__c;
            sessionRec.Address_2__c = parentSession.Address_2__c;
            sessionRec.Address__c = parentSession.Address__c;
            sessionRec.City__c = parentSession.City__c;
            sessionRec.Facility__c = parentSession.Facility__c;
            //sessionRec.Location__c = parentSession.Location__c;
            sessionRec.Phone_Number__c = parentSession.Phone_Number__c;
            sessionRec.Session_Link_Text__c = parentSession.Session_Link_Text__c;
            sessionRec.Zip_Code__c = parentSession.Zip_Code__c;
            sessionRec.Event_Host__c = parentSession.Event_Host__c;
            sessionRec.Event_Fee__c = parentSession.Event_Fee__c;            
        }
        System.debug(sessionsForUpdate);
        try {
            UPSERT sessionsForUpdate;
            return [
                SELECT Id, Scheduled_End__c, Scheduled_Start__c, Location__c, Parent_Session__c FROM Session__c
                WHERE Parent_Session__c = :parentSessionId            
            ];
            //return 'Success: sessions updated successfully';
        }
        catch (Exception e) {
            throw new AuraHandledException('The following exception has occurred: ' + e.getMessage());
            //throw new 'The following exception has occurred: ' + e.getMessage();
        }
    }
}