/**
* @author Synaptic
* @date 2014
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description A controller of Lightning Component. It provides @AuraEnabled methods to check if an account has MRN or not.
*/
public with sharing class RefreshOnMrnController {
    /*******************************************************************************************************
    * @description Returns a boolean to indicate an account has MRN or not
    * @param accountId account's sfid
    * @return if HealthCloudGA__MedicalRecordNumber__c exists for a given account Id
    */
    @AuraEnabled
    public static Boolean hasMrn(Id accountId){
    System.debug('accountId: ' + accountId);
        Account a = [SELECT HealthCloudGA__MedicalRecordNumber__c FROM Account WHERE Id = :accountId LIMIT 1];

        return !(a.HealthCloudGA__MedicalRecordNumber__c == null); 
   }
}