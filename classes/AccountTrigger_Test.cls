/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class provides unit tests to cover the Account Trigger and Account_TriggerHandler apex class.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Synaptic
 * @project     DHAS
 * @since       February 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

@isTest
public class AccountTrigger_Test {
    
    @IsTest
    static void testAccountTrigger() {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        
        List<Account> listOfAccounts = new List<Account>();
        
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        a.Email_Verified__c = false;
        a.Contact_Information_Verified__c = false;
        a.Address_Verified__c = false;
        a.Special_Needs_Verified__c = false;
        a.Interpreter_Needed_Verified__c = false;
        a.Interpreter_Needed__c = 'Y';
        a.Guarantor_Verified__c = false;
        a.PCP_Verified__c = false;
        a.Coverage_Verified__c = false;
//        a.Gender_Identity__c = 'FTM';
        a.Gender_Identity_Youreka__c = 'Female';
        //a.Special_Needs_Multi__c = 'Hearing;None';
		a.Special_Needs_Youreka__c = 'None;Hearing Impaired';
        a.Religion__c = 'Catholic';
        listOfAccounts.add(a);
        
        Account a2 = new Account(FirstName = 'Peter', LastName = 'Parker', Date_of_birth__c=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', RecordTypeId = rt.Id);
//        listOfAccounts.add(a2);

        a.PersonBirthdate=date.ValueOf('2020-12-01');
        a.Email_Verified__c = true;
        a.Date_of_birth__c = date.ValueOf('2020-12-03');
        a.Special_Needs_Verified__c = true;
        a.Contact_Information_Verified__c = true;
        a.Address_Verified__c = true;
		a.Special_Needs_Verified__c = true;
        a.Interpreter_Needed_Verified__c = true;
        a.Guarantor_Verified__c = true;
        a.PCP_Verified__c = true;
        a.Coverage_Verified__c = true;
//        a.Special_Needs_Multi__c = 'Hearing';
        a2.PersonBirthdate=date.ValueOf('2020-12-01');
        a2.Race__c = 'W';
        a2.HealthCloudGA__Gender__pc = 'M';
        a2.HealthCloudGA__PrimaryLanguage__pc = 'Malay';
        a2.Interpreter_Needed__c = 'Y';
        a2.Special_Needs_Multi__c = 'None';
        a2.Ethnicity__c = 'O';
        a2.Religion__c = 'CAT';
        a2.Gender_Identity__c = 'FTM';
        a2.Marital_Status__pc = 'S';
        a2.VeteranStatus__c = 'Y';
        a2.Patient_Status__c = 'Deceased';
        insert a;

        Referral_Information__c ref = new Referral_Information__c(Patient_Name__c = a.Id, Referral_Status__c = 'Open', Referral_Type__c = 'Worker\'s Comp', Referral_ID__c = '123456');
		insert ref;

        system.debug('ref.Id = ' + ref.Id);

        system.debug('multi special needs ' + a.Special_Needs_Multi__c);
        system.debug('youreka special needs ' + a.Special_Needs_Youreka__c);

        a.Special_Needs_Youreka__c = 'Visually Impaired;Wheelchair';
        update a;

        insert a2;
        a2.Race__c = 'B';
        a2.HealthCloudGA__Gender__pc = 'U';
        a2.HealthCloudGA__PrimaryLanguage__pc = 'Akan';
        a2.Interpreter_Needed__c = 'N';
        a2.Special_Needs_Multi__c = 'None';
        a2.Ethnicity__c = 'D';
        a2.Religion__c = 'PRE';
        a2.Gender_Identity__c = 'MTF';
        a2.Marital_Status__pc = 'M';
        a2.VeteranStatus__c = 'N';
        a2.Patient_Status__c = 'Alive';
        update a2;

        a.Email_Verified__c = false;
        a.Contact_Information_Verified__c = false;
        a.Address_Verified__c = false;
        a.Special_Needs_Verified__c = false;
        a.Interpreter_Needed_Verified__c = false;
        a.Guarantor_Verified__c = false;
        a.PCP_Verified__c = false;
        a.Coverage_Verified__c = false;
        update a2;      
    }    
}