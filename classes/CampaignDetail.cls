public without sharing class CampaignDetail {
    @AuraEnabled
    @InvocableVariable(label='Campaign Id')
    public List<String> CampaignId;

    @InvocableMethod(label='Campaign Details' description='Get Campaign Details by Campaign Id.' category='Campaign')
    public static List<Campaign> getCampaign(List<String> CampaignId) {
        List<Campaign> CampaignList = [SELECT Id,Name FROM Campaign WHERE Id = :CampaignId ] ;
        //Campaign CampaignRecord = CampaignList[0];
        return CampaignList;
  }

}