public class SchedulableBatchDeleteExpiredCoverages {
    public void execute(SchedulableContext ctx) {
        database.executeBatch( new BatchDeleteExpiredCoverages());
    }
}