/******************************************
 * 
 * @author Synaptic AP
 * @since  Feb 2021
 * Project DHAS
 */
public inherited sharing class Guarantor {
    public Guarantor() {}

    @AuraEnabled
    public string epicId {get; set;}    

    @AuraEnabled
    public string GuarantorType {get; set;} 
    
    @AuraEnabled
    public string Relationship {get; set;}    

    @AuraEnabled
    public string Active {get; set;}    

    @AuraEnabled
    public string FamilyName1 {get; set;}    

    @AuraEnabled
    public string GivenName1 {get; set;}    

    @AuraEnabled
    public List<CoveragesPojo> Coverages {get; set;}    

}