global without sharing class HasCampaignMemberFlowController implements Process.Plugin { 

    webservice static Process.PluginResult invoke(Process.PluginRequest request) {
        Map<String, String> result = new Map<String, String>();
        List<CampaignMember> campaignMemeberList = new List<CampaignMember>();
        String campaignId = (String) request.inputParameters.get('CampaignId');
        String accountId = (String) request.inputParameters.get('AccountId');
        String leadId = (String) request.inputParameters.get('LeadId');
        
        if (String.isNotBlank(accountId)) {
            campaignMemeberList = [
                SELECT Id, Name 
                FROM CampaignMember
                WHERE CampaignId = :campaignId AND Account__c <> NULL AND Account__c = :accountId
            ];
        } else if (String.isNotBlank(leadId)) {
            campaignMemeberList = [
                SELECT Id, Name 
                FROM CampaignMember
                WHERE CampaignId = :campaignId AND LeadId <> NULL AND LeadId = :leadId
            ];
        }
        
        if (campaignMemeberList.isEmpty() == TRUE) {
            result.put('result', 'NOT_FOUND'); 
        } else {
            result.put('result', 'FOUND'); 
        }

        return new Process.PluginResult(result); 
    }
    
    global Process.PluginDescribeResult describe() { 
        Process.PluginDescribeResult result = new Process.PluginDescribeResult(); 
        result.Name = 'Has Campaign Member';
        result.Tag = 'hasCampaignMember';
        result.inputParameters = new 
            List<Process.PluginDescribeResult.InputParameter>{ 
                new Process.PluginDescribeResult.InputParameter('CampaignId', Process.PluginDescribeResult.ParameterType.STRING, true),
                new Process.PluginDescribeResult.InputParameter('AccountId', Process.PluginDescribeResult.ParameterType.STRING, FALSE),
                new Process.PluginDescribeResult.InputParameter('LeadId', Process.PluginDescribeResult.ParameterType.STRING, FALSE)
            }; 
        result.outputParameters = new 
            List<Process.PluginDescribeResult.OutputParameter>{
                new Process.PluginDescribeResult.OutputParameter('result', Process.PluginDescribeResult.ParameterType.STRING)
            };
        return result;
    }
}