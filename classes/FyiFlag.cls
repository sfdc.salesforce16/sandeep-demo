/******************************************
 * 
 * @author Synaptic AP
 * @since  Feb 2021
 * Project DHAS
 */
public inherited sharing class FyiFlag {
    public FyiFlag() {}


    @AuraEnabled
    public string Summary {get; set;} 
    
    @AuraEnabled
    public string Instant {get; set;}    

    @AuraEnabled
    public string Text {get; set;}    

    @AuraEnabled
    public string FlagActive {get; set;}    

    @AuraEnabled
    public string PatientContactID {get; set;}    

    @AuraEnabled
    public string PatientContactIDType {get; set;}    

    @AuraEnabled
    public string Type {get; set;}        
}