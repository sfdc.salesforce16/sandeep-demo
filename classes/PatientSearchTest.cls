/**
 * Created by dcano on 5/24/21.
 */

@IsTest
private class PatientSearchTest
{
    @IsTest
    static void testBehavior()
    {
        Test.startTest();

        PatientSearch pojo = new PatientSearch();
        pojo.mrn = 'Something';
        pojo.title = 'Something';
        pojo.name = 'Something';
        pojo.suffix = 'Something';
        pojo.birthDate = 'Something';
        pojo.gender = 'Something';
        pojo.address = 'Something';
        pojo.city = 'Something';
        pojo.district = 'Something';
        pojo.state = 'Something';
        pojo.postalCode = 'Something';
        pojo.country = 'Something';
        pojo.phone = 'Something';
        pojo.rank = 'Something';

        System.debug('Field Value: ' + pojo.mrn);
        System.debug('Field Value: ' + pojo.title);
        System.debug('Field Value: ' + pojo.name);
        System.debug('Field Value: ' + pojo.suffix);
        System.debug('Field Value: ' + pojo.birthDate);
        System.debug('Field Value: ' + pojo.gender);
        System.debug('Field Value: ' + pojo.address);
        System.debug('Field Value: ' + pojo.city);
        System.debug('Field Value: ' + pojo.district);
        System.debug('Field Value: ' + pojo.state);
        System.debug('Field Value: ' + pojo.postalCode);
        System.debug('Field Value: ' + pojo.country);
        System.debug('Field Value: ' + pojo.phone);
        System.debug('Field Value: ' + pojo.rank);

        Test.stopTest();
    }
}