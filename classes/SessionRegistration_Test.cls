@isTest
public class SessionRegistration_Test {
    
    @testSetup
    static void setup() {
        
        // Program
        CareProgram p = new CareProgram(
        	Name = 'Residential Weight Loss',
        	Price__c = 100,
        	Deposit__c = 5,
            Duration__c = 7
        );
        insert p;
        
        Session__c s = new Session__c(
        	Program__c = p.Id
        );
        insert s;
        
        Lead l = new Lead(
            FirstName = 'Test',
            LastName = 'Test',
            Email = 'test@test.com'
        );
        insert l;
        
        Account a = new Account(
            Name = 'Test Test',
            Email__c = 'test@test.com'
        );
        insert a;
    }
    
    @isTest
    static void test_Lead() {
        SessionRegistration r = new SessionRegistration();
        r.sessionId = [SELECT Id FROM Session__c].Id;
        r.leadId = [SELECT Id FROM Lead].Id;
        r.email = 'test@test.com';
        List<SessionRegistration> regs = new List<SessionRegistration>{r};
		SessionRegistration.findMatch(regs);
    }
    
    @isTest
    static void test_Account() {
        SessionRegistration r = new SessionRegistration();
        r.sessionId = [SELECT Id FROM Session__c].Id;
        r.accountId = [SELECT Id FROM Account].Id;
        r.email = 'test@test.com';
        List<SessionRegistration> regs = new List<SessionRegistration>{r};
		SessionRegistration.findMatch(regs);
    }
    
    @isTest
    static void test_Error() {
        SessionRegistration r = new SessionRegistration();
        r.sessionId = '123';
        r.accountId = [SELECT Id FROM Account].Id;
        r.email = 'test@test.com';
        List<SessionRegistration> regs = new List<SessionRegistration>{r};
		SessionRegistration.findMatch(regs);
    }

}