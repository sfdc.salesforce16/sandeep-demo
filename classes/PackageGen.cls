public with sharing class PackageGen {
    public static void PackageGen() {

        //List<String> sobjNames = new List<String> {'REVVY__MnQuote__c', 'REVVY__MnCatalogNode__c', 'REVVY__MnQuoteItem__c', 'REVVY__MnQuoteItemSubLine__c',
        // 'REVVY__MnPriceList__c', 'REVVY__MnPriceListLine__c', 'REVVY__MnCatalogAttributeValue__c', 'REVVY__MnConfigEngineOption__c', 'REVVY__MnConfigEngineOptionGroup__c',
        // 'REVVY__MnQiSubLineAttribute__c', 'REVVY__MnQuoteItemAttribute__c'};
        List<String> sobjNames = new List<String> {'Account','Activity','Agent_Skill_Group__c','Allergies__c','Applied_Fund__c','Break_the_Glass_Request__c','CampaignMember','Case','Contact','ContentVersion','DHAS_Exception__c','Dismissal__c','EpicLookupByMrn__e','Epic_Web_Service__mdt','FYI_Flag__c','Family_History__c','Form_Creator_Configuration__mdt','GL_Account__c','Guarantor_Self_Copy_Field_Mapping__mdt','HL7_Inbound__e','HL7_Message__c','HL7_Scenario__c','HealthCloudGA__EhrEncounter__c','HealthCloudGA__EhrRelatedPerson__c','Health_Maintenance__c','HealthcarePractitionerFacility','HealthcareProvider','Insurance_Card__c','Invoice_Item__c','Lead','Medical_History__c','Medication__c','MemberPlan','MemberPlanChangeEvent__e','Member_Visit__c','NewPatient__e','Opportunity','Order_Custom__c','Order_Item__c','Patient_Card__c','Patient_Contact__c','Permission_Assignment__mdt','PersonAccount','Product__c','Program_Application__c','Program_Enrollment__c','Provider_Orders__c','PurchaserPlan','Recalls__c','Referral_Information__c','Referrals__c','Registration_Youreka_Form_ID__mdt','Release_of_Information__c','Service_Line_Enrollment__c','Service_Line_Inquiry__c','Service_Line__c','Session_Registrant__c','Session__c','Skill_Group__c','ToEpicGuarantor__e','ToEpicNextOfKinEdit__e','ToEpicPatientEdit__e','Transaction__c','Wait_List__c','disco__Form__c'};
        system.debug('<types>');
        for(String sobjName : sobjNames) {
            if(Schema.getGlobalDescribe().get(sobjName.toLowerCase()) != null) {
                Schema.Describesobjectresult dr = Schema.getGlobalDescribe().get(sobjName.toLowerCase()).getDescribe();
                Map<String, Schema.SObjectField> fieldMap = dr.fields.getMap();
                for( String fieldName : fieldMap.keySet() ) {
                    Schema.SObjectField field = fieldMap.get( fieldName );
                    Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
                    if(fieldDescribe.isCustom()) {
                    system.debug('<members>'+sobjName + '.' + fieldDescribe.getLocalName() + '</members>' );
                    }
                }
            }
        }
        system.debug('<name>CustomField</name>');
        system.debug('</types>');
        system.debug('<types>');
        for(String sobjName : sobjNames) {
            if(Schema.getGlobalDescribe().get(sobjName.toLowerCase()) != null) {
                system.debug('<members>'+sobjName + '</members>' );
            }
        }
        
        system.debug('<name>CustomObject</name>');
        system.debug('</types>');
    }
}