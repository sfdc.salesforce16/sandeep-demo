public class SessionTriggerHandler {
    public static void AfterUpdateMethod(list<Session__c>newlist,map<id,Session__c>TriggreOldMap){
        list<Session__c>ChildSessionList = new list<Session__c>();
        map<id,Session__c> triggerNewMap = new map<id,Session__c>();
        for(Session__c s: newlist){
            triggerNewMap.put(s.id,s);
        }
        for(Session__c s : [select id,name,Event_Fee__c,Address__c,Address_2__c,HTML_Description__c, Event_Host__c,Description__c ,Facility__c,Available_Openings__c,Campaign__c ,Category__c,City__c,Parent_Session__c,Event_Type__c, Location__c, Phone_Number__c, Program__c, RecordTypeId, Scheduled_End__c, Session_Details__c,Scheduled_Start__c, Session_Link__c, Session_Link_Text__c, Speakers__c, State__c,Status__c,Virtual__c,Zip_Code__c  from Session__c where Parent_Session__c In: triggerNewMap.keyset()]){
            System.debug('test record type ' + triggerNewMap.get(s.Parent_Session__c).RecordTypeId);
            if (triggerNewMap.get(s.Parent_Session__c).RecordTypeId != 'Event'){
                s.Event_Fee__c  = triggerNewMap.get(s.Parent_Session__c).Event_Fee__c;
                s.name  = triggerNewMap.get(s.Parent_Session__c).name;
                s.Location__c  = triggerNewMap.get(s.Parent_Session__c).Location__c;
                s.Address__c  = triggerNewMap.get(s.Parent_Session__c).Address__c;
                s.Address_2__c  = triggerNewMap.get(s.Parent_Session__c).Address_2__c;
                s.Facility__c  = triggerNewMap.get(s.Parent_Session__c).Facility__c;
                s.Available_Openings__c  = triggerNewMap.get(s.Parent_Session__c).Available_Openings__c;
                s.Campaign__c   = triggerNewMap.get(s.Parent_Session__c).Campaign__c ;
                s.Category__c   = triggerNewMap.get(s.Parent_Session__c).Category__c ;
                s.City__c   = triggerNewMap.get(s.Parent_Session__c).City__c ;
                s.Description__c   = triggerNewMap.get(s.Parent_Session__c).Description__c ;
                s.Event_Host__c   = triggerNewMap.get(s.Parent_Session__c).Event_Host__c ;
                s.Event_Type__c    = triggerNewMap.get(s.Parent_Session__c).Event_Type__c  ;
                //s.Location__c    = triggerNewMap.get(s.Parent_Session__c).Location__c;
                s.Phone_Number__c    = triggerNewMap.get(s.Parent_Session__c).Phone_Number__c  ;
                s.Program__c    = triggerNewMap.get(s.Parent_Session__c).Program__c  ;
                //s.RecordTypeId    = triggerNewMap.get(s.Parent_Session__c).RecordTypeId  ;
                //s.Scheduled_End__c    = triggerNewMap.get(s.Parent_Session__c).Scheduled_End__c  ;
                //s.Scheduled_Start__c    = triggerNewMap.get(s.Parent_Session__c).Scheduled_Start__c  ;
                s.Session_Details__c    = triggerNewMap.get(s.Parent_Session__c).Session_Details__c  ;
                s.Session_Link__c    = triggerNewMap.get(s.Parent_Session__c).Session_Link__c  ;
                s.Session_Link_Text__c    = triggerNewMap.get(s.Parent_Session__c).Session_Link_Text__c  ;
                s.Speakers__c    = triggerNewMap.get(s.Parent_Session__c).Speakers__c  ;
                s.State__c    = triggerNewMap.get(s.Parent_Session__c).State__c  ;
                s.Status__c    = triggerNewMap.get(s.Parent_Session__c).Status__c  ;
                s.Virtual__c     = triggerNewMap.get(s.Parent_Session__c).Virtual__c   ;
                s.Zip_Code__c     = triggerNewMap.get(s.Parent_Session__c).Zip_Code__c   ;
                s.HTML_Description__c    = triggerNewMap.get(s.Parent_Session__c).HTML_Description__c  ; 
                ChildSessionList.add(s);
            }
            
        }
        if(ChildSessionList.size()>0){
            update ChildSessionList;
        }
    }
}