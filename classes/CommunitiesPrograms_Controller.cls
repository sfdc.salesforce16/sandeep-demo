public without sharing class CommunitiesPrograms_Controller {
    
    @AuraEnabled
    public static List<CareProgram> getCommunityVisiblePrograms(String parentProgramId){
        List<CareProgram> result = new List<CareProgram>();
        try {
            if(String.isEmpty(parentProgramId)) result = [
                SELECT Id, Name, Service_Line__r.Name, ParentProgramId
                FROM CareProgram
                WHERE Is_Community_Visible__c = true
                ORDER BY Name];
            else result = [
                SELECT Id, Name, Service_Line__r.Name, ParentProgramId
                FROM CareProgram
                WHERE Is_Community_Visible__c = true
                  AND (Id = :parentProgramId OR ParentProgramId = :parentProgramId)
                ORDER BY Name];
        }
        catch(Exception e) {
            System.debug('Error!' + e.getMessage());
        }
        return result;
    }

}