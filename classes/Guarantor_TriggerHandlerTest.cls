@isTest
public class Guarantor_TriggerHandlerTest 
{
    @IsTest
    static void testGuarantor_TriggerHandler() 
    {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        List<Account> listOfAccounts = new List<Account>();
        Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        listOfAccounts.add(a);
        insert listOfAccounts;
        system.debug(a);
        Date twoDaysAgo = System.Today().addDays(-2);
        Date yesterday = System.Today().addDays(-1);
        List<HealthCloudGA__EhrRelatedPerson__c> listOfGuars = new List<HealthCloudGA__EhrRelatedPerson__c>();
        HealthCloudGA__EhrRelatedPerson__c ehrP = new HealthCloudGA__EhrRelatedPerson__c(Responsible_Party__c = 'SLF', HealthCloudGA__Account__c = a.Id);
        ehrP.Sex__c = 'M';
        ehrP.Account_Status__c = 'Internationa'; // added by Karthik
        ehrP.Account_Status_Youreka__c = 'International'; // added by Karthik
        ehrP.Selected_For_Registration__c = TRUE; // added by Karthik
        ehrP.Sex_Youreka__c = 'Male'; // added by Karthik
    	listOfGuars.add(ehrP);
        //Added by Karthik
        HealthCloudGA__EhrRelatedPerson__c ehrP2 = new HealthCloudGA__EhrRelatedPerson__c(Responsible_Party__c = 'SLF', HealthCloudGA__Account__c = a.Id);
        ehrP2.Sex__c = 'M';        
        ehrP2.Selected_For_Registration__c = TRUE; // added by Karthik
        ehrP2.Account_Type__c = 'HiTech'; // added by Karthik
        ehrP2.Sex_Youreka__c = 'Male'; // added by Karthik
    	listOfGuars.add(ehrP2);

        Test.startTest();

        insert listOfGuars;

        listOfGuars[0].Responsible_Party__c = 'SPO';
        listOfGuars[0].Active__c = true;
        listOfGuars[0].Self_Pay__c = true;
        update listOfGuars;

        Test.stopTest();
    }

    @IsTest
    static void testGuarantor_TriggerHandler_checkSelfPayCoverages() 
    {
        RecordType rt =[select Id from RecordType where sObjectType='Account' AND Name='Person Account' limit 1];
        Account aActiveMP = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Account a2InactiveMP = new Account(FirstName = 'Bruce', LastName = 'Wayne', HealthCloudGA__MedicalRecordNumber__pc = '12234', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = rt.Id);
        Date twoDaysAgo = System.Today().addDays(-2);
        Date yesterday = System.Today().addDays(-1);

        Test.startTest();
            insert aActiveMP;
            insert a2InactiveMP;
            MemberPlan mpActive = new MemberPlan(MemberId=aActiveMP.Id,Name='test1');
            insert mpActive;
            
            try {
                HealthCloudGA__EhrRelatedPerson__c gActiveMP = new HealthCloudGA__EhrRelatedPerson__c(HealthCloudGA__Account__c=aActiveMP.Id, Self_Pay__c=true, Responsible_Party__c = 'SLF');
                insert gActiveMP;
            } catch (Exception e) {
                Boolean expectedExceptionThrown =  e.getMessage().contains('You cannot mark a Patient with active coverage(s) as Self-Pay') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }    

            HealthCloudGA__EhrRelatedPerson__c gInactiveMP = new HealthCloudGA__EhrRelatedPerson__c(HealthCloudGA__Account__c=a2InactiveMP.Id, Self_Pay__c=true, Responsible_Party__c = 'SLF');
            insert gInactiveMP;

            List<HealthCloudGA__EhrRelatedPerson__c> gList = [SELECT Id FROM HealthCloudGA__EhrRelatedPerson__c];
            System.assert(gList.size()==1);

        Test.stopTest();

    }
}