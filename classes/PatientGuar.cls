/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description A controller for a Lightning component. Performs SOQL on HealthCloudGA__EhrRelatedPerson__c to retrieve Guarantors w.r.t. a patient.
*/
public inherited sharing class PatientGuar {

    /*******************************************************************************************************
    * @description Query and return a list of guarantors related to a patient  
    * @param recordId the current record Id
    * @return a list of Guarantors
    */
    @AuraEnabled(Cacheable = true)
    public static List<HealthCloudGA__EhrRelatedPerson__c> getGuar( string recordId){
        System.debug('getGuar.recordId: ' + recordId);
        List<HealthCloudGA__EhrRelatedPerson__c> returnVal = new List<HealthCloudGA__EhrRelatedPerson__c>();

            returnVal = [
                SELECT 
                    Id, 
                    HealthCloudGA__CalculatedName__c, 
                    Name, 
                    HealthCloudGA__NameFull__c, 
                    toLabel(Account_Type__c), 
                    Guarantor_Phone__c, 
                    HealthCloudGA__Address1Line1__c, 
                    HealthCloudGA__Address1Line2__c, 
                    HealthCloudGA__Address1City__c, 
                    HealthCloudGA__Address1State__c, 
                    HealthCloudGA__Address1Country__c,
                    HealthCloudGA__Relationship__c,
                    toLabel(Responsible_Party__c),
                    Active__c,
                    HealthCloudGA__PeriodStart__c,
                    HealthCloudGA__PeriodEnd__c,
                    toLabel(Account_Type_Other__c),
                    toLabel(Service_Area__c)
                FROM 
                    HealthCloudGA__EhrRelatedPerson__c 
                WHERE 
                    // HealthCloudGA__Relationship__c = 'GUARANTOR'
                    // AND 
                    HealthCloudGA__Account__c = :recordId];
    
    
        return returnVal;
    }

    /*******************************************************************************************************
    * @description This methods calls a RESTFul API defineds in
    * http://epic-to-sf-sprint4.us-e2.cloudhub.io/guarantor to retrieve guarantors related to a patient
    * @param recordId the patient sfid
    * @return a list of Gurantors with type of HealthCloudGA__EhrRelatedPerson__c
    */
    @AuraEnabled(Cacheable = true)
    public static List<HealthCloudGA__EhrRelatedPerson__c> getRelated( string recordId ){
        // return [SELECT Id, HealthCloudGA__CalculatedName__c, Name, HealthCloudGA__NameFull__c, 
        // Account_Type__c, Guarantor_Phone__c, HealthCloudGA__Address1Line1__c, HealthCloudGA__Address1Line2__c, 
        // HealthCloudGA__Address1City__c, HealthCloudGA__Address1State__c, HealthCloudGA__Address1Country__c
        // FROM HealthCloudGA__EhrRelatedPerson__c WHERE HealthCloudGA__Account__c = :recordId];

        Account patient = [SELECT Id, HealthCloudGA__MedicalRecordNumber__pc FROM Account WHERE Id = :recordId LIMIT 1];
        List<HealthCloudGA__EhrRelatedPerson__c> returnVal = new List<HealthCloudGA__EhrRelatedPerson__c>();

        List<HealthCloudGA__EhrRelatedPerson__c> guarantors = [
            SELECT 
                Id, 
                HealthCloudGA__CalculatedName__c, 
                Name, 
                HealthCloudGA__NameFull__c, 
                Account_Type__c, 
                Guarantor_Phone__c, 
                HealthCloudGA__Address1Line1__c, 
                HealthCloudGA__Address1Line2__c, 
                HealthCloudGA__Address1City__c, 
                HealthCloudGA__Address1State__c, 
                HealthCloudGA__Address1Country__c,
                HealthCloudGA__Relationship__c,
                Active__c,
                HealthCloudGA__PeriodStart__c,
                HealthCloudGA__PeriodEnd__c,
                Account_Type_Other__c,
                Service_Area__c
            FROM 
                HealthCloudGA__EhrRelatedPerson__c 
            WHERE 
            HealthCloudGA__Relationship__c != 'GUARANTOR'
                AND HealthCloudGA__Account__c = :recordId];

        returnVal.addAll(guarantors);

        if(patient.HealthCloudGA__MedicalRecordNumber__pc == null){
            return returnVal;
        }

        HttpRequest req = new HttpRequest();
        String endpoint = String.format('http://epic-to-sf-sprint4.us-e2.cloudhub.io/guarantor?mrn={0}', new String[] {patient.HealthCloudGA__MedicalRecordNumber__pc});
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        
        // Specify the required user name and password to access the endpoint
        // As well as the header and header information
    
        // String username = 'myname';
        // String password = 'mypwd';
    
        // Blob headerValue = Blob.valueOf(username + ':' + password);
        // String authorizationHeader = 'Basic ' +
        // EncodingUtil.base64Encode(headerValue);
        // req.setHeader('Authorization', authorizationHeader);
    
        // Create a new http object to send the request object
        // A response object is generated as a result of the request  
    
        Http http = new Http();
        HTTPResponse res = http.send(req);
        System.debug(res.getBody());

        //List<Object> epicGuarantors = (List<Object>) JSON.deserializeUntyped(res.getBody());
        List<PatientGuar.Guarantor> epicGuarantors = (List<PatientGuar.Guarantor>) JSON.deserialize(res.getBody(), List<PatientGuar.Guarantor>.class);
        for ( PatientGuar.Guarantor g : epicGuarantors) {
            HealthCloudGA__EhrRelatedPerson__c a = new HealthCloudGA__EhrRelatedPerson__c();
            a.Id = g.id;
            a.HealthCloudGA__NameFull__c = g.name;
            a.Account_Type__c = g.relationship;
            a.Guarantor_Phone__c = g.phone;
//            a.nameUrl = '/'+g.Id;
            a.HealthCloudGA__Address1Line1__c = g.address;
            a.HealthCloudGA__Address1City__c = g.city;
            a.HealthCloudGA__Address1State__c = g.state;
            a.HealthCloudGA__Address1Country__c = g.country;
            returnVal.add(a);
        }
        // Map<String, Object> epicGuarantors = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        // System.debug('epicGuarantors: ' + epicGuarantors);

        // for (Object g : epicGuarantors) {
        //     System.debug('g: ' + g);            
        //     PatientGuar.Guarantor guarantor = (PatientGuar.Guarantor)System.JSON.deserialize(g.toString(), PatientGuar.Guarantor.class);
        //     //Map<String, Object> epicGuarantor = (Map<String, Object>) JSON.deserializeUntyped(g.toString());
        //     //System.debug('epicGuarantor: ' + epicGuarantor);

        // }

        // return [SELECT Id, HealthCloudGA__CalculatedName__c, Name, HealthCloudGA__NameFull__c, 
        // Account_Type__c, Guarantor_Phone__c, HealthCloudGA__Address1Line1__c, HealthCloudGA__Address1Line2__c, 
        // HealthCloudGA__Address1City__c, HealthCloudGA__Address1State__c, HealthCloudGA__Address1Country__c
        // FROM HealthCloudGA__EhrRelatedPerson__c WHERE HealthCloudGA__Account__c = :recordId];

        return returnVal;
    }

    /*******************************************************************************************************
    * @description A class used for JSON serialization/Deserialization
    */
    public class Guarantor{
        public String id;
        public String address;
        public String name;
        public String phone;
        public String relationship;
        public String city;
        public String state;
        public String country;
        public String postalCode;
        public String ssn;
        public String employer;
        public String employmentStatus;
//        public String nameUrl;
    }
}