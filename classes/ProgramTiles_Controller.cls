/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description A controller of Lightning Component. It provides @AuraEnabled method to return a list of CareProgram
*/public without sharing class ProgramTiles_Controller {
    /*******************************************************************************************************
    * @description Returns a list of CarePrograms
    * @param sortby SOQL Order By fields
    * @param sortdir SOQL Order by direction
    * @param featuredNum SOQL LIMIT count
    * @return a list of CarePrograms
    */
    @AuraEnabled
    public static List<CareProgram> getFeaturedPrograms(String sortby, String sortdir, Integer featuredNum) {
        List<CareProgram> result = new List<CareProgram>();
        System.debug('Query Info: ' + sortby + ' ' + sortdir + ' ' + featuredNum);
        
        String query = 'SELECT Id, Name, Description__c, Program_Hero_Image__c, Hero_Image__c FROM CareProgram WHERE Is_Community_Visible__c = true ORDER BY ';
        
        if(!String.isEmpty(sortby) && !String.isEmpty(sortdir)) query += sortby + ' ' + sortdir + ' NULLS LAST';
        else query += 'CreatedDate DESC NULLS LAST';
        
        if(featuredNum != null && featuredNum > 0) query += ' LIMIT ' + featuredNum;
        else query += ' LIMIT 12';
        
        try {
            result = database.query(query);
        }
        catch(Exception e) {
            System.debug('Unable to get Programs! ' + e.getMessage() + '\n' + e.getStackTraceString());
        }
        return result;
    }

}