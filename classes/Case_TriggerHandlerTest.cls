/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class provides unit tests to cover the Case_Trigger and 
 * Case_TriggerHandler apex class.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Synaptic
 * @project     DHAS
 * @since       April 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

@isTest
public class Case_TriggerHandlerTest {
    
    @IsTest
    static void testSpecialtyAndInCounty() {

        // Create Test Data
        String personRtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
		Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = personRtId);
		Account a2 = new Account(FirstName = 'Bruce2', LastName = 'Wayne2', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = personRtId);
        Account dept = new Account(Name='Duke Ortho Trauma Clinic', Department__c='Orthopaedics');
        HealthcareProvider hp = new HealthcareProvider (Department_Specialty__c = 'Family Medicine', Name = 'Doctor Fish');

        Test.startTest();
        // Insert Test Data
        insert a;
        insert a2;
        insert dept;
        insert hp;
        Referral_Information__c ref = new Referral_Information__c(Account__c=a.Id, Referral_ID__c='12345', Patient_Name__c = a.Id, Referral_Type__c = 'Project Access', Referral_Status__c = 'Authorized', Referred_to_Provider_Specialty__c = 'Family Medicine', Referred_To_Department__c=dept.id);
        insert ref;
        Skill_Group__c skg = new Skill_Group__c(Name='123DPC BR CREEK PC APPT SG', Skill_Group__c = '123DPC_BR_CREEK_PC_APPT_SG', Duke_Department__c = hp.Id, Specialty__c='Orthopaedics');
        insert skg;
        Case cas = new Case(Status = 'New', AccountId = a.Id, Skill_Group__c = skg.Id);
        insert cas;
        Case cas2 = new Case(Status = 'Closed', AccountId = a2.Id, Skill_Group__c = skg.Id);
        insert cas2;

        // Get associated account and check that Is Specialty Match = true
        Account acc = [SELECT Is_Specialty_Match__c FROM Account WHERE id=:a.id LIMIT 1];
        System.assert(acc.Is_Specialty_Match__c=true);

        // Get associated account and check that Is Specialty Match = false
        Account acc2 = [SELECT Is_Specialty_Match__c FROM Account WHERE id=:a2.id LIMIT 1];
        System.assert(acc2.Is_Specialty_Match__c!=true);

        cas.Status = 'Closed';
        update cas;
        cas2.Status = 'New';
        update cas2;

        // Get associated account and check that Is Specialty Match = true
        Account acc3 = [SELECT Is_Specialty_Match__c FROM Account WHERE id=:a2.id LIMIT 1];
        System.assert(acc3.Is_Specialty_Match__c=true);

        cas.Skill_Group__c = null;
        update cas;
        Test.stopTest();
    }

    @IsTest
    static void testVASpecialty() {

        // Create Test Data
        String personRtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
		Account a = new Account(FirstName = 'Bruce', LastName = 'Wayne', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = personRtId);
		Account a2 = new Account(FirstName = 'Bruce2', LastName = 'Wayne2', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = personRtId);
		Account a3 = new Account(FirstName = 'Bruce3', LastName = 'Wayne3', PersonBirthdate=date.ValueOf('2020-12-05'), PersonHomePhone = '333-123-8888', County__c = 'Broward', RecordTypeId = personRtId);
        Account dept = new Account(Name='Duke Ortho Trauma Clinic', Department__c='Orthopaedics');
        Account dept2 = new Account(Name='Cardiology Clinic', Department__c='Cardiology');
        HealthcareProvider hp = new HealthcareProvider (Department_Specialty__c = 'Family Medicine', Name = 'Doctor Fish');

        Test.startTest();
        // Insert Test Data
        insert a;
        insert a2;
        insert a3;
        insert dept;
        insert dept2;
        insert hp;
        Referral_Information__c ref = new Referral_Information__c(Account__c=a.Id,  Referral_ID__c='12345', Patient_Name__c = a.Id, Referral_Type__c = 'Project Access', Referral_Status__c = 'VA', Referred_to_Provider_Specialty__c = 'Family Medicine', Referred_To_Department__c=dept.id);
        Referral_Information__c ref2 = new Referral_Information__c(Referral_ID__c='12345', Patient_Name__c = a2.Id, Referral_Type__c = 'Project Access', Referral_Status__c = 'Other', Referred_to_Provider_Specialty__c = 'Family Medicine', Referred_To_Department__c=dept2.id);
        Referral_Information__c ref3 = new Referral_Information__c(Referral_ID__c='12345', Patient_Name__c = a2.Id, Referral_Type__c = 'Project Access', Referral_Status__c = 'Tricare', Referred_to_Provider_Specialty__c = 'Family Medicine', Referred_To_Department__c=dept.id);
        Referral_Information__c ref4 = new Referral_Information__c(Referral_ID__c='12345', Patient_Name__c = a3.Id, Referral_Type__c = 'Project Access', Referral_Status__c = 'VA', Referred_to_Provider_Specialty__c = 'Family Medicine', Referred_To_Department__c=dept2.id);
        Referral_Information__c ref5 = new Referral_Information__c(Referral_ID__c='12345', Patient_Name__c = a3.Id, Referral_Type__c = 'Project Access', Referral_Status__c = 'Other', Referred_to_Provider_Specialty__c = 'Family Medicine', Referred_To_Department__c=dept.id);
        insert ref;
        insert ref2;
        insert ref3;
        insert ref4;
        insert ref5;
        Skill_Group__c skg = new Skill_Group__c(Name='123DPC BR CREEK PC APPT SG', Skill_Group__c = '123DPC_BR_CREEK_PC_APPT_SG', Duke_Department__c = hp.Id, Specialty__c='Orthopaedics');
        insert skg;
        Case cas = new Case(Status = 'New', AccountId = a.Id, Skill_Group__c = skg.Id);
        insert cas;
        Case cas2 = new Case(Status = 'Closed', AccountId = a2.Id, Skill_Group__c = skg.Id);
        insert cas2;
        Case cas3 = new Case(Status = 'New', AccountId = a3.Id, Skill_Group__c = skg.Id);
        insert cas3;

        // Get associated account and check that Is Specialty Match = true
        Account acc = [SELECT Is_VA_Specialty_Match__c FROM Account WHERE id=:a.id LIMIT 1];
        System.assert(acc.Is_VA_Specialty_Match__c=true);

        // Get associated account and check that Is Specialty Match = false
        Account acc2 = [SELECT Is_VA_Specialty_Match__c FROM Account WHERE id=:a2.id LIMIT 1];
        System.assert(acc2.Is_VA_Specialty_Match__c!=true);

        // Get associated account and check that Is Specialty Match = false
        Account acc3 = [SELECT Is_VA_Specialty_Match__c FROM Account WHERE id=:a3.id LIMIT 1];
        System.assert(acc3.Is_VA_Specialty_Match__c!=true);

        cas.Status = 'Closed';
        update cas;
        cas2.Status = 'New';
        update cas2;

        // Get associated account and check that Is Specialty Match = true
        Account acc2again = [SELECT Is_VA_Specialty_Match__c FROM Account WHERE id=:a2.id LIMIT 1];
        System.assert(acc2again.Is_VA_Specialty_Match__c=true);

        cas.Skill_Group__c = null;
        update cas;
        Test.stopTest();
    }
}