public with sharing class WaitListEntry implements Comparable{
    public WaitListEntry() {
        Providers = new List<WaitListEntry.Provider>();
    }

    @AuraEnabled
    public string StartDate { get; set; }    

    @AuraEnabled
    public string OnUntilDate { get; set; }    

    @AuraEnabled
    public string Priority { get; set; }    

    @AuraEnabled
    public string VisitTypeName { get; set; }    
    
    @AuraEnabled
    public string Status { get; set; }    

    @AuraEnabled
    public List<String> AppointmentNotes { get; set; }    

    @AuraEnabled
    public String Department{ get{
        if(!Providers.isEmpty()){
            return Providers[0].DepartmentName;
        }
        return null;
    }}

    @AuraEnabled
    public String Provider{ get{
        if(!Providers.isEmpty()){
            return Providers[0].ProviderName;
        }
        return null;
    }}
    

    @AuraEnabled
    public List<WaitListEntry.Provider> Providers { get; set; }    

    public class Provider{

        @AuraEnabled
        public string DepartmentName { get; set; }    

        @AuraEnabled
        public string ProviderName { get; set; }    
    
    }

    public Integer compareTo(Object obj){
        WaitListEntry target = (WaitListEntry)obj;

        if(this.Department > target.Department){
            return 1;
        }

        if(this.Department == target.Department){
            return 0;
        }

        return -1;
    }
}