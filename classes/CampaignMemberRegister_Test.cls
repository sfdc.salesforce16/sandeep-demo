@IsTest
public class CampaignMemberRegister_Test {
    @IsTest
    Public static void testCampaignMemberRegister(){
        Map<String, String> inputParams = new Map<String, String>();
        Campaign camp = new Campaign();
        camp.Name = 'Test Campaign';
        insert camp;
        List<Campaign> campaignQuery = [SELECT Id,Name FROM Campaign WHERE Name = 'Test Campaign'];
        String campRecordId = campaignQuery[0].Id;
        
        String RecTypeId= [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id;
        Account acc = new Account();
        acc.RecordTypeId = RecTypeId;
        acc.LastName = 'TestAccount';
        acc.Email__c = 'accountTest@Email.com';
        insert acc;
        String accRecordId =acc.Id;
        
        Lead ld = new Lead();
        ld.LastName = 'TestLead';
        ld.Email = 'LeadTest@Email.com';
        insert ld;
        List<Lead> leadQuery = [SELECT Id,Name,Email FROM Lead WHERE Email = 'LeadTest@Email.com'];
        String ldRecordId = leadQuery[0].Id;   
        
       /**  Contact con = new Contact();
        con.FirstName='Contact';
        con.AccountId = accRecordId;
        con.LastName = 'Test';
        con.Email = 'Lead@Email.com';
        insert con; **/
        List<Contact> contactQuery = [SELECT Id,Name,Email,AccountId FROM Contact WHERE AccountId = :accRecordId];
        String contactRecordId = contactQuery[0].Id;   
        
        
        
        CampaignMemberRegister CreateCampaignMemberRegister = new CampaignMemberRegister();
        inputParams.put('CampaignId',campRecordId);
        inputParams.put('AccountId', accRecordId);
        inputParams.put('LeadId', ldRecordId);
        inputParams.put('ContactId',contactRecordId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        CreateCampaignMemberRegister.describe();          
        CampaignMemberRegister.invoke(request); // create a campaign Member with above values
        
        // asserts to check the CampaignMember creation
        List<CampaignMember> cmpMemberList = [SELECT Id,Name,Account__c,LeadId,campaignId FROM CampaignMember WHERE campaignId = :campRecordId AND (Account__c = :accRecordId OR LeadId =:ldRecordId)];
        System.assertEquals(1, cmpMemberList.size());
    }
}