@isTest
public class ProgramTiles_Controller_Test {
    
    private static Id parentProgramId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Parent_Program' AND SObjectType = 'CareProgram'].Id;
    
    @testSetup
    static void setup() {
        // Service Line
        Service_Line__c s = new Service_Line__c(Name = 'Diet & Fitness');
        insert s;
        
        // Parent Program
        CareProgram p = new CareProgram(
        	Name = 'Residential Weight Loss',
            Service_Line__c = s.Id,
            RecordTypeId = parentProgramId,
        	Price__c = 100,
        	Deposit__c = 5,
            Duration__c = 7,
            Is_Community_Visible__c = true,
            Description__c = 'Test',
            Featured_Program__c = 1
        ); insert p;
        
        // Program
        CareProgram p2 = new CareProgram(
        	Name = 'Residential Weight Loss - 1 week',
            Service_Line__c = s.Id,
            ParentProgramId = p.Id,
        	Price__c = 100,
        	Deposit__c = 5,
            Duration__c = 7,
            Is_Community_Visible__c = true,
            Description__c = 'Test',
            Featured_Program__c = 2
        ); insert p2;
        
    }
    
    @isTest
    static void query_Limit1() {
        List<CareProgram> result = ProgramTiles_Controller.getFeaturedPrograms('Featured_Program__c', 'ASC', 1);
        System.assert(result.size() == 1); // finds limited to 1
    }
    
    @isTest
    static void query_noLimit() {
        List<CareProgram> result = ProgramTiles_Controller.getFeaturedPrograms('Featured_Program__c', 'ASC', null);
        System.assert(result.size() == 2); // finds all
    }
    
    @isTest
    static void query_noSortBy() {
        List<CareProgram> result = ProgramTiles_Controller.getFeaturedPrograms(null, null, null);
        System.assert(result.size() == 2); // finds all
    }
    
    @isTest
    static void query_Error() {
        List<CareProgram> result = ProgramTiles_Controller.getFeaturedPrograms('FakeField', 'ASC', null);
        System.assert(result.size() == 0); // error catches and return empty list
    }

}