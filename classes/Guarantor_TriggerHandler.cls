/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description This class implements the TriggerHandler Framework for the 
* HealthCloudGA__EhrRelatedPerson__c object.  Validation on Guarantor preventing user 
* from selecting Self-Pay when the patient has active Coverages.  
* Updates the following fields on Account: Self-Pay, Has International Guarantor, 
* Has HITECH Guarantor.  Syncs Youreka picklist fields for Account Status, 
* Responsible Party and Sex with the corresponding EHR Related Person picklist 
* field mapped to the integration.
* object.
*/
public without sharing class Guarantor_TriggerHandler extends TriggerHandler {
    public Guarantor_TriggerHandler() {
        
    }
    
    public override void beforeInsert(){
        System.debug('beforeInsert');
        checkSelfPayCoverages();
        CopyGuarantorRecord();
        insertSyncResponsible_Party();
        insertSyncSex();
        insertHiTechPatient();
        insertInternationalGuarantor();
        insertSyncAccountStatus();
    }
    
    public override void beforeUpdate(){
        System.debug('beforeUpdate');
        checkSelfPayCoverages();
        CopyGuarantorRecord();
        updateSyncResponsibleParty();
        updateSyncSex();
        updateHiTechPatient();
        updateInternationalGuarantor();
        updateSyncAccountStatus();
    }
    
    public override void afterUpdate(){
        isEditForEpic();
        // By Roy Lou - 07/24/2021 - DHAS-1078: Guarantor "Self-pay" automation
        handleSelfPayOnUpdate();
    }

    /*******************************************************************************************************
    * @description override the base class afterInsert function to handle Self-Pay 
    * synchronization with patient record
    * By Roy Lou - 07/24/2021 - DHAS-1078: Guarantor "Self-pay" automation
    */
    public override void afterInsert(){
        handleSelfPayOnInsert();
    }

    /*******************************************************************************************************
    * @description If a user attempts to mark a patient with active coverage(s) as self-pay, throw an error
    * By Shelby Dremely - 08/19/2021 - DHAS-1156: Apex Validation on Guarantor for Coverage Records
    *
    */
    private static void checkSelfPayCoverages() {
        System.debug(logginglevel.debug, 'Entering checkSelfPayCoverages');
        for(HealthCloudGA__EhrRelatedPerson__c g : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.new){
            //if Self Pay is True
            if(g.Self_Pay__c) {
                List<MemberPlan> mp = [SELECT Id FROM MemberPlan WHERE MemberId = :g.HealthCloudGA__Account__c AND (EffectiveTo < TODAY OR EFfectiveTo = null)];
                if(mp.size()>0){
                    g.addError('You cannot mark a Patient with active coverage(s) as Self-Pay');
                }
            }
        }
    }
    
    /*******************************************************************************************************
    * @description handle the insert when both of Self_pay__c and Active__c are true
    * By Roy Lou - 07/24/2021 - DHAS-1078: Guarantor "Self-pay" automation
    *
    */
    @testVisible
    private void handleSelfPayOnInsert() {
        System.debug(logginglevel.debug, 'Entering handleSelfPayOnInsert');
        List<Account> accounts = new List<Account>();
        for(HealthCloudGA__EhrRelatedPerson__c g : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.new){
            if(Schema.sObjectType.Account.fields.Self_Pay__c.isUpdateable() && g.active__c && g.Self_Pay__c) {
                accounts.add(new Account(Id = g.HealthCloudGA__Account__c, Self_Pay__c=true ));
            }
        }
        updateAccounts(accounts);
    }

    /*******************************************************************************************************
    * @description handle the DML operation of Account
    * By Roy Lou - 07/24/2021 - DHAS-1078: Guarantor "Self-pay" automation
    * @param accounts list of accounts to be updated
    *
    */
    @testVisible
    private void updateAccounts(List<Account> accounts ) {
        Database.SaveResult[] srList  = Database.update(accounts, false);
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug(logginglevel.debug, 'Successfully inserted account. Account ID: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug(logginglevel.error, 'The following error has occurred.');                    
                    System.debug(logginglevel.error, err.getStatusCode() + ': ' + err.getMessage());
                    System.debug(logginglevel.error, 'Account fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    /*******************************************************************************************************
    * @description handle the update of Self_pay__c and Active__c
    * By Roy Lou - 07/24/2021 - DHAS-1078: Guarantor "Self-pay" automation
    * 1. Collect RelatedPerson's HealthCloudGA__Account__c if either active__c or self_pay__c is modified
    * 2. Retrieve accounts and associated Guarantors with account's Id in step 1
    * 3. Iterate through accounts from step 2 and set Self_Pay__c to true if any of its children Guarantor has 
    *    both its Self_Pay__c and Active__c are True; otherwise set it false 
    */
    @testVisible
    private void handleSelfPayOnUpdate() {
        System.debug(logginglevel.debug, 'Entering handleSelfPayOnUpdate');
        set<Id> accountIds = new set<Id>();
        //collect account Ids whose associated relatedperson records have modified active__c and self_pay__c.
        for(HealthCloudGA__EhrRelatedPerson__c g : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.new){
            HealthCloudGA__EhrRelatedPerson__c oldEhrp = (HealthCloudGA__EhrRelatedPerson__c)Trigger.oldMap.get(g.ID);
            if((g.active__c != oldEhrp.active__c) || (g.Self_Pay__c != oldEhrp.Self_Pay__c)){
                accountIds.add(g.HealthCloudGA__Account__c);
            }
        }
        //retrieve accounts and assocaited relatedperson records
        List<Account> updateAccounts = [SELECT Id , (SELECT Active__c,Self_Pay__c FROM HealthCloudGA__RelatedPeople__r) FROM Account where Id in :accountIds WITH SECURITY_ENFORCED];
        if (Schema.sObjectType.Account.fields.Self_Pay__c.isUpdateable()) {
            for(Account a : updateAccounts) {
                a.Self_Pay__c = false;
                //if any children relatedperson record has both active__c and Self_Pay__c are true, 
                //set the parent account's self_pay__c to true
                for(HealthCloudGA__EhrRelatedPerson__c g : a.HealthCloudGA__RelatedPeople__r) {
                    if(g.active__c && g.Self_Pay__c) {
                        a.Self_Pay__c = true;
                        break;
                    }
                }    
            }
            updateAccounts(updateAccounts);
        }
    }
    
    private void isEditForEpic() {
        List<ToEpicGuarantor__e> epicGuarantorEvents = new List<ToEpicGuarantor__e>();
        
        for(HealthCloudGA__EhrRelatedPerson__c g : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.new){
            Boolean isUpdate = false;
            System.debug('g: ' + g);
            // if(a.inHL7Update__c){
            //     a.inHL7Update__c = false;
            //     //receiving message from Epic, don't respond with outbound message
            //     continue;
            // }
            
            if(Trigger.isUpdate){
                System.debug('Trigger.isUpdate: ' + Trigger.isUpdate);
                HealthCloudGA__EhrRelatedPerson__c old = (HealthCloudGA__EhrRelatedPerson__c)Trigger.oldMap.get(g.ID);
                if((g.Responsible_Party__c != old.Responsible_Party__c)
                  ){
                      isUpdate = true;
                  }
            }
            
            if(isUpdate || Trigger.isInsert){
                System.debug('Trigger.isInsert: ' + Trigger.isInsert);
                ToEpicGuarantor__e eg = new ToEpicGuarantor__e();
                if(Trigger.isUpdate){
                    eg.ChangeType__c = 'UPDATE';
                }
                if(Trigger.isInsert){
                    eg.ChangeType__c = 'CREATE';
                }
                eg.GuarantorId__c = g.Id;
                eg.AccountId__c = g.HealthCloudGA__Account__c;
                epicGuarantorEvents.add(eg);
            }
        }
        
        try {
            System.debug('publishing: ' + epicGuarantorEvents);
            if(!epicGuarantorEvents.isEmpty()){
                List<Database.SaveResult> results = EventBus.publish(epicGuarantorEvents);
                // Inspect publishing results
                for (Database.SaveResult result : results) {
                    if (!result.isSuccess()) {
                        for (Database.Error error : result.getErrors()) {
                            System.debug('Error returned for (epicGuarantorEvents): ' + error.getStatusCode() +' - '+ error.getMessage());
                        }
                    }
                    else{
                        System.debug('published: ' + epicGuarantorEvents);
                    }
                }            
            }
        } catch (Exception ex) {
            System.debug('Guarantor.isEditForEpic.exception: ' + ex);
        }
    }
    
    /*******************************************************************************************************
    * @description On insert, if HealthCloudGA__EhrRelatedPerson__c.Account_Status__c = International, 
    *   set Has_International_Guarantor__c = TRUE on related Account (HealthCloudGA__Account__c)
    *   
    */
    private static void insertInternationalGuarantor() {
        for(HealthCloudGA__EhrRelatedPerson__c ehrP : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.new){
            try {
                Account a;
                if(ehrP.HealthCloudGA__Account__c != null){
                    string accId = ehrP.HealthCloudGA__Account__c;
                    a = [SELECT Id, Has_International_Guarantor__c FROM Account WHERE Id = :accId];
                }
                if(ehrP.Account_Status__c == 'Internationa' && ehrP.HealthCloudGA__Account__c != null && (ehrP.Selected_For_Registration__c == true || ehrP.Active__c == TRUE)){                    
                    //Account a = [SELECT Id, Has_International_Guarantor__c FROM Account WHERE Id = :accId];
                    a.Has_International_Guarantor__c = true;
                    update a;                
                }
                
                if(ehrP.Account_Status__c == 'Internationa' && ehrP.HealthCloudGA__Account__c != null && (ehrP.Selected_For_Registration__c == true || ehrP.Active__c == FALSE)){                    
                    //Account a = [SELECT Id, Has_International_Guarantor__c FROM Account WHERE Id = :accId];
                    a.Has_International_Guarantor__c = FALSE;
                    update a;                
                }
            } catch (Exception ex) {
                System.debug('insertInternationalGuarantor.exception: ' + ex);
            }
        }
    }
    
    /*******************************************************************************************************
    * @description On insert, if HealthCloudGA__EhrRelatedPerson__c.Account_Type__c = “HiTech”, 
    *   set Has_HiTech_Guarantor__c = TRUE on related Account (HealthCloudGA__Account__c)
    *   
    */
    private static void insertHiTechPatient() {
        for(HealthCloudGA__EhrRelatedPerson__c ehrP : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.new){
            if(ehrP.Account_Type__c == 'HiTech' && ehrP.HealthCloudGA__Account__c != null){
                string accId = ehrP.HealthCloudGA__Account__c;
                try {
                    Account a = [SELECT Id, Has_HiTech_Guarantor__c FROM Account WHERE Id = :accId];
                    a.Has_HiTech_Guarantor__c = true;
                    update a;
                } catch (Exception ex) {
                    System.debug('insertHiTechPatient.exception: ' + ex);
                }
            }            
        }
    }
    
    /*******************************************************************************************************
    * @description On update, if HealthCloudGA__EhrRelatedPerson__c.Account_Type__c = “HiTech”, 
    *   set Has_HiTech_Guarantor__c = TRUE on related Account (HealthCloudGA__Account__c)
    *   
    */
    private static void updateHiTechPatient() {
        for(HealthCloudGA__EhrRelatedPerson__c ehrP : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.newMap.values()){
            HealthCloudGA__EhrRelatedPerson__c oldEhrp = (HealthCloudGA__EhrRelatedPerson__c)Trigger.oldMap.get(ehrP.ID);
            HealthCloudGA__EhrRelatedPerson__c newEhrp = (HealthCloudGA__EhrRelatedPerson__c)Trigger.newMap.get(ehrP.ID);
            System.debug('in updateHiTechPatient');
            if(newEhrp.Account_Type__c != oldEhrp.Account_Type__c){
                if(newEhrp.Account_Type__c == 'HiTech' && newEhrp.HealthCloudGA__Account__c != null){
                    string accId = newEhrp.HealthCloudGA__Account__c;
                    try {
                        Account a = [SELECT Id, Has_HiTech_Guarantor__c FROM Account WHERE Id = :accId];
                        a.Has_HiTech_Guarantor__c = true;
                        update a;
                    } catch (Exception ex) {
                        System.debug('updateHiTechPatient.exception: ' + ex);
                    }
                }   
            }
        }
    }
    
    /*******************************************************************************************************
    * @description On update, if HealthCloudGA__EhrRelatedPerson__c.Account_Status__c = International, 
    *   set Has_International_Guarantor__c = TRUE on related Account (HealthCloudGA__Account__c)
    *   
    */
    private static void updateInternationalGuarantor() {        
        Map<Id, Account> acctWithIntl = new Map<Id, Account>();
        Map<Id, Account> acctNoIntl = new Map<Id, Account>();
        
        for(HealthCloudGA__EhrRelatedPerson__c ehrP : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.newMap.values()){
            System.debug(ehrP.HealthCloudGA__Account__c);
            System.debug(ehrP.Account_Status__c);
            System.debug(ehrP.Selected_For_Registration__c);
            System.debug(ehrP.Active__c);
            if(ehrP.HealthCloudGA__Account__c != null){
                if(ehrP.Account_Status__c == 'Internationa' && (ehrP.Selected_For_Registration__c == true || ehrP.Active__c == TRUE )){
                    acctWithIntl.put(ehrP.HealthCloudGA__Account__c, new Account(Id = ehrp.HealthCloudGA__Account__c,  Has_International_Guarantor__c = true));                
                }
                else{
                    acctNoIntl.put(ehrP.HealthCloudGA__Account__c, new Account(Id = ehrp.HealthCloudGA__Account__c,  Has_International_Guarantor__c = false)); 
                }            
            }
        }

        List<Account> accountsToUpdate = acctWithIntl.values();

        for(Account acct : acctNoIntl.values()){
            if(!acctWithIntl.containsKey(acct.Id)){
                accountsToUpdate.add(acct);
            }
        }

        if(!accountsToUpdate.isEmpty()){
            system.debug(accountsToUpdate);
            update accountsToUpdate;
        }
    }
    
    /*******************************************************************************************************
    * @description When HealthCloudGA__EhrRelatedPerson__c is created, sync Sex__c and  Sex_Youreka__c
    *   
    */
    private static void insertSyncSex(){
        
        for(HealthCloudGA__EhrRelatedPerson__c guar : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.new){
            if(guar.Sex__c != null){
                Schema.DescribeFieldResult fieldResult = HealthCloudGA__EhrRelatedPerson__c.Sex__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == guar.Sex__c){
                        guar.Sex_Youreka__c = pickListVal.getLabel();
                    }
                }
                System.debug('guar.Sex_Youreka__c: ' + guar.Sex_Youreka__c);
            }
            
            if(guar.Sex_Youreka__c != null){
                Schema.DescribeFieldResult fieldResult = HealthCloudGA__EhrRelatedPerson__c.Sex__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == guar.Sex_Youreka__c){
                        guar.Sex__c = pickListVal.getValue();
                    }
                }
                System.debug('guar.Sex__c: ' + guar.Sex__c);
            }
        }
    }    
    
    /*******************************************************************************************************
    * @description When HealthCloudGA__EhrRelatedPerson__c is created, sync Responsible_Party__c and Rel_to_Patient_Youreka__c
    *   
    */
    private static void insertSyncResponsible_Party(){
        
        for(HealthCloudGA__EhrRelatedPerson__c guar : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.new){
            if(guar.Responsible_Party__c != null){
                Schema.DescribeFieldResult fieldResult = HealthCloudGA__EhrRelatedPerson__c.Responsible_Party__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == guar.Responsible_Party__c){
                        guar.Rel_to_Patient_Youreka__c = pickListVal.getLabel();
                    }
                }
                System.debug('guar.Rel_to_Patient_Youreka__c: ' + guar.Rel_to_Patient_Youreka__c);
            }
            
            if(guar.Rel_to_Patient_Youreka__c != null){
                Schema.DescribeFieldResult fieldResult = HealthCloudGA__EhrRelatedPerson__c.Responsible_Party__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == guar.Rel_to_Patient_Youreka__c){
                        guar.Responsible_Party__c = pickListVal.getValue();
                    }
                }
                System.debug('guar.Responsible_Party__c: ' + guar.Responsible_Party__c);
            }
        }
    }
    
    /*******************************************************************************************************
    * @description When Responsible Party is Self, copy fields from Associated Patient record 
    *   Name, Birth Date, Sex, SSN, Street Address, City, State, Country, Zip, 
    *   Home Phone, Work Phone & Mobile Phone
    *   
    */
    private static void CopyGuarantorRecord(){
        
        for(HealthCloudGA__EhrRelatedPerson__c newGuarantor : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.new){
            Id relatedAccountId = newGuarantor.HealthCloudGA__Account__c;
            System.debug('newGuarantor.HealthCloudGA__Account__c ' + newGuarantor.HealthCloudGA__Account__c);
            System.debug('newGuarantor.Responsible_Party__c ' + newGuarantor.Responsible_Party__c);
            System.debug('newGuarantor.Rel_to_Patient_Youreka__c ' + newGuarantor.Rel_to_Patient_Youreka__c);
            if(newGuarantor.Responsible_Party__c == 'SLF' || newGuarantor.Rel_to_Patient_Youreka__c == 'Self'){
                try {
                    
                    //retrieve custom metadata for list of fields that are mapped
                    List<Guarantor_Self_Copy_Field_Mapping__mdt> syncedFields= [SELECT Id, Account_Field__c, Guarantor_Field__c from Guarantor_Self_Copy_Field_Mapping__mdt];
                    List<String> listaccountQueryFields = new List<String>();
                    
                    //build query string and query the account being copied                   
                    String accountQueryFields = '';
                    for(Guarantor_Self_Copy_Field_Mapping__mdt fieldToCopy : syncedFields){
                        listaccountQueryFields.add(fieldToCopy.Account_Field__c);
                    }
                    accountQueryFields = String.join(listaccountQueryFields,' ,');
                    Account relatedAccount = (Account)database.query('SELECT ' + accountQueryFields + ' FROM Account WHERE Id = :relatedAccountId')[0];
                    
                    //copy account to guarantor
                    for(Guarantor_Self_Copy_Field_Mapping__mdt fieldToCopy : syncedFields){
                        newGuarantor.put(fieldToCopy.Guarantor_Field__c, relatedAccount.get(fieldToCopy.Account_Field__c));
                    }
                    
                } catch (Exception ex) {
                    System.debug('CopyGuarantorRecord.exception: ' + ex);
                }
            }
        }
    }
    
    /*******************************************************************************************************
    * @description When HealthCloudGA__EhrRelatedPerson__c is created, sync Account_Status__c and  Account_Status_Youreka__c
    *   
    */
    private static void insertSyncAccountStatus(){
        
        for(HealthCloudGA__EhrRelatedPerson__c guar : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.new){
            if(guar.Account_Status__c != null){
                Schema.DescribeFieldResult fieldResult = HealthCloudGA__EhrRelatedPerson__c.Account_Status__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == guar.Account_Status__c){
                        guar.Account_Status_Youreka__c = pickListVal.getLabel();
                    }
                }
                System.debug('guar.Account_Status_Youreka__c: ' + guar.Account_Status_Youreka__c);
            }
            
            if(guar.Account_Status_Youreka__c != null){
                Schema.DescribeFieldResult fieldResult = HealthCloudGA__EhrRelatedPerson__c.Account_Status__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == guar.Account_Status_Youreka__c){
                        guar.Account_Status__c = pickListVal.getValue();
                    }
                }
                System.debug('guar.Account_Status__c: ' + guar.Account_Status__c);
            }
        }
    }    
    
    /*******************************************************************************************************
    * @description When HealthCloudGA__EhrRelatedPerson__c is udated, sync Account_Status__c and Account_Status_Youreka__c
    *   
    */
    private static void updateSyncAccountStatus(){
        for(HealthCloudGA__EhrRelatedPerson__c ehr : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.newMap.values()){
            HealthCloudGA__EhrRelatedPerson__c oldEhr = (HealthCloudGA__EhrRelatedPerson__c)Trigger.oldMap.get(ehr.ID);
            HealthCloudGA__EhrRelatedPerson__c newEhr = (HealthCloudGA__EhrRelatedPerson__c)Trigger.newMap.get(ehr.ID);
            
            if(newEhr.Account_Status__c != oldEhr.Account_Status__c){
                Schema.DescribeFieldResult fieldResult = HealthCloudGA__EhrRelatedPerson__c.Account_Status__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == newEhr.Account_Status__c){
                        newEhr.Account_Status_Youreka__c = pickListVal.getLabel();
                    }
                }             
            }
            
            if(newEhr.Account_Status_Youreka__c != oldEhr.Account_Status_Youreka__c){
                Schema.DescribeFieldResult fieldResult = HealthCloudGA__EhrRelatedPerson__c.Account_Status__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == newEhr.Account_Status_Youreka__c){
                        string pickVa = pickListVal.getValue();
                        newEhr.Account_Status__c = pickVa;
                    }
                }
            }
        }
    }
    
    /*******************************************************************************************************
    * @description When HealthCloudGA__EhrRelatedPerson__c is udated, sync Responsible_Party__c and Rel_to_Patient_Youreka__c
    *   
    */
    private static void updateSyncResponsibleParty(){
        for(HealthCloudGA__EhrRelatedPerson__c ehr : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.newMap.values()){
            HealthCloudGA__EhrRelatedPerson__c oldEhr = (HealthCloudGA__EhrRelatedPerson__c)Trigger.oldMap.get(ehr.ID);
            HealthCloudGA__EhrRelatedPerson__c newEhr = (HealthCloudGA__EhrRelatedPerson__c)Trigger.newMap.get(ehr.ID);
            
            if(newEhr.Responsible_Party__c != oldEhr.Responsible_Party__c){
                Schema.DescribeFieldResult fieldResult = HealthCloudGA__EhrRelatedPerson__c.Responsible_Party__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == newEhr.Responsible_Party__c){
                        newEhr.Rel_to_Patient_Youreka__c = pickListVal.getLabel();
                    }
                }             
            }
            
            if(newEhr.Rel_to_Patient_Youreka__c != oldEhr.Rel_to_Patient_Youreka__c){
                Schema.DescribeFieldResult fieldResult = HealthCloudGA__EhrRelatedPerson__c.Responsible_Party__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == newEhr.Rel_to_Patient_Youreka__c){
                        string pickVa = pickListVal.getValue();
                        newEhr.Responsible_Party__c = pickVa;
                    }
                }
            }
        }
    }
    
    /*******************************************************************************************************
    * @description When HealthCloudGA__EhrRelatedPerson__c is udated, sync Sex__c and  Sex_Youreka__c
    *   
    */
    private static void updateSyncSex(){
        for(HealthCloudGA__EhrRelatedPerson__c ehr : (List<HealthCloudGA__EhrRelatedPerson__c>)Trigger.newMap.values()){
            HealthCloudGA__EhrRelatedPerson__c oldEhr = (HealthCloudGA__EhrRelatedPerson__c)Trigger.oldMap.get(ehr.ID);
            HealthCloudGA__EhrRelatedPerson__c newEhr = (HealthCloudGA__EhrRelatedPerson__c)Trigger.newMap.get(ehr.ID);
            
            if(newEhr.Sex__c != oldEhr.Sex__c){
                Schema.DescribeFieldResult fieldResult = HealthCloudGA__EhrRelatedPerson__c.Sex__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getValue() == newEhr.Sex__c){
                        newEhr.Sex_Youreka__c = pickListVal.getLabel();
                    }
                }             
            }
            
            if(newEhr.Sex_Youreka__c != oldEhr.Sex_Youreka__c){
                Schema.DescribeFieldResult fieldResult = HealthCloudGA__EhrRelatedPerson__c.Sex__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(pickListVal.getLabel() == newEhr.Sex_Youreka__c){
                        string pickVa = pickListVal.getValue();
                        newEhr.Sex__c = pickVa;
                    }
                }
            }
        }
    }
}