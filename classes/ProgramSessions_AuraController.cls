/*
 * Public without sharing, because Sessions are not private. 
 */
/**
* @author Synaptic
* @date February 2021
*
* @group DHAS
* @group-content ../../ApexDocContent/DHAS.htm
*
* @description A controller of Process Builder and Lightning Component. It provides @InvocableMethod/@AuraEnabled methods to support
* Account Query, Program Query, Session Query, and Email Match
*/
public without sharing class ProgramSessions_AuraController {
    
    /*@AuraEnabled
    public static String queryAccount(String memberId) {
        Id userId = UserInfo.getUserId();
        User u = [SELECT Id, ContactId, Contact.AccountId from User where Id = :userId];
        system.debug(u);
        return u.Contact.AccountId;
    }
    
    @AuraEnabled
    public static CareProgram queryProgram(String programId) {
        if(String.isEmpty(programId)) return null;
        Id pId = programId;
        
        CareProgram p;
        try {
            Schema.SObjectType token = pId.getSObjectType();
            System.debug(token);
            p = [SELECT Id, Name, Primary_Campaign__c FROM CareProgram WHERE Id = :pId];
        }
        catch(Exception e) {
            System.debug('Unable to find Program based on query for Program with Id: ' + programId);
            return null;
        }
        return p;
    }
    
    @AuraEnabled
    public static List<Session__c> querySessions(String programId) {
        
        List<Session__c> sessions = new List<Session__c>();
        try {
            Id pId = programId;
            Schema.SObjectType token = pId.getSObjectType();
            
            if(token == Schema.CareProgram.SObjectType) {
                sessions = [SELECT Id, Name, Scheduled_Start__c, Session_Details__c, Program_Name__c, Community_Program_Redirect__c FROM Session__c WHERE Program__c = :pId AND Scheduled_Start__c > :System.NOW()];
            }
            else {
                sessions = [SELECT Id, Name, Scheduled_Start__c, Session_Details__c, Program_Name__c, Community_Program_Redirect__c FROM Session__c WHERE Scheduled_Start__c > :System.NOW()];
            }
        }
        catch(Exception e) {
            System.debug('Unable to find Sessions based on query for Program with Id: ' + programId);
            try {
                sessions = [SELECT Id, Name, Scheduled_Start__c, Session_Details__c, Program_Name__c, Community_Program_Redirect__c FROM Session__c WHERE Scheduled_Start__c > :System.NOW()];
            }
            catch(Exception e2) {
                System.debug('Unable to find Sessions based on query without a Program Id: ' + e.getMessage());
                return null;
            }
        }
        return sessions;
    }*/

    /*******************************************************************************************************
    * @description Returns a list of EmailResults
    * @param emails a list of emails
    * @return a list of EmalResults
    */
    @InvocableMethod(label='Email Search' description='Find Account or Lead Id with Email')
    public static List<EmailResults> findMatch(List<String> emails) {
        System.debug('Find results for: ' + emails);
        // check for value and clean up input
        String email;
        if(emails.isEmpty() || String.isEmpty(emails[0])) return null;
        else email = emails[0].trim();
        EmailResults r = new EmailResults();
        try {
            List<Account> matchingAccounts = [SELECT Id FROM Account WHERE PersonEmail = :email AND IsPersonAccount = true ORDER BY LastModifiedDate DESC];
            if(!matchingAccounts.isEmpty()) {
                r.accountId = matchingAccounts[0].Id;
            }
            else {
                List<Lead> matchingLeads = [SELECT Id FROM Lead WHERE Email = :email ORDER BY LastModifiedDate DESC];
                if(!matchingLeads.isEmpty()) {
                    r.leadId = matchingLeads[0].Id;
                }
            }
        }
        catch(Exception e) {
            System.debug('Error finding a match! ' + e.getMessage() + '\n' + e.getStackTraceString());
        }
        
        return new List<EmailResults>{r};
    }


}