public class EmailResults {
    @AuraEnabled
    @InvocableVariable(label='Lead Id Matched')
    public String leadId;
    
    @AuraEnabled
    @InvocableVariable(label='Account Id Matched')
    public String accountId;
}