/**
 * @author Ian McNear
 * @date 03.17.2021
 * @group DCRC
 * @testCoverage 93%
 */

@RestResource(urlMapping='/session/registrant/*')
global with sharing class ApiRegistrant {
    
    // Post method
    @HttpPost
    global static PostResponseWrapper createRegistrant(String sessionId, String salutation, String firstName, String middleName, String lastName, 
            String suffix, String addressLine1, String addressLine2, String city, String state, String zipCode, String email, String phone,
            Integer numberOfGuests, String comments, String previouslySeen, String referringSource, String utm_source, String utm_medium, String utm_campaign, 
            String utm_content, String keyword) {
        // Set API Values
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        PostResponseWrapper response = new PostResponseWrapper();
        
        // Set save point in case we need to rollback
   		Savepoint sp = Database.setSavepoint();
        try {

            if (sessionId == null || sessionId.length() == 0 || lastName == null || lastName.length() == 0 || numberOfGuests == null) {
                res.StatusCode = 400;
                response.message = 'SessionId and LastName fields are required. Or NumberOfGuests must be an integer.';
            } else {
                List<Session__c> sessionList = [SELECT Slots_Remaining__c FROM Session__c WHERE Id = :sessionId];

                if (sessionList.isEmpty()) { 
                    res.StatusCode = 404;
                    response.message = 'SessionId does not return a valid session.';
                } else if (1 + numberOfGuests > sessionList[0].Slots_Remaining__c) {
                    res.StatusCode = 406;
                    response.message = 'Slots Remaining is less then the registrants and number of guests. Slots Remaining: ' + sessionList[0].Slots_Remaining__c + ' Registering: ' + (1 + numberOfGuests);
                } else {
                    // Create a Lead
                    Lead theLead = new Lead(
                        FirstName = firstName,
                        LastName = lastName,
                        Salutation = salutation,
                        Suffix = suffix,
                        City = city,
                        State = state,
                        PostalCode =  zipCode,
                        Email = email,
                        Phone = phone
                    );

                    insert theLead;

                    // Create the registrant.
                    Session_Registrant__c myRegistrant = new Session_Registrant__c(
                        Session__c = sessionId,
                        Lead__c = theLead.Id,
                        Salutation__c = salutation,
                        First_Name__c = firstName,
                        Middle_Name__c = middleName,
                        Last_Name__c = lastName,
                        Address__c = addressLine1,
                        Address_2__c = addressLine2,
                        City__c = city,
                        State__c = state,
                        Zip_Code__c = zipCode,
                        Email__c = email,
                        Phone__c = phone,
                        Number_of_Guests__c = numberOfGuests,
                        Comment__c = comments,
                        Previously_Seen__c = previouslySeen,
                        Referring_Source__c = referringSource,
                        UTM_Source__c = utm_source,
                        UTM_Medium__c = utm_medium,
                        UTM_Campaign__c = utm_campaign,
                        UTM_Content__c = utm_content,
                        Keyword__c = keyword
                    );
                    
                    // Can use this to make updates to the case.
                    insert myRegistrant;
                    response.registrantId = myRegistrant.Id;
                    res.StatusCode = 201;
                    response.status = 'Success';
                }
            }
        } catch (Exception e) {
            // Roll back the database changes.
            Database.rollback(sp);
            res.StatusCode = 500;
            response.message = 'Your request failed with the following error: ' + e.getLineNumber() + ' ' + e.getMessage();
        }
        
        return response;
    }
    
    // Response Wrapper so we can have our own status's and messages
    global with sharing class PostResponseWrapper {
        @TestVisible Id registrantId;
        @TestVisible String status;
        @TestVisible String message;

        public PostResponseWrapper() {
            // Set to error on default. Avoid having to set it later.
            status = 'Error';
            message = 'Session Registrant was created successfully.';
        }
    }
}