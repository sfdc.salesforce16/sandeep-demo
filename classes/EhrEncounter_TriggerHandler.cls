/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * This class implements the TriggerHandler Framework for the HealthCloudGA__EhrEncounter__c object.
 * 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author      Synaptic
 * @project     DHAS
 * @since       February 2021
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public with sharing class EhrEncounter_TriggerHandler extends TriggerHandler {
    public EhrEncounter_TriggerHandler() {

    }

    public override void beforeInsert(){
        System.debug('beforeInsert');
        updateVerifiedFields();
        // updateAddressVerified();
        // updateEmailVerified();
        // updateContactVerified();
        // updateSpecialNeedsVerified();
        // updateInterpreterNeededVerified();
        // updatePCPVerified();
        // updateGuarantorVerified();
    }
/*
    public override void beforeUpdate(){
        System.debug('beforeUpdate');
    }

    public override void afterUpdate(){
    }
*/
    /*  When an EHR Encounter record is created, if the associated Account's "Address Verified" = True 
    *   -> set "Address Verified Date" = Date of EHR Encounter & reset the 30 day timer
    *   If "Address Verified" = False -> Set "Address Verified" = True,  "Address Verified Date" = 
    *   Date of EHR Encounter & reset the 30 day timer
    */
    private static void updateVerifiedFields(){

        for(HealthCloudGA__EhrEncounter__c ehrE : (List<HealthCloudGA__EhrEncounter__c>)Trigger.new){
            if(ehrE.HealthCloudGA__Account__c	 != null){
                    System.debug('ehrE.HealthCloudGA__Account__c	 ' + ehrE.HealthCloudGA__Account__c	);
                    account a = [Select Id, Address_Verified__c, Address_Verified_Date__c, 
                                Email_Verified__c, Email_Verified_Date__c, 
                                Contact_Information_Verified__c, Contact_Information_Verified_Date__c,
                                Special_Needs_Verified__c, Special_Needs_Verified_Date__c
                                From Account Where Id = :ehrE.HealthCloudGA__Account__c	 Limit 1];
                    System.debug('account a ' + a);
                    a.Address_Verified__c = true;
                    a.Email_Verified__c = true;
                    a.Contact_Information_Verified__c = true;
                    a.Special_Needs_Verified__c = true;
                    a.Address_Verified_Date__c = date.Today();
                    a.Email_Verified_Date__c = date.Today();
                    a.Contact_Information_Verified_Date__c = date.Today();
                    a.Special_Needs_Verified_Date__c = date.Today();
                try {
                        update a;
                } catch (Exception ex) {
                    System.debug('CopyGuarantorRecord.exception: ' + ex);
                } 
            }
        }
    }

    /*  When an EHR Encounter record is created, if the associated Account's "Email Verified" = True 
    *   -> set "Email Verified Date" = Date of EHR Encounter & reset the 30 day timer
    *   If "Email Verified" = False -> Set "Email Verified" = True,  "Email Verified Date" = 
    *   Date of EHR Encounter & reset the 30 day timer
    */
    /*
    private static void updateEmailVerified(){

        for(HealthCloudGA__EhrEncounter__c ehrE : (List<HealthCloudGA__EhrEncounter__c>)Trigger.new){

            if(ehrE.HealthCloudGA__Account__c != null){
                System.debug('ehrE.HealthCloudGA__Account__c ' + ehrE.HealthCloudGA__Account__c	);
                account a = [Select Id, Email_Verified__c, Email_Verified_Date__c From Account Where Id = :ehrE.HealthCloudGA__Account__c Limit 1];
                if(a.Email_Verified__c == true){
                    a.Email_Verified_Date__c = date.Today();
                }else if(a.Email_Verified__c == false){
                    a.Email_Verified__c = true;
                    a.Email_Verified_Date__c = date.Today();
                }
            }
        }
    }
    */
    
    /*  When an EHR Encounter record is created, if the associated Account's "Contact Information Verified" 
    *   = True -> set "Contact Information Verified Date" = Date of EHR Encounter & reset the 30 day timer
    *   If "Contact Information Verified" = False -> Set "Address Verified" = True,  "Contact Information Verified 
    *   Date" = Date of EHR Encounter & reset the 30 day timer
    */
    /*
    private static void updateContactVerified(){

        for(HealthCloudGA__EhrEncounter__c ehrE : (List<HealthCloudGA__EhrEncounter__c>)Trigger.new){

            if(ehrE.HealthCloudGA__Account__c != null){
                System.debug('ehrE.HealthCloudGA__Account__c ' + ehrE.HealthCloudGA__Account__c);
                account a = [Select Id, Contact_Information_Verified__c, Contact_Information_Verified_Date__c From Account Where Id = :ehrE.HealthCloudGA__Account__c Limit 1];
                if(a.Contact_Information_Verified__c == true){
                    a.Contact_Information_Verified_Date__c = date.Today();
                }else if(a.Contact_Information_Verified__c == false){
                    a.Contact_Information_Verified__c = true;
                    a.Contact_Information_Verified_Date__c = date.Today();
                }
            }
        }
    }
    */
    /*  When an EHR Encounter record is created, if the associated Account's "SpecialNeeds Verified" = True 
    *   -> set "SpecialNeeds Verified Date" = Date of EHR Encounter & reset the 30 day timer
    *   If "SpecialNeeds Verified" = False -> Set "SpecialNeeds Verified" = True,  "SpecialNeeds Verified Date" = 
    *   Date of EHR Encounter & reset the 30 day timer
    */
    /*
    private static void updateSpecialNeedsVerified(){

        for(HealthCloudGA__EhrEncounter__c ehrE : (List<HealthCloudGA__EhrEncounter__c>)Trigger.new){

            if(ehrE.HealthCloudGA__Account__c != null){
                System.debug('ehrE.HealthCloudGA__Account__c ' + ehrE.HealthCloudGA__Account__c);
                account a = [Select Id, Special_Needs_Verified__c, Special_Needs_Verified_Date__c From Account Where Id = :ehrE.HealthCloudGA__Account__c Limit 1];
                if(a.Special_Needs_Verified__c == true){
                    a.Special_Needs_Verified_Date__c = date.Today();
                }else if(a.Special_Needs_Verified__c == false){
                    a.Special_Needs_Verified__c = true;
                    a.Special_Needs_Verified_Date__c = date.Today();
                }
            }
        }
    }
    */
    /*  When an EHR Encounter record is created, if the associated Account's "InterpreterNeeded Verified" = True 
    *   -> set "InterpreterNeeded Verified Date" = Date of EHR Encounter & reset the 30 day timer
    *   If "InterpreterNeeded Verified" = False -> Set "InterpreterNeeded Verified" = True,  "InterpreterNeeded Verified Date" = 
    *   Date of EHR Encounter & reset the 30 day timer
    */
    /*
    private static void updateInterpreterNeededVerified(){

        for(HealthCloudGA__EhrEncounter__c ehrE : (List<HealthCloudGA__EhrEncounter__c>)Trigger.new){

            if(ehrE.HealthCloudGA__Account__c != null){
                System.debug('ehrE.HealthCloudGA__Account__c ' + ehrE.HealthCloudGA__Account__c);
                account a = [Select Id, Interpreter_Needed_Verified__c, Interpreter_Needed_Verified_Date__c From Account Where Id = :ehrE.HealthCloudGA__Account__c Limit 1];
                if(a.Interpreter_Needed_Verified__c == true){
                    a.Interpreter_Needed_Verified_Date__c = date.Today();
                }else if(a.Interpreter_Needed_Verified__c == false){
                    a.Interpreter_Needed_Verified__c = true;
                    a.Interpreter_Needed_Verified_Date__c = date.Today();
                }
            }
        }
    }
    */
    /*  When an EHR Encounter record is created, if the associated Account's "PCP Verified" = True 
    *   -> set "PCP Verified Date" = Date of EHR Encounter & reset the 30 day timer
    *   If "PCP Verified" = False -> Set "PCP Verified" = True,  "PCP Verified Date" = 
    *   Date of EHR Encounter & reset the 30 day timer
    */
    /*
    private static void updatePCPVerified(){

        for(HealthCloudGA__EhrEncounter__c ehrE : (List<HealthCloudGA__EhrEncounter__c>)Trigger.new){

            if(ehrE.HealthCloudGA__Account__c != null){
                System.debug('ehrE.HealthCloudGA__Account__c ' + ehrE.HealthCloudGA__Account__c);
                account a = [Select Id, PCP_Verified__c, PCP_Verified_Date__c From Account Where Id = :ehrE.HealthCloudGA__Account__c Limit 1];
                if(a.PCP_Verified__c == true){
                    a.PCP_Verified_Date__c = date.Today();
                }else if(a.PCP_Verified__c == false){
                    a.PCP_Verified__c = true;
                    a.PCP_Verified_Date__c = date.Today();
                }
            }
        }
    }
    */
    /*  When an EHR Encounter record is created, if the associated Account's "Guarantor Verified" = True 
    *   -> set "Guarantor Verified Date" = Date of EHR Encounter & reset the 30 day timer
    *   If "Guarantor Verified" = False -> Set "Guarantor Verified" = True,  "Guarantor Verified Date" = 
    *   Date of EHR Encounter & reset the 30 day timer
    */
    /*
    private static void updateGuarantorVerified(){

        for(HealthCloudGA__EhrEncounter__c ehrE : (List<HealthCloudGA__EhrEncounter__c>)Trigger.new){

            if(ehrE.HealthCloudGA__Account__c != null){
                System.debug('ehrE.HealthCloudGA__Account__c ' + ehrE.HealthCloudGA__Account__c);
                account a = [Select Id, Guarantor_Verified__c, Guarantor_Verified_Date__c From Account Where Id = :ehrE.HealthCloudGA__Account__c Limit 1];
                if(a.Guarantor_Verified__c == true){
                    a.Guarantor_Verified_Date__c = date.Today();
                }else if(a.Guarantor_Verified__c == false){
                    a.Guarantor_Verified__c = true;
                    a.Guarantor_Verified_Date__c = date.Today();
                }
            }
        }
    }
    */
}