/******************************************
 * 
 * @author Synaptic AP
 * @since  Feb 2021
 * Project DHAS
 */
public with sharing class WaitlistController {
    public WaitlistController() {}

    @AuraEnabled
    public static List<WaitListEntry> callEpicWaitlist(String accountId){
        Account patient = [SELECT Id, HealthCloudGA__MedicalRecordNumber__c FROM Account WHERE Id = :accountId LIMIT 1];
        String mrn = patient.HealthCloudGA__MedicalRecordNumber__c;

        //validation
        if(String.isBlank(mrn)){ return null;}

        EpicWebService ews = new EpicWebService();
        HTTPResponse res = ews.call('waitlist', mrn, 'GET');

        List<WaitListEntry> epicWaitListEntries = (List<WaitListEntry>) JSON.deserialize(res.getBody(), List<WaitListEntry>.class);
        System.debug('epicWaitListEntries: ' + epicWaitListEntries);

        return epicWaitListEntries;
    }
}