trigger MemberPlanTrigger on MemberPlan (before insert, before update, after update) {
	new MemberPlan_TriggerHandler().run();
}