/* This trigger was created by the Youreka package and is integral to it. 
Please do not delete */
trigger Youreka_SessionLD_trigger on Session__c (after update){
    disco.Util.updateAnswersInLinkedSections(trigger.new,'Session__c');
}