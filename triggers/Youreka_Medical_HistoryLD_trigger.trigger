/* This trigger was created by the Youreka package and is integral to it. 
Please do not delete */
trigger Youreka_Medical_HistoryLD_trigger on Medical_History__c (after update){
    disco.Util.updateAnswersInLinkedSections(trigger.new,'Medical_History__c');
}