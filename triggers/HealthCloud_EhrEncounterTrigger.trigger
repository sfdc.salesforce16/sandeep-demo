trigger HealthCloud_EhrEncounterTrigger on HealthCloudGA__EhrEncounter__c (before insert) {
	new EhrEncounter_TriggerHandler().run();
}