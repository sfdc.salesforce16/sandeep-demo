/* This trigger was created by the Youreka package and is integral to it. 
Please do not delete */
trigger Youreka_TaskLD_trigger on Task (after update){
    disco.Util.updateAnswersInLinkedSections(trigger.new,'Task');
}