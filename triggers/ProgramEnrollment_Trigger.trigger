trigger ProgramEnrollment_Trigger on Program_Enrollment__c (after insert) {
    
    if(Trigger.isAfter && Trigger.isInsert) {
        ProgramEnrollment_TriggerHandler.afterInsert();
    }

}