trigger PatientContact_c_Trigger on Patient_Contact__c (before insert, before update, after update) {
	new PatientContact_TriggerHandler().run();
}