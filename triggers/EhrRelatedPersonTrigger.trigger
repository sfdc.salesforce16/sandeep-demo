trigger EhrRelatedPersonTrigger on HealthCloudGA__EhrRelatedPerson__c (before insert, before update, after update, after insert) {
	new Guarantor_TriggerHandler().run();
}