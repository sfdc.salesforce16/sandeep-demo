trigger Form_Trigger on disco__Form__c (after update) {

    if(Trigger.isAfter && Trigger.isUpdate) { 
        Form_TriggerHandler.afterUpdate(); 
    }
    
}