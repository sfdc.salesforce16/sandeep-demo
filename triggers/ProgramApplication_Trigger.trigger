trigger ProgramApplication_Trigger on Program_Application__c (after insert) {
    
    if(Trigger.isAfter && Trigger.isInsert) {
        ProgramApplication_TriggerHandler.afterInsert();
    }

}