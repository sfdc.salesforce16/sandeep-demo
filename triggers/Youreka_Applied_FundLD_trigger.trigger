/* This trigger was created by the Youreka package and is integral to it. 
Please do not delete */
trigger Youreka_Applied_FundLD_trigger on Applied_Fund__c (after update){
    disco.Util.updateAnswersInLinkedSections(trigger.new,'Applied_Fund__c');
}