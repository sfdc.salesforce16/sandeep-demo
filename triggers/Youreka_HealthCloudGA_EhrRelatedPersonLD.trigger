/* This trigger was created by the Youreka package and is integral to it. 
Please do not delete */
trigger Youreka_HealthCloudGA_EhrRelatedPersonLD on HealthCloudGA__EhrRelatedPerson__c (after update){
    disco.Util.updateAnswersInLinkedSections(trigger.new,'HealthCloudGA__EhrRelatedPerson__c');
}