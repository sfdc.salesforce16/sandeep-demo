trigger AccountTrigger on Account (before insert, after insert, before update, after update) {
	new Account_TriggerHandler().run();
}