/* This trigger was created by the Youreka package and is integral to it. 
Please do not delete */
trigger Youreka_AllergiesLD_trigger on Allergies__c (after update){
    disco.Util.updateAnswersInLinkedSections(trigger.new,'Allergies__c');
}