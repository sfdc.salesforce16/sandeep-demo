/* This trigger was created by the Youreka package and is integral to it. 
Please do not delete */
trigger Youreka_Order_Custom_trigger on Order_Custom__c (after update){
    disco.Util.updateObjectsFieldLinkAnswers(trigger.new,'Order_Custom__c');
}