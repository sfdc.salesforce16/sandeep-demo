/* This trigger was created by the Youreka package and is integral to it. 
Please do not delete */
trigger Youreka_TransactionLD_trigger on Transaction__c (after update){
    disco.Util.updateAnswersInLinkedSections(trigger.new,'Transaction__c');
}