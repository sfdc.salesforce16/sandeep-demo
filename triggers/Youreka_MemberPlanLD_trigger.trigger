/* This trigger was created by the Youreka package and is integral to it. 
Please do not delete */
trigger Youreka_MemberPlanLD_trigger on MemberPlan (after update){
    disco.Util.updateAnswersInLinkedSections(trigger.new,'MemberPlan');
}