trigger Referral_Information_c_Trigger on Referral_Information__c (before insert, before update, after update) {
	new Referral_Information_c_TriggerHandler().run();
}