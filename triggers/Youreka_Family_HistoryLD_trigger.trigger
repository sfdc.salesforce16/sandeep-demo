/* This trigger was created by the Youreka package and is integral to it. 
Please do not delete */
trigger Youreka_Family_HistoryLD_trigger on Family_History__c (after update){
    disco.Util.updateAnswersInLinkedSections(trigger.new,'Family_History__c');
}