/* This trigger was created by the Youreka package and is integral to it. 
Please do not delete */
trigger Youreka_Patient_ContactLD_trigger on Patient_Contact__c (after update){
    disco.Util.updateAnswersInLinkedSections(trigger.new,'Patient_Contact__c');
}