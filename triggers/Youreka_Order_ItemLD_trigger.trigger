/* This trigger was created by the Youreka package and is integral to it. 
Please do not delete */
trigger Youreka_Order_ItemLD_trigger on Order_Item__c (after update){
    disco.Util.updateAnswersInLinkedSections(trigger.new,'Order_Item__c');
}