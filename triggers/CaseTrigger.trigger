trigger CaseTrigger on Case (before insert, after insert, before update, after update) {
	new Case_TriggerHandler().run();
}