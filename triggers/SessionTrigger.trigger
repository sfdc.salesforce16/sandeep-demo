trigger SessionTrigger on Session__c (After Update) {
    if(trigger.isAfter && trigger.isUpdate){
        SessionTriggerHandler.AfterUpdateMethod(trigger.new,trigger.oldmap);
    }
}