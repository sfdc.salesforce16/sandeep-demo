import { LightningElement, wire } from 'lwc';
import findContacts from '@salesforce/apex/ProviderLookupController.findContacts';

// Import message service features required for publishing and the message channel
import { publish, MessageContext } from 'lightning/messageService';
import RECORD_SELECTED_CHANNEL from '@salesforce/messageChannel/Record_Selected__c';

/** The delay used when debouncing event handlers before invoking Apex. */
const DELAY = 350;

export default class CompositionContactSearch extends LightningElement {
    contacts;
    contactSelected = false;
    error;
    msgForUser;
    _records = [];
    noRecords=false;
    contacts = [];
    selectedId;

    @wire(MessageContext)
    messageContext;

    handleKeyChange(event) {
        // Debouncing this method: Do not actually invoke the Apex call as long as this function is
        // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
        window.clearTimeout(this.delayTimeout);
        const searchKey = event.target.value;
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            findContacts({ searchKey })
                .then((result) => {
                    if(result.length){
                        this.contacts = result;
                        this.error = undefined;
                        this.noRecords=false;
                    } else {
                        this.noRecords=true;
                    }
                })
                .catch((error) => {
                    this.error = error;
                    this.msgForUser = 'There was an issue loading providers.';
                    this.contacts = undefined;
                });
        }, DELAY);
    }

    // handleClick() {
    //     this.contacts.forEach(c => {
    //         if (c.Id === this.selectedId) {
    //             c.selected = true;
    //         } else {
    //             c.selected = false;
    //         }
    //     });
    // }

    // Respond to UI event by publishing message
    handleContactSelect(event) {
        this.selectedId = event.detail.recordId;
        const payload = { pcpRecordId: event.detail.recordId,
                            practitionerId: event.detail.practitionerId,
                            pcpName: event.detail.pcpName };

        publish(this.messageContext, RECORD_SELECTED_CHANNEL, payload);
       // this.handleClick();
    }
}