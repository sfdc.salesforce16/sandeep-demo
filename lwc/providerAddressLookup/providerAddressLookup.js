import { LightningElement, api, wire, track } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import findProviderAddresses from '@salesforce/apex/ProviderAddressLookupController.findProviderAddresses';
import setProviderAddress from '@salesforce/apex/ProviderAddressLookupController.setProviderAddress';

export default class ProviderAddressLookup extends LightningElement {
    @api recordId;
    @api practitionerId;
    @api practitionerName;
    @api locationId;

    @track addresses;
    @track selectedMarkerValue;
    @track zoomLevel = 10;
    @track noRows = false;
	error;

    mapMarkers = [];
    
    currentPageReference = null; 
    urlStateParameters = null;

    @wire(CurrentPageReference)
    getStateParameters(currentPageReference) {
       if (currentPageReference) {
        this.urlStateParameters = currentPageReference.state;
          this.setParametersBasedOnUrl();
       }
    }
 
    setParametersBasedOnUrl() {
        this.recordId = this.urlStateParameters.c__recordId || null;
        this.practitionerId = this.urlStateParameters.c__practitionerId || null;
        this.practitionerName = this.urlStateParameters.c__practitionerName || null;
        console.log('this.urlStateParameters', this.urlStateParameters);
        console.log('this.urlStateParameters.c__practitionerName:', this.urlStateParameters.c__practitionerName);
    }    

    connectedCallback() {
        this.handleSearch();
    }    

    handleSearch(event){
        console.log('handleSearch: ' + this.practitionerId);

        window.clearTimeout(this.delayTimeout);

        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            findProviderAddresses({practitionerId : this.practitionerId})
            .then((result) => {
                console.log('result: ', result);
                this.addresses = result;

                for(var i = 0; i < result.length; i++){
                    var a = result[i];
                    if(a.IsActive){
                        var title = a.IsPrimary ? a.Phone + ' (Primary)' : a.Phone;
                        var street = a.StreetLine1 + '\n';
                        if(a.StreetLine2){ street += a.StreetLine2 + '\n';}
        
                        
                        var pin = {"location":{
                            "Street": street,
                            "City": a.City,
                            "PostalCode": a.Zip,
                            "State": a.StateAbbr,
                            "Country": a.CountryAbbr,
                            },
                            "value": a.UniqueID,
                            icon: 'standard:address',
                            "title": title};
                        this.mapMarkers.push(pin);
                        console.log(pin);
                    }
                }
                if(this.mapMarkers.length == 1){this.zoomLevel = 15;}
                console.log('zoomLevel: ' + this.zoomLevel);

                if(this.mapMarkers.length == 0){
                    this.noRows = true;
                    this.addresses = null;
                    return;
                }

                if(!this.addresses.length){
                    this.noRows = true;
                }else{
                    this.noRows = false;
                }
            })
            .catch((error) => {
                console.log('error', error);
                this.addresses = undefined;
            });
        }, 100);		
    }

    handleMarkerSelect(event) {
        this.selectedMarkerValue = event.target.selectedMarkerValue;
        console.log('selectedMarkerValue: ' + this.selectedMarkerValue);
        var address = '';
        for(var i = 0; i < this.addresses.length; i++){
            if(this.selectedMarkerValue == this.addresses[i].UniqueID){
                var a = this.addresses[i];
                var street = a.StreetLine1 + '\n';
                if(a.StreetLine2){ street += a.StreetLine2 + '\n';}
                address = street + a.City + ', ' + a.StateAbbr + '  ' + a.Zip + '\n' + a.Phone;
            }
        }

        setProviderAddress({
            accountId: this.recordId,
            locationId: this.selectedMarkerValue,
            address: address
        });
        
        const toastEvent = new ShowToastEvent({
            title: 'PCP Address Selected',
            message: 'Please close this tab to return and refresh the registration form.',
        });
        this.dispatchEvent(toastEvent);
    }
}