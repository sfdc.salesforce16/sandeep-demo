/******************************************
 * 
 * @author Synaptic AP
 * @since  Feb 2021
 * Project DHAS
 */
import { LightningElement, api, track } from 'lwc';
import callEpicDismissal from '@salesforce/apex/DismissalController.callEpicDismissal';

export default class Dismissal extends LightningElement {
	@api recordId;
	@track dismissalExpandRows = [];
	@track dismissalData;

	@track dismissalColumns = [
        { label: 'Reason', type: 'text', fieldName: 'ReasonForDismissal' },
        { label: 'EffectiveDate', type: 'date', fieldName: 'EffDateOfDismissal' },
        { label: 'EndDate', fieldName: 'DismissalEndDate', type: 'date'},
        { label: 'Level', fieldName: 'DismissalLevel'},
        { label: 'Department', fieldName: 'DeptDismissed'},
        { label: 'Notes', fieldName: 'DismissalNotes'},
    ];
	

	connectedCallback(){
		//var tempjson;
		console.log('callEpicDismissal');
		callEpicDismissal({ accountId : this.recordId})
			.then(data => {
				console.log('this.recordId: ' + this.recordId);
				console.log('data: ',data);
				this.dismissalData = data;
			})
			.catch(error => {
				console.log('errors.dismissal: ', error);
			});
	}
}