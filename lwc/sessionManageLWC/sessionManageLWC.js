import { LightningElement, wire, api } from 'lwc';
import fetchChildSessions from '@salesforce/apex/SessionController.fetchChildSessions';
import updateSessions from '@salesforce/apex/SessionController.updateSessions';
//
import deleteChildSessions from '@salesforce/apex/SessionController.deleteChildSessions';
import { refreshApex } from '@salesforce/apex';
import { updateRecord } from 'lightning/uiRecordApi';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import SCHEDULED_START_FIELD from '@salesforce/schema/Session__c.Scheduled_Start__c';
import SCHEDULED_END_FIELD from '@salesforce/schema/Session__c.Scheduled_End__c';
import LOCATION_FIELD from '@salesforce/schema/Session__c.Location__c';
import ID_FIELD from '@salesforce/schema/Session__c.Id';
import UserPreferencesRecordHomeReservedWTShown from '@salesforce/schema/User.UserPreferencesRecordHomeReservedWTShown';


const actions = [
    //{ label: 'Show details', name: 'show_details' },
    { label: 'Delete', name: 'delete' }
];
const COLS = [
    {
        label: 'Scheduled Start', fieldName: 'Scheduled_Start__c', type: "date",
        typeAttributes: {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "2-digit",
            minute: "2-digit"
        }, editable: true, hideDefaultActions: true
    },
    {
        label: 'Scheduled End', fieldName: 'Scheduled_End__c', type: "date",
        typeAttributes: {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "2-digit",
            minute: "2-digit"
        }, editable: true, hideDefaultActions: true
    },
    { label: 'Location', fieldName: 'Location__c', type: 'text', editable: true, hideDefaultActions: true },
    { type: 'action', typeAttributes: { rowActions: actions, menuAlignment: 'auto' } }

];

export default class SessionManageLWC extends LightningElement {
    sessions;
    error;
    rowCount = 0;

    columns = COLS;
    @api childSessionID;
    @api recordId;
    draftValues = [];
    mapOfRowToIds = new Map();

    @wire(fetchChildSessions, { parentSessionId: '$recordId' })
    wiredChildSession({ error, data }) {
        console.log(data, error);
        if (data) {
            this.buildModalSessions(data);
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.sessions = undefined;
        }
    }


    buildModalSessions(sessions) {
        console.log('buildModalSessions');
        console.log(JSON.parse(JSON.stringify(sessions)));

        let modifiedSessions = JSON.parse(JSON.stringify(sessions));
        const modalSessions = modifiedSessions.map((session, index) => {
            let { Id, Scheduled_End__c, Scheduled_Start__c, Location__c, Parent_Session__c } = session;
            let sessionModal = { rowNo: index + '-' + Id, Id, Scheduled_End__c, Scheduled_Start__c, Location__c, Parent_Session__c };
            this.rowCount = index;
            return sessionModal;
        });
        this.sessions = modalSessions;
    }

    // connectedCallback(){
    //     fetchChildSessions({ parentSessionId: this.recordId})
    //     .then((result) => {
    //         this.buildModalSessions(result);})
    //         .catch((error) => {
    //             this.error = error;
    //             this.session = undefined;
    //         });

    // }

    async handleSave(event) {
        //const updatedFields = event.detail.draftValues;

        const updatedFields = JSON.parse(JSON.stringify(event.detail.draftValues));
        console.log(updatedFields);
        // Prepare the record IDs for getRecordNotifyChange()

        const sessionRecords = updatedFields.map(session => {
            if (session.rowNo && session.rowNo.indexOf('-') > 0) {
                let sessionId = session.rowNo.split('-')[1];
                return {
                    'Id': sessionId,
                    'Scheduled_End__c': session.Scheduled_End__c,
                    'Scheduled_Start__c': session.Scheduled_Start__c,
                    'Location__c': session.Location__c,
                    'Parent_Session__c': session.Parent_Session__c
                }
            }
            else {
                return {
                    'Scheduled_End__c': session.Scheduled_End__c,
                    'Scheduled_Start__c': session.Scheduled_Start__c,
                    'Location__c': session.Location__c,
                    'Parent_Session__c': this.recordId
                }
            }

        });

        console.log(sessionRecords);
        // Pass edited fields to the updateContacts Apex controller
        const result = await updateSessions({ data: sessionRecords, parentSessionId: this.recordId })
            .then((result) => {
                console.log(JSON.stringify("Apex update result: " + result));
                this.buildModalSessions(result);

                const notifyChangeIds = this.sessions.map(row => { return { "recordId": row.Id } });//.filter(session => session.recordId !=  undefined);
                console.log(notifyChangeIds);
                getRecordNotifyChange(notifyChangeIds);

                this.draftValues = [];

                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Session(s) created/updated',
                        variant: 'success'
                    })
                );

                this.error = undefined;

            })
            .catch((error) => {
                this.error = error;
                this.session = undefined;
            });


        //refreshApex(this.sessions);
        //this.draftValues = [];

        // Display fresh data in the datatable
        // refreshApex(this.sessions).then((result) => {
        //     // Clear all draft values in the datatable
        //     console.log(result);
        //     this.draftValues = [];
        // });

    }

    newSession() {
        console.log('newSession');
        let tempSession = JSON.parse(JSON.stringify(this.sessions));

        this.rowCount = this.rowCount + 1;
        tempSession.push({ rowNo: this.rowCount, Id: '', Scheduled_End__c: '', Scheduled_Start__c: '', Location__c: '' });
        console.log(tempSession);
        this.sessions = [...tempSession];

    }

    handleRowAction(event) {
        debugger;     
        const action = event.detail.action;
       // this.childSessionID = event.target.value;
        const selectedsession = event.detail.row;
        this.childSessionID = selectedsession.Id;
        this.deleteSession();
        switch (action.name) {    
              
            case 'delete':
                const rows = this.sessions;                
                const rowIndex = rows.findIndex(session => session.rowNo === selectedsession.rowNo);
                rows.splice(rowIndex, 1);
                this.sessions = [...rows];                
                break;
        }
    }

    deleteSession() {
        deleteChildSessions({ SessionId: this.childSessionID })
            .then((result) => {
              debugger;
            })
            .catch((error) => {
                this.error = error;
                
            });
    }
}