import { LightningElement, track, wire, api } from 'lwc';

// import apex class methods
import getReferralInfos from '@salesforce/apex/PatientReferrals.getReferralInfos';
import getWaitlists from '@salesforce/apex/PatientReferrals.getWaitlists';
import getRecalls from '@salesforce/apex/PatientReferrals.getRecalls';
import getOrders from '@salesforce/apex/PatientReferrals.getOrders';
import getHealthMaint from '@salesforce/apex/PatientReferrals.getHealthMaintInfos';
import getDismissals from '@salesforce/apex/PatientReferrals.getDismissals';
import getFyiFlags from '@salesforce/apex/PatientReferrals.getFyiFlags';

// import to show toast notifictions
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

// import to update Flow variable
import { FlowAttributeChangeEvent } from 'lightning/flowSupport';

// import to refresh the apex if any record changes the datas
import {refreshApex} from '@salesforce/apex';

// row actions without edit
const actions = [
    { label: 'View Record', name: 'record_details'}, 
];


// referrals datatable
const columnsReferrals = [
    { label: 'Epic Referral ID', fieldName: 'idUrl', type: 'text', typeAttributes: {label: { fieldName: 'Referral_ID__c' }}, type: 'url' },
    { label: 'Referred By', fieldName: 'Referred_By_ID__c', typeAttributes: {label: { fieldName: 'Referred_By__c' }}, type: 'url'},
    { label: 'Referred To', fieldName: 'Referred_To__c', type: 'text'},
    { label: 'Status', fieldName: 'Referral_Status__c', type: 'text', sortable: 'true' },
    { label: 'Scheduling Status', fieldName: 'Scheduling_Status__c', type: 'text', sortable: 'true' },
    { label: 'Effective Date', fieldName: 'Effective_Date__c', type: 'date', sortable: 'true' },
];

// recalls datatable
const columnsRecalls = [
    { label: 'Id', fieldName: 'idUrl', type: 'text', typeAttributes: {label: { fieldName: 'Recall_Record_ID__c' }}, type: 'url' },
    { label: 'Providers', fieldName: 'Providers__c', type: 'text', sortable: 'true'  },
    { label: 'Specialty/Dept.', fieldName: 'Specialty_or_Department__c', type: 'text', sortable: 'true'  },
    { label: 'Status', fieldName: 'Recall_Status__c', type: 'text', sortable: 'true'  },
    { label: 'Visit Type', fieldName: 'Visit_Type__c', type: 'text', sortable: 'true'  },
    { label: 'Date', fieldName: 'Recall_Date__c', type: 'text', sortable: 'true'  }
];

// waitlists datatable
const columnsWaitlists = [
    { label: 'Name', fieldName: 'idUrl', type: 'text', typeAttributes: {label: { fieldName: 'Name' }}, type: 'url' },
    { label: 'Id', fieldName: 'Id', type: 'text' },
    { label: 'Dept.', fieldName: 'Department__c', type: 'text', sortable: 'true'  },
    { label: 'Provider', fieldName: 'Provider__c', type: 'text', sortable: 'true'  },
    { label: 'Visit Type', fieldName: 'Visit_Type__c', type: 'text', sortable: 'true'  },
    { label: 'Status', fieldName: 'Status__c', type: 'text', sortable: 'true'  },
    { label: 'Start Date', fieldName: 'Start_Date__c', type: 'text', sortable: 'true'  },
    { label: 'Until Date', fieldName: 'Until_Date__c', type: 'text', sortable: 'true'  }
];

// provider orders datatable
const columnsOrders = [
    { label: 'Id', fieldName: 'idUrl', type: 'text', typeAttributes: {label: { fieldName: 'Id' }}, type: 'url' },
    { label: 'Procedure/Visit Type', fieldName: 'Proc_Visit_Type__c', type: 'text', sortable: 'true'  },
    { label: 'Requesting Provider', fieldName: 'Requesting_Provider__c', type: 'text', sortable: 'true'  },
    { label: 'Categpry', fieldName: 'Category__c', type: 'text', sortable: 'true'  }
];

// Health Maintenance datatable
const columnsHealthMaint = [
    { label: 'Id', fieldName: 'idUrl', type: 'text', typeAttributes: {label: { fieldName: 'Id' }}, type: 'url', fixedWidth: 40 },
    { label: 'Name', fieldName: 'Name', type: 'text', sortable: 'true'  },
    { label: 'Due Date', fieldName: 'Due_Date__c', type: 'text', sortable: 'true'  },
    { label: 'Status', fieldName: 'Status__c', type: 'text', sortable: 'true'  },
    { label: 'Last Satisfied', fieldName: 'Last_Satisfied__c', type: 'text', sortable: 'true'  }
];

const columnsDismissal = [
    { label: 'Id', fieldName: 'idUrl', type: 'text', typeAttributes: {label: { fieldName: 'Id' }}, type: 'url', fixedWidth: 40 },
    { label: 'Reason', fieldName: 'Reason_for_Dismissal__c', type: 'text' },
    { label: 'EffectiveDate', fieldName: 'Dismissal_Effective_Date__c', type: 'date' },
    { label: 'EndDate', fieldName: 'Dismissal_End_Date__c', type: 'date'},
    { label: 'Level', fieldName: 'Dismissal_Level__c'},
    { label: 'Department', fieldName: 'Dept_Dismissed_Name__c'},
    { label: 'Notes', fieldName: 'Dismissal_Notes__c'},
];

const columnsFyiFlag = [
    { label: 'Id', fieldName: 'idUrl', type: 'text', typeAttributes: {label: { fieldName: 'Id' }}, type: 'url', fixedWidth: 40 },
    { label: 'Summary', type: 'text', fieldName: 'FYI_Flag_Text__c' },
    { label: 'Type', fieldName: 'FYI_Flag__c'},
    { label: 'Active', fieldName: 'FYI_Flag_Status__c'},
    { label: 'Contact', fieldName: 'contactUrl', typeAttributes: {label: { fieldName: 'EHR_Encounter' }}, type: 'url'},
    { label: 'Date', fieldName: 'FYI_Flag_Date__c'},
];

export default class DatatableLWC extends LightningElement { 
    // reactive variables
    @track data;
    @track columnsReferrals = columnsReferrals;
    @track columnsRecalls = columnsRecalls;
    @track columnsWaitlists = columnsWaitlists;
    @track columnsOrders = columnsOrders;
    @track columnsHealthMaint = columnsHealthMaint;
    @track columnsDismissal = columnsDismissal;
    @track columnsFyiFlag = columnsFyiFlag;
    @track record = [];
    @track bShowModal = false;
    @track currentRecordId;
    @track isViewForm = false;
    @track isEditForm = false;
    @track isCreateForm = false;
    @track showLoadingSpinner = false;
    @track referralInfos;
    @track recallInfos;
    @track waitlistInfos;
    @track OrderInfos;
    @track healthMaintInfos;
    @track dismissalInfos;
    @track fyiFlagInfos;
    @api recordId;
    @api createNewGuar = false;
    @api relName;
    @api allowNew = false;
    @api relationshipValue = '';
    @api listIconName;
    @api sObjct;
    @api referralsWQR = false;
    @api recallsWQR = false;
    @api waitlistsWQR = false;
    @api providerOrdersWQR = false;
    @api healthMaintWQR = false;
    @api dismissalWQR = false;
    @api fyiFlagWQR = false;
    @track sortBy;
    @track sortDirection;
    @api referralsLink;
    @api recallsLink;
    @api waitlistsLink;
    @api ordersLink;
    @api healthMaintLink;
    @api dismissalLink;
    @api fyiFlagLink;
    
    // non-reactive variables
    selectedRecords = [];
    refreshTable;
    error;

    connectedCallback(){
        console.log('this.recordId' + this.recordId);
        console.log('this.recallsWQR' + this.recallsWQR);
        console.log('this.waitlistsWQR' + this.waitlistsWQR);
        console.log('this.providerOrdersWQR' + this.providerOrdersWQR);
        console.log('this.referralsWQR' + this.referralsWQR);
        console.log('this.healthMaintWQR' + this.healthMaintWQR);
        if(this.referralsWQR == true){
            getReferralInfos({ recordId : this.recordId})
            .then((result) => {
                this.referralsLink = "/lightning/r/" + this.recordId + "/related/Referral_Informations__r/view";
                this.refreshTable = result;
                if (result) {
                    let allReferralInfos = [];
                    result.forEach(referralInfo => {
                        let referralInfoRecord = {};
                        referralInfoRecord.Name = referralInfo.Name;
                        referralInfoRecord.Referral_ID__c = referralInfo.Referral_ID__c;
                        referralInfoRecord.Referral_Type__c = referralInfo.Referral_Type__c;
                        referralInfoRecord.Referred_to_Provider__c = referralInfo.Referred_to_Provider__c;
                        referralInfoRecord.Referred_to_Department__c = referralInfo.Referred_to_Department__c;
                        referralInfoRecord.Referred_To__c = referralInfo.Referred_To__c;
                        referralInfoRecord.Referred_By__c = referralInfo.Referred_By__c;
                        referralInfoRecord.Referred_To_ID__c = '/' + referralInfo.Referred_To_ID__c;
                        referralInfoRecord.Referred_By_ID__c = '/' + referralInfo.Referred_By_ID__c;
                        referralInfoRecord.Referral_Status__c = referralInfo.Referral_Status__c;
                        referralInfoRecord.Expiration_Date__c = referralInfo.Expiration_Date__c;
                        referralInfoRecord.Scheduling_Status__c = referralInfo.Scheduling_Status__c;
                        referralInfoRecord.Effective_Date__c = referralInfo.Effective_Date__c;
                        referralInfoRecord.Id = referralInfo.Id;
                        referralInfoRecord.Start__c=referralInfo.Start__c;
                        referralInfoRecord.idUrl = '/' + referralInfo.Id;
                        allReferralInfos.push(referralInfoRecord);
                    });
                    this.referralInfos = allReferralInfos;
                }
                if (result.error) {
                    this.error = result.error;
                }
            })
        } else if(this.waitlistsWQR == true){
            getWaitlists({ recordId : this.recordId})
            .then((result) => {
                this.waitlistsLink = "/lightning/r/" + this.recordId + "/related/Wait_List1__r/view";
                this.refreshTable = result;
                if (result) {
                    let allWaitlistInfos = [];
                    result.forEach(waitlistInfo => {
                        let waitlistInfoRecord = {};
                        waitlistInfoRecord.Name = waitlistInfo.Name;
                        waitlistInfoRecord.Department__c = waitlistInfo.Department__c;
                        waitlistInfoRecord.Provider__c = waitlistInfo.Provider__c;
                        waitlistInfoRecord.Visit_Type__c = waitlistInfo.Visit_Type__c;
                        waitlistInfoRecord.Status__c = waitlistInfo.Status__c;
                        waitlistInfoRecord.Start_Date__c = waitlistInfo.Start_Date__c;
                        waitlistInfoRecord.Until_Date__c = waitlistInfo.Until_Date__c;
                        waitlistInfoRecord.Id = waitlistInfo.Id;
                        waitlistInfoRecord.idUrl = '/' + waitlistInfo.Id;
                        allWaitlistInfos.push(waitlistInfoRecord);
                    });
                    this.waitlistInfos = allWaitlistInfos;
                }
                if (result.error) {
                    this.error = result.error;
                }
            })
        } else if(this.providerOrdersWQR == true){
            getOrders({ recordId : this.recordId})
            .then((result) => {
                this.ordersLink = "/lightning/r/" + this.recordId + "/related/Provider_Orders__r/view";
                this.refreshTable = result;
                if (result) {
                    let allOrderInfos = [];
                    result.forEach(OrderInfo => {
                        let OrderInfoRecord = {};
                        OrderInfoRecord.Proc_Visit_Type__c = OrderInfo.Proc_Visit_Type__c;
                        OrderInfoRecord.Requesting_Provider__c = OrderInfo.Requesting_Provider__c;
                        OrderInfoRecord.Category__c = OrderInfo.Category__c;
                        OrderInfoRecord.Id = OrderInfo.Id;
                        OrderInfoRecord.idUrl = '/' + OrderInfo.Id;
                        allOrderInfos.push(OrderInfoRecord);
                    });
                    this.OrderInfos = allOrderInfos;
                }
                if (result.error) {
                    this.error = result.error;
                }
            })
        } else if(this.recallsWQR == true){
            getRecalls({ recordId : this.recordId})
            .then((result) => {
                this.recallsLink = "/lightning/r/" + this.recordId + "/related/Recalls__r/view";
                this.refreshTable = result;
                if (result) {
                    let allRecallInfos = [];
                    result.forEach(recallInfo => {
                        let recallInfoRecord = {};
                        recallInfoRecord.Recall_Record_ID__c = recallInfo.Recall_Record_ID__c;
                        recallInfoRecord.Providers__c = recallInfo.Providers__c;
                        recallInfoRecord.Specialty_or_Department__c = recallInfo.Specialty_or_Department__c;
                        recallInfoRecord.Recall_Status__c = recallInfo.Recall_Status__c;
                        recallInfoRecord.Visit_Type__c = recallInfo.Visit_Type__c;
                        recallInfoRecord.Recall_Date__c = recallInfo.Recall_Date__c;
                        recallInfoRecord.Id = recallInfo.Id;
                        recallInfoRecord.idUrl = '/' + recallInfo.Id;
                        allRecallInfos.push(recallInfoRecord);
                    });
                    this.recallInfos = allRecallInfos;
                }
                if (result.error) {
                    this.error = result.error;
                }
            })
        } else if(this.healthMaintWQR == true){
            getHealthMaint({ recordId : this.recordId})
            .then((result) => {
                console.log('getHealthMaint results', result);
                this.healthMaintLink = "/lightning/r/" + this.recordId + "/related/Health_Maintenance__r/view";
                this.refreshTable = result;
                if (result) {
                    let allHealthMaintInfos = [];
                    result.forEach(healthMaintInfo => {
                        console.log('healthMaintInfo', healthMaintInfo);
                        let healthMaintInfoRecord = {};
                        healthMaintInfoRecord.Name = healthMaintInfo.Name;
                        healthMaintInfoRecord.Due_Date__c = healthMaintInfo.Due_Date__c;
                        healthMaintInfoRecord.Last_Satisfied__c = healthMaintInfo.Last_Satisfied__c;
                        healthMaintInfoRecord.Status__c = healthMaintInfo.Status__c;
                        healthMaintInfoRecord.Id = healthMaintInfo.Id;
                        healthMaintInfoRecord.idUrl = '/' + healthMaintInfo.Id;
                        allHealthMaintInfos.push(healthMaintInfoRecord);
                    });
                    this.healthMaintInfos = allHealthMaintInfos;
                    console.log('this.healthMaintInfos', this.healthMaintInfos);
                }
                if (result.error) {
                    this.error = result.error;
                }
            })
        } else if(this.dismissalWQR == true){
            getDismissals({ recordId : this.recordId})
            .then((result) => {
                console.log('getDismissals results', result);
                this.dismissalLink = "/lightning/r/" + this.recordId + "/related/Dismissal__r/view";
                this.refreshTable = result;
                if (result) {
                    let allDismissalInfos = [];
                    result.forEach(dismissalInfo => {
                        console.log('dismissalInfo', dismissalInfo);
                        let dismissalInfoRecord = {};
                        dismissalInfoRecord.Reason_for_Dismissal__c = dismissalInfo.Reason_for_Dismissal__c;
                        dismissalInfoRecord.Dismissal_Effective_Date__c = dismissalInfo.Dismissal_Effective_Date__c;
                        dismissalInfoRecord.Dismissal_End_Date__c = dismissalInfo.Dismissal_End_Date__c;
                        dismissalInfoRecord.Dismissal_Level__c = dismissalInfo.Dismissal_Level__c;
                        dismissalInfoRecord.Dept_Dismissed_Name__c = dismissalInfo.Dept_Dismissed_Name__c;
                        dismissalInfoRecord.Dismissal_Notes__c = dismissalInfo.Dismissal_Notes__c;
                        dismissalInfoRecord.Id = dismissalInfo.Id;
                        dismissalInfoRecord.idUrl = '/' + dismissalInfo.Id;
                        allDismissalInfos.push(dismissalInfoRecord);
                    });
                    this.dismissalInfos = allDismissalInfos;
                    console.log('this.dismissalInfos', this.dismissalInfos);
                }
                if (result.error) {
                    this.error = result.error;
                }
            })
        } else if(this.fyiFlagWQR == true){
            getFyiFlags({ recordId : this.recordId})
            .then((result) => {
                console.log('getFyiFlags results', result);
                this.fyiFlagLink = "/lightning/r/" + this.recordId + "/related/Fyi_Flag__r/view";
                this.refreshTable = result;
                if (result) {
                    let allFyiFlagInfos = [];
                    result.forEach(fyiFlagInfo => {
                        console.log('fyiFlagInfo', fyiFlagInfo);
                        let fyiFlagInfoRecord = {};
                        fyiFlagInfoRecord.FYI_Flag_Text__c = fyiFlagInfo.FYI_Flag_Text__c;
                        fyiFlagInfoRecord.FYI_Flag__c = fyiFlagInfo.FYI_Flag__c;
                        fyiFlagInfoRecord.FYI_Flag_Status__c = fyiFlagInfo.FYI_Flag_Status__c;
                        fyiFlagInfoRecord.EHR_Encounter = fyiFlagInfo.EHR_Encounter__r.Name;
                        fyiFlagInfoRecord.FYI_Flag_Date__c = fyiFlagInfo.FYI_Flag_Date__c;
                        fyiFlagInfoRecord.Id = fyiFlagInfo.Id;
                        fyiFlagInfoRecord.idUrl = '/' + fyiFlagInfo.Id;
                        fyiFlagInfoRecord.contactUrl = '/' + fyiFlagInfo.EHR_Encounter__c;
                        console.log('fyiFlagInfoRecord:', fyiFlagInfoRecord);
                        allFyiFlagInfos.push(fyiFlagInfoRecord);
                    });
                    console.log('allFyiFlagInfos:', allFyiFlagInfos);
                    this.fyiFlagInfos = allFyiFlagInfos;
                    //this.fyiFlagInfos
                    console.log('this.fyiFlagInfos', this.fyiFlagInfos);
                }
                if (result.error) {
                    this.error = result.error;
                }
            })
        }
    }

    handleSortdata(event) {
        // field name
        this.sortBy = event.detail.fieldName;

        // sort direction
        this.sortDirection = event.detail.sortDirection;

        // calling sortdata function to sort the data based on direction and selected field
        this.sortData(event.detail.fieldName, event.detail.sortDirection);
    }

    sortData(fieldname, direction) {
        // serialize the data before calling sort function
        let parseData = JSON.parse(JSON.stringify(this.referralInfos));

        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };

        // cheking reverse direction 
        let isReverse = direction === 'asc' ? 1: -1;

        // sorting data 
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        // set the sorted data to data table data
        this.referralInfos = parseData;

    }


    handleRowActions(event) {
        let actionName = event.detail.action.name;

        let row = event.detail.row;

        // eslint-disable-next-line default-case
        switch (actionName) {
            case 'record_details':
                this.viewCurrentRecord(row);
                break;
            case 'edit':
                this.editCurrentRecord(row);
                break;
        }
    }

    // view the current record details
    viewCurrentRecord(currentRow) {
        this.bShowModal = true;
        this.isViewForm = true;
        this.isEditForm = false;
        this.isCreateForm = false;
        this.record = currentRow;
    }

    // closing modal box
    closeModal() {
        this.isViewForm = false;
        this.isEditForm = false;
        this.isCreateForm = false;
        this.bShowModal = false;
    }

    // refreshing the datatable after record edit form success
    handleSuccess() {
        return refreshApex(this.refreshTable);
    }

}