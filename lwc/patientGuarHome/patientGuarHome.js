import { LightningElement, track, wire, api } from 'lwc';

// importing apex class methods
import getGuar from '@salesforce/apex/PatientGuar.getGuar';

// import fields for record creation
import GUARANTOR_OBJECT from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c';
import NAME_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__NameFull__c';
import PHONE_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.Guarantor_Phone__c';
import TYPE_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.Guarantor_Type__c';
import LINE1_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Address1Line1__c';
import LINE2_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Address1Line2__c';
import CITY_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Address1City__c';
import STATE_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Address1State__c';
import COUNTRY_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Address1Country__c';
import ACCOUNT_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Account__c';
import RELATION_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Relationship__c';
import ACTIVE_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.Active__c';
//import DOB_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__BirthDate__c';

// importing to show toast notifictions
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

// import to update Flow variable
import { FlowAttributeChangeEvent } from 'lightning/flowSupport';

// importing to refresh the apex if any record changes the datas
import {refreshApex} from '@salesforce/apex';

// row actions
const actions = [
    { label: 'View Record', name: 'record_details'}, 
    { label: 'Edit Record', name: 'edit'}
];

// datatable columns with row actions
const columns = [
    { label: 'Full Name', fieldName: 'HealthCloudGA__NameFull__c' },
    { label: 'Type', fieldName: 'Guarantor_Type__c' },
    { label: 'Active', fieldName: 'Active__c',type:"boolean" },
    { label: 'Relationship', fieldName: 'HealthCloudGA__Relationship__c' },
    //{ label: 'Relationship', fieldName: 'HealthCloudGA__Relationship__c' },

    {
        type: 'action',
        typeAttributes: {
            rowActions: actions,
            menuAlignment: "slds-popover__body"
        }
    }
];

export default class DeleteRowsInDatatableLWC extends LightningElement { 
    // reactive variable
    @track data;
    @track columns = columns;
    @track record = [];
    @track bShowModal = false;
    @track currentRecordId;
    @track isViewForm = false;
    @track isEditForm = false;
    @track isCreateForm = false;
    @track showLoadingSpinner = false;
    @api recordId;
    @api createNewGuar;
    @api editedRecordIds; 
    @api createdRecordIds; 
    
    editedRecordIds = [];
    createdRecordIds = [];

    set editedRecordIds(editedRecordIds = []) {
        this.editedRecordIds = [...editedRecordIds];
    }

    set createdRecordIds(createdRecordIds = []) {
        this.createdRecordIds = [...createdRecordIds];
    }

    // non-reactive variables
    selectedRecords = [];
    refreshTable;
    error;

    handleCreate(event) {
        this.bShowModal = true;
        this.isViewForm = false;
        this.isEditForm = false;
        this.isCreateForm = true;
    }

    guarantorObject = GUARANTOR_OBJECT;
    nameField = NAME_FIELD;
    relationField = RELATION_FIELD;
    //dobField = DOB_FIELD;
    actiiveField = ACTIVE_FIELD;
    phoneField = PHONE_FIELD;
    line1Field = LINE1_FIELD;
    line2Field = LINE2_FIELD;
    cityField = CITY_FIELD;
    stateField = STATE_FIELD;
    countryField = COUNTRY_FIELD;
    typeField = TYPE_FIELD;
    accountField = ACCOUNT_FIELD;

    // retrieving the data using wire service
    @wire(getGuar, {recordId:'$recordId'})
    guarantors(result) {
        this.refreshTable = result;
        console.log(result);
        if (result.data) {
            this.data = result.data;
            this.error = undefined;

        } else if (result.error) {
            this.error = result.error;
            this.data = undefined;
        }
    }

    handleRowActions(event) {
        let actionName = event.detail.action.name;

        let row = event.detail.row;

        // eslint-disable-next-line default-case
        switch (actionName) {
            case 'record_details':
                this.viewCurrentRecord(row);
                break;
            case 'edit':
                this.editCurrentRecord(row);
                break;
        }
    }

    // view the current record details
    viewCurrentRecord(currentRow) {
        this.bShowModal = true;
        this.isViewForm = true;
        this.isEditForm = false;
        this.isCreateForm = false;
        this.record = currentRow;
    }

    // closing modal box
    closeModal() {
        this.isViewForm = false;
        this.isEditForm = false;
        this.isCreateForm = false;
        this.bShowModal = false;
    }


    editCurrentRecord(currentRow) {
        // open modal box
        this.bShowModal = true;
        this.isViewForm = false;
        this.isCreateForm = false;
        this.isEditForm = true;

        // assign record id to the record edit form
        this.currentRecordId = currentRow.Id;

        window.console.log('currentRecordId: ' + this.currentRecordId);
        //window.console.log('row: ' + this.currentRecordId);
        if(this.editedRecordIds.indexOf(this.currentRecordId) !== -1){
            window.console.log('editedRecordIds: ' + this.editedRecordIds);
        } else {
            this.editedRecordIds.push(this.currentRecordId);
            const attributeChangeEvent = new FlowAttributeChangeEvent('editedRecordIds', this.editedRecordIds);
            this.dispatchEvent(attributeChangeEvent);
            window.console.log('editedRecordIds: ' + this.editedRecordIds);
        }
    }

    // handling record edit form submit
    handleSubmit(event) {
        // prevending default type sumbit of record edit form
        event.preventDefault();

        console.log('handleSubmit');
        console.log('updated ', event.detail);

        // querying the record edit form and submiting fields to form
        this.template.querySelector('lightning-record-edit-form').submit(event.detail.fields);

        // closing modal
        this.bShowModal = false;

        // showing success message
        this.dispatchEvent(new ShowToastEvent({
            title: 'Success!!',
            message: event.detail.fields.HealthCloudGA__NameFull__c +' Record Updated Successfully!',
            variant: 'success'
        }),);

    }

    // handling record edit form submit
    handleCreatedSubmit(event) {
        // prevending default type sumbit of record edit form
//        event.preventDefault();
        
        console.log('handleGuarCreated');
//        console.log('created ', event.detail.id);

//        this.createdRecordIds.push(event.detail.id);
//        const attributeChangeEvent = new FlowAttributeChangeEvent('createdRecordIds', this.createdRecordIds);
//        this.dispatchEvent(attributeChangeEvent);
//        window.console.log('createdRecordIds: ' + this.createdRecordIds);

        // querying the record edit form and submiting fields to form
//        this.template.querySelector('lightning-record-edit-form').submit(event.detail.fields);

        // closing modal
        this.bShowModal = false;
        this.isCreateForm = false;

        // showing success message
        this.dispatchEvent(new ShowToastEvent({
            title: 'Success!',
            message: ' Record Created Successfully!',
            variant: 'success'
        }),);

        return refreshApex(this.refreshTable);
    }

    // refreshing the datatable after record edit form success
    handleSuccess() {
        return refreshApex(this.refreshTable);
    }

}