import { LightningElement, api, track } from 'lwc';
import hasMrn from '@salesforce/apex/RefreshOnMrnController.hasMrn';

export default class RefreshOnMrn extends LightningElement {
	@api recordId;
	@api isMrnReturned = false;
	timeIntervalInstance;
	timerInstance;
	@track totalMilliseconds = 0;

	counter = 0;
	
	connectedCallback(){
		var parentThis = this;

		console.log('refreshOnMrn.setInterval');
		console.log('refreshOnMrn: ' + parentThis.totalMilliseconds);
		//console.log('recordId: ' + this.recordId);
		//this.timerInstance = setInterval(this.timerFunc(this.recordId), 1000);
		this.timerInstance = setInterval(function(id){
			console.log('recordId: ' + parentThis.recordId);
			// parentThis.counter ++;
			// console.log('counter: ' + parentThis.counter);
			// if(parentThis.counter > 10){
			// 	eval("$A.get('e.force:refreshView').fire();");
			// }
			hasMrn({ accountId: parentThis.recordId })
				.then(result => {
					console.log('result:' + result);
					if(result){
						clearInterval(parentThis.timerInstance);
						parentThis.isMrnReturned = true;
						console.log('isMrnReturned: ' + parentThis.isMrnReturned);
						
						eval("$A.get('e.force:refreshView').fire();");
					}
				})

		}, 1000)
	}
}