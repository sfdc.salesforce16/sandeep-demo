import { LightningElement,api,track, wire } from 'lwc';

// importing apex class methods
import getCoverages from '@salesforce/apex/PatientCov.getCoverages';

// import refresh the apex if any record changes the datas
import {refreshApex} from '@salesforce/apex';

// guar table row actions without edit
const covActions = [
    { label: 'View Record', name: 'record_details'}, 
];

// setting up lightning-datatable
const columns = [
    { label: 'EpicId', fieldName: 'epicId__c', type: 'text' },
    { label: 'Plan', fieldName: 'Duke_Plan_Name__c', type: 'text' },
    { label: 'Payor', fieldName: 'Duke_Payer_Name__c', type: 'text' },
    { label: 'Group No.', fieldName: 'GroupNumber', type: 'text' },
    { label: 'Subscriber ID', fieldName: 'Subscriber_ID__c', type: 'text'},
    {
        type: 'action',
        typeAttributes: {
            rowActions: covActions,
            menuAlignment: "slds-popover__body"
        }
    }

];
export default class LightningDataTable extends LightningElement {
    @api recordId;
    @api editedRecordIds; 
    @api createdRecordIds; 
    @api selectNewCoverage;
    @track error; 
    @track data = [];
    @track columns = columns;
    @track coverages;
    @track record = [];
    @track bShowModal = false;
    @track currentRecordId;
    @track viewRecord = false;
    @track isEditForm = false;
    @track isDetachForm = false;
    @track addNewCoverage = false;
    @track showLoadingSpinner = false;
    editedRecordIds = [];
    createdRecordIds = [];
    selectedRecords = [];
    refreshTable;
    error;
    
    


    // wire data to datatable
    @wire(getCoverages, { recordId: '$recordId' }) 
    wired(result) {
        console.log('getCoverages.result:', result);
        this.refreshTable = result;
        if (result.data) {
            let allCoverages = [];
            result.data.forEach(coverage => {
                let coverageRecord = {};
                console.log('coverageRecord:', coverageRecord);
                coverageRecord.epicId__c = coverage.epicId__c;
                coverageRecord.GroupNumber = coverage.GroupNumber;
                coverageRecord.Duke_Plan_Name__c = coverage.Duke_Plan_Name__c;
                coverageRecord.Duke_Payer_Name__c = coverage.Duke_Payer_Name__c;
                coverageRecord.Subscriber_ID__c = coverage.Subscriber_ID__c;
                coverageRecord.EffectiveFrom = coverage.EffectiveFrom;
                coverageRecord.EffectiveTo = coverage.EffectiveTo;
                coverageRecord.Id = coverage.Id;
                allCoverages.push(coverageRecord);
                console.log(allCoverages);
            });
            this.coverages = allCoverages;
            console.log('coverages', this.coverages);
        }
        if (result.error) {
            this.error = result.error;
        }
    }

    handleRowActions(event) {
        let actionName = event.detail.action.name;

        let row = event.detail.row;

        // eslint-disable-next-line default-case
        switch (actionName) {
            case 'record_details':
                this.viewCurrentRecord(row);
                break;
            case 'edit':
                this.editCurrentRecord(row);
                break;
        }
    }

    // view the current record details
    viewCurrentRecord(currentRow) {
        this.bShowModal = true;
        this.isViewForm = true;
        this.isEditForm = false;
        this.isCreateForm = false;
        this.record = currentRow;
        this.recordUrl = '/' + currentRow.Id;
    }

    // closing modal box
    closeModal() {
        this.isViewForm = false;
        this.isEditForm = false;
        this.isCreateForm = false;
        this.bShowModal = false;
    }
    
    // refreshing the datatable after record edit form success
    handleSuccess() {
        return refreshApex(this.refreshTable);
    }
}