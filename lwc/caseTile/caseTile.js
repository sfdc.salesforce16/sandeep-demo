import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class CaseTile extends NavigationMixin(LightningElement) {
	@api record;
	@api recordId;
    @api
    get name() {
		if(this.record.Account != null){
			return this.record.Account.Name;
		}
        return this.record.Subject;
    }


    handleClick(event) {
		//Keep - we may want this later SM
        // 1. Prevent default behavior of anchor tag click which is to navigate to the href url
		//event.preventDefault();
		// this[NavigationMixin.Navigate]({
		// 	type: 'standard__recordPage',
		// 	attributes: {
		// 		recordId: this.account.Id,
		// 		objectApiName: 'Account',
		// 		actionName: 'view'
		// 	},
		// });		
		
		this.recordId = this.record.Id;

		const foundRecord = new CustomEvent("foundrecord", {
            detail: this.recordId
		});
		this.dispatchEvent(foundRecord);
    }	
}