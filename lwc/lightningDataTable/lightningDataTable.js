import { LightningElement, wire, track, api } from 'lwc';
import getSobjectRecords from '@salesforce/apex/LightningDataTableController.getSobjectRecords'
import TIME_ZONE from '@salesforce/i18n/timeZone';

var typeAttributes = {day: "numeric",month: "short", year: "numeric",hour: "2-digit",minute: "2-digit", second: "2-digit",hour12: true, timeZone: TIME_ZONE};

export default class LightningDataTable extends LightningElement {
    @track mydata;
    @track mycolumns;
    @track nameUrl;
    @track error;
    @api sObjectName;
    @api fieldSetName;
    @api recordId = '';
    @api sObjectLookupIDField = '';
    @api additionalWhereClause = '';
    @api tableTitle = '';
    @api includeName = false;
    @api orderBy = '';
    @api iconName;

    @wire(getSobjectRecords,({
        sObjectName : '$sObjectName',
        fieldSetName : '$fieldSetName',
        recordId : '$recordId',
        sObjectLookupIDField : '$sObjectLookupIDField',
        additionalWhereClause : '$additionalWhereClause',
        includeName: '$includeName',
        orderBy: '$orderBy'
    }))

   

    wiredAccounts({error, data}){
        if(data){
            if(this.includeName){
                var nameField = [{
                    label: 'Name',
                    fieldName: 'nameUrl',
                    type: 'url',
                    typeAttributes: {label: { fieldName: 'Name' }, 
                    target: '_self'},
                    sortable: true
                }];
                this.mycolumns = nameField.concat( data.listColumns );
                //console.log('this my columns:' + JSON.stringify(this.mycolumns) );
                let nameUrl;
                if(this.includeName)
                    this.mydata = data.dataTableData.map(row => { 
                        nameUrl = `/${row.Id}`;
                        return {...row , nameUrl} 
                    })
                //console.log('this my data:' + JSON.stringify(this.mydata) );
                this.error = undefined;
            }
            else{
                this.mydata = data.dataTableData;
                this.mycolumns = data.listColumns;
            }
            //convert date time
            this.mycolumns = this.mycolumns.map(element => { 
                if(element.type == 'date'){
                    return {...element,typeAttributes};
                }
                else return element;
            })
            console.log('this my converted columns:' + JSON.stringify(this.mycolumns) );
        }else if(error){
            this.error = error;
            this.mydata = undefined;    
            this.mycolumns = undefined;
        }
    }
}