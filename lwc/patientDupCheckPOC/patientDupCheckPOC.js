import { LightningElement, api, track } from 'lwc';
import { FlowAttributeChangeEvent, FlowNavigationNextEvent } from 'lightning/flowSupport';
import dupeCheck from '@salesforce/apex/PatientDupCheckController.dupeCheck';

export default class PatientDupCheck extends LightningElement {
    showModal = false;

	@api recordId;
    @api availableActions = [];	
    @api dob;
	@api first;
	@api last;
	@api gender;
	@api ssn;
    @api suffix;
    @api title;
	@api isExactMatch;
	@api exactMatchId;
    @track accounts;
    @api
    set header(value) {
        this.hasHeaderString = value !== '';
        this._headerPrivate = value;
    }
    get header() {
        return this._headerPrivate;
    }

    hasHeaderString = false;
    _headerPrivate;

    @api show() {
        this.showModal = true;
    }

    @api hide() {
        this.showModal = false;
    }

	get genderOptions() {
		return [
				 { label: 'Male', value: 'M' },
				 { label: 'Female', value: 'F' },
				 { label: 'Unknown', value: 'U' },
			   ];
	}

	get suffixOptions() {
		return [
				 { label: 'Sr.', value: 'SR.' },
				 { label: 'Jr.', value: 'JR.' },
				 { label: 'II', value: 'II' },
                 { label: 'III', value: 'III' },
                 { label: 'IV', value: 'IV' },
                 { label: 'V', value: 'V' },
                 { label: 'VI', value: 'VI' },
			   ];
	}

    get titleOptions() {
		return [
                 { label: 'Ms.', value: 'MS.' },
                 { label: 'Mr.', value: 'MR.' },
                 { label: 'Mrs.', value: 'MRS.' },
				 { label: 'Miss', value: 'MISS' },
                 { label: 'Dr.', value: 'DR.' },
                 { label: 'Fr.', value: 'FR.' },
                 { label: 'Sr.', value: 'SR.' },
                 { label: 'Rev.', value: 'REV.' },
                 { label: 'Lord', value: 'LORD' },
			   ];
	}


    connectedCallback(){
        window.clearTimeout(this.delayTimeout);
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            console.log('duplicate check', this.dob);
            	dupeCheck({ dob : this.dob,
                            title: this.title,
                            first : this.first,
                            last : this.last,
                            suffix: this.suffix,
                            gender : this.gender,
                            ssn : this.ssn
                        })
                .then((result) => {
                    console.log('result: ', result);
					this.accounts = result;
                })
                .catch((error) => {
                    console.log('error', error);
                    this.accounts = undefined;
                });
        }, 200);		
    }

    handleKeyChange(event){
        window.clearTimeout(this.delayTimeout);
        if(event.target.dataset.id === 'dob'){
            this.dob = event.target.value;
        }else if(event.target.dataset.id === 'first'){
            this.first = event.target.value;
        }else if(event.target.dataset.id == 'last'){
            this.last = event.target.value;
        }else if(event.target.dataset.id == 'gender'){
            this.gender = event.detail.value;
        }
		this.checkEpicForDupes();

    }

    handleTitleSuffixChange(event){
        window.clearTimeout(this.delayTimeout);
        if(event.target.dataset.id === 'title'){
            this.title = event.target.value;
        }else if(event.target.dataset.id === 'suffix'){
            this.suffix = event.target.value;
        }
    }

	handleUpdateSSN(event){
		this.ssn = event.detail;
		this.checkEpicForDupes();
	}

	checkEpicForDupes(){
        this.delayTimeout = setTimeout(() => {
            dupeCheck({ dob : this.dob,
				first : this.first,
				last : this.last,
				gender : this.gender,
				ssn : this.ssn})
			.then((result) => {
				this.accounts = result;
				if(result.length > 0 && result[0].Rank__c >= 62){
					this.isExactMatch = true;
					this.header = 'The patient record you are creating may be a duplicate of an existing patient record. Please review the list of potential duplicates and either select the appropriate record or create a new one if there is no appropriate record.';
					this.show();
				}
            })
            .catch((error) => {
                console.log('error', error);
				this.accounts = undefined;
            });
        }, 200);		
	}

	handleFoundRecord(event){
        this.recordId = event.detail;
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
	}

    handleDialogClose() {
        //Let parent know that dialog is closed (mainly by that cross button) so it can set proper variables if needed
        const closedialog = new CustomEvent('closedialog');
        this.dispatchEvent(closedialog);
        this.hide();
    }

    handleSlotTaglineChange() {
        // Only needed in "show" state. If hiding, we're removing from DOM anyway
        // Added to address Issue #344 where querySelector would intermittently return null element on hide
        if (this.showModal === false) {
            return;
        }
        const taglineEl = this.template.querySelector('p');
        taglineEl.classList.remove(CSS_CLASS);
    }

    handleSlotFooterChange() {
        // Only needed in "show" state. If hiding, we're removing from DOM anyway
        // Added to address Issue #344 where querySelector would intermittently return null element on hide
        if (this.showModal === false) {
            return;
        }
        const footerEl = this.template.querySelector('footer');
        footerEl.classList.remove(CSS_CLASS);
    }	

    @api
    validate() {

        if(this.gender
            && this.first
            && this.last
            && this.dob) { 
            return { isValid: true }; 
            } 
        else { 
            var msg = '';
            if(!this.dob){msg += 'Date of Birth is required.\n';}
            if(!this.first){msg += 'First Name is required.\n';}
            if(!this.last){msg += 'Last Name is required.\n';}
            if(!this.gender){msg += 'Gender is required.\n';}

            return { 
                isValid: false, 
                errorMessage: msg
                    }; 
        } 
   }
}