import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import epicOnlySoCreate from '@salesforce/apex/NewPatientSearchController.epicOnlySoCreate';

export default class NewRecordTile extends NavigationMixin(LightningElement) {
	@api record;
	@api recordId;
	//@track isRank = false;

	get isRank() { 
		console.log('rank' + this.record.Rank__c)
		return (this.record.Rank__c >= 37);}
	get isRankPotential() { 
		return (this.record.Rank__c >= 37 && this.record.Rank__c <= 61);}
	get isRankOver() { 
		return (this.record.Rank__c >= 62);}
	get rankLabel() { 
		//this.isRank = false;
		if(this.isRankPotential){ return 'Potential Duplicate Patient';}
		if(this.isRankOver){ return 'Exact Match - Duplicate Patient';}
		return 'none';}
			
    handleClick(event) {
		//Keep - we may want this later SM
        // 1. Prevent default behavior of anchor tag click which is to navigate to the href url
		//event.preventDefault();
		// this[NavigationMixin.Navigate]({
		// 	type: 'standard__recordPage',
		// 	attributes: {
		// 		recordId: this.account.Id,
		// 		objectApiName: 'Account',
		// 		actionName: 'view'
		// 	},
		// });		

		if(this.record.Id == null){
			console.log('no id', { 
				"name": this.record.Salutation + ' ' + this.record.Name + ' ' + this.record.Suffix,
				"mrn": this.record.HealthCloudGA__MedicalRecordNumber__c,
				"dob": this.record.Date_of_Birth__c});

			epicOnlySoCreate({ "name" : this.record.Name,
								"dob" : this.record.Date_of_Birth__c,
								"mrn": this.record.HealthCloudGA__MedicalRecordNumber__c})
				.then((result) => {
					console.log('epicOnlySoCreate.result: ', result);
					this.recordId = result;
					const foundRecord = new CustomEvent("foundrecord", {
						detail: this.recordId
					});
					console.log('foundRecord', foundRecord);
					this.dispatchEvent(foundRecord);
				})
				.catch((error) => {
					console.log('error.epicOnlySoCreate', error);
				});

		}
		else{
			this.recordId = this.record.Id;
			const foundRecord = new CustomEvent("foundrecord", {
				detail: this.recordId
			});
			this.dispatchEvent(foundRecord);
		}
    }	
}