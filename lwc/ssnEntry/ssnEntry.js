import { LightningElement, api } from 'lwc';

export default class ssnEntry extends LightningElement {
	@api ssn;

	showSpecial = false;

	actions = [
		{label: 'Select', name: 'select'}
	];

	columns = [
		{ label: 'Label', fieldName: 'label' },
		{ label: 'SSN', fieldName: 'ssn' }
	];

	data = [
		{label: 'International', ssn: '111-11-1111'},
		{label: 'Newborn Unknown SSN', ssn: '222-22-2222'},
		{label: 'Refuses', ssn: '333-33-3333'},
		{label: 'No SSN', ssn: '444-44-4444'},
		{label: 'Standard Unknown SSN', ssn: '999-99-9999'},
		{label: 'Transplant', ssn: '000-00-0000'}
	];

	showModalBox(event){
		this.showSpecial = true;
	}

	hideModalBox(event){
		this.showSpecial = false;
	}

	handleRowSelection(event){
		var selectedRows = event.detail.selectedRows;

		if(selectedRows.length === 1){
			this.ssn = selectedRows[0].ssn;
		}
		this.showSpecial = false;

		const updateSSN = new CustomEvent("updatessn", {
			detail: this.ssn
		});
		this.dispatchEvent(updateSSN);
	}

	handleSSNChange(event){
		this.ssn = event.target.value;
		const updateSSN = new CustomEvent("updatessn", {
			detail: this.ssn
		});
		this.dispatchEvent(updateSSN);
	}
}