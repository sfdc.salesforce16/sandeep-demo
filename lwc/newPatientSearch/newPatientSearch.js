import { LightningElement, api, track } from 'lwc';
import { FlowAttributeChangeEvent, FlowNavigationNextEvent } from 'lightning/flowSupport';
import findRecords from '@salesforce/apex/NewPatientSearchController.findRecords';

export default class NewPatientSearch extends LightningElement {
    @api recordId;
//    @api ini;
    @api ini;
    @api dob;
    @api searchText = '';
    @api mrn = '';
    records;
    @track accounts;
    @track cases;
    @track noRows = false;
	error;
    toggleIconName = 'utility:preview';
    toggleButtonLabel = 'Hide content';

//    this.ini = ini;

    connectedCallback(){
        window.clearTimeout(this.delayTimeout);
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            console.log('initial searchString', this.ini + ' OR ' + this.ini);
            console.log('initial searchDate', this.dob);
            findRecords({ searchText : this.ini,
                          searchIni : this.ini,
                          searchDate : this.dob,
                          mrn : this.mrn })
                .then((result) => {
                    console.log('result: ', result);
                    this.accounts = result[0];
                    console.log('soslaccounts ', result[0]);
                    this.cases = result[1];
                    console.log('soslcases ', result[1]);
                })
                .catch((error) => {
                    console.log('error', error);
                    this.records = undefined;
                });
        }, 200);		
    }

    handleSearch(event){
        console.log('handleSearch');
        console.log({   searchText : this.searchText,
            searchIni : this.ini,
            searchDate : this.dob,
            mrn : this.mrn});

        window.clearTimeout(this.delayTimeout);


        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            findRecords({   searchText : this.searchText,
                            searchIni : this.ini,
                            searchDate : this.dob,
                            mrn : this.mrn})
            .then((result) => {
                console.log('result: ', result);
                this.accounts = result[0];
                console.log('soslaccounts ', result[0]);
                this.cases = result[1];
                console.log('soslcases ', result[1]);
                if(!this.accounts.length && !this.cases.length){
                    this.noRows = true;
                }else{
                    this.noRows = false;
                }
            })
            .catch((error) => {
                console.log('error', error);
                this.records = undefined;
            });
        }, 100);		
    }

    handleKeyChange(event){
        //window.clearTimeout(this.delayTimeout);

        if(event.target.dataset.id === 'iniField'){
            if(event.target.value.length == 0){
                this.ini = '';
                return;
            }
            try {
                if(event.target.value.split('-')[2].length != 4){return;}
            } catch (error) {
                console.log('iniField:', event.target.value);
                return;
            }
            this.ini = event.target.value;
        }else if(event.target.dataset.id === 'dobField'){
            if(event.target.value.length == 0){
                this.dob = '';
                return;
            }
            try {
                if(event.target.value.split('/')[2].length != 4){return;}

            } catch (error) {
                return;
            }
            this.dob = event.target.value;
        }else if(event.target.dataset.id == 'searchField'){
            if(event.target.value.length == 0){ 
                this.searchText = '';
                return;
            }
            if(event.target.value.length < 4){ return;}
            this.searchText = event.target.value;
        }else if(event.target.dataset.id == 'mrnField'){
            if(event.target.value.length == 0){ 
                this.mrn = '';
                return;
            }
            if(event.target.value.length < 8){ return;}
            this.mrn = event.target.value;
        }
        //var mrn = this.mrn;

        // eslint-disable-next-line @lwc/lwc/no-async-operation
        /*
        this.delayTimeout = setTimeout(() => {
            console.log('terms', {name: this.searchText,
                                    'dob': this.dob,
                                    'phone': this.ini,
                                    'mrn': this.mrn});
            findRecords({   searchText : this.searchText,
                            searchIni : this.ini,
                            searchDate : this.dob,
                            mrn : mrn})
            .then((result) => {
                console.log('result: ', result);
                this.accounts = result[0];
                console.log('soslaccounts ', result[0]);
                this.cases = result[1];
                console.log('soslcases ', result[1]);
                if(!this.accounts.length && !this.cases.length){
                    this.noRows = true;
                }else{
                    this.noRows = false;
                }
            })
            .catch((error) => {
                console.log('error', error);
                this.records = undefined;
            });
        }, 1000);	
        */	
    }

	handleFoundRecord(event){
        this.recordId = event.detail;
        console.log('handleFoundRecord: ' + this.recordId);
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
        //var navigationEvent = new FlowNavigationNextEvent();
        //this.dispatchEvent(navigationEvent);
	}
    
    // Handles click on the 'Show/hide content' button
    handleToggleClick() {
        // retrieve the classList from the specific element
        const contentBlockClasslist = this.template.querySelector(
            '.lgc-id_content-toggle'
        ).classList;
        // toggle the hidden class
        contentBlockClasslist.toggle('slds-hidden');

        // if the current icon-name is `utility:preview` then change it to `utility:hide`
        if (this.toggleIconName === 'utility:preview') {
            this.toggleIconName = 'utility:hide';
            this.toggleButtonLabel = 'Reveal content';
        } else {
            this.toggleIconName = 'utility:preview';
            this.toggleButtonLabel = 'Hide content';
        }
    }
}