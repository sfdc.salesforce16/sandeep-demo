import { LightningElement, track, wire, api } from 'lwc';

// import apex class methods
import getGuar from '@salesforce/apex/PatientGuar.getGuar';

// import custom permission
import canEditGuarantor from '@salesforce/customPermission/lwc_canEditGuarantor';

// import fields for record creation
import GUARANTOR_OBJECT from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c';
import NAME_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__NameFull__c';
import PHONE_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.Guarantor_Phone__c';
import TYPE_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.Account_Type__c';
// import LINE1_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Address1Line1__c';
import LINE2_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Address1Line2__c';
import CITY_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Address1City__c';
import STATE_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Address1State__c';
import COUNTRY_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Address1Country__c';
import ACCOUNT_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Account__c';
import RELATION_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__Relationship__c';
import ACTIVE_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.Active__c';
import ID_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.Id';
//import DOB_FIELD from '@salesforce/schema/HealthCloudGA__EhrRelatedPerson__c.HealthCloudGA__BirthDate__c';

// import to show toast notifictions
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

// import to update Flow variable
import { FlowAttributeChangeEvent } from 'lightning/flowSupport';

// import to refresh the apex if any record changes the datas
import {refreshApex} from '@salesforce/apex';

// guar table row actions without edit
const guarActions = [
    { label: 'View Record', name: 'record_details'}, 
];


// guar datatable columns without edit
//{ label: 'Guarantor Id', fieldName: 'Id' },
//{ label: 'Responsible Party', fieldName: 'Responsible_Party__c' },
const guarColumns = [
    { label: 'Account Type', type: 'Picklist', fieldName: 'Account_Type__c' },
    { label: 'Account Type Other', type: 'Picklist', fieldName: 'Account_Type_Other__c' },
    { label: 'Responsible Party', type: 'Picklist', fieldName: 'Responsible_Party__c' },
    { label: 'Name', fieldName: 'HealthCloudGA__NameFull__c' },
    { label: 'Service Area', type: 'Picklist', fieldName: 'Service_Area__c' },


    {
        type: 'action',
        typeAttributes: {
            rowActions: guarActions,
            menuAlignment: "slds-popover__body"
        }
    }
];



// guar table row actions with edit
const guarActionsEdit = [
    { label: 'View Record', name: 'record_details'}, 
    { label: 'Edit Record', name: 'edit'}
];

// guar datatable columns with view and edit
const guarColumnsEdit = [
    { label: 'Guarantor Id', fieldName: 'Id' },
    { label: 'Type', fieldName: 'Account_Type__c' },
    { label: 'Responsible Party', fieldName: 'Responsible_Party__c' },
    { label: 'Active', fieldName: 'Active__c', type:"boolean" },

    {
        type: 'action',
        typeAttributes: {
            rowActions: guarActionsEdit,
            menuAlignment: "slds-popover__body"
        }
    }
];

// guar table row actions without edit
const relConActions = [
    { label: 'View Record', name: 'record_details'}, 
];

// guar datatable columns without edit
const relConColumns = [
    { label: 'Name', fieldName: 'HealthCloudGA__NameFull__c' },
    { label: 'Relationship', fieldName: 'HealthCloudGA__Relationship__c' },
    { label: 'Start Date', fieldName: 'HealthCloudGA__PeriodStart__c', type:"date" },
    { label: 'End Date', fieldName: 'HealthCloudGA__PeriodEnd__c', type:"date" },
    //{ label: 'Relationship', fieldName: 'HealthCloudGA__Relationship__c' },

    {
        type: 'action',
        typeAttributes: {
            rowActions: relConActions,
            menuAlignment: "slds-popover__body"
        }
    }
];

// guar table row actions with edit
const relConActionsEdit = [
    { label: 'View Record', name: 'record_details'}, 
    { label: 'Edit Record', name: 'edit'}
];

// guar datatable columns with view and edit
const relConColumnsEdit = [
    { label: 'Name', fieldName: 'HealthCloudGA__NameFull__c' },
    { label: 'Relationship', fieldName: 'HealthCloudGA__Relationship__c' },
    { label: 'Start Date', fieldName: 'HealthCloudGA__PeriodStart__c', type:"date" },
    { label: 'End Date', fieldName: 'HealthCloudGA__PeriodEnd__c', type:"date" },
    //{ label: 'Relationship', fieldName: 'HealthCloudGA__Relationship__c' },

    {
        type: 'action',
        typeAttributes: {
            rowActions: relConActionsEdit,
            menuAlignment: "slds-popover__body"
        }
    }
];

export default class DeleteRowsInDatatableLWC extends LightningElement { 
    // reactive variable
    @track data;
//    @track columns = columns;
    @track relConColumns = relConColumns;
    @track relConColumnsEdit = relConColumnsEdit;
    @track guarColumns = guarColumns;
    @track guarColumnsEdit = guarColumnsEdit;
    @track record = [];
    @track bShowModal = false;
    @track currentRecordId;
    @track isViewForm = false;
    @track isEditForm = false;
    @track isCreateForm = false;
    @track showLoadingSpinner = false;
    @api recordId;
    @api createNewGuar;
    @api editedRecordIds; 
    @api createdRecordIds; 
    @api relName;
    @api isGuarantor;
    @api allowEdit;
    @api allowNew = false;
    @api relationshipValue = '';
    @api listIconName;
    @api isEditAccessGranted;
    
    // evaluate permission
    get isEditAccessGranted() {
        return canEditGuarantor;
    }

    editedRecordIds = [];
    createdRecordIds = [];

    set editedRecordIds(editedRecordIds = []) {
        this.editedRecordIds = [...editedRecordIds];
    }

    set createdRecordIds(createdRecordIds = []) {
        this.createdRecordIds = [...createdRecordIds];
    }

    // non-reactive variables
    selectedRecords = [];
    refreshTable;
    error;

    handleCreate(event) {
        this.bShowModal = true;
        this.isViewForm = false;
        this.isEditForm = false;
        this.isCreateForm = true;
    }

    guarantorObject = GUARANTOR_OBJECT;
    nameField = NAME_FIELD;
    relationField = RELATION_FIELD;
    //dobField = DOB_FIELD;
    actiiveField = ACTIVE_FIELD;
    phoneField = PHONE_FIELD;
    // line1Field = LINE1_FIELD;
    line2Field = LINE2_FIELD;
    cityField = CITY_FIELD;
    stateField = STATE_FIELD;
    countryField = COUNTRY_FIELD;
    typeField = TYPE_FIELD;
    accountField = ACCOUNT_FIELD;
    linkField = '/' + ID_FIELD;

    // retrieving the data using wire service
    @wire(getGuar, {recordId:'$recordId'})
    guarantors(result) {
        this.refreshTable = result;
        console.log(result);
        if (result.data) {
            this.data = result.data;
            this.error = undefined;

        } else if (result.error) {
            this.error = result.error;
            this.data = undefined;
        }
    }

    handleRowActions(event) {
        let actionName = event.detail.action.name;

        let row = event.detail.row;

        // eslint-disable-next-line default-case
        switch (actionName) {
            case 'record_details':
                this.viewCurrentRecord(row);
                break;
            case 'edit':
                this.editCurrentRecord(row);
                break;
        }
    }

    // view the current record details
    viewCurrentRecord(currentRow) {
        this.bShowModal = true;
        this.isViewForm = true;
        this.isEditForm = false;
        this.isCreateForm = false;
        this.record = currentRow;
        this.recordUrl = '/' + currentRow.Id;
    }

    // closing modal box
    closeModal() {
        this.isViewForm = false;
        this.isEditForm = false;
        this.isCreateForm = false;
        this.bShowModal = false;
    }

    editCurrentRecord(currentRow) {
        // open modal box
        this.bShowModal = true;
        this.isViewForm = false;
        this.isCreateForm = false;
        this.isEditForm = true;

        // assign record id to the record edit form
        this.currentRecordId = currentRow.Id;

        window.console.log('currentRecordId: ' + this.currentRecordId);
        //window.console.log('row: ' + this.currentRecordId);
        if(this.editedRecordIds.indexOf(this.currentRecordId) !== -1){
            window.console.log('editedRecordIds: ' + this.editedRecordIds);
        } else {
            this.editedRecordIds.push(this.currentRecordId);
            const attributeChangeEvent = new FlowAttributeChangeEvent('editedRecordIds', this.editedRecordIds);
            this.dispatchEvent(attributeChangeEvent);
            window.console.log('editedRecordIds: ' + this.editedRecordIds);
        }
    }

    // handling record edit form submit
    handleSubmit(event) {
        // prevending default type sumbit of record edit form
        event.preventDefault();

        console.log('handleSubmit');
        console.log('updated ', event.detail);

        // querying the record edit form and submiting fields to form
        this.template.querySelector('lightning-record-edit-form').submit(event.detail.fields);

        // closing modal
        this.bShowModal = false;

        // showing success message
        this.dispatchEvent(new ShowToastEvent({
            title: 'Success!!',
            message: event.detail.fields.HealthCloudGA__NameFull__c +' Record Updated Successfully!',
            variant: 'success'
        }),);

    }

    // handling record edit form submit
    handleCreatedSubmit(event) {
        // prevending default type sumbit of record edit form
        event.preventDefault();
        
        console.log('handleGuarCreated');
        console.log('created ', event.detail.id);

//        this.createdRecordIds.push(event.detail.id);
//        const attributeChangeEvent = new FlowAttributeChangeEvent('createdRecordIds', this.createdRecordIds);
//        this.dispatchEvent(attributeChangeEvent);
//        window.console.log('createdRecordIds: ' + this.createdRecordIds);

        // querying the record edit form and submiting fields to form
        this.template.querySelector('lightning-record-edit-form').submit(event.detail.fields);

        // closing modal
        this.bShowModal = false;
        this.isCreateForm = false;

        // showing success message
        this.dispatchEvent(new ShowToastEvent({
            title: 'Success!',
            message: ' Record Created Successfully!',
            variant: 'success'
        }),);

        return refreshApex(this.refreshTable);
    }

    // refreshing the datatable after record edit form success
    handleSuccess() {
        return refreshApex(this.refreshTable);
    }

}