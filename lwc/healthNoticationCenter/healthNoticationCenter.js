import { LightningElement, api, track } from 'lwc';
import getAlerts from '@salesforce/apex/HealthNotificationCenterController.getAlerts';
import updateFromEpic from '@salesforce/apex/HealthNotificationCenterController.updateFromEpic';

export default class HealthNoticationCenter extends LightningElement {
	@api recordId;
	alerts;

	showModal = false;
	isLoading = true;


	connectedCallback(){
			console.log('HealthNoticationCenter:this.recordId:', this.recordId);
			this.showModal = true;
			updateFromEpic({ accountId : this.recordId}).then(()=>{
				this.showModal = false;
				getAlerts({ accountId : this.recordId})
                .then((result) => {
						console.log('getAlerts.result: ', result[0]);
						this.alerts = result;
                })
                .catch((error) => {
                    console.log('getAlerts.error', error);
                });
			})
			.catch((error) =>{
				console.log('updateFromEpic.error', error);
			});
    }
}