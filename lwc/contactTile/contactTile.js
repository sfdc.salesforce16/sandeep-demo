import { LightningElement, api } from 'lwc';

export default class ContactTile extends LightningElement {
    @api contact;
    @api selected = false;

    // get cssClass() {
    //     return this.contact.muted ? 'mute pointer' : 'pointer';
    // }

    // Set the background color class if this attribute is true.
    get selectedClass(){
        return this.selected ? 'class1' : ''; // you can use your custom class here.
    }

    handleSelect(event) {
        // 1. Prevent default behavior of anchor tag click which is to navigate to the href url
        event.preventDefault();
        // 2. Create a custom event that bubbles. Read about event best practices at http://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.events_best_practices
        const selectEvent = new CustomEvent('contactselect', {
            bubbles: true,
            detail: {
                recordId: this.contact.Id,
                practitionerId: this.contact.Duke_Practitioner_ID__c,
                pcpName: this.contact.Name
            }
        });
        // 3. Fire the custom event
        this.dispatchEvent(selectEvent);
    }
}