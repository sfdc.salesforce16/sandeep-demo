import { LightningElement, wire, api, track } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { reduceErrors } from 'c/ldsUtils';
import findProviderAddresses from '@salesforce/apex/ProviderAddressSearchController.findProviderAddresses';
import setProviderAddress from '@salesforce/apex/ProviderAddressSearchController.setProviderAddress';

// Import message service features required for subscribing and the message channel
import { subscribe, MessageContext } from 'lightning/messageService';
import RECORD_SELECTED_CHANNEL from '@salesforce/messageChannel/Record_Selected__c';

import NAME_FIELD from '@salesforce/schema/Contact.Name';
import SER_NUMBER_FIELD from '@salesforce/schema/Contact.Provider_SER_Number__c';

const fields = [
    NAME_FIELD,
    SER_NUMBER_FIELD
];

export default class ProviderAddressSearch extends LightningElement {
    @api recordId;
    @api practitionerId;
    @api Name;
    @api locationId;
    @api pcpRecordId;
    @api pcpName;
    
    @track addresses;
    @track selectedMarkerValue;
    @track zoomLevel = 10;
    @track noAddresses = false;
    @track pcpSelected = false;
	error;
    
    subscription = null;
    mapMarkers = [];

     // By using the MessageContext @wire adapter, unsubscribe will be called
    // implicitly during the component descruction lifecycle.
    @wire(MessageContext)
    messageContext;

    @wire(CurrentPageReference)
    getStateParameters(currentPageReference) {
       if (currentPageReference) {
        this.urlStateParameters = currentPageReference.state;
          this.setParametersBasedOnUrl();
       }
    }
 
    setParametersBasedOnUrl() {
        this.recordId = this.urlStateParameters.c__recordId || null;
        console.log('this.urlStateParameters', this.urlStateParameters);
        console.log('this.urlStateParameters.c__recordId:', this.urlStateParameters.c__recordId);
    } 

    // //getPCP Info
    // @wire(getRecord, { recordId: '$pcpRecordId', fields })
    // wiredRecord({ error, data }) {
    //     if (error) {
    //         this.dispatchToast(error);
    //     } else if (data) {
    //         fields.forEach(
    //             (item) => (this[item.fieldApiName] = getFieldValue(data, item))
    //         );
    //     }
    // }

    // Encapsulate logic for LMS subscribe.
    subscribeToMessageChannel() {
        this.subscription = subscribe(
            this.messageContext,
            RECORD_SELECTED_CHANNEL,
            (message) => this.handleMessage(message)
        );
    }

    // Handler for message received by component
    handleMessage(message) {
        this.pcpRecordId = message.pcpRecordId;
        this.practitionerId = message.practitionerId;
        this.pcpName = message.pcpName;
        this.pcpSelected=true;
        this.handleSearch();
    }

    // Standard lifecycle hooks used to sub/unsub to message channel
    connectedCallback() {
        console.log('connected callback entered');
        this.subscribeToMessageChannel();
    }

    // Helper
    dispatchToast(error) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Error loading contact',
                message: reduceErrors(error).join(', '),
                variant: 'error'
            })
        );
    }

    handleSearch(event){
        console.log('handleSearch: ' + this.practitionerId);

        window.clearTimeout(this.delayTimeout);

        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            findProviderAddresses({practitionerId : this.practitionerId})
            .then((result) => {
                console.log('result: ', result);
                this.addresses = result;

                for(var i = 0; i < result.length; i++){
                    var a = result[i];
                    if(a.IsActive){
                        var title = a.IsPrimary ? a.Phone + ' (Primary)' : a.Phone;
                        var street = a.StreetLine1 + '\n';
                        if(a.StreetLine2){ street += a.StreetLine2 + '\n';}
        
                        
                        var pin = {"location":{
                            "Street": street,
                            "City": a.City,
                            "PostalCode": a.Zip,
                            "State": a.StateAbbr,
                            "Country": a.CountryAbbr,
                            },
                            "value": a.UniqueID,
                            icon: 'standard:address',
                            "title": title};
                        this.mapMarkers.push(pin);
                        console.log(pin);
                    }
                }
                if(this.mapMarkers.length == 1){this.zoomLevel = 15;}
                console.log('zoomLevel: ' + this.zoomLevel);

                if(this.mapMarkers.length == 0){
                    this.noAddresses = true;
                    this.addresses = null;
                    return;
                }

                if(!this.addresses.length){
                    this.noAddresses = true;
                }else{
                    this.noAddresses = false;
                }
            })
            .catch((error) => {
                console.log('error', error);
                this.addresses = undefined;
            });
        }, 100);		
    }

    handleMarkerSelect(event) {
        this.selectedMarkerValue = event.target.selectedMarkerValue;
        console.log('selectedMarkerValue: ' + this.selectedMarkerValue);
        var address = '';
        for(var i = 0; i < this.addresses.length; i++){
            if(this.selectedMarkerValue == this.addresses[i].UniqueID){
                var a = this.addresses[i];
                var street = a.StreetLine1 + '\n';
                if(a.StreetLine2){ street += a.StreetLine2 + '\n';}
                address = street + a.City + ', ' + a.StateAbbr + '  ' + a.Zip + '\n' + a.Phone;
            }
        }

        setProviderAddress({
            accountId: this.recordId,
            locationId: this.selectedMarkerValue,
            address: address,
            hpContactId: this.pcpRecordId
        });
        
        const toastEvent = new ShowToastEvent({
            title: 'PCP Address Selected',
            message: 'Please close this tab to return and refresh the registration form.',
            variant: 'success'
        });
        this.dispatchEvent(toastEvent);
    }

}