import { LightningElement, api } from 'lwc';
//import findGuarantor from '@salesforce/apex/patientGuarantor.findGuarantor';

export default class PatientGuarantor extends LightningElement {
    @api recordId;
    @api guarantorId;
    @api createNewGuar;
    @api creatingGuar;
    @api guarSelected;
    @api noGuarSelected;
    @api altGuarSelected;
    @api guarRemoved;
    @api firstPass;
    @api updateGuar;

    firstPass = false; 
    createNewGuar = false; 
    creatingGuar = false;
    guarSelected = false;
    noGuarSelected = true; 
    altGuarSelected = false; 
    guarRemoved = false; 
    updateGuar = false; 

    handleChange(event) {
        if( event.target.name == 'guarantorId') {
            this.guarantorId = event.target.value;
            console.log('this.guarantorId', this.guarantorId);
            if(this.guarantorId == '') {
                this.guarRemoved = true;
                this.altGuarSelected = false;
                this.noGuarSelected = false;
            } else {
                this.guarRemoved = false;
                this.altGuarSelected = true;
                this.noGuarSelected = false;
            }
        }
    }

    confirmDeselect(event){
        this.noGuarSelected = true;
        this.altGuarSelected = false;
        this.guarRemoved = false;
        this.firstPass = false;
    }
    handleSelect(event) {
        this.guarSelected = true;
        this.creatingGuar = false; 
        this.createNewGuar = false;
        this.firstPass = false;
    }
    handleCreate(event) {
        this.noGuarSelected = true;
        this.altGuarSelected = false;
        this.creatingGuar = true; 
        this.createNewGuar = true;
        this.firstPass = false;
    }
    editExisting(event) {
        console.log('this.guarantorId', this.guarantorId);
        this.guarSelected = true;
        this.updateGuar = true;
        this.creatingGuar = false; 
        this.createNewGuar = false;
        this.firstPass = false;
        this.creatingGuar = true;
    }
    handleRedo(event) {
        this.updateGuar = false; 
        this.createNewGuar = false;
        this.firstPass = false;
    }
    updateRecord(event) {
        this.updateGuar = true;
        this.altGuarSelected = false;
    }
    handleInserted(event) {
        this.creatingGuar = false; 
        this.createNewGuar = false;
    }
    handleSubmit(event) {
        this.altGuarSelected = false;
    }
    doneUpdating(event) {
        this.updateGuar = false; 
    }



    /*   options = [
        {'label': 'Update This Guarantor (edit record)', 'value': 'option1'},
        {'label': 'Add This Guarantor (do NOT edit record)', 'value': 'option2'},
    ];

//   value = 'option1';

    checkMe(event) {
        const selectedOption = event.detail.value;
        if(selectedOption == 'option2') {
            this.createNewGuar = true;
            this.updateSelectedGuar = false;
        } else if(selectedOption == 'option1') {
            this.createNewGuar = false;
            this.updateSelectedGuar = true;
        } else {
            this.createNewGuar = false;
            this.updateSelectedGuar = false;            
        }
    }*/





}