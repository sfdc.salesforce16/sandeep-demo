import { LightningElement, api, track } from 'lwc';
import callEpicHealthMaint from '@salesforce/apex/HealthMaintController.callEpicHealthMaint';

export default class HealthMaintenance extends LightningElement {
	@api recordId;
	@track healthExpandRows = [];
	@track healthData;

	@track healthColumns = [
        { label: 'Name', type: 'text', fieldName: 'Name' },
        { label: 'Due', type: 'text', fieldName: 'DueDate' },
        { label: 'Status', fieldName: 'Status'},
        { label: 'Last Satisfied', fieldName: 'LastSatisfactionDate'},
    ];
	

	connectedCallback(){
		//var tempjson;
		callEpicHealthMaint({ accountId : this.recordId})
			.then(data => {
				console.log('data: ',data);
				this.healthData = data;
			})
			.catch(error => {
				console.log('errors.healthMaintenance: ', error);
			});
	}
}