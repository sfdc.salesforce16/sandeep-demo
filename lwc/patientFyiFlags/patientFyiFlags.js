import { LightningElement, api, track } from 'lwc';
import callEpicFyiFlags from '@salesforce/apex/PatientFyiFlagController.callEpicFyiFlags';

export default class PatientFyiFlags extends LightningElement {
	@api recordId;
	@track fyiFlagExpandRows = [];
	@track fyiFlagData;

	@track fyiFlagColumns = [
        { label: 'Summary', type: 'text', fieldName: 'Summary' },
        { label: 'Text', type: 'text', fieldName: 'Text' },
        { label: 'Type', fieldName: 'Type'},
        { label: 'Active', fieldName: 'FlagActive'},
        { label: 'Contact ID', fieldName: 'PatientContactID'},
        { label: 'ID Type', fieldName: 'PatientContactIDType'},
        { label: 'Instant', fieldName: 'Instant'},
    ];

	connectedCallback(){
		//var tempjson;
		callEpicFyiFlags({ accountId : this.recordId})
			.then(data => {
				console.log('fyiFlagData: ',data);
				this.fyiFlagData = data;
			})
			.catch(error => {
				console.log('errors.fyiFlag: ', error);
			});
	}
}