({
	init : function(component, event, helper) {
        helper.startSpinner(component);
		var sortby = component.get("v.sortby");
		var sortdir = component.get("v.sortdir");
		var featuredNum = component.get("v.featuredNum");
        helper.getPrograms(component, sortby, sortdir, featuredNum);
	}
    
})