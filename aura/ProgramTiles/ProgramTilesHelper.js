({
	getPrograms : function(component, sortby, sortdir, featuredNum) {
        console.log('Query featured programs ' + sortby + ' ' + sortdir);
        var action = component.get("c.getFeaturedPrograms");
        action.setParams({ 
            "sortby": sortby,
            "sortdir": sortdir,
            "featuredNum": featuredNum});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var featured = response.getReturnValue(); 
                component.set("v.featured", featured);
                this.stopSpinner(component);
            }
            else { console.log("Failed with state: " + state); }
        });
        $A.enqueueAction(action); 
    },
    
    stopSpinner: function (component) {
        console.log("Spinner Stop... ");
        var spinner = component.find("spinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    
    startSpinner: function (component) {
        console.log("Spinner Start... ");
        var spinner = component.find("spinner");
        $A.util.removeClass(spinner, "slds-hide");
    }
})