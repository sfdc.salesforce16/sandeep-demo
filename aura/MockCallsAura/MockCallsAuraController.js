({
    handleExactMatch : function(component, event, helper){
        
        var inputVariables = [
            {
               name : "recordId",
               type : "String",
               value : "0015C00000XMU9gQAH"
            }];

        component.set("v.showVerifyRegistration", true);
        component.set("v.showVerifyRegistrationButton", false);
        component.set("v.showFirstCall", false);
        component.set("v.showFirstCallButton", false);
        component.set("v.showSecondCall", false);
        component.set("v.showSecondCallButton", false);
        component.set("v.showPatientSearch", false);
        component.set("v.showPatientSearchButton", false);

        var flow = component.find("verify_Registration");
        flow.startFlow("Verify_Registration", inputVariables);
    },
    handleFirstCall : function(component, event, helper){

        var inputVariables = [
            {
                name : "dnis",
                type : "String",
                value : "919-484-3937"
             },
             {
               name : "ini",
               type : "String",
               value : "919-404-5879"
            },
            {
                name : "dob",
                type : "String",
                value : "11/16/1967"
            }];
        
        component.set("v.showVerifyRegistration", false);
        component.set("v.showVerifyRegistrationButton", false);
        component.set("v.showFirstCall", true);
        component.set("v.showFirstCallButton", false);
        component.set("v.showSecondCall", false);
        component.set("v.showSecondCallButton", false);
        component.set("v.showPatientSearch", false);
        component.set("v.showPatientSearchButton", false);

        var flow = component.find("firstCall");
        flow.startFlow("Front_Door", inputVariables);
    },
    handleSecondCall : function(component, event, helper){

        var inputVariables = [
            {
               name : "dnis",
               type : "String",
               value : "919-681-3937"
            },
            {
                name : "ini",
                type : "String",
                value : "919-123-4567"
             },
             {
                name : "dob",
                type : "String",
                value : "10/02/1970"
            }];

         
        component.set("v.showVerifyRegistration", false);
        component.set("v.showVerifyRegistrationButton", false);
        component.set("v.showFirstCall", false);
        component.set("v.showFirstCallButton", false);
        component.set("v.showSecondCall", true);
        component.set("v.showSecondCallButton", false);
        component.set("v.showPatientSearch", false);
        component.set("v.showPatientSearchButton", false);

        var flow = component.find("secondCall");
        flow.startFlow("Front_Door", inputVariables);
    },
    handlePatientSearch : function(component, event, helper){

        component.set("v.showVerifyRegistration", false);
        component.set("v.showVerifyRegistrationButton", false);
        component.set("v.showFirstCall", false);
        component.set("v.showFirstCallButton", false);
        component.set("v.showSecondCall", false);
        component.set("v.showSecondCallButton", false);
        component.set("v.showPatientSearch", true);
        component.set("v.showPatientSearchButton", false);
  
        var flow = component.find("patientSearch");
        flow.startFlow("PatientSearch");
    }


})