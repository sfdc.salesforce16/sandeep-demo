({
    getPrograms : function(component, parentProgramId) {
        console.log('Query Programs for ' + parentProgramId);
        var action = component.get("c.getCommunityVisiblePrograms");
        action.setParams({"parentProgramId": parentProgramId});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var programs = response.getReturnValue(); 
                //console.log(JSON.stringify(programs));
                let programsMap = this.buildHierarchy(programs);
                let servicelineScope = new Map();
                var hasChildren = false;
                //var allItems = [];
                for(var x = 0; x < programs.length; x++) {
                    var p = programs[x];
                    var allItems = servicelineScope.get(p.Service_Line__r.Name); // get items from scope list
                    if(!allItems) allItems = [];
                    if(!p.ParentProgramId || p.Id == parentProgramId) {
                        var hasChild = this.programHasChildren(p, programsMap);
                        allItems.push(this.buildItem(p, programsMap, parentProgramId));
                        console.log("Has child? " + hasChild);
                        if(hasChild) hasChildren = true;
                        if(servicelineScope.has(p.Service_Line__r.Name)) servicelineScope.delete(p.Service_Line__r.Name);
                        servicelineScope.set(p.Service_Line__r.Name, allItems);
                        //console.log("Added to service line scope: " + p.Service_Line__r.Name + " --- " + JSON.stringify(allItems));
                    }
                }
                var scope = [];
                for (let key of servicelineScope.keys()) {
                    var sItem = {"label" : key, "items" : servicelineScope.get(key)};
                    scope.push(sItem);
                }
                //console.log("SCOPE-------" + JSON.stringify(scope));
                component.set("v.scope", Array.from(scope));
                component.set("v.hasChildren", hasChildren);
                //component.set("v.scope", allItems);
            }
            else { console.log(stringifyErrors(response)); }
        });
        $A.enqueueAction(action); 
    },

    // returns map of parent id to direct children
    // used to populate items lists
    buildHierarchy : function(programs) {
        //console.log("Building hierarchy for: " + JSON.stringify(programs));
        let programsMap = new Map();
        for(var x = 0; x < programs.length; x++) {
            var p = programs[x];
            //console.log("Mapping Program: " + JSON.stringify(p));
            var siblings = programsMap.get(p.ParentProgramId);
            //console.log("Mapping Sibbling: " + JSON.stringify(siblings));
            if(!siblings) siblings = [];
            siblings.push(p);
            programsMap.set(p.ParentProgramId, siblings);
        }
        //console.log("Programs map: " + JSON.stringify(Array.from(programsMap)));
        return programsMap;
    },

    programHasChildren : function(p, programsMap) {
        let children = programsMap.get(p.Id);
        if(children) return true;
        else return false;
    },

    // calls itself recursively to build tree dynamically to any level
    // only call this method for programs with no ParentProgramId, because we don't want to create duplicates
    buildItem : function(p, programsMap, parentProgramId) {
        var expanded = (parentProgramId) ? true : false;

        // build tree going down from this program
        let children = programsMap.get(p.Id);
        var items = [];
        //console.log(JSON.stringify(children));
        if(children) {
            for(var x = 0; x < children.length; x++) {
                items.push(this.buildItem(children[x], programsMap, parentProgramId));
            }
        }
        var item = {
            "label": p.Name,
            "name" : p.Id,
            "href" : ("/s/program?recordId=" + p.Id),
            "expanded" : expanded,
            "items" : items
        }
        return item;
    },
    
    // helper for friendly errors
    stringifyErrors : function(response) {
        let errors = response.getError();
        let message = 'Unknown error'; // Default error message
        // Retrieve the error message sent by the server
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        // Display the message
        console.error(message);
        return message;
    }
})